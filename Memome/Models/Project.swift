//
//  Project.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/15/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import Foundation
import CoreData

@objc(Project)
class Project: NSManagedObject {

    @NSManaged var id: Int32
    @NSManaged var title: String
    @NSManaged var subtitle: String
    @NSManaged var productId: Int32
    @NSManaged var productData: Data
    @NSManaged var productDetailData: Data
    @NSManaged var createdAt: Date
    
    @NSManaged var corporateVoucherId: Int32
    @NSManaged var corporateVoucherName: String
    @NSManaged var corporateVoucherQty: Int32
    @NSManaged var corporateVoucherCode: String
    
    override func willSave() {
        //check if id > 0
        var isNew: Bool = true
        if id > 0 {
            isNew = false
        }
        
        if isNew {
            //get the last ID
            let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.managedObjectContext
            let error: NSErrorPointer? = nil
            
            let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Project", in: context)!
            let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
            request.entity = entityDesc
            request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
            var objects: [AnyObject]!
            do {
                objects = try context.fetch(request)
            } catch let error1 as NSError {
                error??.pointee = error1
                objects = nil
            }
            self.createdAt = Date()
            if objects.count == 0 {
                self.id = 1
            }
            else{
                let latestObj: Project = objects[0] as! Project
                self.id = latestObj.id + 1
            }
        }
    }
    
    override func prepareForDeletion() {
        self.deleteProjectDetail()
        self.deleteCart()
    }
    
    fileprivate func deleteProjectDetail(){
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "ProjectDetail", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(projectId = %i)", self.id)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count > 0 {
            context.delete(objects[0] as! NSManagedObject)
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
            if error != nil {
                print("Error deleting \(entityDesc) - error:\(error)");
            }
        }
    }
    
    fileprivate func deleteCart(){
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cart", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(projectId = %i)", self.id)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count > 0 {
            context.delete(objects[0] as! NSManagedObject)
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
            if error != nil {
                print("Error deleting \(entityDesc) - error:\(error)", entityDesc, error!);
            }
        }
    }
}
