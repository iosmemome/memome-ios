//
//  ProjectDetail.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/15/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import Foundation
import CoreData

@objc(ProjectDetail)
class ProjectDetail: NSManagedObject {

    @NSManaged var id: Int32
    @NSManaged var projectId: Int32
    @NSManaged var image: String
    @NSManaged var type: String
    @NSManaged var width: Float
    @NSManaged var height: Float
    @NSManaged var offsetX: Float
    @NSManaged var offsetY: Float
    @NSManaged var zoom: Float
    @NSManaged var rotation: Float
    @NSManaged var imageData: Data
    
    static let TYPE_COVER  = "cover"
    static let TYPE_CONTENT = "content"
    
    override func willSave() {
        //check if id > 0
        var isNew: Bool = true
        if id > 0 {
            isNew = false
        }
        
        if isNew {
            //get the last ID
            let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.managedObjectContext
            let error: NSErrorPointer? = nil
            
            let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "ProjectDetail", in: context)!
            let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
            request.entity = entityDesc
            request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
            var objects: [AnyObject]!
            do {
                objects = try context.fetch(request)
            } catch let error1 as NSError {
                error??.pointee = error1
                objects = nil
            }
            if objects.count == 0 {
                self.id = 1
            }
            else{
                let latestObj: ProjectDetail = objects[0] as! ProjectDetail
                self.id = latestObj.id + 1
            }
        }
    }
}
