//
//  BSSlider.swift
//  v0.1
//  Simple slider with animation options
//
//  Created by Bobby Stenly Irawan - iceman.bsi@gmail.com - on 12/23/15.
//  Copyright © 2015 Bobby Stenly Irawan. All rights reserved.
//

import UIKit

protocol BSSliderDelegate {
    func setupSlides(_ slider: BSSlider)
    func onSlideSelected(_ slider: BSSlider, index: Int)
    func onSlideChanged(_ slider: BSSlider, index: Int)
}
extension BSSliderDelegate {
    func onSlideSelected(_ slider: BSSlider, index: Int) {
    }
    func onSlideChanged(_ slider: BSSlider, index: Int) {
    }
}

class BSSlider: UIView {
    fileprivate var slidesContainer: UIView!
    fileprivate var paginationContainer: UIView!
    
    fileprivate var currentSlide: Int = 0
    fileprivate var timer: Timer!
    
    fileprivate var delegate: BSSliderDelegate!
    fileprivate weak var scrollView: UIScrollView?
    
    open var autoplay: Bool = true
    
    deinit { NSLog("BSSlider is being deinitialized") }
    
    
    
    func getSlidesContainer() -> UIView? {
        return self.slidesContainer
    }
    
    func getPaginationContainer() -> UIView? {
        return self.paginationContainer
    }
    
    func setDelegate(_ delegate: BSSliderDelegate){
        self.delegate = delegate
        self.delegate.setupSlides(self)
        self.setupPagination()
        self.currentSlide = 0
        self.animateSlideshow(true)
    }
    
    func getDelegate() -> BSSliderDelegate {
        return self.delegate
    }
    
    func setScrollView(_ view: UIScrollView) {
        self.scrollView = view
    }
    
    func getScrollView() -> UIScrollView? {
        return self.scrollView
    }
    
    func reloadSlides() {
        for slide in self.slidesContainer.subviews {
            slide.removeFromSuperview()
        }
        for pages in self.paginationContainer.subviews {
            pages.removeFromSuperview()
        }
        self.delegate.setupSlides(self)
        self.setupPagination()
        self.currentSlide = 0
        self.animateSlideshow(true)
    }
    
    #if !TARGET_INTERFACE_BUILDER
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    #endif
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    fileprivate func setup() {
        self.clipsToBounds = true
        
        
        let slidesContainer = UIView()
        self.slidesContainer = slidesContainer
        self.slidesContainer.backgroundColor = UIColor.clear
        self.slidesContainer.clipsToBounds = true
        self.addSubview(self.slidesContainer)
        self.slidesContainer.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint(
            item: self.slidesContainer,
            attribute: NSLayoutAttribute.top,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.top,
            multiplier: 1,
            constant: 0
            ))
        
        self.addConstraint(NSLayoutConstraint(
            item: self.slidesContainer,
            attribute: NSLayoutAttribute.bottom,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.bottom,
            multiplier: 1,
            constant: 0
            ))
        
        self.addConstraint(NSLayoutConstraint(
            item: self.slidesContainer,
            attribute: NSLayoutAttribute.left,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.left,
            multiplier: 1,
            constant: 0
            ))
        
        self.addConstraint(NSLayoutConstraint(
            item: self.slidesContainer,
            attribute: NSLayoutAttribute.right,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.right,
            multiplier: 1,
            constant: 0
            ))
        
        let paginationContainer = UIView()
        self.paginationContainer = paginationContainer
        self.addSubview(self.paginationContainer)
        self.paginationContainer.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint(
            item: self.paginationContainer,
            attribute: NSLayoutAttribute.bottom,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.bottom,
            multiplier: 1,
            constant: 0
            ))
        
        self.addConstraint(NSLayoutConstraint(
            item: self.paginationContainer,
            attribute: NSLayoutAttribute.left,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.left,
            multiplier: 1,
            constant: 0
            ))
        
        self.addConstraint(NSLayoutConstraint(
            item: self.paginationContainer,
            attribute: NSLayoutAttribute.right,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.right,
            multiplier: 1,
            constant: 0
            ))
        
        self.addConstraint(NSLayoutConstraint(
            item: self.paginationContainer,
            attribute: NSLayoutAttribute.height,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1,
            constant: 20
            ))
        
        
        let pgr: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(BSSlider.panGestureHandler(_:)))
        self.slidesContainer.addGestureRecognizer(pgr)
        
        let singleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BSSlider.slideTapGestureHandler(_:)))
        self.slidesContainer.addGestureRecognizer(singleTap)
        
        self.paginationContainer.backgroundColor = UIColor(white: 0, alpha: 0)
    }
    
    func setupPagination(){
        let pageWidth : CGFloat = 10;
        let pageHeight : CGFloat = 10;
        let pageColor : UIColor = UIColor(white: 0.6, alpha: 1)
        let borderColor : UIColor = UIColor(white: 0.3, alpha: 1)
        let inactivePageColor : UIColor = UIColor(white:1, alpha: 1)
        let inactiveBorderColor : UIColor = UIColor(white: 0.6, alpha: 1)
        let pageMargin : CGFloat = 3;
        let pages: UIView = UIView()
        var totalWidth: CGFloat = 0
        
        for i in 0 ..< self.slidesContainer.subviews.count {
            //set pagination
            let page: UIView = UIView(frame: CGRect(x: CGFloat(i) * (pageWidth + pageMargin), y: 0, width: pageWidth, height: pageHeight))
            page.layer.cornerRadius = pageWidth / 2
            page.layer.borderWidth = 1
            if Int(i) == self.currentSlide {
                page.backgroundColor = pageColor
                page.layer.borderColor = borderColor.cgColor
            }
            else {
                page.backgroundColor = inactivePageColor
                page.layer.borderColor = inactiveBorderColor.cgColor
            }
            pages.addSubview(page)
        }
        totalWidth = CGFloat(self.slidesContainer.subviews.count) * pageWidth + CGFloat(self.slidesContainer.subviews.count - 1) * pageMargin;
        
        self.paginationContainer.addSubview(pages)
        pages.translatesAutoresizingMaskIntoConstraints = false
        self.paginationContainer.addConstraint(NSLayoutConstraint(
            item: pages,
            attribute: NSLayoutAttribute.centerX,
            relatedBy: NSLayoutRelation.equal,
            toItem: self.paginationContainer,
            attribute: NSLayoutAttribute.centerX,
            multiplier: 1,
            constant: 0
            ))
        self.paginationContainer.addConstraint(NSLayoutConstraint(
            item: pages,
            attribute: NSLayoutAttribute.centerY,
            relatedBy: NSLayoutRelation.equal,
            toItem: self.paginationContainer,
            attribute: NSLayoutAttribute.centerY,
            multiplier: 1,
            constant: 0
            ))
        self.paginationContainer.addConstraint(NSLayoutConstraint(
            item: pages,
            attribute: NSLayoutAttribute.width,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1,
            constant: totalWidth
            ))
        self.paginationContainer.addConstraint(NSLayoutConstraint(
            item: pages,
            attribute: NSLayoutAttribute.height,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1,
            constant: pageHeight
            ))
        
    }
    
    func slideTapGestureHandler(_ recognizer:UITapGestureRecognizer) {
        if self.slidesContainer.subviews.count > 0 && self.currentSlide < self.slidesContainer.subviews.count {
            self.delegate.onSlideSelected(self, index: self.currentSlide)
        }
    }
    
    func panGestureHandler(_ recognizer:UIPanGestureRecognizer) {
        if self.slidesContainer.subviews.count > 0 && self.currentSlide < self.slidesContainer.subviews.count {
            let translation: CGPoint = recognizer.translation(in: self.slidesContainer)
            recognizer.view!.center = CGPoint(x: recognizer.view!.center.x + translation.x, y: recognizer.view!.center.y)
            
            if let view = self.scrollView {
                //                view.panGestureRecognizer.view!.center = CGPointMake(view.panGestureRecognizer.view!.center.x, view.panGestureRecognizer.view!.center.y + translation.y)
                view.contentOffset.y -= translation.y
                if view.contentOffset.y < 0 {
                    view.contentOffset.y = 0
                }
                
                if view.contentSize.height > view.frame.size.height && view.contentOffset.y > view.contentSize.height - view.frame.size.height {
                    view.contentOffset.y = view.contentSize.height - view.frame.size.height
                }
                
            }
            
            if recognizer.state == UIGestureRecognizerState.ended {
                let totalTranslation = self.slidesContainer.frame.size.width / 2 - recognizer.view!.center.x
                for item in self.slidesContainer.subviews {
                    (item ).frame = CGRect(x: item.frame.origin.x - totalTranslation, y: item.frame.origin.y, width: item.frame.size.width, height: item.frame.size.height)
                }
                
                let currentView:UIView = self.slidesContainer.subviews[self.currentSlide]
                var isChange = true
                if currentView.frame.origin.x < 20 - self.frame.size.width * 0.25 {
                    //slide to left (next ++ )
                    self.currentSlide += 1
                }
                else if currentView.frame.origin.x > 20 + self.frame.size.width * 0.25 {
                    //slide to right (prev -- )
                    self.currentSlide -= 1
                }
                else{
                    //not slide
                    isChange = false
                }
                
                if self.currentSlide < 0 {
                    self.currentSlide = self.slidesContainer.subviews.count - 1
                }
                else if self.currentSlide == self.slidesContainer.subviews.count {
                    self.currentSlide = 0
                }
                
                recognizer.view!.center = CGPoint(x: self.slidesContainer.frame.size.width / 2, y: recognizer.view!.center.y)
                
                self.animateSlideshow(isChange)
            }
            recognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.slidesContainer)
        }
    }
    
    func animateSlideshow(_ slideChanged: Bool) {
        if timer != nil {
            timer.invalidate()
        }
        var prev = self.currentSlide - 1
        var next = self.currentSlide + 1
        let maxIndex = self.slidesContainer.subviews.count - 1
        
        if maxIndex >= 0 {
            if self.currentSlide == 0 {
                prev = maxIndex
            }
            else if self.currentSlide == maxIndex{
                next = 0
            }
            
            if prev < 0 {
                prev = 0
            }
            if next > maxIndex {
                next = 0
            }
            
            let currentView: UIView = self.slidesContainer.subviews[self.currentSlide]
            let nextView: UIView = self.slidesContainer.subviews[next]
            let prevView: UIView = self.slidesContainer.subviews[prev]
            
            let pageColor : UIColor = UIColor.colorPrimary//(white: 0.6, alpha: 1)
            let borderColor : UIColor = UIColor(white: 0, alpha: 0)
            let inactivePageColor : UIColor = UIColor(white:1, alpha: 1)
            let inactiveBorderColor : UIColor = UIColor(white: 0, alpha: 0)
            let pages: UIView = self.paginationContainer.subviews[0]
            var i:Int = 0
            for item in pages.subviews {
                let page: UIView = item
                if i == self.currentSlide {
                    page.backgroundColor = pageColor
                    page.layer.borderColor = borderColor.cgColor
                }
                else {
                    page.backgroundColor = inactivePageColor
                    page.layer.borderColor = inactiveBorderColor.cgColor
                }
                i += 1
            }
            
            let width:CGFloat = currentView.frame.size.width
            
            if next != self.currentSlide {
                nextView.frame = CGRect(x: currentView.frame.origin.x + width,
                                            y: nextView.frame.origin.y,
                                            width: nextView.frame.size.width,
                                            height: nextView.frame.size.height)
            }
            
            if prev != self.currentSlide {
                prevView.frame = CGRect(x: currentView.frame.origin.x - width,
                                            y: prevView.frame.origin.y,
                                            width: prevView.frame.size.width,
                                            height: prevView.frame.size.height)
            }
            
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            
            currentView.frame = CGRect(x: 0,
                                           y: currentView.frame.origin.y,
                                           width: currentView.frame.size.width,
                                           height: currentView.frame.size.height)
            
            if next != self.currentSlide {
                nextView.frame = CGRect(x: width,
                                            y: nextView.frame.origin.y,
                                            width: nextView.frame.size.width,
                                            height: nextView.frame.size.height)
            }
            
            if prev != self.currentSlide {
                prevView.frame = CGRect(x: 0 - width,
                                            y: prevView.frame.origin.y,
                                            width: prevView.frame.size.width,
                                            height: prevView.frame.size.height)
            }
            
            UIView.commitAnimations()
            
            if slideChanged {
                self.delegate.onSlideChanged(self, index: self.currentSlide)
            }
            
            if self.autoplay {
                timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(BSSlider.update), userInfo: nil, repeats: false)
            }
        }
    }
    
    func update(){
        self.currentSlide += 1;
        if self.currentSlide == self.slidesContainer.subviews.count {
            self.currentSlide = 0
        }
        self.animateSlideshow(true)
    }
}
