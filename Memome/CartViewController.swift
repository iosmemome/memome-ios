//
//  CartViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/26/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class CartViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var cartTableView: UITableView!
    
    @IBOutlet weak var lblItemCount: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    @IBOutlet weak var btnCheckout: UIButton!
    @IBOutlet weak var btnAddMore: UIButton!
    
    var isAddToCart: Bool = false
    var cartDataSource: NSMutableArray! = NSMutableArray()
    var orderProductsDataSource: NSMutableArray! = NSMutableArray()
    var idOrder: Int32 = Int32()
    var arrayData = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.cartTableView.register(UINib(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: "cartTableViewCell")
        
        if self.attributes != nil{
            if let isAddToCart = self.attributes.object(forKey: "isAddToCart") as? Bool{
                self.isAddToCart = isAddToCart
            if let order = self.attributes.object(forKey: "cart") as? NSDictionary{
//                self.cartDataSource.addObjects(from: order as [AnyObject])
            }
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadDataCart()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //Table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cartDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let minHeight: CGFloat = 167.0
        var heightByPhoto: CGFloat = 82.0
        var heightByTitle: CGFloat = 145.0
        if let cart = self.cartDataSource.object(at: (indexPath as NSIndexPath).row) as? Cart {
            if let project = ProjectHelper.getProject(cart.projectId){
                let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData) as! NSDictionary
                
                let coverWidth: CGFloat = CGFloat((productData.object(forKey: "cover_width") as! NSString).floatValue)
                let coverHeight: CGFloat = CGFloat((productData.object(forKey: "cover_height") as! NSString).floatValue)
                
                let wrapperWidth: CGFloat = 85.0
                let wrapperHeight = CGFloat(coverHeight / coverWidth) * wrapperWidth
                
                heightByPhoto += wrapperHeight
                
                var title : NSString!
                if project.title == ""{
                    if let productTitle = productData.object(forKey: "title") as? String {
                        title = productTitle as NSString!
                    }
                }
                else{
                    title = String(format: "\"%@\"", arguments: [project.title]) as NSString!
                }
                
                let titleHeight : CGFloat = title.boundingRect(
                    with: CGSize(width: tableView.frame.size.width - 145, height: 99999),
                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 16)!],
                    context: nil
                    ).size.height
                
                heightByTitle += titleHeight
            }
            
            
        }
        return max(minHeight, max(heightByTitle, heightByPhoto))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartTableViewCell", for: indexPath)
        
        let coverWrapper: UIView = cell.viewWithTag(100)!
        let lblTitle: UILabel = cell.viewWithTag(200) as! UILabel
        let lblPhotoCount: UILabel = cell.viewWithTag(300) as! UILabel
        let lblTotalPrice: UILabel = cell.viewWithTag(400) as! UILabel
        let lblQty: UILabel = cell.viewWithTag(500) as! UILabel
        let btnPlus: EditButton = cell.viewWithTag(600) as! EditButton
        let btnMinus: EditButton = cell.viewWithTag(700) as! EditButton
        let btnDelete: EditButton = cell.viewWithTag(800) as! EditButton
        
        if let cart = self.cartDataSource.object(at: (indexPath as NSIndexPath).row) as? Cart {
            if let project = ProjectHelper.getProject(cart.projectId){
                let projectDetails = ProjectHelper.getAllProjectDetails(cart.projectId)
                let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData) as! NSDictionary
                
                    let cover = ProjectHelper.getCoverImage(cart.projectId, true)
                    let coverWidth: CGFloat = CGFloat((productData.object(forKey: "cover_width") as! NSString).floatValue)
                    let coverHeight: CGFloat = CGFloat((productData.object(forKey: "cover_height") as! NSString).floatValue)
                    
                    let wrapperWidth = coverWrapper.frame.size.width
                    let wrapperHeight = CGFloat(coverHeight / coverWidth) * wrapperWidth
                    
                    if let heightConstraint = Globals.getConstraint("height", view: coverWrapper) {
                        heightConstraint.constant = wrapperHeight
                    }
                
//                if cart.reorderId == 0{
                    MemomeHelper.setCoverView(coverWrapper, project: project, cover: cover, productData: productData, size: CGSize(width: wrapperWidth, height: wrapperHeight))
                    
                    lblPhotoCount.text = String(format: "%i Photos", arguments: [projectDetails!.count])
                    
//                }

                
                if project.title == ""{
                    if let productTitle = productData.object(forKey: "title") as? String {
                        lblTitle.text = productTitle
                    }
                }
                else{
                    lblTitle.text = project.title
                }
                
                if project.corporateVoucherId > 0 {
                    lblPhotoCount.text = lblPhotoCount.text! + "\nFree \(project.corporateVoucherQty)x"
                }
                
                lblTotalPrice.text = Globals.numberFormat(NSNumber(value: cart.total as Double))
                lblQty.text = Globals.numberFormat(NSNumber(value: cart.qty as Int32))
                lblQty.layer.cornerRadius = lblQty.frame.size.width / 2.0
                lblQty.layer.borderColor = UIColor.colorPrimary.cgColor
                lblQty.layer.borderWidth = 1.0
                
                btnPlus.layer.cornerRadius = btnPlus.frame.size.width / 2.0
                btnMinus.layer.cornerRadius = btnMinus.frame.size.width / 2.0
                
                btnPlus.index = (indexPath as NSIndexPath).row
                btnMinus.index = (indexPath as NSIndexPath).row
                btnDelete.index = (indexPath as NSIndexPath).row
                
                btnPlus.addTarget(self, action: #selector(CartViewController.btnPlusTouched(_:)), for: UIControlEvents.touchUpInside)
                btnMinus.addTarget(self, action: #selector(CartViewController.btnMinusTouched(_:)), for: UIControlEvents.touchUpInside)
                btnDelete.addTarget(self, action: #selector(CartViewController.btnDeleteTouched(_:)), for: UIControlEvents.touchUpInside)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cart = self.cartDataSource.object(at: (indexPath as NSIndexPath).row) as? Cart {
            if let project = ProjectHelper.getProject(cart.projectId){
                if let cover = ProjectHelper.getCoverImage(project.id) {
                    MemomeHelper.coverImage = NSMutableDictionary(dictionary: [
                        "title" : project.title,
                        "subtitle" : project.subtitle,
                        "url" : cover.image,
                        "size" : NSValue(cgSize: CGSize(width: CGFloat(cover.width), height: CGFloat(cover.height))),
                        "offset" : NSValue(cgPoint: CGPoint(x: CGFloat(cover.offsetX), y: CGFloat(cover.offsetY))),
                        "zoomScale" : NSNumber(value: cover.zoom as Float),
                        "rotation" : NSNumber(value: Int(cover.rotation) as Int),
                        ])
                }
                else{
                    MemomeHelper.coverImage = NSMutableDictionary()
                }
                
                MemomeHelper.images = NSMutableArray()
                
                guard let details = ProjectHelper.getAllProjectDetails(project.id) else {return}
                for item in details {
                    if let detail = item as? ProjectDetail {
                        if detail.type != ProjectDetail.TYPE_COVER {
                            MemomeHelper.images.add(NSMutableDictionary(dictionary: [
                                "url" : detail.image,
                                "size" : NSValue(cgSize: CGSize(width: CGFloat(detail.width), height: CGFloat(detail.height))),
                                "offset" : NSValue(cgPoint: CGPoint(x: CGFloat(detail.offsetX), y: CGFloat(detail.offsetY))),
                                "zoomScale" : NSNumber(value: detail.zoom as Float),
                                "rotation" : NSNumber(value: Int(detail.rotation) as Int)
                                ]))
                        }
                    }
                }
                
                let attributes = NSMutableDictionary(dictionary: [
                    "product" : NSKeyedUnarchiver.unarchiveObject(with: project.productData as Data) as! NSDictionary,
                    "productDetails" : NSKeyedUnarchiver.unarchiveObject(with: project.productDetailData as Data) as! NSArray,
                    "project" : project,
                    "projectDetails" : details
                    ])
                if project.corporateVoucherId > 0 {
                    
                    attributes.setValue(project.corporateVoucherCode, forKeyPath: "voucherCode")
                    attributes.setValue([
                        "id" : NSNumber(value: project.corporateVoucherId as Int32),
                        "qty" : NSNumber(value: project.corporateVoucherQty as Int32),
                        "name" : project.corporateVoucherName
                        ], forKey: "voucher")
                }
                
                var isTitleRequired = true
                let product = NSKeyedUnarchiver.unarchiveObject(with: project.productData as Data) as! NSDictionary
                
                attributes.setValue(product, forKey: "titleProduct")
                
                if let isRequireTitle = product.object(forKey: "is_require_title") as? NSNumber {
                    isTitleRequired = isRequireTitle.intValue == 1
                }
                else if let isRequireTitle = product.object(forKey: "is_require_title") as? NSString {
                    isTitleRequired = isRequireTitle.integerValue == 1
                }
                if cart.reorderId == 0 {
                    if isTitleRequired {
                        self.showViewController("ProductPhotosViewController", attributes: attributes)
                    }
                    else {
                        self.showViewController("ProductPhotosViewController", attributes: attributes)
                    }
                }
                else{
                    Globals.showAlertWithTitle("Sorry", message: "Image can not be edited because this order is from reorder", viewController: self)
                }
            }
        }
    }
    
    // Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        if let parent = self._parentViewController as? MakePrintDetailViewController{
            parent.productTableViewAdapter.slideBool = true
        }
        self.dismissSelf()
    }
    @IBAction func btnCheckoutTouched(_ sender: AnyObject) {
        if self.cartDataSource.count == 0 {
            Globals.showAlertWithTitle("Cart is empty", message: "Please add one or more item to your shopping cart. You can add it from My Project or create a new project.", viewController: self, completion: { (action) in
                self.showHomeViewController()
            })
        }
        else{
            if Globals.isLoggedIn() {
                self.showViewController("CheckoutViewController", attributes: nil)
            }
            else{
                self.showViewController("LoginViewController", attributes: nil)
            }
        }
    }
    @IBAction func btnAddMoreTouched(_ sender: AnyObject) {
        self.showHomeViewController()
    }
    
    func btnPlusTouched(_ sender: AnyObject){
        if let btn = sender as? EditButton {
            if let cart = self.cartDataSource.object(at: btn.index) as? Cart {
                CartHelper.updateCart(cart.id, qty: cart.qty+1)
                self.loadDataCart()
            }
        }
    }
    
    func btnMinusTouched(_ sender: AnyObject){
        if let btn = sender as? EditButton {
            if let cart = self.cartDataSource.object(at: btn.index) as? Cart {
                if cart.qty > 1 {
                    CartHelper.updateCart(cart.id, qty: cart.qty-1)
                    self.loadDataCart()
                }
            }
        }
    }
    
    func btnDeleteTouched(_ sender: AnyObject){
        if let btn = sender as? EditButton {
            if let cart = self.cartDataSource.object(at: btn.index) as? Cart {
                Globals.showConfirmAlertWithTitle("Delete this item", message: "Are you sure want to delete this item?", viewController: self, completion: { (action) in
                    CartHelper.deleteFromCart(cart.id)
                    self.loadDataCart()
                })
            }
        }
    }
    
    
    // general methods
    func loadDataCart() {
        self.cartDataSource.removeAllObjects()
        
        self.cartDataSource.addObjects(from: CartHelper.getCart() as [AnyObject])
        self.lblItemCount.text = String(format: "%i Item%@", arguments: [self.cartDataSource.count, (self.cartDataSource.count > 1 ? "s" : "")])
        
        var total: Double = 0.0
        for item in self.cartDataSource {
            if let cart = item as? Cart {
                total += cart.total
                
                if let project = ProjectHelper.getProject(cart.projectId) {
                    if project.corporateVoucherId > 0 {
                        total -= Double(project.corporateVoucherQty) * cart.price
                    }
                }
            }
        }
        
        self.lblTotalPrice.text = Globals.numberFormat(NSNumber(value: total as Double))
        
        self.cartTableView.reloadData()
    }
    
    func loadDataImage(){
        
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(
                Globals.getApiUrl("orders/getCompletedOrders"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String,
                ],
                completion: { (result) in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        self.orderProductsDataSource.removeAllObjects()
                        
                        if let orders = result.object(forKey: "orders") as? NSArray {
                            
                            for order in orders {
                                
                                if let products = (order as! NSDictionary).object(forKey: "order_product") as? NSArray {
                                    for product in products {
                                        
                                        let dict = NSMutableDictionary()
                                        
                                        let orderProductid = (product as! NSDictionary).object(forKey: "id") as? String
                                        if orderProductid == String(self.idOrder) {
                                            if let count_photo = (product as! NSDictionary).object(forKey: "count_photo") as? String {
                                                dict.setValue(count_photo, forKey: "count_photo")
                                            }
                                                                            if let cover_images = (product as! NSDictionary).object(forKey: "cover_image") as? NSArray{
                                                
                                                                                for cover in cover_images {
                                                                                    let image = (cover as! NSDictionary).object(forKey: "image") as? NSDictionary
                                                
                                                                                    if image != nil{
                                                                                        let cover_image = (cover as! NSDictionary).object(forKey: "image") as? NSDictionary
                                                                                        if let image_large = cover_image?.object(forKey: "large") as? String{
                                                
                                                                                            dict.setValue(image_large, forKey: "image")
                                                                                            
                                                                                        }
                                                                                    }
                                                
                                                                                    else{
                                                
                                                                                        if let order_product_details = (product as! NSDictionary).object(forKey: "order_product_detail") as? NSArray{
                                                                                            if let order_product = order_product_details.object(at: 0) as? NSDictionary{
                                                                                                if let image = order_product.object(forKey: "image") as? NSDictionary{
                                                                                                    if let image_large = image.object(forKey: "large") as? String{
                                                
                                                                                                        dict.setValue(image_large, forKey: "image")
                                                                                                        
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    } //else
                                                
                                                                                } //for
                                                                            } // if
//                                                                        }
                                            
                                        }
                                        if dict.count != 0{
                                            self.arrayData.add(dict)
                                        }
                                    }
                                }
                            }
                        }
                    }
                        
                    else{
                        Globals.showAlertWithTitle("My Orders Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
            }
        } // loadDataImage
    
    
}
