//
//  BSPhotoCrop.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/14/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class BSPhotoOverlayView: UIView {
    
    internal var gridColor: UIColor! = UIColor.white
    internal var imageRatio: CGFloat = 1
    
    func transparentRect(_ rect: CGRect) -> CGRect {
        let width: CGFloat = rect.size.width
        let height: CGFloat = rect.size.height
        var targetHeight: CGFloat = width / self.imageRatio
        var targetWidth: CGFloat = width
        
        if targetHeight > height {
            targetHeight = height
            targetWidth = height * self.imageRatio
        }
        
        return CGRect(x: (width - targetWidth) / 2, y: (height - targetHeight) / 2, width: targetWidth, height: targetHeight)
    }
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        if let bgColor = self.backgroundColor {
            bgColor.setFill()
            UIRectFill(rect)
        }
        
        // clear the background in the given rectangles
        let holeRect: CGRect = self.transparentRect(rect)
        let holeRectIntersection = holeRect.intersection(rect )
        UIColor.clear.setFill()
        UIRectFill(holeRectIntersection)
        
        //grid
        let grids: NSMutableArray = NSMutableArray()
        grids.add(NSValue(cgRect: CGRect(x: holeRect.origin.x, y: holeRect.origin.y, width: holeRect.size.width, height: 2))) // T
        grids.add(NSValue(cgRect: CGRect(x: holeRect.origin.x, y: holeRect.origin.y + holeRect.size.height - 2, width: holeRect.size.width, height: 2))) // B
        grids.add(NSValue(cgRect: CGRect(x: holeRect.origin.x, y: holeRect.origin.y, width: 2, height: holeRect.size.height))) // L
        grids.add(NSValue(cgRect: CGRect(x: holeRect.origin.x + holeRect.size.width - 2, y: holeRect.origin.y, width: 2, height: holeRect.size.height))) // R
        
        let vSpace = holeRect.size.height / 3.0
        let hSpace = holeRect.size.width / 3.0
        grids.add(NSValue(cgRect: CGRect(x: holeRect.origin.x + hSpace, y: holeRect.origin.y, width: 1, height: holeRect.size.height))) // V1
        grids.add(NSValue(cgRect: CGRect(x: holeRect.origin.x + (2 * hSpace), y: holeRect.origin.y, width: 1, height: holeRect.size.height))) // V2
        
        grids.add(NSValue(cgRect: CGRect(x: holeRect.origin.x, y: holeRect.origin.y + vSpace, width: holeRect.size.width, height: 1))) // H1
        grids.add(NSValue(cgRect: CGRect(x: holeRect.origin.x, y: holeRect.origin.y + (2 * vSpace), width: holeRect.size.width, height: 1))) // H2
        
        for value in grids {
            if let grid = value as? NSValue {
                let lineRect = grid.cgRectValue.intersection(rect)
                self.gridColor.setFill()
                UIRectFill(lineRect)
            }
        }
    }
}

open class BSPhotoCrop: UIView, UIScrollViewDelegate {
    
    fileprivate var originalImage: UIImage? = nil
    
    fileprivate var photoScrollView: UIScrollView!
    fileprivate var photoInnerScrollView: UIView!
    
    fileprivate var photoView: UIImageView!
    fileprivate var photoViewHeight: NSLayoutConstraint!
    fileprivate var photoViewWidth: NSLayoutConstraint!
    
    fileprivate var photoOverlay: BSPhotoOverlayView!
    
    fileprivate var initialized: Bool! = false
    
    // options
    open var imageRatio: CGFloat = 1 // w : h
    open var gridColor: UIColor! = UIColor.white
    open var maxZoomScale: CGFloat = 2.0
    // -- end of options
    
    //global static func
    static func cropImage(_ image: UIImage, rect: CGRect) -> UIImage? {
        let rectTransform = self.transformRectForImage(image)
        var rect = rect
        if image.scale > 1.0 {
            rect = CGRect(x: rect.origin.x * image.scale,
                y: rect.origin.y * image.scale,
                width: rect.size.width * image.scale,
                height: rect.size.height * image.scale)
        }
        rect = rect.applying(rectTransform)
        if let imageRef: CGImage = image.cgImage?.cropping(to: rect) {
            let image = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
            return image
        }
        else{
            return nil
        }
    }
    
    static func rotateImageBy90Degree(_ image: UIImage, degree: Int) -> UIImage{
        var orientation: UIImageOrientation! = .up
        
        let degree = degree % 360
        
        switch (degree) {
        case 90 : orientation = .right
            break
        case 180 : orientation = .down
            break
        case 270 : orientation = .left
            break
        case -90 : orientation = .right
            break
        case -180 : orientation = .down
            break
        case -270 : orientation = .left
            break
        default:
            break
        }
        
        return UIImage(cgImage: image.cgImage! ,
            scale: image.scale ,
            orientation: orientation)
    }
    
    fileprivate static func transformRectForImage(_ image: UIImage) -> CGAffineTransform {
        var rectTransform: CGAffineTransform!
        
        switch image.imageOrientation {
        case UIImageOrientation.left:
            rectTransform = CGAffineTransform(rotationAngle: 90.0 / 180.0 * CGFloat(Double.pi)).translatedBy(x: 0, y: -image.size.height)
            break
        case UIImageOrientation.down:
            rectTransform = CGAffineTransform(rotationAngle: 180.0 / 180.0 * CGFloat(Double.pi)).translatedBy(x: -image.size.width, y: -image.size.height)
            break
        case UIImageOrientation.right:
            rectTransform = CGAffineTransform(rotationAngle: -90.0 / 180.0 * CGFloat(Double.pi)).translatedBy(x: -image.size.width, y: 0)
            break
        default:
            rectTransform = CGAffineTransform.identity
        }
        return rectTransform
    }
    
    func initView(){
        if self.photoScrollView == nil {
            self.photoScrollView = UIScrollView()
            self.addSubview(self.photoScrollView)
            self.photoScrollView.translatesAutoresizingMaskIntoConstraints = false
            self.addConstraint(NSLayoutConstraint(
                item: self.photoScrollView,
                attribute: NSLayoutAttribute.top,
                relatedBy: NSLayoutRelation.equal,
                toItem: self,
                attribute: NSLayoutAttribute.top,
                multiplier: 1,
                constant: 0))
            self.addConstraint(NSLayoutConstraint(
                item: self.photoScrollView,
                attribute: NSLayoutAttribute.bottom,
                relatedBy: NSLayoutRelation.equal,
                toItem: self,
                attribute: NSLayoutAttribute.bottom,
                multiplier: 1,
                constant: 0))
            self.addConstraint(NSLayoutConstraint(
                item: self.photoScrollView,
                attribute: NSLayoutAttribute.leading,
                relatedBy: NSLayoutRelation.equal,
                toItem: self,
                attribute: NSLayoutAttribute.leading,
                multiplier: 1,
                constant: 0))
            self.addConstraint(NSLayoutConstraint(
                item: self.photoScrollView,
                attribute: NSLayoutAttribute.trailing,
                relatedBy: NSLayoutRelation.equal,
                toItem: self,
                attribute: NSLayoutAttribute.trailing,
                multiplier: 1,
                constant: 0))
            self.photoScrollView.maximumZoomScale = self.maxZoomScale
            self.photoScrollView.showsHorizontalScrollIndicator = false
            self.photoScrollView.showsVerticalScrollIndicator = false
            self.photoScrollView.delegate = self
            self.photoScrollView.backgroundColor = UIColor.black
            
            self.photoInnerScrollView = UIView()
            self.photoScrollView.addSubview(self.photoInnerScrollView)
            
            self.photoView = UIImageView()
            self.photoView.contentMode = .scaleAspectFill
            self.photoInnerScrollView.addSubview(self.photoView)
            
            //photo overlay
            self.photoOverlay = BSPhotoOverlayView()
            self.photoOverlay.gridColor = self.gridColor
            self.photoOverlay.imageRatio = self.imageRatio
            self.photoOverlay.backgroundColor = UIColor(white: 0, alpha: 0.6)
            self.addSubview(self.photoOverlay)
            self.photoOverlay.translatesAutoresizingMaskIntoConstraints = false
            self.addConstraint(NSLayoutConstraint(
                item: self.photoOverlay,
                attribute: NSLayoutAttribute.top,
                relatedBy: NSLayoutRelation.equal,
                toItem: self,
                attribute: NSLayoutAttribute.top,
                multiplier: 1,
                constant: 0))
            self.addConstraint(NSLayoutConstraint(
                item: self.photoOverlay,
                attribute: NSLayoutAttribute.bottom,
                relatedBy: NSLayoutRelation.equal,
                toItem: self,
                attribute: NSLayoutAttribute.bottom,
                multiplier: 1,
                constant: 0))
            self.addConstraint(NSLayoutConstraint(
                item: self.photoOverlay,
                attribute: NSLayoutAttribute.leading,
                relatedBy: NSLayoutRelation.equal,
                toItem: self,
                attribute: NSLayoutAttribute.leading,
                multiplier: 1,
                constant: 0))
            self.addConstraint(NSLayoutConstraint(
                item: self.photoOverlay,
                attribute: NSLayoutAttribute.trailing,
                relatedBy: NSLayoutRelation.equal,
                toItem: self,
                attribute: NSLayoutAttribute.trailing,
                multiplier: 1,
                constant: 0))
            
            self.photoOverlay.addGestureRecognizer(self.photoScrollView.pinchGestureRecognizer!)
            self.photoOverlay.addGestureRecognizer(self.photoScrollView.panGestureRecognizer)
            
            self.initialized = true
        }
    }
    
    func initWithImage(_ image: UIImage?){
        self.initView()
        self.setImage(image)
    }
    
    func initWithImage(_ image: UIImage, cropSize: CGSize, cropOffset: CGPoint) {
        self.initView()
        self.setImage(image, cropSize: cropSize, cropOffset: cropOffset)
    }
    
    open func setImage(_ image: UIImage?){
        self.photoView.image = image
        self.originalImage = image
        
        if let img = image {
            let rect = self.photoOverlay.transparentRect(self.frame)
            self.photoScrollView.minimumZoomScale = max(rect.size.width / img.size.width, rect.size.height / img.size.height)
            self.photoScrollView.setZoomScale(self.photoScrollView.minimumZoomScale, animated: false)
            self.scrollViewDidZoom(self.photoScrollView)
        }
        else{
            self.photoView.frame = CGRect.zero
            self.photoInnerScrollView.frame = CGRect.zero
            self.photoScrollView.contentSize = CGSize.zero
        }
    }
    
    open func setImage(_ image: UIImage, cropSize: CGSize, cropOffset: CGPoint) {
        self.photoView.image = image
        self.originalImage = image
        let rect = self.photoOverlay.transparentRect(self.frame)
        self.photoScrollView.minimumZoomScale = max(rect.size.width / image.size.width, rect.size.height / image.size.height)
        let scale = max(rect.size.width / cropSize.width, rect.size.height / cropSize.height)
        self.photoScrollView.setZoomScale(scale, animated: false)
        self.scrollViewDidZoom(self.photoScrollView)
        
        self.photoScrollView.contentOffset = CGPoint(x: cropOffset.x * scale, y: cropOffset.y * scale)
        
        
    }
    
    
    open func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.photoInnerScrollView
    }
    
    open func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let rect = self.photoOverlay.transparentRect(self.frame)
        if let img = self.originalImage {
            let topSpace = rect.origin.y / self.photoScrollView.zoomScale
            let leftSpace = rect.origin.x / self.photoScrollView.zoomScale
            self.photoInnerScrollView.frame = CGRect(x: 0, y: 0, width: img.size.width + (2 * leftSpace), height: img.size.height + (2 * topSpace))
            self.photoView.frame = CGRect(x: leftSpace, y: topSpace, width: img.size.width, height: img.size.height)
            self.photoScrollView.contentSize = CGSize(width: self.photoInnerScrollView.frame.size.width * self.photoScrollView.zoomScale, height: self.photoInnerScrollView.frame.size.height * self.photoScrollView.zoomScale)
        }
    }
    
    open func getOriginalImage() -> UIImage? {
        return self.originalImage
    }
    
    open func getCroppedImage() -> UIImage? {
        if let img = self.originalImage {
            let rect = self.photoOverlay.transparentRect(self.frame)
            let offsetLeft = self.photoScrollView.contentOffset.x / self.photoScrollView.zoomScale
            let offsetTop = self.photoScrollView.contentOffset.y / self.photoScrollView.zoomScale
            let width = rect.size.width / self.photoScrollView.zoomScale
            let height = rect.size.height / self.photoScrollView.zoomScale
            
            let croppedRect = CGRect(x: offsetLeft, y: offsetTop, width: width, height: height)
            return BSPhotoCrop.cropImage(img, rect: croppedRect)
        }
        else {
            return nil
        }
    }
    
    open func getOffset() -> CGPoint {
        let offsetLeft = self.photoScrollView.contentOffset.x / self.photoScrollView.zoomScale
        let offsetTop = self.photoScrollView.contentOffset.y / self.photoScrollView.zoomScale
        return CGPoint(x: offsetLeft, y: offsetTop)
    }
    
    open func getSize() -> CGSize {
        let rect = self.photoOverlay.transparentRect(self.frame)
        let width = rect.size.width / self.photoScrollView.zoomScale
        let height = rect.size.height / self.photoScrollView.zoomScale
        return CGSize(width: width, height: height)
    }
    
    open func getZoomScale() -> CGFloat {
        return self.photoScrollView.zoomScale
    }
    
    open func isInitialized() -> Bool {
        return self.initialized
    }
}
