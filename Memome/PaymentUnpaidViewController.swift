//
//  PaymentUnpaidViewController.swift
//  Memome
//
//  Created by Trio-1602 on 4/14/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class PaymentUnpaidViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    
    @IBOutlet weak var paymentTableView: UITableView!
    
    var paymentDataSource: NSMutableArray = NSMutableArray()
    let dbDateFormat: DateFormatter! = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.btnShoppingCart = self.btnCart
        
        self.paymentTableView.register(UINib(nibName: "PaymentUnpaidTableViewCell", bundle: nil), forCellReuseIdentifier: "paymentUnpaidTableViewCell")
        
        self.dbDateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadDataPaymentUnpaid()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // tableview delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.paymentDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymentUnpaidTableViewCell", for: indexPath)
        
        let lblNumber = cell.viewWithTag(1) as! UILabel
        let lblDate = cell.viewWithTag(2) as! UILabel
        let lblTotal = cell.viewWithTag(3) as! UILabel
        let lblPayment = cell.viewWithTag(4) as! UILabel
        
        let cellInfoArray = self.paymentDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        
        if let number = cellInfoArray.object(forKey: "number") as? String {
            lblNumber.text = "ORDER #\(number)"
        }
        else{
            lblNumber.text = ""
        }
        
        if let createdAt = cellInfoArray.object(forKey: "created_at") as? String {
            let calendar = Calendar.current
            let components: DateComponents = (calendar as NSCalendar).components([.month, .day, .year, .hour, .minute, .second], from: self.dbDateFormat.date(from: createdAt)!)
            
            lblDate.text = String(format: "%i %@ %i", arguments: [
                components.day!,
                Globals.getMonthName(components.month!),
                components.year!
                ])
        }
        else{
            lblDate.text = ""
        }
        
        if let total = cellInfoArray.object(forKey: "grand_total") as? NSString {
            lblTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: total.doubleValue as Double))
        }
        else{
            lblTotal.text = ""
        }
        
        if let payment = cellInfoArray.object(forKey: "payment_type") as? String {
            lblPayment.text = payment.uppercased()
        }
        else{
            lblPayment.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row < self.paymentDataSource.count {
            let cellInfoArray = self.paymentDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
            if let payment = cellInfoArray.object(forKey: "payment_type") as? String {
                if payment == Constant.paymentCreditCard {
                    Globals.showWebViewWithUrl(Constant.getPaymentUrl(cellInfoArray.object(forKey: "id") as! String, grandTotal: cellInfoArray.object(forKey: "grand_total") as! String), parentView: self, title: "PAYMENT")
                }
                else{
                    self.showViewController("PaymentConfirmationViewController", attributes: [
                        "orderId" : cellInfoArray.object(forKey: "id") as! String
                        ])
                }
            }
            
        }
    }

    // actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    
    // general methods
    func loadDataPaymentUnpaid() {
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(
                Globals.getApiUrl("orders/getOrderNotYetPaid"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        self.paymentDataSource.removeAllObjects()
                        if let orders = result.object(forKey: "orders") as? NSArray {
                            self.paymentDataSource.addObjects(from: orders as [AnyObject])
                        }
                        
                        self.paymentTableView.reloadData()
                        
                    }
                    else{
                        Globals.showAlertWithTitle("Payment Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
}
