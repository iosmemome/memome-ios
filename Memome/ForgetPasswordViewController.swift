//
//  ForgetPasswordViewController.swift
//  Memome
//
//  Created by staff on 1/4/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    @IBAction func btnSubmitTouched(_ sender: AnyObject) {
        let params = NSMutableDictionary()
        params.setValue(self.txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "email")
        params.setValue(Globals.getProperty("clientId"), forKey: "clientId")
        
        if !self.isLoading() && self.validateForm(params) {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("users/forgetPassword"),
                requestType: Globals.HTTPRequestType.http_POST,
                params: params,
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        self.dismissSelf()
                        if let parent = self._parentViewController {
                            Globals.showAlertWithTitle("Forget Password Success", message: "We have sent a password resetter link to your associative email.", viewController: parent)
                        }
                    }
                    else{
                        Globals.showAlertWithTitle("Forget Password Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    //general methods
    func validateForm(_ params: NSDictionary) -> Bool {
        var isValid = true
        if params.object(forKey: "email") as! String == "" || !Globals.isValidEmail(params.object(forKey: "email") as! String) {
            Globals.showAlertWithTitle("Forget Password Error", message: "Email must not be empty and must be a valid email address!", viewController: self)
            isValid = false
        }
        
        return isValid
    }
}
