//
//  ProjectPreviewViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/26/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class ProjectPreviewViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    
    var productData: NSDictionary!
    var productDetails: NSArray!
    
    var project: Project!
    var projectDetails: NSMutableArray! = NSMutableArray()
    
    var photoCount: Int = 0
    var hasCover = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let project = self.attributes.object(forKey: "project") as? Project {
            self.productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData as Data) as! NSDictionary
            self.productDetails = NSKeyedUnarchiver.unarchiveObject(with: project.productDetailData as Data) as! NSArray
            
            self.project = project
            if let details = ProjectHelper.getAllProjectDetails(project.id) {
                self.projectDetails.addObjects(from: details as [AnyObject])
            }
        }
        
        self.photoCount = 0
        
        self.photoCollectionView.register(UINib(nibName: "CoverCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "coverCollectionViewCell")
        self.photoCollectionView.register(UINib(nibName: "ContentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "contentCollectionViewCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.photoCount = self.projectDetails.count
        if let _ = ProjectHelper.getCoverImage(self.project.id) {
//            self.photoCount += 1
            self.hasCover = true
        }
        else {
//            self.photoCount += 1
            self.hasCover = false
        }
        self.photoCollectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photoCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell!
        
        if (indexPath as NSIndexPath).row == 0 {
            //cover
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coverCollectionViewCell", for: indexPath)
            self.setupCoverPreview(cell)
        }
        else{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contentCollectionViewCell", for: indexPath)
            if (indexPath as NSIndexPath).row < self.photoCount {
                self.setupContentPreview(cell, indexPath: indexPath)
            }
        }
        
        return cell
    }
    
    // collection view flow layout delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionViewCellSizeAtIndexPath(indexPath)
    }
    
    func collectionViewCellSizeAtIndexPath(_ indexPath: IndexPath) -> CGSize{
        var col: Int = 3
        if self.view.frame.size.width > 618 { // ipad
            col = 5
        }
        
        if (indexPath as NSIndexPath).row == 0 {
            col = 1
        }
        
        let width: CGFloat = (self.view.frame.size.width - 2.0) / CGFloat(col)
        var height: CGFloat = width
        
        let index = self.hasCover ? (indexPath as NSIndexPath).row - 1 : (indexPath as NSIndexPath).row - 1
        
        if (indexPath as NSIndexPath).row == 0 {
            let coverWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_width") as! NSString).floatValue)
            let coverHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_height") as! NSString).floatValue)
            let ratio: CGFloat = MemomeHelper.calculateRatio(coverWidth, targetSize: width)
            height = coverHeight * ratio
        }
        else {
            if let detail = self.productDetails.object(at: index) as? NSDictionary {
                let wrapperWidth: CGFloat = CGFloat((detail.object(forKey: "width") as! NSString).floatValue)
                let wrapperHeight: CGFloat = CGFloat((detail.object(forKey: "height") as! NSString).floatValue)
                let ratio: CGFloat = MemomeHelper.calculateRatio(wrapperWidth, targetSize: width)
                height = wrapperHeight * ratio
            }
        }
//        NSLog("cell size : \(width) x \(height)")
        return CGSize(width: width, height: height)
    }
    

    // Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.photoCollectionView.removeFromSuperview()
        self.dismissSelf()
    }
    
    @IBAction func btnDeletetouched(_ sender: AnyObject) {
        Globals.showConfirmAlertWithTitle("Delete Project", message: "Are you sure want to delete this project?\nYou can't undo this action.", viewController: self, completion: { (action) -> Void in
            ProjectHelper.deleteProjectFromCart(self.project.id)
            ProjectHelper.deleteProject(self.project.id)
            if let parent = self._parentViewController as? MyProjectViewController {
                parent.setEmpty()
                parent.loadDataProject()
            }
            self.btnBackTouched(sender)
        })
    }
    
    @IBAction func btnEditTouched(_ sender: AnyObject) {
        self.dismissSelf {
            
            //transfer data to memome helper
            
            if let cover = ProjectHelper.getCoverImage(self.project.id) {
                MemomeHelper.coverImage = NSMutableDictionary(dictionary: [
                    "title" : self.project.title,
                    "subtitle" : self.project.subtitle,
                    "url" : cover.image,
                    "size" : NSValue(cgSize: CGSize(width: CGFloat(cover.width), height: CGFloat(cover.height))),
                    "offset" : NSValue(cgPoint: CGPoint(x: CGFloat(cover.offsetX), y: CGFloat(cover.offsetY))),
                    "zoomScale" : NSNumber(value: cover.zoom as Float),
                    "rotation" : NSNumber(value: Int(cover.rotation) as Int),
                    ])
            }
            else{
                MemomeHelper.coverImage = NSMutableDictionary()
            }
            
            MemomeHelper.images = NSMutableArray()
            for item in self.projectDetails {
                if let detail = item as? ProjectDetail {
                    if detail.type != ProjectDetail.TYPE_COVER {
                        MemomeHelper.images.add(NSMutableDictionary(dictionary: [
                            "url" : detail.image,
                            "size" : NSValue(cgSize: CGSize(width: CGFloat(detail.width), height: CGFloat(detail.height))),
                            "offset" : NSValue(cgPoint: CGPoint(x: CGFloat(detail.offsetX), y: CGFloat(detail.offsetY))),
                            "zoomScale" : NSNumber(value: detail.zoom as Float),
                            "rotation" : NSNumber(value: Int(detail.rotation) as Int)
                            ]))
                    }
                }
            }
            
            let attributes = NSMutableDictionary(dictionary: [
                "product" : NSKeyedUnarchiver.unarchiveObject(with: self.project.productData as Data) as! NSDictionary,
                "productDetails" : NSKeyedUnarchiver.unarchiveObject(with: self.project.productDetailData as Data) as! NSArray,
                "project" : self.project,
                "projectDetails" : self.projectDetails
                ])
            if self.project.corporateVoucherId > 0 {
                attributes.setValue(self.project.corporateVoucherCode, forKeyPath: "voucherCode")
                attributes.setValue([
                    "id" : NSNumber(value: self.project.corporateVoucherId as Int32),
                    "qty" : NSNumber(value: self.project.corporateVoucherQty as Int32),
                    "name" : self.project.corporateVoucherName
                    ], forKey: "voucher")
            }
            
            var isTitleRequired = true
            let product = NSKeyedUnarchiver.unarchiveObject(with: self.project.productData as Data) as! NSDictionary
            
            if let isRequireTitle = product.object(forKey: "is_require_title") as? NSNumber {
                isTitleRequired = isRequireTitle.intValue == 1
            }
            else if let isRequireTitle = product.object(forKey: "is_require_title") as? NSString {
                isTitleRequired = isRequireTitle.integerValue == 1
            }
            
            if isTitleRequired {
                self.showViewController("ProductTitleViewController", attributes: attributes)
            }
            else {
                self.showViewController("ProductCoverViewController", attributes: attributes)
            }
        }
    }
    
    @IBAction func btnAddToCartTouched(_ sender: AnyObject) {
        if CartHelper.isProductExists(self.project) {
            BSNotification.show("This project is already in your cart.", view: self)
        }
        else{
            CartHelper.addToCart(self.project, qty: 1)
            BSNotification.show("This project has been added to your cart.", view: self)
        }
    }
    
    // General methods
    func setupCoverPreview(_ cell: UICollectionViewCell) {
        let cellSize = self.collectionViewCellSizeAtIndexPath(IndexPath(row: 0, section: 0))
        let coverView: UIView! = cell.viewWithTag(1)
        let coverViewBackground: UIImageView! = cell.viewWithTag(2) as! UIImageView
        let coverImageView: UIImageView! = cell.viewWithTag(3) as! UIImageView
        let coverTitleView: UIView! = cell.viewWithTag(4)
        let coverTitleInnerView: UIView! = cell.viewWithTag(5)
        let lblCoverTitle: UILabel! = cell.viewWithTag(6) as! UILabel
        let lblCoverSubtitle: UILabel! = cell.viewWithTag(7) as! UILabel
        let btnEdit: EditButton = cell.viewWithTag(99) as! EditButton
        
        btnEdit.isHidden = true
        
        let coverWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_width") as! NSString).floatValue)
        let coverHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_height") as! NSString).floatValue)
        let coverBorderTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_border_top") as! NSString).floatValue)
        let coverBorderLeft: CGFloat = CGFloat((self.productData.object(forKey: "cover_border_left") as! NSString).floatValue)
        let coverContentWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_content_width") as! NSString).floatValue)
        let coverContentHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_content_height") as! NSString).floatValue)
        let coverTitleAreaWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_width") as! NSString).floatValue)
        let coverTitleAreaHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_height") as! NSString).floatValue)
        let coverTitleAreaTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_top") as! NSString).floatValue)
        let coverTitleAreaLeft: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_left") as! NSString).floatValue)
        let coverTitleTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_top") as! NSString).floatValue)
        let coverSubtitleTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_subtitle_top") as! NSString).floatValue)
        let coverTitleFontSize: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_font_size") as! NSString).floatValue)
        let coverSubtitleFontSize: CGFloat = CGFloat((self.productData.object(forKey: "cover_subtitle_font_size") as! NSString).floatValue)
        
//        NSLog("cover width : \(coverView.frame.size.width)")
//        let ratio: CGFloat = MemomeHelper.calculateRatio(coverWidth, targetSize: coverView.frame.size.width)
//        NSLog("cover width : \(cellSize.width - 16)")
        let ratio: CGFloat = MemomeHelper.calculateRatio(coverWidth, targetSize: cellSize.width - 16)
        let coverTitleAreaBorder: CGFloat = MemomeHelper.coverTitleAreaBorder
        
        if let coverImages = self.productData.object(forKey: "cover_images") as? NSDictionary {
            if let image = coverImages.object(forKey: "thumb") as? String {
                Globals.setImageFromUrl(image, imageView: coverViewBackground, placeholderImage: nil)
            }
        }
        
        //cover size
        if let height = Globals.getConstraint("height", view: coverView) {
            height.constant = coverHeight * ratio
        }
        
        //cover content
        if let top = Globals.getConstraint("coverImageTop", view: coverView) {
            top.constant = coverBorderTop * ratio
        }
        if let left = Globals.getConstraint("coverImageLeft", view: coverView) {
            left.constant = coverBorderLeft * ratio
        }
        if let width = Globals.getConstraint("width", view: coverImageView) {
            width.constant = coverContentWidth * ratio
        }
        if let height = Globals.getConstraint("height", view: coverImageView) {
            height.constant = coverContentHeight * ratio
        }
        
        
        //title area
        if let top = Globals.getConstraint("titleViewTop", view: coverView) {
            top.constant = coverTitleAreaTop * ratio
        }
        if let left = Globals.getConstraint("titleViewLeft", view: coverView) {
            left.constant = coverTitleAreaLeft * ratio
        }
        if let width = Globals.getConstraint("width", view: coverTitleView) {
            width.constant = coverTitleAreaWidth * ratio
        }
        if let height = Globals.getConstraint("height", view: coverTitleView) {
            height.constant = coverTitleAreaHeight * ratio
        }
        
        if let top = Globals.getConstraint("titleInnerTop", view: coverTitleView) {
            top.constant = coverTitleAreaBorder * ratio
        }
        if let left = Globals.getConstraint("titleInnerLeft", view: coverTitleView) {
            left.constant = coverTitleAreaBorder * ratio
        }
        if let bottom = Globals.getConstraint("titleInnerBottom", view: coverTitleView) {
            bottom.constant = coverTitleAreaBorder * ratio
        }
        if let right = Globals.getConstraint("titleInnerRight", view: coverTitleView) {
            right.constant = coverTitleAreaBorder * ratio
        }
        
        //title & subtitle
        if let top = Globals.getConstraint("titleTop", view: coverTitleInnerView) {
            top.constant = (coverTitleTop - coverTitleAreaBorder) * ratio
        }
        if let top = Globals.getConstraint("subtitleTop", view: coverTitleInnerView) {
            top.constant = (coverSubtitleTop - coverTitleAreaBorder) * ratio
        }
        
        let coverTitleFont = lblCoverTitle.font
        lblCoverTitle.font = coverTitleFont?.withSize( coverTitleFontSize * ratio )
        let coverSubtitleFont = lblCoverSubtitle.font
        lblCoverSubtitle.font = coverSubtitleFont?.withSize( coverSubtitleFontSize * ratio )
        
        coverView.layer.borderColor = UIColor(white: 0.25, alpha: 1).cgColor
        coverView.layer.borderWidth = 1.0
        coverTitleView.layer.borderColor = UIColor(white: 0.57, alpha: 1).cgColor
        coverTitleView.layer.borderWidth = 1.0
        
        if let coverImage = self.projectDetails.object(at: 0) as? ProjectDetail {
            var x: CGFloat = 0.0
            var y: CGFloat = 0.0
            var width: CGFloat = 0.0
            var height: CGFloat = 0.0
            let scale: CGFloat = CGFloat(coverImage.zoom)
            var rotation: Int = 0
            
            width = CGFloat(coverImage.width) * scale
            height = CGFloat(coverImage.height) * scale
            
            x = CGFloat(coverImage.offsetX) * scale
            y = CGFloat(coverImage.offsetY) * scale
            
            rotation = Int(coverImage.rotation)
            
            let screenScale = max( coverImageView.frame.size.width / width, coverImageView.frame.size.height / height )
            let cropRect = CGRect(x: x * screenScale, y: y * screenScale, width: width * screenScale, height: height * screenScale)
            
            Globals.checkImageFrom(coverImage.image, scale: scale * screenScale, resultHandler: { (image) -> Void in
                if let img = image {
                    coverImageView.image = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect)
                }
            })
            
            lblCoverTitle.text = self.project.title
            lblCoverSubtitle.text = self.project.subtitle
            
        }
        
    }
    
    func setupContentPreview(_ cell: UICollectionViewCell, indexPath: IndexPath) {
        let cellSize = self.collectionViewCellSizeAtIndexPath(indexPath)
        let contentView: UIView! = cell.viewWithTag(1)
        let contentViewBackground: UIImageView! = cell.viewWithTag(2) as! UIImageView
        let contentImageView: UIImageView! = cell.viewWithTag(3) as! UIImageView
        let btnEdit: EditButton = cell.viewWithTag(99) as! EditButton
        
        btnEdit.isHidden = true
        
        let index = self.hasCover ? (indexPath as NSIndexPath).row - 1 : (indexPath as NSIndexPath).row - 1
        let projectIndex = self.hasCover ? (indexPath as NSIndexPath).row : (indexPath as NSIndexPath).row - 1
        
//        let index = indexPath.row - 1
        
        if let detail = self.productDetails.object(at: index) as? NSDictionary {
            let wrapperWidth: CGFloat = CGFloat((detail.object(forKey: "width") as! NSString).floatValue)
            let wrapperHeight: CGFloat = CGFloat((detail.object(forKey: "height") as! NSString).floatValue)
            let borderTop: CGFloat = CGFloat((detail.object(forKey: "border_top") as! NSString).floatValue)
            let borderLeft: CGFloat = CGFloat((detail.object(forKey: "border_left") as! NSString).floatValue)
            let contentWidth: CGFloat = CGFloat((detail.object(forKey: "content_width") as! NSString).floatValue)
            let contentHeight: CGFloat = CGFloat((detail.object(forKey: "content_height") as! NSString).floatValue)
            
            var parentWidth: CGFloat = cellSize.width
            //content size
            parentWidth -= 16
//            if let leading = Globals.getConstraint("leading", view: contentView) {
//                parentWidth -= leading.constant
//            }
//            
//            if let trailing = Globals.getConstraint("trailing", view: contentView) {
//                parentWidth -= trailing.constant
//            }
            
            NSLog("widht after : %f", parentWidth)
            
            let ratio: CGFloat = MemomeHelper.calculateRatio(wrapperWidth, targetSize: parentWidth)
            
            if let contentImages = detail.object(forKey: "images") as? NSDictionary {
                if let image = contentImages.object(forKey: "thumb") as? String {
                    Globals.setImageFromUrl(image, imageView: contentViewBackground, placeholderImage: nil)
                }
            }
            
            //content size
            if let height = Globals.getConstraint("height", view: contentView) {
                height.constant = wrapperHeight * ratio
            }
            
            //content content
            
            if let top = Globals.getConstraint("contentImageTop", view: contentView) {
                top.constant = borderTop * ratio
            }
            if let left = Globals.getConstraint("contentImageLeft", view: contentView) {
                left.constant = borderLeft * ratio
            }
            if let width = Globals.getConstraint("width", view: contentImageView) {
                width.constant = contentWidth * ratio
            }
            if let height = Globals.getConstraint("height", view: contentImageView) {
                height.constant = contentHeight * ratio
            }
            
            contentView.layer.borderColor = UIColor(white: 0.25, alpha: 1).cgColor
            contentView.layer.borderWidth = 1.0
            
            if self.projectDetails.count > projectIndex {
                if let image = self.projectDetails.object(at: projectIndex) as? ProjectDetail {
                    var x: CGFloat = 0.0
                    var y: CGFloat = 0.0
                    var width: CGFloat = 0.0
                    var height: CGFloat = 0.0
                    let scale: CGFloat = CGFloat(image.zoom)
                    var rotation: Int = 0
                    
                    width = CGFloat(image.width) * scale
                    height = CGFloat(image.height) * scale
                    
                    x = CGFloat(image.offsetX) * scale
                    y = CGFloat(image.offsetY) * scale
                    
                    rotation = Int(image.rotation)
                    
                    let screenScale = max( contentImageView.frame.size.width / width, contentImageView.frame.size.height / height )
                    let cropRect = CGRect(x: x * screenScale, y: y * screenScale, width: width * screenScale, height: height * screenScale)
                    
                    Globals.checkImageFrom(image.image, scale: scale * screenScale, resultHandler: { (image) -> Void in
                        if let img = image {
                            autoreleasepool(invoking: { () -> () in
                                contentImageView.image = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect)
                            })
                        }
                    })
                }
            }
        }
        
    }
    
//    func setupContentPreview(_ cell: UICollectionViewCell, indexPath: IndexPath) {
//        let contentView: UIView! = cell.viewWithTag(1)
//        let contentViewBackground: UIImageView! = cell.viewWithTag(2) as! UIImageView
//        let contentImageView: UIImageView! = cell.viewWithTag(3) as! UIImageView
////        let btnEdit: EditButton = cell.viewWithTag(99) as! EditButton
//        
//        if let detail = self.productDetails.object(at: (indexPath as NSIndexPath).row - 1) as? NSDictionary {
//            let wrapperWidth: CGFloat = CGFloat((detail.object(forKey: "width") as! NSString).floatValue)
//            let wrapperHeight: CGFloat = CGFloat((detail.object(forKey: "height") as! NSString).floatValue)
//            let borderTop: CGFloat = CGFloat((detail.object(forKey: "border_top") as! NSString).floatValue)
//            let borderLeft: CGFloat = CGFloat((detail.object(forKey: "border_left") as! NSString).floatValue)
//            let contentWidth: CGFloat = CGFloat((detail.object(forKey: "content_width") as! NSString).floatValue)
//            let contentHeight: CGFloat = CGFloat((detail.object(forKey: "content_height") as! NSString).floatValue)
//            
//            let ratio: CGFloat = MemomeHelper.calculateRatio(wrapperWidth, targetSize: contentView.frame.size.width)
//            
//            if let contentImages = detail.object(forKey: "images") as? NSDictionary {
//                if let image = contentImages.object(forKey: "thumb") as? String {
//                    Globals.setImageFromUrl(image, imageView: contentViewBackground, placeholderImage: nil)
//                }
//            }
//            
//            //content size
//            if let height = Globals.getConstraint("height", view: contentView) {
//                height.constant = wrapperHeight * ratio
//            }
//            
//            //content content
//            if let top = Globals.getConstraint("contentImageTop", view: contentView) {
//                top.constant = borderTop * ratio
//            }
//            if let left = Globals.getConstraint("contentImageLeft", view: contentView) {
//                left.constant = borderLeft * ratio
//            }
//            if let width = Globals.getConstraint("width", view: contentImageView) {
//                width.constant = contentWidth * ratio
//            }
//            if let height = Globals.getConstraint("height", view: contentImageView) {
//                height.constant = contentHeight * ratio
//            }
//            
//            contentView.layer.borderColor = UIColor(white: 0.25, alpha: 1).cgColor
//            contentView.layer.borderWidth = 1.0
//            
//            if let image = self.projectDetails.object(at: projectIndex) as? ProjectDetail {
////                if let url = image.object(forKey: "url") as? String {
//                    var x: CGFloat = 0.0
//                    var y: CGFloat = 0.0
//                    var width: CGFloat = 0.0
//                    var height: CGFloat = 0.0
//                    var scale: CGFloat = 1
//                    var rotation: Int = 0
//                    if let zoomScale = image.object(forKey: "zoomScale") as? NSNumber {
//                        scale = CGFloat(zoomScale.floatValue)
//                    }
//                    if let size = image.object(forKey: "size") as? NSValue {
//                        width = size.cgSizeValue.width * scale
//                        height = size.cgSizeValue.height * scale
//                    }
//                    if let offset = image.object(forKey: "offset") as? NSValue {
//                        x = offset.cgPointValue.x * scale
//                        y = offset.cgPointValue.y * scale
//                    }
//                    if let rotate = image.object(forKey: "rotation") as? NSNumber {
//                        rotation = rotate.intValue
//                    }
//                    let screenScale = max( contentImageView.frame.size.width / width, contentImageView.frame.size.height / height )
//                    let cropRect = CGRect(x: x * screenScale, y: y * screenScale, width: width * screenScale, height: height * screenScale)
//                    
//                    Globals.imageFromAssetLocalIdentifier(image.image, scale: scale * screenScale, resultHandler: { (image) -> Void in
//                        if let img = image {
//                            autoreleasepool(invoking: { () -> () in
//                                contentImageView.image = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect)
//                            })
//                        }
//                    })
////                }
//            }
//        }
//        
////        btnEdit.index = (indexPath as NSIndexPath).row
////        btnEdit.addTarget(self, action: #selector(ProductPhotosViewController.btnEditTouched(_:)), for: UIControlEvents.touchUpInside)
//        
//    }
}
