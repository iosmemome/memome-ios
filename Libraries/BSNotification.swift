//
//  BSNotification.swift
//
//  Created by Bobby Stenly (iceman.bsi@gmail.com) on 6/27/15.
//  Copyright (c) 2015 Bobby Stenly Irawan. All rights reserved.
//
//
//  This Class is used to show a message for 5 seconds only
//
//  How To Use : 
//  - Include this class in your swift project.
//  - BSNotification.show("Hello World!", view: self)
//  
//  For more info / documentation please visit http://bobbystenly.com/ios

import UIKit

class BSNotification: NSObject {
    
    enum BSNotificationPosition {
        case top;
        case bottom;
    }
    
    static func show(_ message: String, view: UIViewController) {
        BSNotification.show(message, view: view, position: BSNotificationPosition.bottom)
    }
    
    static func show(_ message: String, view: UIViewController, position: BSNotificationPosition) {
        //create view
        var posY: CGFloat = 0
        if position == BSNotificationPosition.top {
            posY = 50
        }
        else {
            posY = view.view.frame.size.height - 50
        }
        var notificationFrame: CGRect = CGRect(x: 25, y: posY, width: view.view.frame.size.width - 50, height: 36)
        let notificationView: UIView = UIView(frame: notificationFrame)
//        notificationView.setTranslatesAutoresizingMaskIntoConstraints(false)
        notificationView.clipsToBounds = true
        notificationView.layer.cornerRadius = 10
        notificationView.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        view.view.addSubview(notificationView)
        
        let messageWidth: CGFloat = notificationView.frame.size.width - 16
        var messageFrame: CGRect = CGRect(x: 8, y: 8, width: messageWidth, height: 20)
        let messageLabel: UILabel = UILabel(frame: messageFrame)
//        messageLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        messageLabel.text = message
        messageLabel.textAlignment = NSTextAlignment.center
        messageLabel.textColor = UIColor(white: 1, alpha: 1)
        messageLabel.font = UIFont(name: messageLabel.font.fontName, size: 12)
        messageFrame.size.height = messageLabel.sizeThatFits(CGSize(width: messageWidth, height: 999999)).height
        messageLabel.frame = messageFrame
        
        notificationView.addSubview(messageLabel)
        
        if position == BSNotificationPosition.bottom {
            notificationFrame.size.height = messageFrame.size.height + 16
            posY -= notificationFrame.size.height
            notificationFrame.origin.y = posY
            notificationView.frame = notificationFrame
        }
        
//        notificationView.addConstraint(NSLayoutConstraint(
//            item: messageLabel,
//            attribute: NSLayoutAttribute.Leading,
//            relatedBy: NSLayoutRelation.Equal,
//            toItem: notificationView,
//            attribute: NSLayoutAttribute.Leading,
//            multiplier: 1,
//            constant: 8))
//        
//        notificationView.addConstraint(NSLayoutConstraint(
//            item: messageLabel,
//            attribute: NSLayoutAttribute.Top,
//            relatedBy: NSLayoutRelation.Equal,
//            toItem: notificationView,
//            attribute: NSLayoutAttribute.Top,
//            multiplier: 1,
//            constant: 8))
//        
//        notificationView.addConstraint(NSLayoutConstraint(
//            item: messageLabel,
//            attribute: NSLayoutAttribute.Trailing,
//            relatedBy: NSLayoutRelation.Equal,
//            toItem: notificationView,
//            attribute: NSLayoutAttribute.Trailing,
//            multiplier: 1,
//            constant: 8))
//        
//        notificationView.addConstraint(NSLayoutConstraint(
//            item: messageLabel,
//            attribute: NSLayoutAttribute.Bottom,
//            relatedBy: NSLayoutRelation.Equal,
//            toItem: notificationView,
//            attribute: NSLayoutAttribute.Bottom,
//            multiplier: 1,
//            constant: 8))
//        
//        notificationView.addConstraint(NSLayoutConstraint(
//            item: messageLabel,
//            attribute: NSLayoutAttribute.Height,
//            relatedBy: NSLayoutRelation.GreaterThanOrEqual,
//            toItem: nil,
//            attribute: NSLayoutAttribute.NotAnAttribute,
//            multiplier: 1,
//            constant: 20))
//        
//        view.view.addConstraint(NSLayoutConstraint(
//            item: notificationView,
//            attribute: NSLayoutAttribute.Leading,
//            relatedBy: NSLayoutRelation.Equal,
//            toItem: view.view,
//            attribute: NSLayoutAttribute.Leading,
//            multiplier: 1,
//            constant: 25
//        ))
//        
//        view.view.addConstraint(NSLayoutConstraint(
//            item: notificationView,
//            attribute: NSLayoutAttribute.Trailing,
//            relatedBy: NSLayoutRelation.Equal,
//            toItem: view.view,
//            attribute: NSLayoutAttribute.Trailing,
//            multiplier: 1,
//            constant: 25
//            ))
////        
////        view.view.addConstraint(NSLayoutConstraint(
////            item: notificationView,
////            attribute: NSLayoutAttribute.Height,
////            relatedBy: NSLayoutRelation.Equal,
////            toItem: nil,
////            attribute: NSLayoutAttribute.NotAnAttribute,
////            multiplier: 1,
////            constant: 36
////            ))
////        view.view.addConstraint(NSLayoutConstraint(
////            item: notificationView,
////            attribute: NSLayoutAttribute.Width,
////            relatedBy: NSLayoutRelation.Equal,
////            toItem: nil,
////            attribute: NSLayoutAttribute.NotAnAttribute,
////            multiplier: 1,
////            constant: 36
////            ))
//        
//        if position == BSNotificationPosition.Top {
//            view.view.addConstraint(NSLayoutConstraint(
//                item: notificationView,
//                attribute: NSLayoutAttribute.Top,
//                relatedBy: NSLayoutRelation.Equal,
//                toItem: view.view,
//                attribute: NSLayoutAttribute.Top,
//                multiplier: 1,
//                constant: 50
//                ))
//        }
//        else{
//            view.view.addConstraint(NSLayoutConstraint(
//                item: notificationView,
//                attribute: NSLayoutAttribute.Bottom,
//                relatedBy: NSLayoutRelation.Equal,
//                toItem: view.view,
//                attribute: NSLayoutAttribute.Bottom,
//                multiplier: 1,
//                constant: 50
//                ))
//        }
//        notificationView.frame = CGRectMake(0, 0, 270, 30)
        
//        NSLog("Notification label frame : %f, %f, %f, %f", messageLabel.frame.origin.x, messageLabel.frame.origin.y, messageLabel.frame.size.width, messageLabel.frame.size.height)
//        NSLog("Notification view frame : %f, %f, %f, %f", notificationView.frame.origin.x, notificationView.frame.origin.y, notificationView.frame.size.width, notificationView.frame.size.height)
//        NSLog("Message : %@", messageLabel.text!)
        notificationView.alpha = 0
        view.view.bringSubview(toFront: notificationView)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        notificationView.alpha = 1
        UIView.commitAnimations()
        
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(BSNotification.hide(_:)), userInfo: notificationView, repeats: false)
    }
    
    static func hide(_ timer: Timer) {
        if let BSNotificationView: UIView = timer.userInfo as? UIView {
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            BSNotificationView.alpha = 0
            UIView.commitAnimations()
            Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(BSNotification.remove(_:)), userInfo: BSNotificationView, repeats: false)
        }
    }
    
    static func remove(_ timer: Timer) {
        if let BSNotificationView: UIView = timer.userInfo as? UIView {
            BSNotificationView.removeFromSuperview()
        }
    }
}
