//
//  LoginViewController.swift
//  Memome
//
//  Created by staff on 12/18/15.
//  Copyright © 2015 Trio Digital Agency. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn

class LoginViewController: BaseViewController, GIDSignInUIDelegate {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var loginScrollView: UIScrollView!
    @IBOutlet weak var loginContentScrollView: UIView!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLoginViaGoogle: UIButton!
    @IBOutlet weak var btnLoginViaFacebook: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    
    let fbReadPermission = ["public_profile", "email", "user_photos"]
    
    var onSuccess : ( ()->Void )?
    var onCancel : ( ()->Void )?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.btnLoginViaGoogle.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.btnLoginViaFacebook.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.btnLoginViaGoogle.layer.cornerRadius = 5
        self.btnLoginViaFacebook.layer.cornerRadius = 5
        self.emailView.layer.cornerRadius = 5
        self.passwordView.layer.cornerRadius = 5
        self.btnLogin.layer.cornerRadius = 5
        
        self.defaultScrollView = self.loginScrollView
        self.defaultContentScrollView = self.loginContentScrollView
        
        
        GIDSignIn.sharedInstance().uiDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Text Fields Delegate
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtEmail{
            self.txtPassword.becomeFirstResponder()
        }
        else if textField == self.txtPassword{
            self.btnLoginTouched(self.btnLogin)
        }
        
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //google sign in delegate
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    @nonobjc func sign(inWillDispatch signIn: GIDSignIn!, error: NSError!) {
//        myActivityIndicator.stopAnimating()
//        self.hideLoading()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        print("login google from login view")
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true) { () -> Void in
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                if let user = appDelegate.googleUser {
                    self.googleLogin(user)
                }
                else{
                    appDelegate.pendingGoogleLogin = true
                    appDelegate.loginViewController = self
                }
            }
        }
    }
    
    
    // actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        if !self.isLoading() {
            self.dismissSelf()
        }
    }
    
    @IBAction func btnLoginTouched(_ sender: AnyObject) {
        if !self.isLoading() {
            self.showLoading()
            
            let params:  NSDictionary! = NSMutableDictionary(dictionary: [
                "clientId" : Globals.getProperty("clientId"),
                "email" : self.txtEmail.text!,
                "password" : self.txtPassword.text!,
                "deviceId" : Globals.getDataSetting("deviceId") as! String
                ])
            
            #if DEBUG
                params.setValue("1", forKey: "isSandbox")
            #endif
            
            Globals.getDataFromUrl(Globals.getApiUrl("users/login"),
                requestType: Globals.HTTPRequestType.http_POST,
                params: params) { (result) -> Void in
                    self.hideLoading()
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        Globals.setDataSetting("signInAgent", value: "default")
                        self.afterLogin(result)
                    }
                    else{
                        Globals.showAlertWithTitle("Login Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            }
        }
    }
    
    @IBAction func btnForgotPasswordTouched(_ sender: AnyObject) {
        self.showViewController("ForgetPasswordViewController", attributes: nil)
    }
    
    @IBAction func btnLoginViaGoogleTouched(_ sender: AnyObject) {
        print("google login begin")
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.pendingGoogleLogin = true
            appDelegate.loginViewController = self
        }
        GIDSignIn.sharedInstance().signIn()
        
    }
    
    @IBAction func btnLoginViaFacebookTouched(_ sender: AnyObject) {
        let login = FBSDKLoginManager()
        login.logOut()
//        login.loginBehavior = FBSDKLoginBehavior.web
        login.logIn(withReadPermissions: self.fbReadPermission,
            from: self) { (result, error) -> Void in
                if (error != nil){
                    print("process error : \(error?.localizedDescription)")
                    Globals.showAlertWithTitle("Login Error", message: "There is an error occurred while logging in with your Facebook account", viewController: self)
                }
                else if (result?.isCancelled)! {
                    print("cancelled")
                    
                }
                else {
                    //                    NSLog("Logged in")
                    //                    NSLog("result : %@", result.token)
                    if (FBSDKAccessToken.current() != nil) {
                        self.showLoading()
                        FBSDKGraphRequest(graphPath: "me", parameters: [
                            "fields" : "id,name,email,first_name,last_name"
                            ]).start(completionHandler: { (connection, result, error) -> Void in
                                //                                NSLog("User : %@", res as! NSDictionary)
                                self.facebookLogin(result as! NSDictionary)
                            })
                    }
                    
                }
        }
    }
    
    @IBAction func btnRegisterTouched(_ sender: AnyObject) {
        self.showViewController("RegisterViewController", attributes: nil)
    }
    
    func facebookLogin(_ result:NSDictionary!){
        
        let params:  NSDictionary! = NSMutableDictionary(dictionary: [
            "clientId" : Globals.getProperty("clientId"),
            "facebookId" : Chiper.encrypt(result.object(forKey: "id") as! String, key: result.object(forKey: "email") as! String),
            "email" : result.object(forKey: "email") as! String,
            "deviceId" : Globals.getDataSetting("deviceId") as! String
            ])
        
        var nameString = ""
        if let firstName = result.object(forKey: "first_name") as? String{
            nameString = firstName
        }
        else if let name = result.object(forKey: "name") as? String{
            nameString =  name
        }
        
        if let lastName = result.object(forKey: "last_name") as? String{
            nameString = nameString.appendingFormat(" %@", lastName)
        }
        
        params.setValue(nameString, forKey: "name")
        
        if let birthday = result.object(forKey: "birthday") as? String {
            params.setValue(birthday, forKey: "birthdate")
        }
        
        #if DEBUG
            params.setValue("1", forKey: "isSandbox")
            NSLog("debug params : %@", params)
        #endif
        
        Globals.getDataFromUrl(Globals.getApiUrl("users/facebookLogin"),
            requestType: Globals.HTTPRequestType.http_POST,
            params: params) { (result) -> Void in
                self.hideLoading()
                FBSDKLoginManager().logOut()
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    Globals.setDataSetting("signInAgent", value: "facebook")
                    self.afterLogin(result)
                }
                else{
                    Globals.showAlertWithTitle("Login Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
        }
        
    }
    
    func googleLogin(_ user: GIDGoogleUser!) {
        self.showLoading()
        
        let userId = user.userID                  // For client-side use only!
        _ = user.authentication.idToken // Safe to send to the server
        let name = user.profile.name
        let email = user.profile.email
        
        let params:  NSDictionary! = NSMutableDictionary(dictionary: [
            "clientId" : Globals.getProperty("clientId"),
            "googleId" : Chiper.encrypt(userId!, key: email!),
            "email" : email ?? "",
            "deviceId" : Globals.getDataSetting("deviceId") as! String
            ])
        
        params.setValue(name, forKey: "name")
        
        #if DEBUG
            params.setValue("1", forKey: "isSandbox")
            NSLog("debug params : %@", params)
        #endif
        
        Globals.getDataFromUrl(Globals.getApiUrl("users/googleLogin"),
            requestType: Globals.HTTPRequestType.http_POST,
            params: params) { (result) -> Void in
                self.hideLoading()
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    Globals.setDataSetting("signInAgent", value: "google")
                    self.afterLogin(result)
                }
                else{
                    Globals.showAlertWithTitle("Login Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
        }
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.pendingGoogleLogin = false
            appDelegate.loginViewController = nil
        }
    }
    
    func afterLogin(_ result: NSDictionary){
        let user : NSDictionary! = result.object(forKey: "user") as? NSDictionary
        // store access token & user id
        Globals.setDataSetting("userId", value: user.object(forKey: "id") as! String)
        Globals.setDataSetting("userName", value: user.object(forKey: "name") as! String)
        Globals.setDataSetting("userEmail", value: user.object(forKey: "email") as! String)
        Globals.setDataSetting("accessToken",  value: result.object(forKey: "accessToken") as! String)
        
        if let images = user.object(forKey: "images") as? NSDictionary {
            if let image = images.object(forKey: "original") as? String {
                Globals.setDataSetting("userProfilePicture", value: image)
            }
        }
        
        //        self.dismissSelf()
        
//        let navController : UINavigationController = self.revealViewController().frontViewController as! UINavigationController
////        navController.pushViewController(ControllerHelper.getHomeViewController()!, animated: true)
//        navController.setViewControllers([ControllerHelper.getHomeViewController()!], animated: true)
//        ControllerHelper.setActiveViewController(ControllerHelper.getHomeViewController()!)
//        self.revealViewController().setFrontViewPosition(FrontViewPosition.Left, animated: true)
        
        if self.onSuccess != nil {
            DispatchQueue.main.async(execute: {
                self.onSuccess!()
            })
        }
        
        self.showHomeViewController()
    }
}
