//
//  Globals.swift
//  Kreate
//
//  Created by Bobby on 5/11/15.
//  Copyright (c) 2015 Trio Digital Agency. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import AssetsLibrary
import Photos
import Alamofire
import GoogleSignIn

class Globals: NSObject {
   
    static var error: String!
    static var format: PropertyListSerialization.PropertyListFormat!
    static var rootPath: String!
    static var plistPath: String!
    static var plistData: NSDictionary!
    
    enum HTTPRequestType {
        case http_POST
        case http_GET
    }
    
    static var assetsLibrary: ALAssetsLibrary! = ALAssetsLibrary()
    
    static var imageOptions: PHImageRequestOptions! = PHImageRequestOptions()
    static var highQualityImageOptions: PHImageRequestOptions! = PHImageRequestOptions()
    
    override static func initialize() {
        rootPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] 
        plistPath = rootPath + "Properties.plist"
        if(!FileManager.default.fileExists(atPath: plistPath)){
            plistPath = Bundle.main.path(forResource: "Properties", ofType: "plist")
        }
        plistData = NSDictionary(contentsOfFile: plistPath)
        
        imageOptions.resizeMode = .exact
        
        highQualityImageOptions.resizeMode = .exact
        highQualityImageOptions.deliveryMode = .highQualityFormat
    }
    
    static func getManagedObjectContext() -> NSManagedObjectContext {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.managedObjectContext
//        let context: NSManagedObjectContext!// = appDelegate.managedObjectContext
//        if let ctx = appDelegate.managedObjectContext {
//            context = ctx
//        }
//        else {
//            context = NSManagedObjectContext()
//            context.persistentStoreCoordinator = appDelegate.persistentStoreCoordinator
//        }
//        
//        return context
    }
    
    static func getDataFromUrl(_ rawUrl: String, requestType: HTTPRequestType, params: NSDictionary, completion:((_ result:NSDictionary) -> Void)?) {
        Globals.getDataFromUrl(rawUrl, noCache: false, requestType: requestType, params: params, completion: completion)
    }
    
    static func getDataFromUrl(_ rawUrl: String, noCache: Bool, requestType: HTTPRequestType, params: NSDictionary, completion:((_ result:NSDictionary) -> Void)?) {
        var stringData: String = ""
        var first = true
        var string: String
        var url: String = rawUrl
        
        for (key, value) in params {
            if first {
                first = false
            }
            else {
                stringData += "&"
            }
            
            string = value as! String
            string = string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            //string = string.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
            stringData = stringData.appendingFormat("%@=%@", key as! String, string)
        }
        
        if requestType == HTTPRequestType.http_GET {
            url += "?"
            url = url + stringData
        }
        
//        let request:NSMutableURLRequest = NSMutableURLRequest(url: URL(string: url)!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 60.0)
        var request = URLRequest(url: URL(string: url)!)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.timeoutInterval = 60.0
        request.httpMethod = requestType == HTTPRequestType.http_POST ? "POST" : "GET"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
        
        if(requestType == HTTPRequestType.http_POST){
            let requestBodyData: Data = stringData.data(using: String.Encoding.utf8)!
            request.httpBody = requestBodyData
        }
        
        
        if Globals.hasInternetConnection() {
            
            var loadFromInternet : Bool = true
            if !noCache {
                if let data: Data = Globals.getCache(url) {
                    //load from cache
                    let error: NSErrorPointer! = nil
                    var result: NSDictionary! = NSDictionary()
//                    NSLog("Data  : %@", NSString(data: data, encoding: NSUTF8StringEncoding)!)
                    do {
                        try result = JSONSerialization.jsonObject(with: data, options: [JSONSerialization.ReadingOptions.allowFragments, JSONSerialization.ReadingOptions.mutableContainers]) as? NSDictionary
                    } catch let error1 as NSError {
                        NSLog("json serialization error - 3 : %@", error1.debugDescription)
                    }
                    
                    if result == nil || result.count == 0 {
                        if error != nil {
                            print("Cannot get data from Cache Url (\(url)): \(error)")
                        }
//                        result = [
//                            "message" : [
//                                "code" : 901,
//                                "content" : "Cannot get data. Please check your internet connection"
//                            ]
//                        ]
                        
                        #if DEBUG
                            NSLog("Data - 3 : %@", NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
                        #endif
                    }
                    else{
                        loadFromInternet = false
                        
                        DispatchQueue.main.async(execute: {
                            completion!(result)
                        })
                    }
                }
            }
            
            if loadFromInternet {
                Alamofire.request(request)
                    .responseJSON{ response in
                        
                        let error: NSErrorPointer! = nil
                        var result: NSDictionary! = NSDictionary()
                        switch response.result {
                            
                        case .success:
                            if let value = response.data {
                                do {
                                    
                                    try result = JSONSerialization.jsonObject(with: value, options: [JSONSerialization.ReadingOptions.allowFragments, JSONSerialization.ReadingOptions.mutableContainers]) as? NSDictionary
                                    
                                } catch let error1 as NSError {
                                    NSLog("json serialization error - 2 : %@", error1.debugDescription)
                                }
                                #if DEBUG
                                    NSLog("Data - 2 : %@", NSString(data: value, encoding: String.Encoding.utf8.rawValue)!)
                                #endif
                            }
                        case .failure:
                            #if DEBUG
                                NSLog("Data - 1 : nil %@", url)
                            #endif
                        }
                        
                        if result == nil || result.count == 0 {
                            if error != nil {
                                print("Cannot get data from Url (\(url)): \(error)")
                            }
                            result = [
                                "message" : [
                                    "code" : 901,
                                    "content" : "Cannot get data. Please check your internet connection"
                                ]
                            ]
                        }
                        else{
                            //store to cache if http get
                            if requestType == HTTPRequestType.http_GET {
                                if let data = response.data {
                                    Globals.setCache(url, value: data)
                                }
                            }
                        }
                        DispatchQueue.main.async(execute: {
                            completion!(result)
                        })
                            
                }
            }
        }
        else {
            
            //check cache
            var result: NSDictionary! = NSDictionary()
            if let data: Data = Globals.getCache(url, force: true) {
                //load from cache
                let error: NSErrorPointer! = nil
                //NSLog("Data  : %@", NSString(data: data, encoding: NSUTF8StringEncoding)!)
                do {
                    try result = JSONSerialization.jsonObject(with: data, options: [JSONSerialization.ReadingOptions.allowFragments, JSONSerialization.ReadingOptions.mutableContainers]) as? NSDictionary
                } catch {
                    
                }
                
                if result == nil || result.count == 0 {
                    if error != nil {
                        print("Cannot get data from Url (\(url)): \(error)")
                    }
                    result = [
                        "message" : [
                            "code" : 901,
                            "content" : "Cannot get data. Please check your internet connection"
                        ]
                    ]
                    #if DEBUG
                        NSLog("Data  : %@", NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
                    #endif
                }
            }
            else{
                result = [
                    "message" : [
                        "code" : 900,
                        "content" : "No Internet Connection"
                    ]
                ]
            }
            DispatchQueue.main.async(execute: {
                completion!(result)
            })
            
        }
    }
    
    static func getMultipartDataFromUrl(_ rawUrl:String, params: NSDictionary, images: NSDictionary, completion:((_ result:NSDictionary) -> Void)?) {
        // create request
        
        let request:NSMutableURLRequest = NSMutableURLRequest()
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60.0
        request.httpMethod = "POST"
        
        let BoundaryConstant = "----------V2ymHFg03ehbqgZCaKO6jy";
        
        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=" + BoundaryConstant
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        let body:NSMutableData = NSMutableData()
        
        // add params (all params are strings)
        for (key, value) in params {
            body.append("--".appendingFormat("%@\r\n", BoundaryConstant).data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=".appendingFormat("\"%@\"\r\n\r\n", key as! String).data(using: String.Encoding.utf8)!)
            body.append("".appendingFormat("%@\r\n", (value as AnyObject).stringValue).data(using: String.Encoding.utf8)!)
        }
        
        // add image data
        for (key, image) in images {
            if let imageData:Data = UIImageJPEGRepresentation(image as! UIImage, 1) {
                body.append("--".appendingFormat("%@\r\n", BoundaryConstant).data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=".appendingFormat("\"%@\"; filename=\"%@.jpg\"\r\n", key as! String, key as! String).data(using: String.Encoding.utf8)!)
                body.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        body.append("--".appendingFormat("%@--\r\n", BoundaryConstant).data(using: String.Encoding.utf8)!)
        
        // setting the body of the post to the reqeust
        request.httpBody = body as Data;
        
        // set the content-length
        let postLength = "".appendingFormat("%lu", body.length)
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = URL(string: rawUrl)
        var result: NSDictionary! = NSDictionary()
        if Globals.hasInternetConnection() {
            _ = NSURLConnection()
            let queue = OperationQueue.main
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: queue, completionHandler: { (response, data, connectionError) -> Void in
                let error: NSErrorPointer! = nil
                if data != nil {
                    //NSLog("Data  : %@", NSString(data: data, encoding: NSUTF8StringEncoding)!)
                    do {
                        try result = JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.allowFragments, JSONSerialization.ReadingOptions.mutableContainers]) as? NSDictionary
                    } catch {
                        
                    }
                }
                
                if result == nil || result.count == 0 {
                    if error != nil {
                        print("Cannot get data from Url (\(rawUrl)): \(error)")
                    }
                    result = [
                        "message" : [
                            "code" : 901,
                            "content" : "Cannot get data. Please check your internet connection"
                        ]
                    ]
                    #if DEBUG
                        NSLog("Data  : %@", NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                    #endif
                }
                DispatchQueue.main.async(execute: {
                    completion!(result)
                })
                
            })
        }
        else{
            result = [
                "message" : [
                    "code" : 900,
                    "content" : "No Internet Connection"
                ]
            ]
            DispatchQueue.main.async(execute: {
                completion!(result)
            })
        }
    }
    
    static func showAlertWithTitle(_ title: String, message: String, viewController: UIViewController) {
        Globals.showAlertWithTitle(title, message: message, viewController: viewController, completion: nil)
    }
    
    static func showAlertWithTitle(_ title: String, message: String, viewController: UIViewController, completion:((_ action:UIAlertAction?) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: completion))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    
    static func showConfirmAlertWithTitle(_ title: String, message: String, viewController: UIViewController, completion:((_ action:UIAlertAction?) -> Void)?) {
//        var alert:UIAlertView = UIAlertView(title: title, message: message, delegate: delegate, cancelButtonTitle: "NO", otherButtonTitles: "YES", nil)
//        alert.show()
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: completion))
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    static func showErrorEmptyFromUrl(_ viewController: UIViewController) {
        Globals.showAlertWithTitle("Something went wrong", message: "It seems you have a problem with your internet connection.\nPlease check it and try again later.", viewController: viewController)
    }
    
    static func currencyFormat(_ number: NSNumber) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        numberFormatter.currencySymbol = "Rp "
        numberFormatter.currencyGroupingSeparator = "."
        numberFormatter.currencyDecimalSeparator = ","
        return numberFormatter.string(from: number)!
    }
    
    static func shortNumberFormat(_ number: NSNumber) -> String {
        var units = ["", "K", "M", "B"]
        var iUnit = 0
        var intNumber = number.intValue
        var back = 0
        
        while (intNumber > 1000 && iUnit < 3) {
            back = ((intNumber % 1000) / 100)
            intNumber = intNumber / 1000
            iUnit += 1
        }
        
        if back == 0{
            return String(format: "%lli%@", intNumber, units[iUnit])
        }
        else {
            return String(format: "%lli.%i%@", intNumber, back, units[iUnit])
        }
    }
    
    static func numberFormat(_ number: NSNumber) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.groupingSeparator = "."
        numberFormatter.decimalSeparator = ","
        numberFormatter.generatesDecimalNumbers = false
        return numberFormatter.string(from: number)!
    }
    
    static func numberFormat(_ number: NSNumber, decimal: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.groupingSeparator = "."
        numberFormatter.decimalSeparator = ","
        numberFormatter.generatesDecimalNumbers = true
        return numberFormatter.string(from: number)!
    }
    
    static func timeAgo(_ dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = Date()
        let date = dateFormatter.date(from: dateString)
        var diff = now.timeIntervalSince(date!)
        var n = 0.0;
        
        //seconds
        n = fmod(diff, 60)
        diff = floor(diff/60)
        if diff == 0 {
            return String(format: "%0.f seconds ago", n)
        }
        
        //minutes
        n = fmod(diff, 60)
        diff = floor(diff / 60)
        if diff == 0 {
            return String(format: "%0.f minutes ago", n)
        }
        
        //hours
        n = fmod(diff, 24)
        diff = floor(diff / 24)
        if diff == 0 {
            return String(format: "%0.f hours ago", n)
        }
        else if diff == 1 {
            return "yesterday"
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: date!)
    }
    
    static func dateFormatToDate(_ dateString: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: dateString) ?? Date()
    }
    
    static func dateFormat(_ dateString: String) -> String{
        return dateFormat(dateString, format: "dd M yyyy HH:mm")
    }
    
    static func dateFormat(_ dateString: String, format: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date ?? Date())
    }
    
    static func timeFromNow(_ dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = Date()
        let date = dateFormatter.date(from: dateString)
        var diff = date!.timeIntervalSince(now)
        var n = 0.0;
        
        //seconds
        n = fmod(diff, 60)
        diff = floor(diff/60)
        if diff == 0 {
            return String(format: "%0.f seconds from now", n)
        }
        
        //minutes
        n = fmod(diff, 60)
        diff = floor(diff / 60)
        if diff == 0 {
            return String(format: "%0.f minutes from now", n)
        }
        
        //hours
        n = fmod(diff, 24)
        diff = floor(diff / 24)
        if diff == 0 {
            return String(format: "%0.f hours from now", n)
        }
        else if diff == 1 {
            return "tomorrow"
        }
        
        //days
        n = fmod(diff, 30)
        diff = floor(diff / 30)
        if diff == 0 {
            return String(format: "%0.f days from now", n)
        }
        
        //months
        n = fmod(diff, 12)
        diff = floor(diff / 12)
        if diff == 0 {
            return String(format: "%0.f months from now", n)
        }
        else {
            return String(format: "%0.f years from now", diff)
        }
    }
    
    static func getApiUrl(_ api: String) -> String {
        return String(format: "%@%@", Constant.getApiBaseUrl(), api)
    }
    
    static func getProperty(_ key: String) -> String {
        return plistData[key] as! String
    }
    
    static func getNumberProperty(_ key: String) -> NSNumber{
        return plistData[key] as! NSNumber
    }
    
    static func setProperty(_ key: String, value: AnyObject) {
        plistData.setValue(value, forKey: key)
        let errorPointer: NSErrorPointer? = nil
        var tempPlistData: Data?
        do {
            tempPlistData = try PropertyListSerialization.data(fromPropertyList: plistData, format: PropertyListSerialization.PropertyListFormat.xml, options: 0)
        } catch let error as NSError {
            errorPointer??.pointee = error
            tempPlistData = nil
        }
        if tempPlistData != nil {
            try? tempPlistData!.write(to: URL(fileURLWithPath: plistPath), options: [.atomic])
        }
        else {
            print(error)
        }
    }
    
    static func getSetting(_ key: String) -> AnyObject? {
//        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = Globals.getManagedObjectContext()// appDelegate.managedObjectContext
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Setting", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(name = %@)", key)
        request.predicate = pred
        let error: NSErrorPointer? = nil
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        
        if objects.count == 0 {
            return nil
        }
        else {
            return objects[0]
        }
    }
    
    static func getDataSetting(_ key: String) -> AnyObject? {
        let setting = Globals.getSetting(key) as? Setting
        if setting == nil {
            return nil
        }
        else {
            return setting!.value as AnyObject?
        }
    }
    
    static func setDataSetting(_ key: String, value: String) {
//        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = Globals.getManagedObjectContext() //appDelegate.managedObjectContext
        let setting: Setting? = Globals.getSetting(key) as? Setting
        if setting == nil {
            print("Setting not Found")
        }
        else {
            setting!.value = value
            let error: NSErrorPointer? = nil
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
            if error != nil {
                print("Cannot save setting")
            }
            else{
                
            }
        }
    }
    
    static func hasInternetConnection() -> Bool {
        let networkReachability = Reachability()
        let networkStatus = networkReachability!.currentReachabilityStatus
        if networkStatus == Reachability.NetworkStatus.notReachable {
            return false;
        }
        else {
            return true
        }
    }
    
    
    static func hasGPSConnection(_ viewController: UIViewController) -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            Globals.showAlertWithTitle("No GPS Connection", message: "The location services seems to be disabled from the settings.", viewController: viewController)
            return false
        }
        else if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse {
            Globals.showAlertWithTitle("No GPS Connection", message: "The app is restricted from using location services.", viewController: viewController)
            return false
        }
        else {
            return true
        }
    }
    
    static func isValidEmail(_ email: String) -> Bool {
//        let stricterFilter = false
//        let stricterFilterString = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let laxString = ".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*"
        let emailRegex = laxString //stricterFilter ? stricterFilterString : laxString
        let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: email)
    }
    
    static func flattenHTML(_ html: String) -> String {
        var theScanner: Scanner
        let text: AutoreleasingUnsafeMutablePointer<NSString?>? = nil
        theScanner = Scanner(string: html)
        var editedHtml = html
        while (theScanner.isAtEnd == false) {
            theScanner.scanUpTo("<", into: nil)
            theScanner.scanUpTo(">", into: text)
            editedHtml = editedHtml.replacingOccurrences(of: String(format: "%@>", text!), with: "", options: NSString.CompareOptions.literal, range: nil)
        }
        editedHtml = editedHtml.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return editedHtml
    }
    
    static func login(_ view:BaseViewController, onSuccess:(() -> Void)?) {
        Globals.login(view, onSuccess: onSuccess, onCancel: nil)
    }
    
    static func login(_ view:BaseViewController, onSuccess:(() -> Void)?, onCancel:(() -> Void)?) {
        let storyboardName = "Main"
        let storyboard: UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        vc.onSuccess = onSuccess
        vc.onCancel = onCancel
        vc._parentViewController = view
//        view.presentViewController(vc, animated: true, completion: nil)
        let navController: UINavigationController = view.revealViewController().frontViewController as! UINavigationController
        navController.pushViewController(vc, animated: true)
//        navController.setViewControllers([vc], animated: true)
        ControllerHelper.setActiveViewController(vc)
        view.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
    }

    static func isLoggedIn() -> Bool {
        if let setting: AnyObject = Globals.getDataSetting("userId") {
            let userId : String! = setting as! String
            if userId == "" {
                return false
            }
            return true
        }
        else {
            return false
        }
    }
    
    static func logOut(_ view:BaseViewController, onSuccess:(() -> Void)?) {
        if !view.isLoading(){
            view.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("users/logout"),
                requestType: Globals.HTTPRequestType.http_POST,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String
                ]) { (result) -> Void in
                    view.hideLoading()
                    
                    if Globals.getDataSetting("signInAgent") as! String == "google" {
                        GIDSignIn.sharedInstance().signOut()
                    }
                    
                    Globals.setDataSetting("accessToken", value: "")
                    Globals.setDataSetting("userId", value: "")
                    Globals.setDataSetting("userName", value: "")
                    Globals.setDataSetting("userEmail", value: "")
                    Globals.setDataSetting("userProfilePicture", value: "")
                    Globals.setDataSetting("signInAgent", value: "")
                    
                    
                    
                    ProjectHelper.clearProject()
                    CartHelper.clearCart()
                    
                    //return to home
                    if onSuccess != nil {
                        DispatchQueue.main.async(execute: {
                            onSuccess!()
                        })
                    }
            }
        }
    }
    
    static func showWebViewWithUrl(_ url: String, parentView: BaseViewController, title: String) {
        parentView.showViewController("WebViewController", attributes: ["url" : url, "title": title])
    }
    
    static func getFirstConstraint(_ constraint:NSLayoutAttribute, view:UIView) -> NSLayoutConstraint?{
        // soon to be deleted
        // this function is not recommended
        // reason : with this one, the constraint is not "remembered" so, it can affect only one. if you want to update it several times, it won't works
        for obj in view.constraints{
            if (obj ).firstAttribute == constraint {
                return obj
            }
        }
        return nil
    }
    
    static func getConstraint(_ identifier:String, view:UIView) -> NSLayoutConstraint? {
        //don't forget to set constraint identifier in each constraint
        for obj in view.constraints{
            if obj.identifier == identifier {
                return obj
            }
        }
        return nil
    }
    
    static func setImageFromUrl(_ url:String, imageView:UIImageView, placeholderImage:UIImage?){
        self.setImageFromUrl(url, imageView: imageView, placeholderImage: placeholderImage) { (image) -> Void in
            
        }
    }
    
    static func setImageFromUrl(_ url:String, imageView:UIImageView, placeholderImage:UIImage?, onSuccess:((_ image:UIImage?) -> Void)? ){
        if placeholderImage != nil {
            imageView.image = placeholderImage
        }
        
        if Globals.hasInternetConnection() {
        
            // If this image is already cached, don't re-download
            if let img = Globals.getImageCache(url) {
                imageView.image = UIImage(data: img)
                onSuccess!(imageView.image)
            }
            else {
                // The image isn't cached, download the img data
                // We should perform this in a background thread
                let request: URLRequest = URLRequest(url: URL(string: url)!)
                Alamofire.request(request)
                    .responseData{ response in
                        switch response.result {
                            
                        case .success:
                            if let value = response.data {
                                let image = UIImage(data: value)
                                // Store the image in to our cache
                                Globals.setImageCache(url, image: value)
                                // Update the cell
                                DispatchQueue.main.async(execute: {
                                    imageView.image = image
                                    onSuccess!(image)
                                })
                            }
                        case .failure:
                            if let value = response.error {
                                print("Error: \(value.localizedDescription)")
                            }
                        }
                }
//                let mainQueue = OperationQueue.main
//                NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
//                    if error == nil {
//                        // Convert the downloaded data in to a UIImage object
//                        let image = UIImage(data: data!)
//                        // Store the image in to our cache
//                        Globals.setImageCache(url, image: data!)
//                        // Update the cell
//                        DispatchQueue.main.async(execute: {
//                            imageView.image = image
//                            onSuccess!(image)
//                        })
//                    }
//                    else {
//                        print("Error: \(error!.localizedDescription)")
//                    }
//                })
            }
            
        }
        else{
            if let img = Globals.getImageCache(url, force: true) {
                imageView.image = UIImage(data: img)
                onSuccess!(imageView.image)
            }
        }
    }
    
    static func imageFromUrl(_ url:String, onSuccess:((_ image:UIImage?) -> Void)? ){
        imageFromUrl(url, scale: 1, onSuccess: onSuccess)
    }
    
    static func imageFromUrl(_ url:String, scale: CGFloat, onSuccess:((_ image:UIImage?) -> Void)? ){
        
        if Globals.hasInternetConnection() {
            
            // If this image is already cached, don't re-download
            if let img = Globals.getImageCache(url) {
                if let image = UIImage(data: img) {
                    let newSize = CGSize(width: image.size.width * scale, height: image.size.height * scale)
                    UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
                    image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
                    let newImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    onSuccess!(newImage)
                }
            }
            else {
                // The image isn't cached, download the img data
                // We should perform this in a background thread
                let request: URLRequest = URLRequest(url: URL(string: url)!)
                Alamofire.request(request)
                    .responseData{ response in
                        switch response.result {
                            
                        case .success:
                            if let value = response.data {
                                let image = UIImage(data: value)
                                // Store the image in to our cache
                                Globals.setImageCache(url, image: value)
                                // Update the cell
                                
                                if let image = image {
                                    let newSize = CGSize(width: image.size.width * scale, height: image.size.height * scale)
                                    UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
                                    image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
                                    let newImage = UIGraphicsGetImageFromCurrentImageContext()
                                    UIGraphicsEndImageContext()
                                    DispatchQueue.main.async(execute: {
                                        onSuccess!(newImage)
                                    })
                                }
                            }
                        case .failure:
                            if let value = response.error {
                                print("Error: \(value.localizedDescription)")
                            }
                        }
                }
                //                let mainQueue = OperationQueue.main
                //                NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                //                    if error == nil {
                //                        // Convert the downloaded data in to a UIImage object
                //                        let image = UIImage(data: data!)
                //                        // Store the image in to our cache
                //                        Globals.setImageCache(url, image: data!)
                //                        // Update the cell
                //                        DispatchQueue.main.async(execute: {
                //                            imageView.image = image
                //                            onSuccess!(image)
                //                        })
                //                    }
                //                    else {
                //                        print("Error: \(error!.localizedDescription)")
                //                    }
                //                })
            }
            
        }
        else{
            if let img = Globals.getImageCache(url, force: true) {
                if let image = UIImage(data: img) {
                    let newSize = CGSize(width: image.size.width * scale, height: image.size.height * scale)
                    UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
                    image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
                    let newImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    onSuccess!(newImage)
                }
            }
        }
    }
    
    static func localized(_ key:String) -> String{
        return NSLocalizedString(key, comment: "")
    }
    
    static func scaleImage(_ image: UIImage, newSize: CGSize, offset: CGPoint) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: offset.x, y: offset.y, width: newSize.width-offset.x, height: newSize.height-offset.y))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    
    // MARK: - cached
    static func setCache(_ url:String, value:Data){
//        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = Globals.getManagedObjectContext()//appDelegate.managedObjectContext
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cache", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(url = %@)", url)
        request.predicate = pred
        let error: NSErrorPointer? = nil
        var isError = false
        
        var objects: [AnyObject]!
        context.performAndWait {
            do {
                objects = try context.fetch(request)
            } catch _ as NSError {
                //            error.memory = error1
                isError = true
                objects = nil
            }
        }
        
        var cache: Cache
        
        if objects.count == 0 {
            //new cache
            cache = NSEntityDescription.insertNewObject(forEntityName: "Cache", into: context) as! Cache
            cache.url = url
        }
        else {
            cache = objects[0] as! Cache
        }
        
        cache.value = value
        cache.lastUpdated = Date()
        context.performAndWait { 
            do {
                try context.save()
            } catch _ as NSError {
                //            error.memory = error1
                isError = true
            }
        }
        
        do {
            try context.save()
        } catch _ as NSError {
//            error.memory = error1
            isError = true
        }
        if error != nil || isError {
            print("Cannot store cache")
        }
        else{
            
        }
    }
    
    static func getCache(_ url:String) -> Data? {
        return Globals.getCache(url, force: false)
    }
    
    static func getCache(_ url:String, force:Bool) -> Data? {
//        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = Globals.getManagedObjectContext()//appDelegate.managedObjectContext
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cache", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        var pred: NSPredicate
        
        if !force {
            var lastUpdated: Date = Date()
            lastUpdated = lastUpdated.addingTimeInterval(-3600)
            pred = NSPredicate(format: "(url = %@ and lastUpdated > %@)", url, lastUpdated as CVarArg)
        }
        else{
            pred = NSPredicate(format: "(url = %@)", url)
        }
        
        request.predicate = pred
        let error: NSErrorPointer? = nil
        var objects: [AnyObject]!
        context.performAndWait {
            do {
                objects = try context.fetch(request)
            } catch let error1 as NSError {
                error??.pointee = error1
                objects = nil
            }
        }
        
        if objects.count == 0 {
            return nil
        }
        else {
            return (objects[0] as! Cache).value as Data
        }
    }
    
    static func setImageCache(_ url:String, image:Data) {
//        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = Globals.getManagedObjectContext()//appDelegate.managedObjectContext
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "ImageCache", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(url = %@)", url)
        request.predicate = pred
        let error: NSErrorPointer? = nil
        var isError = false
        var objects: [AnyObject]!
        context.performAndWait {
            do {
                objects = try context.fetch(request)
            } catch let error1 as NSError {
                error??.pointee = error1
                objects = nil
            }
        }
        
        
        var cache: ImageCache
        
        if objects.count == 0 {
            //new cache
            cache = NSEntityDescription.insertNewObject(forEntityName: "ImageCache", into: context) as! ImageCache
            cache.url = url
        }
        else {
            cache = objects[0] as! ImageCache
        }
        
        cache.value = image
        cache.lastUpdated = Date()
        context.performAndWait {
            do {
                try context.save()
            } catch _ as NSError {
                //error.memory = error1
                isError = true
            }
        }
        if error != nil || isError {
            print("Cannot store cache")
        }
        else{
            
        }
    }
    
    static func getImageCache(_ url:String) -> Data? {
        return Globals.getImageCache(url, force: false)
    }
    
    static func getImageCache(_ url:String, force:Bool) -> Data? {
//        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = Globals.getManagedObjectContext()//appDelegate.managedObjectContext
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "ImageCache", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        var lastUpdated: Date = Date()
        lastUpdated = lastUpdated.addingTimeInterval(-3600)
        var pred: NSPredicate
        
        if !force {
            var lastUpdated: Date = Date()
            lastUpdated = lastUpdated.addingTimeInterval(-3600)
            pred = NSPredicate(format: "(url = %@ and lastUpdated > %@)", url, lastUpdated as CVarArg)
        }
        else{
            pred = NSPredicate(format: "(url = %@)", url)
        }
        
        request.predicate = pred
        let error: NSErrorPointer? = nil
        var objects: [AnyObject]!
        context.performAndWait {
            do {
                objects = try context.fetch(request)
            } catch let error1 as NSError {
                error??.pointee = error1
                objects = nil
            }
        }
        
        var _: ImageCache
        
        if objects.count == 0 {
            return nil
        }
        else {
            return (objects[0] as! ImageCache).value as Data
        }
    }
    
    static func clearCache() {
//        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = Globals.getManagedObjectContext()//appDelegate.managedObjectContext
        var entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "ImageCache", in: context)!
        var request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let error: NSErrorPointer? = nil
        var objects: [AnyObject]!
        context.performAndWait {
            do {
                objects = try context.fetch(request)
            } catch let error1 as NSError {
                error??.pointee = error1
                objects = nil
            }
            
            for object in objects {
                context.delete(object as! NSManagedObject)
            }
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
        }
        
        entityDesc = NSEntityDescription.entity(forEntityName: "Cache", in: context)!
        request = NSFetchRequest()
        request.entity = entityDesc
        context.performAndWait {
            do {
                objects = try context.fetch(request)
            } catch let error1 as NSError {
                error??.pointee = error1
                objects = nil
            }
            
            for object in objects {
                context.delete(object as! NSManagedObject)
            }
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
        }
    }
    
    static func getMonthName(_ month:Int) -> String {
        let monthName:String!
        if month == 1 { monthName = "January" }
        else if month == 2 { monthName = "February" }
        else if month == 3 { monthName = "March" }
        else if month == 4 { monthName = "April" }
        else if month == 5 { monthName = "May" }
        else if month == 6 { monthName = "June" }
        else if month == 7 { monthName = "July" }
        else if month == 8 { monthName = "August" }
        else if month == 9 { monthName = "September" }
        else if month == 10 { monthName = "October" }
        else if month == 11 { monthName = "November" }
        else if month == 12 { monthName = "December" }
        else { monthName = "" }
        return monthName
    }
    
    static func getWeekDayName(_ weekDay:Int) -> String {
        //0 for Monday
        switch weekDay {
        case 0 :
            return "Monday"
        case 1 :
            return "Tuesday"
        case 2 :
            return "Wednesday"
        case 3 :
            return "Thursday"
        case 4 :
            return "Friday"
        case 5 :
            return "Saturday"
        case 6 :
            return "Sunday"
        default :
            return ""
        }
    }
    
    static func removeTableViewCellInsets(_ cell:UITableViewCell) {
        if (cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))){
            cell.layoutMargins = UIEdgeInsets.zero
            cell.separatorInset = UIEdgeInsets.zero
            cell.preservesSuperviewLayoutMargins = false
        }
    }
    
    fileprivate static let assetUrlQueue: NSMutableArray! = NSMutableArray()
    
    fileprivate class ImageAsset: NSObject {
        var url: String!
        var onSuccess: ((_ image:UIImage) -> Void)?
        var onError: ((_ error:NSError?) -> Void)?
        
        override init() {
            super.init()
        }
        
        init(url: String){
            self.url = url
        }
        init(url: String, onSuccess: ((_ image:UIImage) -> Void)?, onError: ((_ error:NSError?) -> Void)?){
            self.url = url
            self.onSuccess = onSuccess
            self.onError = onError
        }
    }
    
//    fileprivate static func processImageFromAssetUrlQueue(){
//        var image: UIImage!
//        let obj = Globals.assetUrlQueue.object(at: 0) as! ImageAsset
//        
//        let resultBlock = { (asset: ALAssetLibraryAsset!) -> Void in
//            autoreleasepool(invoking: { () -> () in
//                let rep = asset.defaultRepresentation()
//                let ref: CGImage = rep!.fullResolutionImage().takeUnretainedValue()
//                image = UIImage(cgImage: ref)
//                
//                DispatchQueue.main.async(execute: { () -> Void in
//                    if let successFunction = obj.onSuccess {
//                        successFunction(image)
//                    }
//                })
//            })
//            DispatchQueue.main.async(execute: { () -> Void in
//                Globals.assetUrlQueue.removeObject(at: 0)
//            
//                if Globals.assetUrlQueue.count > 0 {
//                    Globals.processImageFromAssetUrlQueue()
//                }
//            })
//        }
//        let errorBlock = { (error:NSError!) -> Void in
//            DispatchQueue.main.async(execute: { () -> Void in
//                if let errorFunction = obj.onError {
//                    errorFunction(error)
//                }
//            })
//        }
//        assetsLibrary.asset(for: URL(string: obj.url), resultBlock: resultBlock, failureBlock: errorBlock)
//    }
//    
//    static func imageFromAssetUrl(_ url: String, onSuccess: ((_ image:UIImage) -> Void)?, onError: ((_ error:NSError?) -> Void)?) {
//        let temp = ImageAsset(url: url, onSuccess: onSuccess, onError: onError)
//        Globals.assetUrlQueue.add(temp)
//        
//        if Globals.assetUrlQueue.count == 1 {
//            Globals.processImageFromAssetUrlQueue()
//        }
//    }
    
    static func checkImageFrom(_ localIdentifier: String, scale: CGFloat, resultHandler: @escaping ((_ image: UIImage?) -> Void)) {
        if localIdentifier.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_PREFIX) {
            Globals.imageFromUrl(localIdentifier, scale: scale, onSuccess: resultHandler)
        }
        else if localIdentifier.contains(Constant.getPrefixUrl()) {
            Globals.imageFromUrl(localIdentifier, scale: scale, onSuccess: resultHandler)
        }
        else {
            Globals.checkImageFrom(localIdentifier, scale: scale, isHighQuality: false, resultHandler: resultHandler)
        }
    }
    
    static func checkImageFrom(_ localIdentifier: String, scale: CGFloat, isHighQuality: Bool, resultHandler: @escaping ((_ image: UIImage?) -> Void)) {
        if localIdentifier.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_PREFIX) {
            Globals.imageFromUrl(localIdentifier, scale: scale, onSuccess: resultHandler)
        }
        else if localIdentifier.contains(Constant.getPrefixUrl()) {
            Globals.imageFromUrl(localIdentifier, scale: scale, onSuccess: resultHandler)
        }
        else {
            Globals.imageFromAssetLocalIdentifier(localIdentifier, scale: scale, isHighQuality: isHighQuality, resultHandler: resultHandler)
        }
        
    }
    
    static func imageFromAssetLocalIdentifier(_ localIdentifier: String, scale: CGFloat, resultHandler: @escaping ((_ image: UIImage?) -> Void)) {
        
        Globals.imageFromAssetLocalIdentifier(localIdentifier, scale: scale, isHighQuality: false, resultHandler: resultHandler)
        
    }
    
    static func imageFromAssetLocalIdentifier(_ localIdentifier: String, scale: CGFloat, isHighQuality: Bool, resultHandler: @escaping ((_ image: UIImage?) -> Void)) {
        let assets = PHAsset.fetchAssets(withLocalIdentifiers: [localIdentifier], options: nil)
        if let obj = assets.firstObject {
            if let asset = obj as? PHAsset {
                self.imageFromAsset(asset, scale: scale, isHighQuality: isHighQuality, resultHandler: resultHandler)
            }
            
        }
        
    }
    
    static func imageFromAsset(_ asset: PHAsset, scale: CGFloat, resultHandler: @escaping ((_ image: UIImage?) -> Void)){
        Globals.imageFromAsset(asset, scale: scale, isHighQuality: false, resultHandler: resultHandler)
    }
    
    static func imageFromAsset(_ asset: PHAsset, scale: CGFloat, isHighQuality: Bool, resultHandler: @escaping ((_ image: UIImage?) -> Void)){
        autoreleasepool (invoking: {
            PHImageManager.default().requestImage(for: asset,
                targetSize: CGSize(width: CGFloat(asset.pixelWidth) * scale, height: CGFloat(asset.pixelHeight) * scale),
                contentMode: .aspectFill,
                options: isHighQuality ? Globals.highQualityImageOptions : Globals.imageOptions,
                resultHandler: { (result, info) -> Void in
                    DispatchQueue.main.async(execute: { () -> Void in
                        resultHandler(result)
                    })
            })
        })
        
    }
    
    static func registerDeviceId(){
        if let accessToken = Globals.getDataSetting("accessToken") as? String{
            if accessToken != "" {
                if let deviceId = Globals.getDataSetting("deviceId") as? String {
//                    NSLog("device Id : %@", deviceId)
                    let params:  NSMutableDictionary! = NSMutableDictionary(dictionary: [
                        "clientId" : Globals.getProperty("clientId"),
                        "accessToken" : accessToken,
                        "deviceId" : deviceId,
                        ])
                    
                    #if DEBUG
                        params.setValue("1", forKey: "isSandbox")
                    #endif
                    
                    Globals.getDataFromUrl(Globals.getApiUrl("users/registerDeviceId"),
                                           requestType: Globals.HTTPRequestType.http_POST,
                                           params: params) { (result) -> Void in
                                            if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                                                
                                            }
                                            else{
                                                
                                            }
                    }
                }
            }
        }
    }
}
