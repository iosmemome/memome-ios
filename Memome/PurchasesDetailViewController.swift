//
//  PurchasesDetailViewController.swift
//  Memome
//
//  Created by Trio-1602 on 5/3/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class PurchasesDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblPageTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var lblShipping: UILabel!
    @IBOutlet weak var productTableView: UITableView!
    
    var productDataSource = NSMutableArray()
    var orderId = ""
    let dbDateFormat: DateFormatter! = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.productTableView.register(UINib(nibName: "OrderProductTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderProductTableViewCell")
        self.dbDateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        
        if self.attributes != nil {
            if let order = self.attributes.object(forKey: "order") as? NSDictionary {
                
                self.renderOrderData(order)
                
            }
        }
        
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height: CGFloat = 49.0
        let minHeight: CGFloat = 90
        let maxFloat: CGFloat = 99999.0
        
        let cellInfoArray = self.productDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        
        if let title = cellInfoArray.object(forKey: "product_title") as? NSString {
            height += title.boundingRect(
                with: CGSize(width: tableView.frame.size.width, height: maxFloat),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 14)!],
                context: nil
                ).size.height
        }
        
        if let title = cellInfoArray.object(forKey: "title") as? String {
            height += title.boundingRect(
                with: CGSize(width: tableView.frame.size.width, height: maxFloat),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 16)!],
                context: nil
                ).size.height
        }
        
        return max(height, minHeight)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderProductTableViewCell", for: indexPath)
        
        let cellInfoArray = self.productDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        
        let lblProductTitle = cell.viewWithTag(1) as! UILabel
        let lblTitle = cell.viewWithTag(2) as! UILabel
        let lblPrice = cell.viewWithTag(3) as! UILabel
        let lblQty = cell.viewWithTag(4) as! UILabel
        
        if let title = cellInfoArray.object(forKey: "product_title") as? String {
            lblProductTitle.text = title
        }
        else {
            lblProductTitle.text = ""
        }
        
        if let title = cellInfoArray.object(forKey: "title") as? String {
            lblTitle.text = title
        }
        else{
            lblTitle.text = title
        }
        
        if let price = cellInfoArray.object(forKey: "total_price") as? NSString {
            if price.doubleValue == 0 {
                lblPrice.text = "FREE"
            }
            else{
                lblPrice.text = "IDR " + Globals.numberFormat(NSNumber(value: price.doubleValue as Double))
            }
        }
        else{
            lblPrice.text = ""
        }
        
        if let qty = cellInfoArray.object(forKey: "qty") as? NSString {
            lblQty.text = Globals.numberFormat(NSNumber(value: qty.integerValue as Int)) + " X"
        }
        else {
            lblQty.text = ""
        }
        
        return cell
    }

    // actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    //general methods
    func reloadProductTableView() {
        self.productTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.productTableView) {
            height.constant = self.productTableView.contentSize.height
        }
    }
    
    func renderOrderData(_ order: NSDictionary) {
        if let number = order.object(forKey: "number") as? String {
            self.lblPageTitle.text = "ORDER #\(number)"
        }
        else {
            self.lblPageTitle.text = ""
        }
        
        if let number = order.object(forKey: "id") as? String {
            self.orderId = number
        }
        
        if let createdAt = order.object(forKey: "created_at") as? String {
            let calendar = Calendar.current
            let components: DateComponents = (calendar as NSCalendar).components([.month, .day, .year, .hour, .minute, .second], from: self.dbDateFormat.date(from: createdAt)!)
            
            self.lblDate.text = String(format: "%i %@ %i", arguments: [
                components.day!,
                Globals.getMonthName(components.month!),
                components.year!
                ])
        }
        else{
            self.lblDate.text = ""
        }
        
        if let status = order.object(forKey: "status") as? String {
            self.lblStatus.text = status.uppercased()
        }
        else{
            self.lblStatus.text = ""
        }
        
        var total: Double = 0
        if let subtotal = order.object(forKey: "subtotal") as? NSString {
            total += subtotal.doubleValue
        }
        if let subtotal = order.object(forKey: "shipping_price") as? NSString {
            total += subtotal.doubleValue
        }
        self.lblTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: total as Double))
        
        if let discount = order.object(forKey: "voucher_total") as? NSString {
            self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: discount.doubleValue as Double))
            
            if let voucher_code = order.object(forKey: "voucher_code") as? String {
                self.lblDiscount.text = self.lblDiscount.text! + " (\(voucher_code))"
            }
        }
        else{
            self.lblDiscount.text = ""
        }
        
        if let grandTotal = order.object(forKey: "grand_total") as? NSString {
            self.lblGrandTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: grandTotal.doubleValue as Double))
        }
        else{
            self.lblGrandTotal.text = ""
        }
        
        var shipping = ""
        if let item = order.object(forKey: "shipping_first_name") as? String {
            shipping = shipping + item
        }
        
        if let item = order.object(forKey: "shipping_last_name") as? String {
            shipping = shipping + item
        }
        
        if let item = order.object(forKey: "shipping_address") as? String {
            if shipping != "" {
                shipping = shipping + "\n"
            }
            shipping = shipping + item + "\n"
        }
        
        if let item = order.object(forKey: "shipping_city") as? String {
            shipping = shipping + item + ", "
        }
        
        if let item = order.object(forKey: "shipping_province") as? String {
            shipping = shipping + item + " - "
        }
        
        if let item = order.object(forKey: "shipping_postal_code") as? String {
            shipping = shipping + item
        }
        
        if let item = order.object(forKey: "shipping_phone") as? String {
            if shipping != "" {
                shipping = shipping + "\n"
            }
            shipping = shipping + item
        }
        
        if let item = order.object(forKey: "shipping_type") as? String {
            if shipping != "" {
                shipping = shipping + "\n"
            }
            shipping = shipping + item
            
            if let item2 = order.object(forKey: "shipping_number") as? String {
                if item2 != "" {
                    shipping = shipping + " (\(item2))"
                }
            }
        }
        
        self.lblShipping.text = shipping
    }
    
    func loadData() {
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(
                Globals.getApiUrl("orders/getOrderProducts"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String,
                    "orderId" : self.orderId
                ],
                completion: { (result) in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        self.productDataSource.removeAllObjects()
                        if let products = result.object(forKey: "products") as? NSArray {
                            self.productDataSource.addObjects(from: products as [AnyObject])
                        }
                        
                        self.reloadProductTableView()
                        
                    }
                    else{
                        Globals.showAlertWithTitle("My Orders Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
}
