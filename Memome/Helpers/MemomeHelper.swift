//
//  MemomeHelper.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/13/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import CoreData

class MemomeHelper: NSObject {

    static func calculateRatio(_ actualSize: CGFloat, targetSize: CGFloat) -> CGFloat {
        return targetSize / actualSize
    }
    
    static var coverTitleAreaBorder: CGFloat = 40.0
    
    static var coverImage: NSMutableDictionary! = nil
    static var images: NSMutableArray! = nil
    
    static func setCoverView(_ parentView: UIView, project: Project, cover: ProjectDetail?, productData: NSDictionary, size: CGSize) {
        setCoverView(parentView, project: project, cover: cover, productData: productData, size: size, extraTop: 30)
    }
    
    static func setCoverView(_ parentView: UIView, project: Project, cover: ProjectDetail?, productData: NSDictionary, size: CGSize, extraTop: CGFloat) {
        let coverContent = Bundle.main.loadNibNamed("CoverView", owner: self, options: nil)
        let cell: UIView = coverContent!.last as! UIView
        
        cell.frame = CGRect(x: 0, y: extraTop, width: size.width, height: size.height)
        
        let coverView: UIView! = cell.viewWithTag(1)
        let coverViewBackground: UIImageView! = cell.viewWithTag(2) as! UIImageView
        let coverImageView: UIImageView! = cell.viewWithTag(3) as! UIImageView
        let coverTitleView: UIView! = cell.viewWithTag(4)
        let coverTitleInnerView: UIView! = cell.viewWithTag(5)
        let lblCoverTitle: UILabel! = cell.viewWithTag(6) as! UILabel
        let lblCoverSubtitle: UILabel! = cell.viewWithTag(7) as! UILabel
        
        coverView.frame = cell.frame
        
        let coverWidth: CGFloat = CGFloat((productData.object(forKey: "cover_width") as! NSString).floatValue)
        let coverHeight: CGFloat = CGFloat((productData.object(forKey: "cover_height") as! NSString).floatValue)
        let coverBorderTop: CGFloat = CGFloat((productData.object(forKey: "cover_border_top") as! NSString).floatValue)
        let coverBorderLeft: CGFloat = CGFloat((productData.object(forKey: "cover_border_left") as! NSString).floatValue)
        var coverContentWidth: CGFloat = CGFloat((productData.object(forKey: "cover_content_width") as! NSString).floatValue)
        var coverContentHeight: CGFloat = CGFloat((productData.object(forKey: "cover_content_height") as! NSString).floatValue)
        if coverContentWidth == 0 {
            coverContentWidth = coverWidth
        }
        if coverContentHeight == 0 {
            coverContentHeight = coverHeight
        }
        let coverTitleAreaWidth: CGFloat = CGFloat((productData.object(forKey: "cover_title_area_width") as! NSString).floatValue)
        let coverTitleAreaHeight: CGFloat = CGFloat((productData.object(forKey: "cover_title_area_height") as! NSString).floatValue)
        let coverTitleAreaTop: CGFloat = CGFloat((productData.object(forKey: "cover_title_area_top") as! NSString).floatValue)
        let coverTitleAreaLeft: CGFloat = CGFloat((productData.object(forKey: "cover_title_area_left") as! NSString).floatValue)
        let coverTitleTop: CGFloat = CGFloat((productData.object(forKey: "cover_title_top") as! NSString).floatValue)
        let coverSubtitleTop: CGFloat = CGFloat((productData.object(forKey: "cover_subtitle_top") as! NSString).floatValue)
        let coverTitleFontSize: CGFloat = CGFloat((productData.object(forKey: "cover_title_font_size") as! NSString).floatValue)
        let coverSubtitleFontSize: CGFloat = CGFloat((productData.object(forKey: "cover_subtitle_font_size") as! NSString).floatValue)
        
        let ratio: CGFloat = MemomeHelper.calculateRatio(CGFloat(coverWidth), targetSize: size.width)
        let coverTitleAreaBorder: CGFloat = MemomeHelper.coverTitleAreaBorder
        
        coverViewBackground.image = nil
        if let coverImages = productData.object(forKey: "cover_images") as? NSDictionary {
            if let image = coverImages.object(forKey: "thumb") as? String {
                Globals.setImageFromUrl(image, imageView: coverViewBackground, placeholderImage: nil)
            }
        }
        
        //cover size
        if let height = Globals.getConstraint("height", view: coverView) {
            height.constant = coverHeight * ratio
        }
        
        //cover content
        if let top = Globals.getConstraint("coverImageTop", view: coverView) {
            top.constant = coverBorderTop * ratio
        }
        if let left = Globals.getConstraint("coverImageLeft", view: coverView) {
            left.constant = coverBorderLeft * ratio
        }
        if let width = Globals.getConstraint("width", view: coverImageView) {
            width.constant = coverContentWidth * ratio
        }
        if let height = Globals.getConstraint("height", view: coverImageView) {
            height.constant = coverContentHeight * ratio
        }
        
        
        //title area
        if let top = Globals.getConstraint("titleViewTop", view: coverView) {
            top.constant = coverTitleAreaTop * ratio 
        }
        if let left = Globals.getConstraint("titleViewLeft", view: coverView) {
            left.constant = coverTitleAreaLeft * ratio
        }
        if let width = Globals.getConstraint("width", view: coverTitleView) {
            width.constant = coverTitleAreaWidth * ratio
        }
        if let height = Globals.getConstraint("height", view: coverTitleView) {
            height.constant = coverTitleAreaHeight * ratio
        }
        
        if let top = Globals.getConstraint("titleInnerTop", view: coverTitleView) {
            top.constant = coverTitleAreaBorder * ratio
        }
        if let left = Globals.getConstraint("titleInnerLeft", view: coverTitleView) {
            left.constant = coverTitleAreaBorder * ratio
        }
        if let bottom = Globals.getConstraint("titleInnerBottom", view: coverTitleView) {
            bottom.constant = coverTitleAreaBorder * ratio
        }
        if let right = Globals.getConstraint("titleInnerRight", view: coverTitleView) {
            right.constant = coverTitleAreaBorder * ratio
        }
        
        //title & subtitle
        if let top = Globals.getConstraint("titleTop", view: coverTitleInnerView) {
            top.constant = (coverTitleTop - coverTitleAreaBorder) * ratio
        }
        if let top = Globals.getConstraint("subtitleTop", view: coverTitleInnerView) {
            top.constant = (coverSubtitleTop - coverTitleAreaBorder) * ratio
        }
        
        let coverTitleFont = lblCoverTitle.font
        lblCoverTitle.font = coverTitleFont?.withSize( coverTitleFontSize * ratio )
        let coverSubtitleFont = lblCoverSubtitle.font
        lblCoverSubtitle.font = coverSubtitleFont?.withSize( coverSubtitleFontSize * ratio )
        
        coverView.layer.borderColor = UIColor(white: 0.25, alpha: 1).cgColor
        coverView.layer.borderWidth = 1.0
//        coverTitleView.layer.borderColor = UIColor(white: 0.57, alpha: 1).cgColor
//        coverTitleView.layer.borderWidth = 1.0
        
        if let coverDetail = cover {
            if coverDetail.image != "" {
                var x: CGFloat = 0.0
                var y: CGFloat = 0.0
                var width: CGFloat = 0.0
                var height: CGFloat = 0.0
                let scale: CGFloat = CGFloat(coverDetail.zoom)
                var rotation: Int = 0
                
                width = CGFloat(coverDetail.width) * scale
                height = CGFloat(coverDetail.height) * scale
                
                x = CGFloat(coverDetail.offsetX) * scale
                y = CGFloat(coverDetail.offsetY) * scale
                
                rotation = Int(coverDetail.rotation)
                
                let screenScale = max( coverImageView.frame.size.width / width, coverImageView.frame.size.height / height )
                let cropRect = CGRect(x: x * screenScale, y: y * screenScale, width: width * screenScale, height: height * screenScale)
                
                Globals.checkImageFrom(coverDetail.image, scale: scale * screenScale, resultHandler: { (image) -> Void in
                    if let img = image {
                        coverImageView.image = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect)
                    }
                })
            }
        }
        
        lblCoverTitle.text = project.title
        lblCoverSubtitle.text = project.subtitle
        
        
        
        parentView.addSubview(cell)
    }
}
