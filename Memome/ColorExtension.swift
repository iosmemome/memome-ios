//
//  ColorExtension.swift
//  Memome
//
//  Created by iOS Developer on 19/12/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import Foundation

extension UIColor {
    static let colorPrimary = UIColor(red: 101.0/255.0, green: 198.0/255.0, blue: 189.0/255, alpha: 1)
    static let colorPrimarySemiDark = UIColor(red: 90.0/255.0, green: 178.0/255.0, blue: 170.0/255.0, alpha: 1)
    static let colorGrey = UIColor(red: 108.0/255.0, green: 108.0/255.0, blue: 108.0/255.0, alpha: 1)
    static let colorLightRed = UIColor(red: 255.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1)
}
