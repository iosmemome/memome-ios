//
//  PHAssetExtension.swift
//  Memome
//
//  Created by iOS Developer on 17/11/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import Foundation
import Photos

extension PHAsset {
    
    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
        if self.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
            })
        } else if self.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url as URL
                    completionHandler(localVideoUrl)
                } else {
                    completionHandler(nil)
                }
            })
        }
    }
    
    func getSize(completionHandler : @escaping ((_ size : Float?) -> Void)){
        PHImageManager.default().requestImageData(for: self, options: nil) { (data, orientation, imageOrientation, object) in
            //Get bytes size of image
            if data != nil {
                var dataCount = data!.count
                completionHandler(Float(data!.count)/(1024*1024))
            }
            else {
                completionHandler(nil)
            }
        }
    }
}
