//
//  Chiper.swift
//  Bobby Stenly Irawan
//
//  Created by staff on 11/26/15.
//  Copyright © 2015 Bobby Stenly Irawan - (Trio Digital Agency - bobby@tgitriodesign.com). All rights reserved.
//

import UIKit

extension Int {
    func hexString() -> String {
        return NSString(format:"%02x", self) as String
    }
}

extension Data {
    func hexString() -> String {
//        var string = String()
//        for i in UnsafeBufferPointer<UInt8>(start: UnsafePointer<UInt8>(bytes), count: count) {
//            string += Int(i).hexString()
//        }
//        return string
        var bytes = [UInt8](repeating: 0, count: count)
        copyBytes(to: &bytes, count: count)
        
        var hexString = String()
        for byte in bytes {
            hexString += String(format: "%02x", UInt(byte))
        }
        
        return hexString
    }
    
    func MD5() -> Data {
        let result = NSMutableData(length: Int(CC_MD5_DIGEST_LENGTH))!
//        CC_MD5(bytes, CC_LONG(count), UnsafeRawPointer<UInt8>(result.mutableBytes))
        var bytes = [UInt8](repeating: 0, count: count)
        copyBytes(to: &bytes, count: count)
        CC_MD5(bytes, CC_LONG(count), result.mutableBytes.bindMemory(to: UInt8.self, capacity: result.length))
        return result as Data
    }
    
    func SHA1() -> Data {
        let result = NSMutableData(length: Int(CC_SHA1_DIGEST_LENGTH))!
        var bytes = [UInt8](repeating: 0, count: count)
        copyBytes(to: &bytes, count: count)
        CC_SHA1(bytes, CC_LONG(count), result.mutableBytes.bindMemory(to: UInt8.self, capacity: result.length))
        return result as Data
    }
}

extension String {
    func MD5() -> String {
        return (self as NSString).data(using: String.Encoding.utf8.rawValue)!.MD5().hexString()
    }
    
    func SHA1() -> String {
        return (self as NSString).data(using: String.Encoding.utf8.rawValue)!.SHA1().hexString()
    }
    
    func indexOf( _ character:String ) -> Int {
        for i in 0 ..< (self as NSString).length {
            if self.characterAtIndex(i) == character {
                return i
            }
        }
        return -1
    }
    
    func characterAtIndex(_ index:Int) -> String {
        let characters = Array(self.characters)
        if characters.count > index {
            return String(characters[index])
        }
        else {
            return ""
        }
    }
    
    func length() -> Int {
        return (self as NSString).length
    }
}

class Chiper: NSObject {
    
    static var key: String! = "abcdefgh"
    static var allowedChars: String! = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ" //random order
    static var keyChars: String! = "0123456789abcdefghijklmnopqrstuvwxyz" //a-z 0-9 random order

    static func encrypt(_ source: String) -> String {
        return Chiper.encrypt(source, key: "")
    }
    
    static func encrypt(_ source: String, key: String) -> String {
        let key = key + self.key.MD5()
        
        var result = ""
        let mod = self.allowedChars.length()
        
        for i in 0 ..< source.length() {
            let shift = self.keyChars.indexOf(key.characterAtIndex(i))
            let index = self.allowedChars.indexOf(source.characterAtIndex(i))
            if shift > -1 && index > -1 {
                result = result + self.allowedChars.characterAtIndex((index + shift) % mod)
            }
            else {
                result = result + source.characterAtIndex(i)
            }
            
        }
        return result as String
    }
    
    static func decrypt(_ source: String) -> String {
        return Chiper.decrypt(source, key: "")
    }
    
    static func decrypt(_ source: String, key: String) -> String {
        let key = key + self.key.MD5()
        
        var result = ""
        let mod = self.allowedChars.length()
        
        for i in 0 ..< source.length() {
            let shift = self.keyChars.indexOf(key.characterAtIndex(i))
            var index = self.allowedChars.indexOf(source.characterAtIndex(i))
            if shift > -1 && index > -1 {
                index = index - shift
                if index < 0 {
                    index = index + mod
                }
                result = result + self.allowedChars.characterAtIndex(index)
            }
            else {
                result = result + source.characterAtIndex(i)
            }
            
        }
        return result as String
    }
}
