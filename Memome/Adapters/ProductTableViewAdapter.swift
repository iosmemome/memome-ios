//
//  ProductTableViewAdapter.swift
//  Memome
//
//  Created by Trio-1602 on 4/12/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class ProductTableViewAdapter: NSObject, UITableViewDataSource, UITableViewDelegate, BSSliderDelegate {
    
    var productDataSource: NSMutableArray! = NSMutableArray()
    
    var productData: NSDictionary!
    var productDetails: NSArray!
    var project: Project?
    var projectDetails: NSArray?
    var productDetailsDataSource: NSMutableArray! = NSMutableArray()
    var sliderDataSource: NSMutableArray = NSMutableArray()
    var cellArray: NSDictionary = NSDictionary()
    let arrayData = NSMutableArray()
    
    var corporateVoucherId: Int = 0
    var corporateVoucherName: String = ""
    var corporateVoucherQty: Int = 0
    var corporateVoucherCode: String = ""
    var corporateVoucher: NSDictionary = NSDictionary()
    
    var galleryImages = [String]()
    var galleryFirst = ""
    
    var _parentViewController: BaseViewController!
    var tableView: UITableView!
    
    var voucher: NSDictionary!
    var voucherCode: String = ""
    var slideBool: Bool = false
    
    // MARK: - New Properties
    private let kProductInfoNameKey = "product_name"
    private let kProductInfoAmountKey = "product_amount"
    private let kProductInfoDimensionKey = "product_dimension"
    private let kProductInfoDescriptionKey = "product_description"
    private let kProductInfoPriceKey = "product_price"
    private let kProductImageUrlsKey = "product_images"
    
    private var productInfos = [[String: AnyObject]]()
    private var imageUrlStrings = [[String]]()
    
    
    // MARK: - Methods
    func setDataSource(_ data: NSArray) {
        self.productDataSource.removeAllObjects()
        self.productDataSource.addObjects(from: data as [AnyObject])
        
        // Populate Info Dictionary
        productInfos.removeAll()
        for i in 0..<productDataSource.count {
            let dict = productDataSource[i] as! [String: AnyObject]
            var infoDict = [String: AnyObject]()
            
            // Product Name
            infoDict[kProductInfoNameKey] = dict["title"]
            
            // Product Amount
            var amountString = ""
            var minImage = Int(dict["min_image"] as! String) ?? 0
            let maxImage = Int(dict["max_image"] as! String) ?? 0
            
            if minImage == maxImage {
                if minImage == 0 {
                    amountString = "1 Photo"
                }
                else{
                amountString = "\(minImage) Photo"
                }
            }
            else {
                amountString = "\(minImage) - \(maxImage) Photos"
            }
            infoDict[kProductInfoAmountKey] = amountString as NSObject
            
            // Product Size
            infoDict[kProductInfoDimensionKey] = dict["size"]
            
            // Product Description
            infoDict[kProductInfoDescriptionKey] = dict["info"]
            
            // Product Price
            infoDict[kProductInfoPriceKey] = dict["price"]
            
            // Product Images
            var arrUrlStrings = [String]()
            arrUrlStrings.append(dict["front_images"] as! String)
            
            let galleryUrlStrings = dict["gallery_images"] as! [[String: String]]
            for j in 0..<galleryUrlStrings.count {
                let subDict = galleryUrlStrings[j]
                arrUrlStrings.append(subDict["large"]!)
            }
            
            infoDict[kProductImageUrlsKey] = arrUrlStrings as AnyObject
            
            // Add infoDict to ProductInfos
            productInfos.append(infoDict)
        }
    }
    
    func getDataSource() -> NSArray {
        return self.productDataSource
    }
    
    func setParentViewController(_ vc: BaseViewController) {
        self._parentViewController = vc
    }
    
    func getParentViewController() -> BaseViewController {
        return self._parentViewController
    }
    
    
    // MARK: - Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productDataSource.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return ((tableView.frame.width))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return (self.tableView.frame.size.width ) + 90
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return createProductCell(indexPath)
    }
    
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let productCell = cell as? NewProductDetailTableViewCell {
            productCell.setupPageControl()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        slideBool = true
        if (indexPath as NSIndexPath).row < self.productDataSource.count {
            let cellInfoArray = self.productDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
            
            self.productData = cellInfoArray
            
            let attributes = NSMutableDictionary(dictionary: [
                "product" : cellInfoArray
                ])
            
            if self.voucherCode != "" {
                attributes.setValue(self.voucherCode, forKey: "voucherCode")
                attributes.setValue(self.voucher, forKey: "voucher")
            }
            
            var isTitleRequired = true
            
            if let isRequireTitle = cellInfoArray.object(forKey: "is_require_title") as? NSNumber {
                isTitleRequired = isRequireTitle.intValue == 1
            }
            else if let isRequireTitle = cellInfoArray.object(forKey: "is_require_title") as? NSString {
                isTitleRequired = isRequireTitle.integerValue == 1
            }
            
            if isTitleRequired {
                self._parentViewController.showViewController("ProductTitleViewController", attributes: attributes)
            }
            else {
                if cellInfoArray.object(forKey: "id") as? String == "3" || cellInfoArray.object(forKey: "id") as? String == "20" || cellInfoArray.object(forKey: "id") as? String == "22" || cellInfoArray.object(forKey: "id") as? String == "24" || cellInfoArray.object(forKey: "id") as? String == "27"{
                    
                    self._parentViewController.showViewController("ProductCoverViewController", attributes: attributes)

                }
                else{
                    
//                    if self.productDetails == nil {
                        self.loadProductData()
//                    }
//                    else{
//                        let attributes = NSMutableDictionary(dictionary: [
//                            "product" : self.productData,
//                            "productDetails" : self.productDetails,
//                            "numberOfPhotos" : NSNumber(value: self.productDetails.count as Int)
//                            ])
//                        if let proj = self.project {
//                            attributes.setValue(proj, forKey: "project")
//                        }
//                        if let details = self.projectDetails {
//                            attributes.setValue(details, forKey: "projectDetails")
//                        }
//                        if self.corporateVoucherId > 0 {
//                            attributes.setValue(self.corporateVoucherCode, forKey: "voucherCode")
//                            attributes.setValue(self.corporateVoucher, forKey: "voucher")
//                        }
//                        self._parentViewController.showViewController("MainPhotoSelectorViewController", attributes: attributes)
//
//                        }
                    }
                }
        }
        
    }
    
    func setupSlides(_ slider: BSSlider) {
        if let slidesContainer = slider.getSlidesContainer(){
            var i : CGFloat = 0
            var j = Int()
            
            
                if let item = arrayData.object(at: 0) as? NSDictionary{
                    for a in 1...4{
                let contents = Bundle.main.loadNibNamed("GallerySlide", owner: self, options: nil)
                let slide: UIView = contents!.first as! UIView
                let width = UIScreen.main.bounds.width-16
                slide.frame = CGRect(x : 0, y: 0, width : width, height : width)
                slide.clipsToBounds = true
                
                let imgView: UIImageView = slide.viewWithTag(1) as! UIImageView
                imgView.image = UIImage(named: "image-default.png")
                
                j = Int(i)
                
                var l = ""
                l = String(j)
                
                if galleryFirst != ""{
                     Globals.setImageFromUrl(galleryFirst, imageView: imgView, placeholderImage: UIImage(named: "image-default.png"))
                    galleryFirst = ""
                }else{
                    if let images = (item as AnyObject).object(forKey: l) as? String{
                        Globals.setImageFromUrl(images, imageView: imgView, placeholderImage: UIImage(named: "image-default.png"))
                    }
                }
                slidesContainer.addSubview(slide)
                let amount = slidesContainer.subviews.count
                i += 1
                }
            }
        }
        
    }
    
    
    func loadProductData(){
            
            Globals.getDataFromUrl(Globals.getApiUrl("products/view"),
                                   requestType: Globals.HTTPRequestType.http_GET,
                                   params: [
                                    "clientId" : Globals.getProperty("clientId"),
                                    "productId" : self.productData.object(forKey: "id") as! String
                ],
                                   completion: { (result) -> Void in
                                    
                                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                                        self.productDetailsDataSource.removeAllObjects()
                                        
                                        if let productDetails = result.object(forKey: "details") as? NSArray {
                                            self.productDetailsDataSource.addObjects(from: productDetails as [AnyObject])
                                        }
                                        
                                        if let product = result.object(forKey: "product") as? NSDictionary {
                                            self.productData = product
                                        }
                                        
                                        self.productDetails = self.productDetailsDataSource
                                        
                                        if let _ = self.project {
                                        }
                                        else{
                                            let attributes = NSMutableDictionary(dictionary: [
                                                "product" : self.productData,
                                                "productDetails" : self.productDetails,
                                                "numberOfPhotos" : NSNumber(value: self.productDetails.count as Int),
                                                "minimumNumberOfPhotos" : Int(self.productData.object(forKey: "min_image") as? String ?? "0") ?? 0
                                                ])
                                            if let proj = self.project {
                                                attributes.setValue(proj, forKey: "project")
                                            }
                                            if let details = self.projectDetails {
                                                attributes.setValue(details, forKey: "projectDetails")
                                            }
                                            if self.corporateVoucherId > 0 {
                                                attributes.setValue(self.corporateVoucherCode, forKey: "voucherCode")
                                                attributes.setValue(self.corporateVoucher, forKey: "voucher")
                                            }
                                            self._parentViewController.showViewController("MainPhotoSelectorViewController", attributes: attributes)
                                        }
                                    }
            })
    }
    
    // MARK: - Cell Creations
    private func createProductCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as! NewProductDetailTableViewCell
        let infoDict = productInfos[indexPath.row]
        
        cell.nameLabel.text = (infoDict[kProductInfoNameKey] as! String)
        cell.quantityLabel.text = (infoDict[kProductInfoAmountKey] as! String)
        cell.dimensionLabel.text = (infoDict[kProductInfoDimensionKey] as! String)
        cell.qualityLabel.text = (infoDict[kProductInfoDescriptionKey] as! String)
        cell.imageObjects = (infoDict[kProductImageUrlsKey] as! [NSObject])
        if let price = infoDict[kProductInfoPriceKey] as? NSString {
            cell.priceLabel.text = Globals.currencyFormat(NSNumber(value: price.doubleValue as Double))
        }
        
        return cell
    }
    
    private func createOriginalCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "productTableViewCell", for: indexPath)
        
        cell.backgroundColor = UIColor.clear
        
        let imgView = cell.viewWithTag(1) as! UIImageView
        let lblPrice = cell.viewWithTag(2) as! UILabel
        let lblTitle = cell.viewWithTag(3) as! UILabel
        let lblTotalPhoto = cell.viewWithTag(4) as! UILabel
        let lblSize = cell.viewWithTag(5) as! UILabel
        let lblDescription = cell.viewWithTag(6) as! UILabel
        let productSlider = cell.viewWithTag(7) as! BSSlider
        let backgroundView = cell.viewWithTag(10)!
        let categoryproduct = cell.viewWithTag(9) as! UITableViewCell
        let buyView = cell.viewWithTag(11)!
        
        imgView.layer.cornerRadius = 5
        backgroundView.layer.cornerRadius = 5
        buyView.layer.cornerRadius = 5
        
        imgView.isHidden = true
        productSlider.isHidden = false
        var i : CGFloat = 0
        var j = Int()
        let dict = NSMutableDictionary()
        
        let cellInfoArray = self.productDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        
        self.cellArray = cellInfoArray
        arrayData.removeAllObjects()
        
        if let frontImage = cellArray.object(forKey: "front_images") as? String{
            //            dict.setValue(frontImage, forKey: "1")
            galleryFirst = frontImage
        }
        
        if let gallery = cellArray.object(forKey: "gallery_images") as? NSArray{
            
            for item in gallery{
                if let images = (item as AnyObject).object(forKey: "large") as? String{
                    
                    i += 1
                    j = Int(i)
                    
                    var l = ""
                    l = String(j)
                    
                    dict.setValue(images, forKey: l)
                    //                    galleryImages.append(images)
                }
            }
            arrayData.add(dict)
        }
        
        if !slideBool{
            productSlider.setDelegate(self)
        }
        
        if let title = cellInfoArray.object(forKey: "title") as? String {
            lblTitle.text = title
        }
        else {
            lblTitle.text = ""
        }
        if let size = cellInfoArray.object(forKey: "size") as? String {
            lblSize.text = size
        }
        else {
            lblSize.text = ""
        }
        
        if let max_image = cellInfoArray.object(forKey: "max_image") as? String {
            if let min_image = cellInfoArray.object(forKey: "min_image") as? String {
                if min_image == max_image{
                    if min_image == "0" {
                        lblTotalPhoto.text = "1 Photos"
                    }else{
                        lblTotalPhoto.text = min_image + " Photos"
                    }
                }else {
                    lblTotalPhoto.text = min_image + " - " + max_image + " Photos"
                }
            }else {
                lblTotalPhoto.text = max_image + " Photos"
            }
        }else {
            lblTotalPhoto.text = ""
        }
        
        if let info = cellInfoArray.object(forKey: "info") as? String {
            lblDescription.text = info
        }
        else {
            lblDescription.text = ""
        }
        
        var hasDiscount: Bool = false
        if let priceDiscount = cellInfoArray.object(forKey: "special_price") as? NSString {
            if priceDiscount.doubleValue > Double(0) {
                
                hasDiscount = true
                let discountPrice = Globals.currencyFormat(NSNumber(value: priceDiscount.doubleValue as Double))
                var normalPrice = ""
                if let price = cellInfoArray.object(forKey: "price") as? NSString {
                    normalPrice = Globals.currencyFormat(NSNumber(value: price.doubleValue as Double))
                }
                
                let price = NSMutableAttributedString(string: String(format: "%@\n%@", arguments: [normalPrice, discountPrice]))
                
                let normalPriceLength = NSString(string: normalPrice).length
                price.addAttribute(NSStrikethroughStyleAttributeName,
                                   value: NSUnderlineStyle.styleSingle.rawValue,
                                   range: NSRange(location: 0, length: normalPriceLength))
                price.addAttribute(NSForegroundColorAttributeName,
                                   value: UIColor(white: 0.7, alpha: 1),
                                   range: NSRange(location: 0, length: normalPriceLength))
                price.addAttribute(NSFontAttributeName,
                                   value: UIFont(name: lblPrice.font.fontName, size: 12.0)!,
                                   range: NSRange(location: 0, length: normalPriceLength))
                lblPrice.attributedText = price
            }
        }
        
        if !hasDiscount {
            if let price = cellInfoArray.object(forKey: "price") as? NSString {
                lblPrice.text = Globals.currencyFormat(NSNumber(value: price.doubleValue as Double))
            }
        }
        
        return cell
    }
}
