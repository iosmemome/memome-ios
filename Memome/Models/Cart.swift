//
//  Cart.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/26/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import CoreData

@objc(Cart)
class Cart: NSManagedObject {

    @NSManaged var id: Int32
    @NSManaged var projectId: Int32
    @NSManaged var qty: Int32
    @NSManaged var price: Double
    @NSManaged var priceNormal: Double
    @NSManaged var total: Double
    @NSManaged var reorderId: Int32
    @NSManaged var orderId: Int32
    
    override func willSave() {
        //check if id > 0
        var isNew: Bool = true
        if id > 0 {
            isNew = false
        }
        
        if isNew {
            //get the last ID
            let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.managedObjectContext
            let error: NSErrorPointer? = nil
            
            let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cart", in: context)!
            let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
            request.entity = entityDesc
            request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
            var objects: [AnyObject]!
            do {
                objects = try context.fetch(request)
            } catch let error1 as NSError {
                error??.pointee = error1
                objects = nil
            }
            if objects.count == 0 {
                self.id = 1
            }
            else{
                let latestObj: Cart = objects[0] as! Cart
                self.id = latestObj.id + 1
            }
        }
    }
}
