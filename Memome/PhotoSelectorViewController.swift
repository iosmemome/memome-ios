//
//  PhotoSelectorViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/18/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import Photos

class PhotoSelectorViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblPageTitle: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var albumsTableView: UITableView!
    @IBOutlet weak var selectedPhotosCollectionView: UICollectionView!
    
    var mainViewController: UIViewController?
    var albumsDataSource: NSMutableArray! = NSMutableArray()
    var photosDataSource: NSMutableArray! = NSMutableArray()
    var selectedPhotos: NSMutableArray! = NSMutableArray()
    
    var numberOfPhotos: Int = 0
    
    var selectedGroup: PHAssetCollection!
    
    var productData: NSDictionary!
    var productDetails: NSArray!
    
    var fetchOptionsNewest: PHFetchOptions! = PHFetchOptions()
    var posterImageOptions: PHImageRequestOptions! = PHImageRequestOptions()
    var posterImageSize: CGSize!
    var thumbImageOptions: PHImageRequestOptions! = PHImageRequestOptions()
    var thumbImageSize: CGSize!
    
    var selectMode = false
    var swipeSelect = false
    var firstSelectedCell = IndexPath()
    var lastSelectedCell = IndexPath()
    var firstLocation: CGPoint?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.albumsTableView.register(UINib(nibName: "AlbumTableViewCell", bundle: nil), forCellReuseIdentifier: "AlbumTableViewCell")
        self.photosCollectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCollectionViewCell")
        self.selectedPhotosCollectionView.register(UINib(nibName: "SelectedPhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SelectedPhotoCollectionViewCell")
        
        if let number = self.attributes.object(forKey: "numberOfPhotos") as? NSNumber{
            self.numberOfPhotos = number.intValue
        }
        
        setupCollectionView()
        
//        if self.numberOfPhotos == 1 {
            if let height = Globals.getConstraint("height", view: self.selectedPhotosCollectionView){
                height.constant = 0
            }
            self.selectedPhotosCollectionView.clipsToBounds = true
//        }
        
        if let attributes = self.attributes {
            if let data = attributes.object(forKey: "product") as? NSDictionary {
                self.productData = data
            }
            if let details = attributes.object(forKey: "productDetails") as? NSArray {
                self.productDetails = details
            }
        }
        
        self.fetchOptionsNewest.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        self.posterImageOptions.resizeMode = .exact
        self.posterImageOptions.deliveryMode = .opportunistic
        self.posterImageOptions.isSynchronous = false
        self.posterImageSize = CGSize(width: 80 * UIScreen.main.scale, height: 80 * UIScreen.main.scale)
        self.thumbImageOptions.resizeMode = .exact
        self.thumbImageOptions.deliveryMode = .opportunistic
        self.thumbImageOptions.isSynchronous = true
        self.thumbImageSize = CGSize(width: 100 * UIScreen.main.scale, height: 100 * UIScreen.main.scale)
        
        
//        PHPhotoLibrary.sharedPhotoLibrary().registerChangeObserver(self)
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
        {
            self.showAlbums()
            self.loadAlbums()
        }
        else
        {
            PHPhotoLibrary.requestAuthorization(requestAuthorizationHandler)
        }
        
        
    }
    
    func requestAuthorizationHandler(_ status: PHAuthorizationStatus)
    {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
        {
            DispatchQueue.main.async(execute: { () -> Void in
                self.showAlbums()
                self.loadAlbums()
            })
        }
        else
        {
            DispatchQueue.main.async(execute: { () -> Void in
                self.dismissSelf()
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //table view delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.albumsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "AlbumTableViewCell", for: indexPath)
        
        let imageView = cell.viewWithTag(1) as! UIImageView
        let lblTitle = cell.viewWithTag(2) as! UILabel
        let lblCounter = cell.viewWithTag(3) as! UILabel
        
        let groupForCell = self.albumsDataSource.object(at: (indexPath as NSIndexPath).row) as! PHAssetCollection
        
        imageView.image = nil
        self.getPosterImage(groupForCell) { (image) -> Void in
            imageView.image = image
        }
        lblTitle.text = self.isAllPhotos(groupForCell) ? "All Photos" : groupForCell.localizedTitle
        lblCounter.text = String(format: "(%i)", arguments: [self.countAssets(groupForCell)])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row < self.albumsDataSource.count {
            self.showAlbumDetails(self.albumsDataSource.object(at: (indexPath as NSIndexPath).row) as! PHAssetCollection)
        }
    }
    
    //collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.selectedPhotosCollectionView {
            return self.selectedPhotos.count
        }
        else {
            return self.photosDataSource.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let parent = self.mainViewController as? MainPhotoSelectorViewController {
            if collectionView == self.selectedPhotosCollectionView {
                let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedPhotoCollectionViewCell", for: indexPath)
                
                let imageView = cell.viewWithTag(1) as! UIImageView
                let btnDelete = cell.viewWithTag(2) as! EditButton
                
                let asset = self.selectedPhotos.object(at: (indexPath as NSIndexPath).row) as! PHAsset
                PHImageManager.default().requestImage(for: asset, targetSize: self.thumbImageSize, contentMode: .aspectFill, options: self.thumbImageOptions, resultHandler: { (image, info) -> Void in
                    imageView.image = image
                })
                btnDelete.layer.cornerRadius = 15.0
                btnDelete.clipsToBounds = true
                btnDelete.index = (indexPath as NSIndexPath).row
                btnDelete.addTarget(self, action: #selector(PhotoSelectorViewController.btnDeleteSelectedPhotoTouched(_:)), for: UIControlEvents.touchUpInside)
                
                return cell
            }
            else{
                let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath)
                
                let imageView = cell.viewWithTag(1) as! UIImageView
                let lblTitle = cell.viewWithTag(2) as! UILabel
                let imageChecked = cell.viewWithTag(3) as! UIImageView
                let imageWarning = cell.viewWithTag(4) as! UIImageView
                
                let asset = self.photosDataSource.object(at: (indexPath as NSIndexPath).row) as! PHAsset
                PHImageManager.default().requestImage(for: asset, targetSize: self.thumbImageSize, contentMode: .aspectFill, options: self.thumbImageOptions, resultHandler: { (image, info) -> Void in
                    imageView.image = image
                })
                lblTitle.isHidden = true
                imageChecked.isHidden = true
                imageWarning.isHidden = true
                parent.currentselected(asset, completionHandler: { (isHidden, index) in
                    imageChecked.isHidden = isHidden
                })
                
                asset.getSize(completionHandler: { (size) in
                    if size != nil {
                        if size! >= 5 {
                            imageWarning.isHidden = false
                        }
                    }
                    else {
                        imageWarning.isHidden = false
                    }
                })
                //        let selectedIdx = self.selectedPhotos.indexOfObject(indexPath.row)
                //        if selectedIdx != NSNotFound {
                //            lblTitle.hidden = false
                //            if self.numberOfPhotos > 1 {
                //                lblTitle.text = String(format: "%i", arguments: [selectedIdx + 1])
                //            }
                //            else{
                //                lblTitle.text = ""
                //            }
                //        }
                //        else{
                //            lblTitle.hidden = true
                //        }
                
                return cell
            }
        }
        else {
            return UICollectionViewCell()
        }
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        <#code#>
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectionCell(indexPath)
//        if let parent = self.mainViewController as? MainPhotoSelectorViewController {
//            if (indexPath as NSIndexPath).row < self.photosDataSource.count {
//                let asset = self.photosDataSource.object(at: (indexPath as NSIndexPath).row) as! PHAsset
//    //                let copyAsset = asset.copy() as! PHAsset
//                asset.getSize(completionHandler: { (size) in
//                    if size != nil {
//                        print("asset size \(size)")
//                        if size! < 5 {
//                            parent.currentselected(asset, completionHandler: { (isHidden, index) in
//                                if isHidden {
//                                    if parent.selectedPhotos.count < parent.numberOfPhotos {
//                                        parent.selectedPhotos.add(asset)
//                                    }
//                                }
//                                else {
//                                    if index != nil {
//                                        parent.selectedPhotos.removeObject(at: index!)
//                                    }
//                                }
//
//                                if self.numberOfPhotos == 1 {
//                                    parent.btnDoneTouched(parent)
//                                }
//                                else{
//                                    self.photosCollectionView.reloadData()
//                                    //                    self.selectedPhotosCollectionView.reloadData()
//
//                                    //                    self.selectedPhotosCollectionView.scrollToItem(at: IndexPath(row: parent.selectedPhotos.count - 1, section: 0), at: UICollectionViewScrollPosition.right, animated: true)
//                                    parent.updateAlbumDetailTitle()
//                                }
//                            })
//                        }
//                        else {
//                            print("size too large")
//                        }
//                    }
//                    else {
//                        print("there are error for size asset")
//                    }
//                })
////                    parent.selectedPhotos.add(asset)
//            }
//        }
    }
    
    //actions
    func btnDeleteSelectedPhotoTouched(_ sender: EditButton){
        if sender.index < self.selectedPhotos.count {
            self.selectedPhotos.removeObject(at: sender.index)
            self.selectedPhotosCollectionView.reloadData()
            self.updateAlbumDetailTitle()
        }
    }
    
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        if !self.photosCollectionView.isHidden {
            self.showAlbums()
        }
        else{
//            self.dismissSelf()
        }
    }
    @IBAction func btnDoneTouched(_ sender: AnyObject) {
//        if self.selectedPhotos.count < self.numberOfPhotos {
//            let remaining = self.numberOfPhotos - self.selectedPhotos.count
//
//            Globals.showAlertWithTitle("Select More Photos", message: String(format: "Please select %i more photo%@", arguments: [remaining, remaining > 1 ? "s" : ""]), viewController: self)
//        }
//        else{
//            if let parent = self._parentViewController as? ProductCoverViewController {
//                let photos = NSMutableArray()
//                var i = 0
//                let productDetail = self.productDetails.object(at: i) as! NSDictionary
//                for item in self.selectedPhotos {
//                    if let asset = item as? PHAsset {
//                        let temp = NSMutableDictionary()
//                        var offsetLeft: CGFloat = 0.0
//                        var offsetTop: CGFloat = 0.0
//                        let width: CGFloat = CGFloat((productDetail.object(forKey: "content_width") as! NSString).floatValue)
//                        let height: CGFloat = CGFloat((productDetail.object(forKey: "content_height") as! NSString).floatValue)
//                        let imgWidth: CGFloat = CGFloat(asset.pixelWidth)
//                        let imgHeight: CGFloat = CGFloat(asset.pixelHeight)
//                        let scale: CGFloat = max(width / imgWidth, height / imgHeight)
//
//                        offsetLeft = (((imgWidth * scale) - width) / 2.0) / scale
//                        offsetTop = (((imgHeight * scale) - height) / 2.0) / scale
//
//                        temp.setValue(asset.localIdentifier, forKey: "url")
//                        temp.setValue(NSValue(cgSize: CGSize(width: width / scale, height: height / scale)), forKey: "size")
//                        temp.setValue(NSValue(cgPoint: CGPoint(x: offsetLeft, y: offsetTop)), forKey: "offset")
//                        temp.setValue(NSNumber(value: Float(scale) as Float), forKey: "zoomScale")
//                        temp.setValue(NSNumber(value: 0.0 as Float), forKey: "rotation")
//
//                        photos.add( temp )
//                    }
//                    i += 1
//                }
//                parent.setPhotos(photos)
//                self.dismissSelf()
//            }
//            else if let parent = self._parentViewController as? PhotoEditorViewController {
//                for item in self.selectedPhotos {
//                    if let asset = item as? PHAsset {
//                        Globals.imageFromAsset(asset, scale: 1.0, resultHandler: { (image) -> Void in
//                            DispatchQueue.main.async(execute: { () -> Void in
//                                parent.photoCrop.setImage(image)
//                                parent.imageUrl = asset.localIdentifier
//                                parent.image = image
//                                parent.rotation = 0
//                            })
//                        })
//                        self.dismissSelf()
//                    }
//                }
//            }
//            else if let parent = self._parentViewController as? ProfileViewController {
//                for item in self.selectedPhotos {
//                    if let asset = item as? PHAsset {
//                        Globals.imageFromAsset(asset, scale: 1.0, resultHandler: { (image) -> Void in
//                            DispatchQueue.main.async(execute: { () -> Void in
//                                parent.updateProfilePicture(image!)
//                            })
//                        })
//                        self.dismissSelf()
//                    }
//                }
//            }
//        }
    }
    
    
    //general methods
    func setupCollectionView() {
        self.photosCollectionView.canCancelContentTouches = false
        self.photosCollectionView.allowsMultipleSelection = true
        //        longpressGesture = UILongPressGestureRecognizer(target: self, action: #selector(didLongpress))
        //        longpressGesture?.minimumPressDuration = 0.15
        //        longpressGesture?.delaysTouchesBegan = true
        //        longpressGesture?.delegate = self
        //        self.photosCollectionView.addGestureRecognizer(longpressGesture!)
        
//        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(didPan(toSelectCells:)))
//        panGesture.delegate = self
//        self.photosCollectionView.addGestureRecognizer(panGesture)
    }
    
    func selectCell(_ indexPath: IndexPath, before lastIndexPath: IndexPath) {
        selectCell(indexPath, before: lastIndexPath, first: false)
    }
    
    func selectCell(_ indexPath: IndexPath, before lastIndexPath: IndexPath, first selection: Bool) {
        if !lastIndexPath.isEmpty && abs(indexPath.item - lastIndexPath.item) > 1 {
            if (indexPath.item >= firstSelectedCell.item) {
                if (indexPath.item >= lastIndexPath.item) {
                    for i in (lastIndexPath.item+1)...indexPath.item {
                        let index = IndexPath(item: i, section: indexPath.section)
                        if (!selection && index.item != firstSelectedCell.item) {
                            self.selectionCell(index)
                        }
                    }
                }
                else {
                    for i in (indexPath.item+1)...lastIndexPath.item {
                        let index = IndexPath(item: i, section: indexPath.section)
                        if (!selection && index.item != firstSelectedCell.item) {
                            self.selectionCell(index)
                        }
                    }
                }
            }
            else {
                if (indexPath.item >= lastIndexPath.item) {
                    for i in lastIndexPath.item..<indexPath.item {
                        let index = IndexPath(item: i, section: indexPath.section)
                        if (!selection && index.item != firstSelectedCell.item) {
                            self.selectionCell(index)
                        }
                    }
                }
                else {
                    for i in indexPath.item..<lastIndexPath.item {
                        let index = IndexPath(item: i, section: indexPath.section)
                        if (!selection && index.item != firstSelectedCell.item) {
                            self.selectionCell(index)
                        }
                    }
                }
            }
        }
        else {
            if (selection || indexPath.item != firstSelectedCell.item) {
                self.selectionCell(indexPath)
            }
        }
        
        self.photosCollectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
    }
    
    func selectionCell(_ indexPath: IndexPath) {
        if let parent = self.mainViewController as? MainPhotoSelectorViewController {
            if (indexPath as NSIndexPath).row < self.photosDataSource.count {
                let asset = self.photosDataSource.object(at: (indexPath as NSIndexPath).row) as! PHAsset
    //                let copyAsset = asset.copy() as! PHAsset
                asset.getSize(completionHandler: { (size) in
                    if size != nil {
                        print("asset size \(size)")
                        if size! < 5 {
                            parent.currentselected(asset, completionHandler: { (isHidden, index) in
                                if isHidden {
                                    if parent.selectedPhotos.count < parent.numberOfPhotos {
                                        parent.selectedPhotos.add(asset)
                                    }
                                }
                                else {
                                    if index != nil {
                                        parent.selectedPhotos.removeObject(at: index!)
                                    }
                                }

                                if self.numberOfPhotos == 1 {
                                    parent.btnDoneTouched(parent)
                                }
                                else{
                                    self.photosCollectionView.reloadItems(at: [indexPath])
                                    //                    self.selectedPhotosCollectionView.reloadData()

                                    //                    self.selectedPhotosCollectionView.scrollToItem(at: IndexPath(row: parent.selectedPhotos.count - 1, section: 0), at: UICollectionViewScrollPosition.right, animated: true)
                                    parent.updateAlbumDetailTitle()
                                }
                            })
                        }
                        else {
                            Globals.showAlertWithTitle("Sorry", message: "Cannot select image size larger than 5MB", viewController: parent)
                            print("size too large")
                        }
                    }
                    else {
                        print("there are error for size asset")
                        Globals.showAlertWithTitle("Sorry", message: "Cannot select this image", viewController: parent)
                    }
                })
//                    parent.selectedPhotos.add(asset)
            }
        }
    }
    
    func didPan(toSelectCells panGesture: UIPanGestureRecognizer) {
        if panGesture.state == .began {
            firstLocation = panGesture.location(in: self.photosCollectionView)
        }
        else if panGesture.state == .changed {
            let location: CGPoint = panGesture.location(in: self.photosCollectionView)
            print("did pan \(abs(location.x - (firstLocation?.x)!))")
            print("did pan \(swipeSelect)")
            if !swipeSelect {
                if abs(location.x - (firstLocation?.x)!) >= 20 {
                    print("did pan got it")
                    self.photosCollectionView.isUserInteractionEnabled = false
                    self.photosCollectionView.isScrollEnabled = false
                    swipeSelect = true
                    
                    if let indexPath: IndexPath = self.photosCollectionView.indexPathForItem(at: location) {
                        firstSelectedCell = indexPath
                        lastSelectedCell = indexPath
                        self.selectCell(indexPath, before: lastSelectedCell, first: true)
                    }
                }
            }
            else {
                if let indexPath: IndexPath = self.photosCollectionView.indexPathForItem(at: location) {
                    if indexPath != lastSelectedCell {
                        self.selectCell(indexPath, before: lastSelectedCell)
                        lastSelectedCell = indexPath
                    }
                }
            }
        } else if panGesture.state == .ended {
            self.photosCollectionView.isScrollEnabled = true
            self.photosCollectionView.isUserInteractionEnabled = true
            swipeSelect = false
        }
    }
    
    func showAlbums(){
        self.photosCollectionView.isHidden = true
        self.btnBack.isHidden = true
        self.btnDone.isHidden = true
        self.albumsTableView.isHidden = false
//        self.selectedPhotos.removeAllObjects()
        self.photosDataSource.removeAllObjects()
        
        self.lblPageTitle.text = "Select Photos"
    }
    
    func showAlbumDetails(_ group: PHAssetCollection){
        self.selectedGroup = group
        
        self.photosCollectionView.isHidden = false
        self.btnBack.isHidden = false
        self.btnDone.isHidden = true
        self.albumsTableView.isHidden = true
//        self.selectedPhotos.removeAllObjects()
        self.photosDataSource.removeAllObjects()
        
        self.updateAlbumDetailTitle()
        
        self.loadAlbumDetail(group)
    }
    
    func updateAlbumDetailTitle(){
        var title = "Select Photos"
        if let t = self.selectedGroup.localizedTitle {
            title = t
        }
        self.lblPageTitle.text = String(format: "%@", arguments: [title])
    }
    
    func loadAlbums() {
        self.albumsDataSource.removeAllObjects()
        self.albumsDataSource.add( PHAssetCollection() )
        
        let smartAlbums: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumRegular, options: nil)
        let syncedAlbums: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumSyncedAlbum, options: nil)
        let userCollections: PHFetchResult = PHAssetCollection.fetchTopLevelUserCollections(with: nil)
//        let userAlbumsOptions = PHFetchOptions();
//        userAlbumsOptions.predicate = NSPredicate(format: "estimatedAssetCount > 0");
//        let userCollections: PHFetchResult = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options: userAlbumsOptions)
        
        for i in 0 ..< smartAlbums.count {
            if let collection = smartAlbums[i] as? PHAssetCollection {
                if self.countAssets(collection) > 0 {
                    self.albumsDataSource.add(collection)
                }
            }
        }
        for i in 0 ..< syncedAlbums.count {
            if let collection = syncedAlbums[i] as? PHAssetCollection {
                if self.countAssets(collection) > 0 {
                    self.albumsDataSource.add(collection)
                }
            }
        }
        for i in 0 ..< userCollections.count {
            if let collection = userCollections[i] as? PHAssetCollection {
                if self.countAssets(collection) > 0 {
                    self.albumsDataSource.add(collection)
                }
            }
        }
        
        self.albumsTableView.reloadData()
        
    }
    
    func loadAlbumDetail(_ group: PHAssetCollection){
        let options = PHFetchOptions()
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        options.sortDescriptors = [sortDescriptor]
//        with: PHAssetMediaType.image
        let assets: PHFetchResult = !self.isAllPhotos(group) ? PHAsset.fetchAssets(in: group, options: options) : PHAsset.fetchAssets(with: PHAssetMediaType.image, options: options)
        for i in 0 ..< assets.count {
            if let asset = assets[i] as? PHAsset {
                self.photosDataSource.add(asset)
            }
        }
        
        self.photosCollectionView.reloadData()
    }
    
    func getPosterImage(_ group: PHAssetCollection, resultHandler: @escaping (_ image:UIImage)->Void){
        let assets: PHFetchResult<AnyObject>
        if self.isAllPhotos(group) {
            assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: self.fetchOptionsNewest) as! PHFetchResult<AnyObject>
        }
        else{
            assets = PHAsset.fetchAssets(in: group, options: self.fetchOptionsNewest) as! PHFetchResult<AnyObject>
        }
        if let asset = assets.firstObject as? PHAsset {
            PHImageManager.default().requestImage(
                for: asset,
                targetSize: self.posterImageSize,
                contentMode: .aspectFill,
                options: self.posterImageOptions,
                resultHandler: { (image, info) -> Void in
                    resultHandler(image!)
            })
        }
    }
    
    func countAssets(_ group: PHAssetCollection) -> Int {
        if self.isAllPhotos(group) {
            let assets: PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
            return assets.count
        }
        else{
            let assets: PHFetchResult = PHAsset.fetchAssets(in: group, options: nil)
            return assets.count
        }
        
    }
    
    func isAllPhotos(_ collection: PHAssetCollection) -> Bool {
        return collection.localizedTitle == nil || collection.localizedTitle == ""
    }
    
    // uigesture
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

