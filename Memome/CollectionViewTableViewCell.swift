//
//  CollectionViewTableViewCell.swift
//  IRG
//
//  Created by Stillalive on 22/10/17.
//  Copyright © 2017 Stillalive. All rights reserved.
//

import UIKit

protocol CollectionViewTableViewCellDelegate {
    func collectionViewTableViewCell(cell: CollectionViewTableViewCell, didSelectItemAt indexPath: IndexPath)
}

class CollectionViewTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    struct Mode {
        static let carousel = "carousel"
        static let menu = "menu"
    }
    
    var delegate: CollectionViewTableViewCellDelegate?
    var mode = Mode.carousel
    
    var mediaUrls = [String]()
    var cellSize: CGSize?
    
    fileprivate var timer: Timer!
    
    
    // MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        
        timer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        collectionView.reloadData()
    }
    
    @objc fileprivate func timerFired() {
        if mediaUrls.count > 0 {
            var nextPosition = pageControl.currentPage + 1
            if nextPosition >= mediaUrls.count { nextPosition = 0 }
            
            pageControl.currentPage = nextPosition
            pageControlValueChanged(pageControl)
        }
    }
    
    
    // MARK: - Public Methods
    func setupPageControl() {
        if mode == Mode.carousel {
            pageControl.numberOfPages = mediaUrls.count
            
            if mediaUrls.count > 1 {
                pageControl.isHidden = false
            }else {
                pageControl.isHidden = true
            }
        }else if mode == Mode.menu {
            pageControl.isHidden = true
        }
        
    }
    
    @IBAction func pageControlValueChanged(_ sender: Any) {
        collectionView.scrollToItem(at: IndexPath.init(row: pageControl.currentPage, section: 0), at: .centeredHorizontally, animated: true)
    }
}

extension CollectionViewTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - Collection view data source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch mode {
        case Mode.carousel:
            return mediaUrls.count
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize ?? collectionView.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch mode {
        case Mode.carousel:
            let urlString = mediaUrls[indexPath.row]
           
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! SingleImageCollectionViewCell
                return cell
            
        default:
            return UICollectionViewCell()
        }
    }
    
    // MARK: - Collection view delegate
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.collectionViewTableViewCell(cell: self, didSelectItemAt: indexPath)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let xAxis = scrollView.contentOffset.x
        
        if xAxis > 0 {
            let position = xAxis/scrollView.bounds.width
            pageControl.currentPage = Int(position)
        }else {
            pageControl.currentPage = 0
        }
    }
}
