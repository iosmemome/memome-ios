//
//  CategoryTableViewAdapter.swift
//  Memome
//
//  Created by iOS Developer on 08/12/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import UIKit

class CategoryTableViewAdapter: NSObject, UITableViewDataSource, UITableViewDelegate {
    var productDataSource: NSMutableArray! = NSMutableArray()
    var _parentViewController: BaseViewController!
    var tableView: UITableView!
    
    var voucher: NSDictionary!
    var voucherCode: String = ""
    
    func setDataSource(_ data: NSArray) {
        self.productDataSource.removeAllObjects()
        self.productDataSource.addObjects(from: data as [AnyObject])
    }
    
    func getDataSource() -> NSArray {
        return self.productDataSource
    }
    
    func setParentViewController(_ vc: BaseViewController) {
        self._parentViewController = vc
    }
    
    func getParentViewController() -> BaseViewController {
        return self._parentViewController
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.tableView.frame.size.width ) + 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "categoryTableViewCell", for: indexPath)
        
        cell.backgroundColor = UIColor.clear
        
        let imgView = cell.viewWithTag(1) as! UIImageView
        let lblTitle = cell.viewWithTag(2) as! UILabel
        
//        let backgroundView = cell.viewWithTag(10)!
//        let titleBackgroundView = cell.viewWithTag(11)!
        
//        imgView.layer.cornerRadius = 5
//        backgroundView.layer.cornerRadius = 5
//        titleBackgroundView.layer.cornerRadius = titleBackgroundView.frame.height / 2
        
        let cellInfoArray = self.productDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        
        if let title = cellInfoArray.object(forKey: "name") as? String {
            lblTitle.text = title
        }
        else {
            lblTitle.text = ""
        }
        
//        if let product = cellInfoArray.object(forKey: "product") as? AnyObject {
            if let image = cellInfoArray.object(forKey: "photo") as? String {
                Globals.setImageFromUrl(image, imageView: imgView, placeholderImage: nil)
            }
            else{
                imgView.image = nil
            }
//        }
//        else{
//            imgView.image = nil
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row < self.productDataSource.count {
            let cellInfoArray = self.productDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
            let attributes = NSMutableDictionary(dictionary: [
                "title" : cellInfoArray.object(forKey: "name") as? String ?? "Detail",
//                "productList" : cellInfoArray.object(forKey: "productList") as? [AnyObject] ?? [],
                "categoryId" : cellInfoArray.object(forKey: "id") as? String ?? ""
                ])
            
            if self.voucherCode != "" {
                attributes.setValue(self.voucherCode, forKey: "voucherCode")
                attributes.setValue(self.voucher, forKey: "voucher")
            }
            self._parentViewController.showViewController("MakePrintDetailViewController", attributes: attributes)
        }
        
    }
    
}

