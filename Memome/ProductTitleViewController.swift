//
//  ProductTitleViewController.swift
//  Memome
//
//  Created by iOS Developer on 30/11/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import UIKit

class ProductTitleViewController: BaseViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var coverScrollView: UIScrollView!
    @IBOutlet weak var coverContentScrollView: UIView!
    
//    @IBOutlet weak var coverView: UIView!
//    @IBOutlet weak var coverViewBackground: UIImageView!
//    @IBOutlet weak var coverImageView: UIImageView!
//    @IBOutlet weak var btnCoverImage: UIButton!
//    @IBOutlet weak var coverTitleView: UIView!
//    @IBOutlet weak var coverTitleInnerView: UIView!
//    @IBOutlet weak var lblCoverTitle: UILabel!
//    @IBOutlet weak var lblCoverSubtitle: UILabel!
    
    @IBOutlet weak var txtTitleWrapper: UIView!
    @IBOutlet weak var txtSubtitleWrapper: UIView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtSubtitle: UITextField!
    
    @IBOutlet weak var btnChangeCoverImage: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    var productData: NSDictionary!
    var productDetails: NSArray!
    var productDetailsDataSource: NSMutableArray! = NSMutableArray()
    var ratio: CGFloat = 1
    var isTitleRequired: Bool = true
    var isCoverImageRequired: Bool = true
    
    var corporateVoucherId: Int = 0
    var corporateVoucherName: String = ""
    var corporateVoucherQty: Int = 0
    var corporateVoucherCode: String = ""
    var corporateVoucher: NSDictionary = NSDictionary()
    
    var project: Project?
    var projectDetails: NSArray?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.defaultScrollView = self.coverScrollView
        self.defaultContentScrollView = self.coverContentScrollView
        
        if let attributes = self.attributes {
            self.productData = attributes.object(forKey: "product") as! NSDictionary
            self.productDetails = attributes.object(forKey: "productDetails") as? NSArray
            
            if let voucher = attributes.object(forKey: "voucher") as? NSDictionary {
                if let id = voucher.object(forKey: "id") as? NSString {
                    self.corporateVoucherId = id.integerValue
                }
                else if let id = voucher.object(forKey: "id") as? NSNumber {
                    self.corporateVoucherId = id.intValue
                }
                
                if let qty = voucher.object(forKey: "qty") as? NSString {
                    self.corporateVoucherQty = qty.integerValue
                }
                else if let qty = voucher.object(forKey: "qty") as? NSNumber {
                    self.corporateVoucherQty = qty.intValue
                }
                
                if let name = voucher.object(forKey: "name") as? String {
                    self.corporateVoucherName = name
                }
                
                self.corporateVoucher = voucher
            }
            
            if let code = attributes.object(forKey: "voucherCode") as? String {
                self.corporateVoucherCode = code
            }
            
            if let proj = attributes.object(forKey: "project") as? Project{
                self.project = proj
            }
            if let details = attributes.object(forKey: "projectDetails") as? NSArray {
                self.projectDetails = details
            }
        }
        
        //        self.coverView.layer.borderColor = UIColor(white: 0.25, alpha: 1).cgColor
        //        self.coverView.layer.borderWidth = 1
        
//        self.coverTitleInnerView.layer.borderColor = UIColor(white: 0.57, alpha: 1).cgColor
//        self.coverTitleInnerView.layer.borderWidth = 1
        
        self.btnChangeCoverImage.layer.borderColor = UIColor.darkGray.cgColor//UIColor(red: 80.0/255.0, green: 210.0/255.0, blue: 194.0/255.0, alpha: 1).cgColor
        self.btnChangeCoverImage.layer.borderWidth = 1.0
        self.btnChangeCoverImage.layer.cornerRadius = 5//self.btnChangeCoverImage.frame.size.height / 2.0
        
        if let isRequireTitle = self.productData.object(forKey: "is_require_title") as? NSNumber {
            self.isTitleRequired = isRequireTitle.intValue == 1
        }
        else if let isRequireTitle = self.productData.object(forKey: "is_require_title") as? NSString {
            self.isTitleRequired = isRequireTitle.integerValue == 1
        }
        
//        self.coverView.isHidden = true
        
        if let project = self.project {
            self.txtTitle.text = project.title
            
            self.txtSubtitle.text = project.subtitle
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        let coverWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_width") as! NSString).floatValue)
//        self.ratio = MemomeHelper.calculateRatio(coverWidth, targetSize: self.coverView.frame.size.width)
        
//        self.setupCoverView()
        
        
//        if let coverImage = MemomeHelper.coverImage {
//            if let url = coverImage.object(forKey: "url") as? String {
//                var x: CGFloat = 0.0
//                var y: CGFloat = 0.0
//                var width: CGFloat = 0.0
//                var height: CGFloat = 0.0
//                var rotation: Int = 0
//                var scale: CGFloat = 1
//                if let zoomScale = coverImage.object(forKey: "zoomScale") as? NSNumber {
//                    scale = CGFloat(zoomScale.floatValue)
//                }
//                if let size = coverImage.object(forKey: "size") as? NSValue {
//                    width = size.cgSizeValue.width * scale
//                    height = size.cgSizeValue.height * scale
//                }
//                if let offset = coverImage.object(forKey: "offset") as? NSValue {
//                    x = offset.cgPointValue.x * scale
//                    y = offset.cgPointValue.y * scale
//                }
//                if let rotate = coverImage.object(forKey: "rotation") as? NSNumber {
//                    rotation = rotate.intValue
//                }
//                let screenScale = max( self.coverImageView.frame.size.width / width, self.coverImageView.frame.size.height / height )
//                let cropRect = CGRect(x: x * screenScale, y: y * screenScale, width: width * screenScale, height: height * screenScale)
//
//                Globals.checkImageFrom(url, scale: scale * screenScale, resultHandler: { (image) -> Void in
//                    if let img = image {
//                        self.coverImageView.image = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect)
//                    }
//                })
//            }
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtTitle {
            self.txtSubtitle.becomeFirstResponder()
        }
        else if textField == self.txtSubtitle {
            self.btnNextTouched(self.btnNext)
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtTitle {
        let maxAllowedCharactersPerLine = 12
        let lines = (textField.text! as NSString).replacingCharacters(in: range, with: string).components(separatedBy: .newlines)
        for line in lines{
            if line.count > maxAllowedCharactersPerLine{
                return false
            }
        }
        }
        if textField == self.txtSubtitle {
            let maxAllowedCharactersPerLine = 30
            let lines = (textField.text! as NSString).replacingCharacters(in: range, with: string).components(separatedBy: .newlines)
            for line in lines{
                if line.count > maxAllowedCharactersPerLine{
                    return false
                }
            }
        }
        return true
        
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func txtTitleEditingChanged(_ sender: AnyObject) {
//        self.lblCoverTitle.text = self.txtTitle.text
    }
    
    @IBAction func txtSubtitleEditingChanged(_ sender: AnyObject) {
//        self.lblCoverSubtitle.text = self.txtSubtitle.text
    }
    
    // Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        MemomeHelper.coverImage = nil
        self.dismissSelf()
    }
    
    @IBAction func btnCoverImageTouched(_ sender: AnyObject) {
        let attributes: NSMutableDictionary = NSMutableDictionary()
        let coverContentWidth: Float = (self.productData.object(forKey: "cover_content_width") as! NSString).floatValue
        let coverContentHeight: Float = (self.productData.object(forKey: "cover_content_height") as! NSString).floatValue
        attributes.setValue(NSNumber(value: coverContentWidth/coverContentHeight as Float), forKey: "imageRatio")
        if let coverImage = MemomeHelper.coverImage {
            attributes.setValue(coverImage, forKey: "initialImage")
        }
        self.showViewController("PhotoEditorViewController", attributes: attributes)
    }
    
    @IBAction func btnChangeCoverImageTouched(_ sender: AnyObject) {
        self.btnCoverImageTouched(sender)
    }
    
    @IBAction func btnNextTouched(_ sender: AnyObject) {
        
//        if self.isCoverImageRequired && MemomeHelper.coverImage == nil {
//            Globals.showAlertWithTitle("Image is Required", message: "Please choose an image for your cover", viewController: self)
//        }
//        else
        if self.isTitleRequired && self.txtTitle.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
            Globals.showAlertWithTitle("Title is Required", message: "Please set a title", viewController: self)
        }
//        else if self.isTitleRequired && self.txtSubtitle.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
//            Globals.showAlertWithTitle("Subtitle is Required", message: "Please set a subtitle", viewController: self)
//        }
        else{
//            if MemomeHelper.coverImage == nil {
//                MemomeHelper.coverImage = NSMutableDictionary()
//            }
//            MemomeHelper.coverImage.setValue(self.txtTitle.text, forKey: "title")
//            MemomeHelper.coverImage.setValue(self.txtSubtitle.text, forKey: "subtitle")
            
//            if let _ = self.project {
//                self.goToPhotoPreview()
//            }
//            else{
//                if self.productDetails == nil {
//                    self.loadProductData()
//                }
//                else {
                    let attributes = NSMutableDictionary(dictionary: [
                        "product" : self.productData,
                        "title" : (self.txtTitle.text ?? "").trim(),
                        "subtitle" : (self.txtSubtitle.text ?? "").trim()
//                        "productDetails" : self.productDetails,
//                        "numberOfPhotos" : NSNumber(value: self.productDetails.count as Int)
                        ])
                    if let proj = self.project {
                        attributes.setValue(proj, forKey: "project")
                    }
                    if let details = self.projectDetails {
                        attributes.setValue(details, forKey: "projectDetails")
                    }
                    if self.corporateVoucherId > 0 {
                        attributes.setValue(self.corporateVoucherCode, forKey: "voucherCode")
                        attributes.setValue(self.corporateVoucher, forKey: "voucher")
                    }
                    self.showViewController("ProductCoverViewController", attributes: attributes)
                }
//            }
//        }
    }
    
    // General Method
    func goToPhotoPreview(){
        let attributes = NSMutableDictionary(dictionary: [
            "product" : self.productData,
            "productDetails" : self.productDetails
            ])
        if let proj = self.project {
            attributes.setValue(proj, forKey: "project")
        }
        if let details = self.projectDetails {
            attributes.setValue(details, forKey: "projectDetails")
        }
        if self.corporateVoucherId > 0 {
            attributes.setValue(self.corporateVoucherCode, forKey: "voucherCode")
            attributes.setValue(self.corporateVoucher, forKey: "voucher")
        }
        self.showViewController("ProductPhotosViewController", attributes: attributes)
    }
    
    func loadProductData(){
        if !self.isLoading() {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("products/view"),
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                "clientId" : Globals.getProperty("clientId"),
                "productId" : self.productData.object(forKey: "id") as! String
                ],
                completion: { (result) -> Void in
                self.hideLoading()
                
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    self.productDetailsDataSource.removeAllObjects()
                    
                    if let productDetails = result.object(forKey: "details") as? NSArray {
                        self.productDetailsDataSource.addObjects(from: productDetails as [AnyObject])
                    }
                    
                    if let product = result.object(forKey: "product") as? NSDictionary {
                        self.productData = product
                    }
                    
                    self.productDetails = self.productDetailsDataSource
                    
                    let attributes = NSMutableDictionary(dictionary: [
                        "product" : self.productData,
                        "title" : (self.txtTitle.text ?? "").trim(),
                        "subtitle" : (self.txtSubtitle.text ?? "").trim()
//                        "productDetails" : self.productDetails,
//                        "numberOfPhotos" : NSNumber(value: self.productDetails.count as Int)
                        ])
                    if let proj = self.project {
                        attributes.setValue(proj, forKey: "project")
                    }
                    if let details = self.projectDetails {
                        attributes.setValue(details, forKey: "projectDetails")
                    }
                    if self.corporateVoucherId > 0 {
                        attributes.setValue(self.corporateVoucherCode, forKey: "voucherCode")
                        attributes.setValue(self.corporateVoucher, forKey: "voucher")
                    }
                    self.showViewController("ProductCoverViewController", attributes: attributes)
                }
                else{
                    Globals.showAlertWithTitle("Products Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
            })
        }
    }
}
