//
//  WebViewController.swift
//  Kreate
//
//  Created by staff on 6/24/15.
//  Copyright (c) 2015 Trio Digital Agency. All rights reserved.
//

import UIKit

class WebViewController: BaseViewController, UIWebViewDelegate {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var webView: UIWebView!
    
    var requestCode: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setViewStyle()
        
        if let code = self.attributes.object(forKey: "requestCode") as? Int {
            self.requestCode = code
        }
        
        self.webView.delegate = self
        let requestObj: URLRequest = URLRequest(url: URL(string: self.attributes.object(forKey: "url") as! String)!)
        self.webView.loadRequest(requestObj)
    }
    
    func setViewStyle(){
        self.btnBack.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        if let title: String = self.attributes.object(forKey: "title") as? String {
            self.lblTitle.text = title
        }
        else{
            self.lblTitle.text = ""
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Web View Delegate
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        //        NSLog("webview shouldStartLoadWithRequest")
        //        if let url = request.URL {
        //            NSLog("webview url request : %@", url.absoluteString)
        //            if url.scheme == "paymentcomplete" {
        //                NSLog("webview payment complete : %@", url.host!)
        //                if url.host! == "clearCart" {
        //                    self._parentViewController.paymentCompleteAndClearCart(self, requestCode: self.requestCode)
        //                }
        //            }
        //        }
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.showLoading()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        //        NSLog("webview did finish load : %@", webView.request!.URL!.absoluteString)
        self.hideLoading()
        if let parent = self._parentViewController {
            parent.webViewDidLoad(self, webView: webView, requestCode: self.requestCode)
        }
    }
    
    
    // MARK: - Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
}
