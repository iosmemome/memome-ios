//
//  CheckoutReviewViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 2/25/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import MidtransKit

class CheckoutReviewViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, MidtransUIPaymentViewControllerDelegate {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    
    @IBOutlet weak var lblShippingName: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    
    @IBOutlet weak var shippingMethodTableView: UITableView!
    @IBOutlet weak var orderDetailTableView: UITableView!
    @IBOutlet weak var lblTotalItems: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblShippingPrice: UILabel!
    @IBOutlet weak var lblCouponPrice: UILabel!
    @IBOutlet weak var lblCoupon: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var checkoutScrollView: UIScrollView!
    @IBOutlet weak var checkoutContentScrollView: UIView!
    
    @IBOutlet weak var paymentDetailTitleWrapper: UIView!
    @IBOutlet weak var paymentMethodWrapper: UIView!
    
    var cartDataSource: NSMutableArray! = NSMutableArray()
    var shippingMethodDataSource: NSMutableArray! = NSMutableArray()
    
    var address: NSDictionary?
    var coupon: NSDictionary?
    
    var selectedShippingMethod = -1
    
    var total: Double = 0
    var shippingCost: Double = 0
    var couponDisc: Double = 0
    var couponPercent: Double = 0
    var grandTotal: Double = 0
    
    var paymentType = Constant.paymentTransfer
    var paymentName = Constant.paymentTransferName
    var pDisc: Double = 0
    
    var minTransaction: Double = 10000.0 // min transfer BCA
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        payment(String(Date().timeIntervalSince1970))

        // Do any additional setup after loading the view.
        MidtransConfig.shared().setClientKey("VT-client-cwmvxnYb-CTkaAgz", environment: .sandbox, merchantServerURL: "https://demo-merchant-server.herokuapp.com")

        
        if self.attributes != nil {
            if let addr = self.attributes.object(forKey: "address") as? NSDictionary {
                self.address = addr
            }
            
            if let cp = self.attributes.object(forKey: "coupon") as? NSDictionary {
                self.coupon = cp
                
                if let price = cp.object(forKey: "price") as? NSString {
                    if price.doubleValue > 0 {
                        self.couponDisc = price.doubleValue
                    }
                }
                else if let price = cp.object(forKey: "price") as? NSNumber {
                    if price.doubleValue > 0 {
                        self.couponDisc = price.doubleValue
                    }
                }
                
                if let percent = cp.object(forKey: "percent") as? NSString {
                    if percent.doubleValue > 0 {
                        self.couponPercent = percent.doubleValue
                    }
                }
                else if let percent = cp.object(forKey: "percent") as? NSNumber {
                    if percent.doubleValue > 0 {
                        self.couponPercent = percent.doubleValue
                    }
                }
            }
            
            if let type = self.attributes.object(forKey: "paymentType") as? String {
                self.paymentType = type
            }
            
            if let name = self.attributes.object(forKey: "paymentName") as? String {
                self.paymentName = name
            }
        }
        
        self.orderDetailTableView.register(UINib(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: "cartTableViewCell")
        self.shippingMethodTableView.register(UINib(nibName: "ShippingMethodTableViewCell", bundle: nil), forCellReuseIdentifier: "shippingMethodTableViewCell")
        
        self.initRefreshControl(self.checkoutScrollView)
        
        if self.isJustCorporate() {
            self.paymentMethodWrapper.isHidden = true
            self.paymentDetailTitleWrapper.isHidden = true
            if let height = Globals.getConstraint("height", view: self.paymentMethodWrapper) {
                height.constant = 0
            }
            if let height = Globals.getConstraint("height", view: self.paymentDetailTitleWrapper) {
                height.constant = 0
            }
        }
        
        self.countTotal()
        
        self.loadPaymentMethod()
        self.loadShippingDetail()
        self.loadDataCart()
//        self.loadDataShippingMethod() // move inside loadDataCart()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func refresh(_ sender: AnyObject) {
        super.refresh(sender)
        
        self.countTotal()
        
        self.loadPaymentMethod()
        self.loadShippingDetail()
        self.loadDataCart()
//        self.loadDataShippingMethod() // move inside loadDataCart()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //Table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.orderDetailTableView {
            return self.cartDataSource.count
        }
        else if tableView == self.shippingMethodTableView {
            return self.shippingMethodDataSource.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.orderDetailTableView {
            return self.getOrderDetailCellHeight(indexPath)
        }
        else if tableView == self.shippingMethodTableView {
            return self.getShippingMethodCellHeight(indexPath)
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.orderDetailTableView {
            return self.getOrderDetailCell(indexPath)
        }
        else if tableView == self.shippingMethodTableView {
            return self.getShippingMethodCell(indexPath)
        }
        else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.orderDetailTableView {
            
        }
        else if tableView == self.shippingMethodTableView {
            self.shippingCost = 0
            if (indexPath as NSIndexPath).row < self.shippingMethodDataSource.count {
                self.selectedShippingMethod = (indexPath as NSIndexPath).row
                self.reloadShippingMethodTableView()
                
                if let cellInfoArray = self.shippingMethodDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
                    if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                        if prices.count > 0 {
                            if let price = prices.object(at: 0) as? NSDictionary {
                                if let value = price.object(forKey: "value") as? NSNumber {
                                    self.shippingCost = value.doubleValue
                                }
                            }
                        }
                    }
                }
            }
            
            self.countTotal()
        }
    }
    
    //Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnSubmitTouched(_ sender: AnyObject) {
        
        if ControllerHelper.getHomeViewController()!.isUploading {
            Globals.showAlertWithTitle("Memome is uploading your images", message: "Sorry, we could not continue to check this order out. Currently memome is busy uploading your previous order. Please wait until your images uploaded, then you can check this order out.", viewController: self)
        }
        else {
            var isGood = true
            
            if isGood && self.selectedShippingMethod == -1 {
                Globals.showAlertWithTitle("Checkout Error", message: "Please select a shipping method.", viewController: self)
                isGood = false
            }
            
            if isGood && self.grandTotal < self.minTransaction && !self.isJustCorporate() {
                Globals.showAlertWithTitle("Checkout Error", message: String(format: "Min transaction is IDR %@", arguments: [Globals.numberFormat(NSNumber(value: self.minTransaction as Double))]), viewController: self)
                isGood = false
            }
            
            if isGood {
                Globals.showConfirmAlertWithTitle("Checkout", message: "Are you sure you have entered your data correctly?", viewController: self, completion: { (action) -> Void in
                    self.payment(String(Date().timeIntervalSince1970))
                })
            }
        }
        
    }
    
    // general methods
    
    func countTotal() {
        var disc: Double = 0
        if self.couponDisc > 0 {
            disc = self.couponDisc
        }
        else if self.couponPercent > 0 {
            disc = self.couponPercent * (self.total) / 100
        }
        self.pDisc = disc
        
        self.grandTotal = self.total + self.shippingCost - disc
        
        if disc == 0 {
            if let height = Globals.getConstraint("height", view: self.lblCouponPrice){
                height.constant = 0
            }
            
            if let height = Globals.getConstraint("height", view: self.lblCoupon){
                height.constant = 0
            }
        }
        else{
            if let height = Globals.getConstraint("height", view: self.lblCouponPrice){
                height.constant = 18
            }
            
            if let height = Globals.getConstraint("height", view: self.lblCoupon){
                height.constant = 18
            }
        }
        
        self.lblTotalPrice.text = "IDR " + Globals.numberFormat(NSNumber(value: self.total as Double))
        self.lblShippingPrice.text = "IDR " + Globals.numberFormat(NSNumber(value: self.shippingCost as Double))
        self.lblCouponPrice.text = "-IDR " + Globals.numberFormat(NSNumber(value: disc as Double))
        self.lblGrandTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: self.grandTotal as Double))
    }
    
    func loadDataCart() {
        self.cartDataSource.removeAllObjects()
        
        self.cartDataSource.addObjects(from: CartHelper.getCart() as [AnyObject])
        self.lblTotalItems.text = String(format: "%i Item%@", arguments: [self.cartDataSource.count, (self.cartDataSource.count > 1 ? "s" : "")])
        
        self.total = 0
        for item in self.cartDataSource {
            if let cart = item as? Cart {
                self.total += cart.total
                
                if let project = ProjectHelper.getProject(cart.projectId) {
                    if project.corporateVoucherId > 0 {
                        total -= Double(project.corporateVoucherQty) * cart.price
                    }
                }
            }
        }
        
        self.reloadOrderDetailTableView()
        
        self.countTotal()
        
        self.loadDataShippingMethod()
    }
    
    func loadDataShippingMethod() {
        if !self.isLoading() {
            self.showLoading()
            self.shippingMethodDataSource.removeAllObjects()
            
            var cityId = "0"
            if let addr = self.address {
                if let cId = addr.object(forKey: "city_id") as? String {
                    cityId = cId
                }
            }
            
            if !self.isJustCorporate() {
                Globals.getDataFromUrl(
                    Globals.getApiUrl("orders/review"),
                    requestType: Globals.HTTPRequestType.http_POST,
                    params: [
                        "clientId" : Globals.getProperty("clientId"),
                        "productIds" : "-1",
                        "cityId" : cityId
                    ],
                    completion: { (result) -> Void in
                        self.hideLoading()
                        NSLog("loadDataShippingMethod")
                        if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                            
                            self.shippingMethodDataSource.removeAllObjects()
                            if let methods = result.object(forKey: "shippingCosts") as? NSArray {
                                
                                for method in methods {
                                    
                                    if let costs = (method as! NSDictionary).object(forKey: "costs") as? NSArray {
                                        for cost in costs {
                                            let c = NSMutableDictionary(dictionary: cost as! [AnyHashable: Any])
                                            let temp1 = c.object(forKey: "cost") as! NSArray
                                            let temp2 = temp1.object(at: 0) as! NSDictionary
                                            let dbTemp = temp2.object(forKey: "value") as! Double
                                            
                                            if(self.total + dbTemp - self.pDisc < self.minTransaction){
                                                let newFee: Double = self.minTransaction - (self.total - self.pDisc)
                                                ((c.object(forKey: "cost") as! NSArray).object(at: 0) as! NSDictionary).setValue(newFee, forKey: "value")
                                            }
                                            c.setValue((method as! NSDictionary).object(forKey: "code"), forKey: "code")
                                            
                                            self.shippingMethodDataSource.add(c)
                                        }
                                    }
                                    
                                }
                                //                            self.shippingMethodDataSource.addObjectsFromArray(methods as [AnyObject])
                                
                            }
                            
                            self.reloadShippingMethodTableView()
                        }
                        else{
                            Globals.showAlertWithTitle("Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                        }
                })
            }
            else{
                self.shippingMethodDataSource.add([
                    "code" : Constant.shippingFree,
                    "service" : Constant.shippingFree.capitalized
                    ])
                self.selectedShippingMethod = 0
                self.shippingCost = 0
                self.reloadShippingMethodTableView()
                self.hideLoading()
            }
            
        }
    }
    
    func getOrderDetailCellHeight(_ indexPath: IndexPath) -> CGFloat{
        let minHeight: CGFloat = 167.0
        var heightByPhoto: CGFloat = 82.0
        var heightByTitle: CGFloat = 145.0
        if let cart = self.cartDataSource.object(at: (indexPath as NSIndexPath).row) as? Cart {
            if let project = ProjectHelper.getProject(cart.projectId){
                let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData) as! NSDictionary
                
                let coverWidth: CGFloat = CGFloat((productData.object(forKey: "cover_width") as! NSString).floatValue)
                let coverHeight: CGFloat = CGFloat((productData.object(forKey: "cover_height") as! NSString).floatValue)
                
                let wrapperWidth: CGFloat = 85.0
                let wrapperHeight = CGFloat(coverHeight / coverWidth) * wrapperWidth
                
                heightByPhoto += wrapperHeight
                
                var title : NSString!
                if project.title == ""{
                    if let productTitle = productData.object(forKey: "title") as? String {
                        title = productTitle as NSString!
                    }
                }
                else{
                    title = String(format: "\"%@\"", arguments: [project.title]) as NSString!
                }
                
                let titleHeight : CGFloat = title.boundingRect(
                    with: CGSize(width: self.orderDetailTableView.frame.size.width - 145, height: 99999),
                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 16)!],
                    context: nil
                    ).size.height
                
                heightByTitle += titleHeight
            }
            
            
        }
        return max(minHeight, max(heightByTitle, heightByPhoto))
    }
    
    func getShippingMethodCellHeight(_ indexPath: IndexPath) -> CGFloat{
        let minHeight: CGFloat = 41
        var height: CGFloat = 16
        
        if let cellInfoArray = self.shippingMethodDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
            var cost = ""
            
            if let service = cellInfoArray.object(forKey: "service") as? String {
                cost = service
            }
            if cost != Constant.shippingFree.capitalized {
                cost += " "
                
                if let desc = cellInfoArray.object(forKey: "description") as? String {
                    cost += "(" + desc + ")"
                }
                
                cost += " - "
                
                if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                    if prices.count > 0 {
                        if let price = prices.object(at: 0) as? NSDictionary {
                            if let value = price.object(forKey: "value") as? NSNumber {
                                cost += "IDR " + Globals.numberFormat(value)
                            }
                        }
                    }
                }
            }
            
            
            height += NSString(string: cost).boundingRect(
                with: CGSize(width: self.shippingMethodTableView.frame.size.width - 49, height: 99999),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 14)!],
                context: nil
                ).size.height
            
        }
        return max(minHeight, height)
    }
    
    func getOrderDetailCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.orderDetailTableView.dequeueReusableCell(withIdentifier: "cartTableViewCell", for: indexPath)
        
        let coverWrapper: UIView = cell.viewWithTag(100)!
        let lblTitle: UILabel = cell.viewWithTag(200) as! UILabel
        let lblPhotoCount: UILabel = cell.viewWithTag(300) as! UILabel
        let lblTotalPrice: UILabel = cell.viewWithTag(400) as! UILabel
        let lblQtyOld: UILabel = cell.viewWithTag(500) as! UILabel
        let btnPlus: EditButton = cell.viewWithTag(600) as! EditButton
        let btnMinus: EditButton = cell.viewWithTag(700) as! EditButton
        let btnDelete: EditButton = cell.viewWithTag(800) as! EditButton
        let lblQty: UILabel = cell.viewWithTag(900) as! UILabel
        
        if let cart = self.cartDataSource.object(at: (indexPath as NSIndexPath).row) as? Cart {
            if let project = ProjectHelper.getProject(cart.projectId){
                let projectDetails = ProjectHelper.getAllProjectDetails(cart.projectId)
                let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData) as! NSDictionary
                
                let cover = ProjectHelper.getCoverImage(cart.projectId)
                let coverWidth: CGFloat = CGFloat((productData.object(forKey: "cover_width") as! NSString).floatValue)
                let coverHeight: CGFloat = CGFloat((productData.object(forKey: "cover_height") as! NSString).floatValue)
                
                let wrapperWidth = coverWrapper.frame.size.width
                let wrapperHeight = CGFloat(coverHeight / coverWidth) * wrapperWidth
                
                if let heightConstraint = Globals.getConstraint("height", view: coverWrapper) {
                    heightConstraint.constant = wrapperHeight
                }
                
                MemomeHelper.setCoverView(coverWrapper, project: project, cover: cover, productData: productData, size: CGSize(width: wrapperWidth, height: wrapperHeight))
                
                if project.title == ""{
                    if let productTitle = productData.object(forKey: "title") as? String {
                        lblTitle.text = productTitle
                    }
                }
                else{
                    lblTitle.text = project.title
                }
                
                lblPhotoCount.text = String(format: "%i Photos", arguments: [projectDetails!.count])
                
                if project.corporateVoucherId > 0 {
                    lblPhotoCount.text = lblPhotoCount.text! + "\nFree \(project.corporateVoucherQty)x"
                }
                
                lblTotalPrice.text = Globals.numberFormat(NSNumber(value: cart.total as Double))
                lblQty.text = Globals.numberFormat(NSNumber(value: cart.qty as Int32))
                lblQty.layer.cornerRadius = lblQty.frame.size.width / 2.0
                lblQty.layer.borderColor = UIColor.colorPrimary.cgColor
                lblQty.layer.borderWidth = 1.0
                
                btnPlus.isHidden = true
                btnMinus.isHidden = true
                btnDelete.isHidden = true
                
                lblQtyOld.isHidden = true
                lblQty.isHidden = false
            }
        }
        
        return cell
    }
    
    func getShippingMethodCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.shippingMethodTableView.dequeueReusableCell(withIdentifier: "shippingMethodTableViewCell", for: indexPath)
        
        let imgCheck: UIImageView = cell.viewWithTag(1) as! UIImageView
        let lblTitle: UILabel = cell.viewWithTag(2) as! UILabel
        
        if let cellInfoArray = self.shippingMethodDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
            var cost = ""
            
            if let service = cellInfoArray.object(forKey: "service") as? String {
                cost = service
            }
            
            if cost != Constant.shippingFree.capitalized {
                cost += " "
                
                if let desc = cellInfoArray.object(forKey: "description") as? String {
                    cost += "(" + desc + ")"
                }
                
                cost += " - "
                
                if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                    if prices.count > 0 {
                        if let price = prices.object(at: 0) as? NSDictionary {
                            if let value = price.object(forKey: "value") as? NSNumber {
                                cost += "IDR " + Globals.numberFormat(value)
                            }
                        }
                    }
                }
                
            }
            
            lblTitle.text = cost
        }
        
        if (indexPath as NSIndexPath).row == self.selectedShippingMethod {
            imgCheck.image = UIImage(named: "icon-radio-checked.png")
        }
        else{
            imgCheck.image = UIImage(named: "icon-radio-unchecked.png")
        }
        
        return cell
    }
    
    func reloadOrderDetailTableView() {
        self.orderDetailTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.orderDetailTableView) {
            height.constant = self.orderDetailTableView.contentSize.height
        }
    }
    
    func reloadShippingMethodTableView() {
        self.shippingMethodTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.shippingMethodTableView) {
            height.constant = self.shippingMethodTableView.contentSize.height
        }
    }
    
    func loadPaymentMethod() {
        self.lblPaymentMethod.text = self.paymentName
    }
    
    func loadShippingDetail() {
        if let addr = self.address {
            
            if let firstName = addr.object(forKey: "first_name") as? String {
                self.lblShippingName.text = firstName
            }
            else{
                self.lblShippingName.text = ""
            }
            
            if let lastName = addr.object(forKey: "last_name") as? String {
                self.lblShippingName.text = self.lblShippingName.text?.appendingFormat(" %@", lastName)
            }
            
            var stringAddress = ""
            if let a = addr.object(forKey: "address") as? String {
                stringAddress = a + "\n"
            }
            
            var city = ""
            var province = ""
            var zipCode = ""
            if let item = addr.object(forKey: "cityName") as? String {
                city = item
            }
            if let item = addr.object(forKey: "provinceName") as? String {
                province = item
            }
            
            if let item = addr.object(forKey: "postal_code") as? String {
                zipCode = item
            }
            
            var stringCity = ""
            if city != "" {
                stringCity += city
            }
            if province != "" {
                if stringCity != "" {
                    stringCity += ", "
                }
                stringCity += province
            }
            if zipCode != "" {
                if stringCity != "" {
                    stringCity += " - "
                }
                stringCity += zipCode
            }
            
            stringAddress += stringCity + "\n"
            
            if let phone = addr.object(forKey: "phone") as? String {
                stringAddress += phone
            }
            
            self.lblShippingAddress.text = stringAddress
        }
    }
    
    func payment(_ orderID: String) {
        if !self.isLoading() {
            self.showLoading()
            
            var orders:[MidtransItemDetail] = []
            
            for item in CartHelper.getCart() {
                if let cart = item as? Cart {
                    if let project = ProjectHelper.getProject(cart.projectId) {
                        let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData) as! NSDictionary
                        var title = "";
                        if project.title == ""{
                            if let productTitle = productData.object(forKey: "title") as? String {
                                title = productTitle
                            }
                        }
                        else{
                            title = project.title
                        }
                        let order = MidtransItemDetail.init(itemID: String(project.productId), name: title, price: cart.price as NSNumber, quantity: cart.qty as NSNumber)
                        
                        orders.append(order!)
                        print("order added")
                    }
                }
            }
            
            let customerAddress = MidtransAddress.init(firstName: self.address!.object(forKey: "first_name") as! String, lastName: self.address!.object(forKey: "last_name") as! String, phone: self.address!.object(forKey: "phone") as! String, address: self.address!.object(forKey: "address") as! String, city: self.address!.object(forKey: "cityName") as! String, postalCode: self.address!.object(forKey: "postal_code") as! String, countryCode: "IDN")
            
            let customerDetail = MidtransCustomerDetails.init(firstName: self.address!.object(forKey: "first_name") as! String, lastName: self.address!.object(forKey: "last_name") as! String, email: self.address!.object(forKey: "email") as! String, phone: self.address!.object(forKey: "phone") as! String, shippingAddress: customerAddress, billingAddress: customerAddress)
            
            let transactionDetail = MidtransTransactionDetails.init(orderID: orderID, andGrossAmount: self.total as NSNumber)
            
//            Globals.showWebViewWithUrl(Constant.getPaymentUrl(orderID, grandTotal: String(self.total)), parentView: self, title: "PAYMENT")
            
            MidtransMerchantClient.shared().requestTransactionToken(with: transactionDetail!, itemDetails: orders, customerDetails: customerDetail) { (response, error) in
                if (response != nil) {
                    self.hideLoading()
                    //handle response
                    let vc = MidtransUIPaymentViewController.init(token: response)
                    vc?.paymentDelegate = self
                    self.present(vc!, animated: true, completion: nil)
                }
                else {
                    self.hideLoading()
                    print("error request transaction token \(error.debugDescription)")
                    //handle error
                }
            }
        }
    }
    
    func submit(_ status: String) {
        if !self.isLoading() {
            self.showLoading()
            
            let params = NSMutableDictionary()
            
            let shipping = self.shippingMethodDataSource.object(at: self.selectedShippingMethod) as! NSDictionary
            
            params.setValue(Globals.getProperty("clientId"), forKey: "clientId")
            params.setValue(Globals.getDataSetting("accessToken"), forKey: "accessToken")
            params.setValue(self.address!.object(forKey: "province_id") as! String, forKey: "provinceId")
            params.setValue(self.address!.object(forKey: "city_id") as! String, forKey: "cityId")
            params.setValue(self.address!.object(forKey: "first_name") as! String, forKey: "firstName")
            params.setValue(self.address!.object(forKey: "last_name") as! String, forKey: "lastName")
            params.setValue(self.address!.object(forKey: "address") as! String, forKey: "address")
            params.setValue(self.address!.object(forKey: "provinceName") as! String, forKey: "province")
            params.setValue(self.address!.object(forKey: "cityName") as! String, forKey: "city")
            params.setValue(self.address!.object(forKey: "postal_code") as! String, forKey: "postalCode")
            params.setValue(self.address!.object(forKey: "phone") as! String, forKey: "phone")
            params.setValue(self.address!.object(forKey: "email") as! String, forKey: "email")
            params.setValue(self.paymentType, forKey: "paymentType")
            
            params.setValue(shipping.object(forKey: "code") as! String, forKey: "shippingType")
            params.setValue(String(0), forKey: "shippingPrice")
            if shipping.object(forKey: "code") as! String != Constant.shippingFree {
                if let prices = shipping.object(forKey: "cost") as? NSArray {
                    if prices.count > 0 {
                        if let price = prices.object(at: 0) as? NSDictionary {
                            if let value = price.object(forKey: "value") as? NSNumber {
                                params.setValue(String(value.doubleValue), forKey: "shippingPrice")
                            }
                        }
                    }
                }
            }
            
            
            if let voucher = self.coupon {
                params.setValue(voucher.object(forKey: "code") as! String, forKey: "voucherCode")
                params.setValue(voucher.object(forKey: "id") as! String, forKey: "voucherId")
                var disc: Double = 0
                if self.couponDisc > 0 {
                    disc = self.couponDisc
                }
                else if self.couponPercent > 0 {
                    disc = self.couponPercent * (self.total + self.shippingCost) / 100
                }
                params.setValue(String(disc), forKey: "voucherTotal")
            }
            
            let orders = NSMutableArray()
            let orderDetails = NSMutableArray()
            
            for item in CartHelper.getCart() {
                if let cart = item as? Cart {
                    if let project = ProjectHelper.getProject(cart.projectId) {
                        let order = NSMutableDictionary()
                        order.setValue(String(project.id), forKey: "project_id")
                        order.setValue(String(project.productId), forKey: "product_id")
                        order.setValue(project.title, forKey: "title")
                        order.setValue(project.subtitle, forKey: "subtitle")
                        order.setValue(String(cart.qty), forKey: "qty")
                        order.setValue(String(project.corporateVoucherId), forKey: "corporate_voucher_id")
                        order.setValue(String(project.corporateVoucherCode), forKey: "corporate_voucher_code")
                        
                        var hasCover = false
                        if let _ = ProjectHelper.getCoverImage(project.id) {
                            hasCover = true
                        }
                        
                        
                        if let details = ProjectHelper.getAllProjectDetails(project.id) {
                            var i=0
                            let productDetails = NSKeyedUnarchiver.unarchiveObject(with: project.productDetailData) as! NSArray
                            
                            if hasCover {
                                i += 1
                            }
                            
                            var x = 0
                            while i < details.count {
                                if let projectDetail = details.object(at: i) as? ProjectDetail {
                                    let productDetail = productDetails.object(at: x) as! NSDictionary
                                    let orderDetail = NSMutableDictionary()
                                    orderDetail.setValue(String(project.id), forKey: "project_id")
                                    orderDetail.setValue(String(projectDetail.id), forKey: "project_detail_id")
                                    orderDetail.setValue(productDetail.object(forKey: "id") as! String, forKey: "product_detail_id")
                                    
                                    orderDetails.add(orderDetail)
                                    
                                }
                                i += 1
                                x += 1
                            }
                            
                        }
                        
                        orders.add(order)
                    }
                }
            }
            
            params.setValue(String(data: try! JSONSerialization.data(withJSONObject: orders, options: JSONSerialization.WritingOptions(rawValue: 0)), encoding: String.Encoding.utf8), forKey: "orderProduct")
            params.setValue(String(data: try! JSONSerialization.data(withJSONObject: orderDetails, options: JSONSerialization.WritingOptions(rawValue: 0)), encoding: String.Encoding.utf8), forKey: "orderProductDetail")
            
            Globals.getDataFromUrl(Globals.getApiUrl("orders/add"),
                requestType: Globals.HTTPRequestType.http_POST,
                params: params,
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let order = result.object(forKey: "order") as? NSDictionary {
                            if let id = order.object(forKey: "id") as? String {
                                if id == "0" {
                                    Globals.showAlertWithTitle("Checkout Error", message: "Checkout failed. Please try again.", viewController: self)
                                }
                                else{
                                    
                                    ControllerHelper.getHomeViewController()?.uploadingCart.removeAllObjects()
                                    
                                    var i=0;
                                    for item in CartHelper.getCart() {
                                        
                                        if let cart = item as? Cart {
                                            
                                            if let project = ProjectHelper.getProject(cart.projectId) {
                                                ControllerHelper.getHomeViewController()?.uploadingCart.add(project)
                                            }
                                            
                                        }
                                        
                                        i += 1
                                    }
                                    
                                    ControllerHelper.getHomeViewController()?.uploadingGrandTotal = self.grandTotal
                                    ControllerHelper.getHomeViewController()?.uploadingPaymentType = self.paymentType
                                    
                                    for item in self.cartDataSource {
                                        if let cart = item as? Cart {
                                            ProjectHelper.deleteCorporateVoucher(cart.projectId)
                                            
                                        }
                                    }
                                    
                                    CartHelper.clearCart()
                                    
                                    var orderNumber = id
                                    if let number = order.object(forKey: "number") as? String {
                                        orderNumber = number
                                    }
                                    
                                    
                                    self.showHomeViewController()
                                    if let homeVC = ControllerHelper.getHomeViewController() {
                                        homeVC.showViewController("ProgressViewController", attributes: [
                                            "orderId" : id,
                                            "orderNumber" : orderNumber,
                                            "grandTotal" : String(self.grandTotal),
                                            "paymentType" : self.paymentType
                                            ])
                                    }
                                    
                                    
                                }
                            }
                        }
                        
                        
                    }
                    else{
                        Globals.showAlertWithTitle("Checkout Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    func isJustCorporate() -> Bool {
        return self.paymentType == Constant.paymentCorporate
    }
    
    // MARK: - MidtransUIPaymentViewControllerDelegate
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, save result: MidtransMaskedCreditCard!) {
        print("save masked credit card")
    }
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, saveCardFailed error: Error!) {
        print("save credit card")
    }
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentPending result: MidtransTransactionResult!) {
        print("payment pending")
        self.submit("pending")
    }
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentSuccess result: MidtransTransactionResult!) {
        print("payment success")
        self.submit("success")
    }
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentFailed error: Error!) {
        print("payment failed")
    }
    
    func paymentViewController_paymentCanceled(_ viewController: MidtransUIPaymentViewController!) {
        print("payment cancelled")
    }
}
