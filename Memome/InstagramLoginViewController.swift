//
//  InstagramLoginViewController.swift
//  Memome
//
//  Created by iOS Developer on 17/11/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import UIKit

class InstagramLoginViewController: BaseViewController, UIWebViewDelegate {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblPageTitle: UILabel!
    @IBOutlet weak var loginWebView: UIWebView!
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loginWebView.delegate = self
        unSignedRequest()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        loginWebView.delegate = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    //MARK: - unSignedRequest
    func unSignedRequest () {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        loginWebView.loadRequest(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            if requestURLString.contains("#access_token=") {
                let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
                handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            }
            else {
                handleAuth(authToken: nil)
            }
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String?)  {
        print("Instagram authentication token == \(authToken)")
        if authToken != nil {
            Globals.setDataSetting("instagramAuthToken", value: authToken!)
        }
        self.dismissSelf()
    }
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        if loginIndicator != nil {
            loginIndicator.isHidden = false
            loginIndicator.startAnimating()
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if loginIndicator != nil {
            loginIndicator.isHidden = true
            loginIndicator.stopAnimating()
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }
}
