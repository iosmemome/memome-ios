//
//  PurchasesListViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 3/2/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import Presentr

class PurchasesListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    var isInProgress = false
    var shippingEtd: String = "1-3"
    
    @IBOutlet weak var purchasesTableView: UITableView!
    var quantity = 0
    
    var purchasesDataSource: NSMutableArray = NSMutableArray()
    let dbDateFormat: DateFormatter! = DateFormatter()
    var reOrder: NSDictionary = NSDictionary()
//    var isReorder: Bool = false
    let updatedData = NSMutableDictionary()
    var paymentType = ""
    var grandTotal = ""
    var orderNumber = ""
    var orderId = ""
    
    let presentr: Presentr = {
        let presentr = Presentr(presentationType: .popup)
        presentr.transitionType = .crossDissolve
        return presentr
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.purchasesTableView.tableFooterView = UIView()
        self.purchasesTableView.estimatedRowHeight = 75
        self.purchasesTableView.rowHeight = UITableViewAutomaticDimension
        
        self.purchasesTableView.register(UINib(nibName: "PurchaseTableViewCell", bundle: nil), forCellReuseIdentifier: "purchaseTableViewCell")
        self.purchasesTableView.register(UINib(nibName: "PurchaseListTableViewCell", bundle: nil), forCellReuseIdentifier: "purchaseListTableViewCell")
        self.purchasesTableView.register(UINib(nibName: "PurchaseFooterTableViewCell", bundle: nil), forCellReuseIdentifier: "purchaseFooterTableViewCell")
        
        self.dbDateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        self.loadDataPurchases()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    // Table View Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.purchasesDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let cellInfoArray = self.purchasesDataSource.object(at: section) as! NSDictionary
        
        var rows = 2
        
        if let isExpanded = cellInfoArray.object(forKey: "isExpanded") as? Bool {
            if isExpanded {
                if let expandData = cellInfoArray.object(forKey: "expandData") as? NSArray {
                    rows += expandData.count
                }
            }
        }
        
        return rows
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return tableView.rowHeight
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfoArray = self.purchasesDataSource.object(at: (indexPath as NSIndexPath).section) as! NSDictionary
        let order = cellInfoArray.object(forKey: "order") as! NSDictionary
        var rows = 2
        
        if let isExpanded = cellInfoArray.object(forKey: "isExpanded") as? Bool {
            if isExpanded {
                if let expandData = cellInfoArray.object(forKey: "expandData") as? NSArray {
                    rows += expandData.count
                }
            }
        }
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "purchaseTableViewCell", for: indexPath)
            cell.backgroundColor = .clear
            
            let lblNumber = cell.viewWithTag(1) as! UILabel
            let lblDate = cell.viewWithTag(2) as! UILabel
            let lblTotal = cell.viewWithTag(3) as! UILabel
            let lblStatus = cell.viewWithTag(4) as! UILabel
            let btnExpanded: EditButton = cell.viewWithTag(20) as! EditButton
            let totalLabel = cell.viewWithTag(5) as! UILabel
            let separatorView = cell.viewWithTag(21) as! UIView
            let btnReorder: EditButton = cell.viewWithTag(7) as! EditButton
            
            btnExpanded.index = indexPath.section
            btnExpanded.addTarget(self, action: #selector(btnExpandTouched(_:)), for: UIControlEvents.touchUpInside)
            
            btnReorder.index = indexPath.section
            btnReorder.isHidden = true
            btnReorder.addTarget(self, action: #selector(btnReorderTouchUpInside(_:)), for: UIControlEvents.touchUpInside)
            
            if let orderProduct = order.object(forKey: "jumlah_order") as? String{
                    totalLabel.text = "TOTAL \(orderProduct) PRODUCTS"
            }
            
            if let isExpanded = cellInfoArray.object(forKey: "isExpanded") as? Bool {
                if isExpanded {
                    btnExpanded.setImage(UIImage(named: "icon-up.png"), for: UIControlState.normal)
                    if let height = Globals.getConstraint("height", view: separatorView) {
                        height.constant = 1
                    }
                }
                else {
                    btnExpanded.setImage(UIImage(named: "icon-down.png"), for: UIControlState.normal)
                    if let height = Globals.getConstraint("height", view: separatorView) {
                        height.constant = 0
                    }
                }
            }
            else {
                btnExpanded.setImage(UIImage(named: "icon-down.png"), for: UIControlState.normal)
                if let height = Globals.getConstraint("height", view: separatorView) {
                    height.constant = 0
                }
            }
            
            if let number = order.object(forKey: "number") as? String {
                lblNumber.text = "ORDER #\(number)"
            }
            else if let number = order.object(forKey: "id") as? String {
                lblNumber.text = "ORDER #\(number)"
            }
            else{
                lblNumber.text = ""
            }
            
            if let createdAt = order.object(forKey: "created_at") as? String {
                let calendar = Calendar.current
                let components: DateComponents = (calendar as NSCalendar).components([.month, .day, .year, .hour, .minute, .second], from: self.dbDateFormat.date(from: createdAt)!)
                
                lblDate.text = String(format: "%i %@ %i", arguments: [
                    components.day!,
                    Globals.getMonthName(components.month!),
                    components.year!
                    ])
            }
            else{
                lblDate.text = ""
            }
            
            if let total = order.object(forKey: "grand_total") as? NSString {
                lblTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: total.doubleValue as Double))
            }
            else{
                lblTotal.text = ""
            }
            
            if let status = order.object(forKey: "status") as? String {
                if "pending".elementsEqual(status.trim().lowercased()) {
                    lblStatus.textColor = UIColor.gray
                    lblStatus.text = "ON PROGRESS"
                } else if "process".elementsEqual(status.trim().lowercased()) {
                    lblStatus.textColor = UIColor(red: 233/255.0, green: 179/255.0, blue: 67/255.0, alpha: 1)
                    lblStatus.text = "IN PRODUCTION"
                } else if "shipping".elementsEqual(status.trim().lowercased()) {
                    lblStatus.textColor = UIColor(red: 69/255.0, green: 208/255.0, blue: 58/255.0, alpha: 1)
                    lblStatus.text = "ON DELIVERY"
                } else if "cancelled".elementsEqual(status.trim().lowercased()) || "failed".elementsEqual(status.trim().lowercased()) {
                    lblStatus.textColor = UIColor.red
                    lblStatus.text = "CANCELLED"
                } else if "upload failed".elementsEqual(status.trim().lowercased()){
                    lblStatus.textColor = UIColor.red
                    lblStatus.text = "UPLOAD FAILED"
                }
                else if "complete".elementsEqual(status.trim().lowercased()){
                    
                    if order.object(forKey: "keterangan_expired") as? String == "1" || order.object(forKey: "keterangan_expired") as? NSNumber == 1 {
                        btnReorder.isHidden = true
                        lblStatus.isHidden = true
                    }else{
                        btnReorder.isHidden = false
                        btnReorder.layer.cornerRadius = 5
                    }
                    
                }
                else {
                    lblStatus.textColor = UIColor.colorPrimary
                }
            }
            else{
                lblStatus.text = ""
            }
            
            return cell
        }
        else if indexPath.row < rows - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "purchaseListTableViewCell", for: indexPath)
            cell.backgroundColor = .clear
            
            let lblTitle = cell.viewWithTag(1) as! UILabel
            let lblPrice = cell.viewWithTag(2) as! UILabel
            let lblQuantity = cell.viewWithTag(3) as! UILabel
            let lblName = cell.viewWithTag(4) as! UILabel
            
            lblTitle.text = ""
            lblPrice.text = ""
            lblQuantity.text = ""
            lblName.text = ""
            
            if let expandData = cellInfoArray.object(forKey: "expandData") as? NSArray {
                if let data = expandData.object(at: indexPath.row - 1) as? NSDictionary {
                    if let title = data.object(forKey: "title") as? String {
                        lblTitle.text = title.uppercased()
                    }
                    if let value = data.object(forKey: "value") as? NSString {
                        lblPrice.text = "IDR " + Globals.numberFormat(NSNumber(value: value.doubleValue as Double))
                    }
                    if let quantity = data.object(forKey: "quantity") as? String {
                        lblQuantity.text = "x"+quantity
                    }
                    if let name = data.object(forKey: "name") as? String {
                        lblName.text = name
                    }
                }
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "purchaseFooterTableViewCell", for: indexPath)
            cell.backgroundColor = .clear
            
            let lblFirstTitle = cell.viewWithTag(1) as! UILabel
            let lblSecondTitle = cell.viewWithTag(2) as! UILabel
            let lblFirstValue = cell.viewWithTag(11) as! UILabel
            let lblSecondValue = cell.viewWithTag(12) as! UILabel
            let btnReupload: EditButton = cell.viewWithTag(13) as! EditButton
            let btnTrack: EditButton = cell.viewWithTag(20) as! EditButton
            
            btnReupload.isHidden = true
            lblFirstTitle.textColor = UIColor.colorGrey
            lblSecondTitle.textColor = UIColor.colorGrey
            lblFirstValue.textColor = UIColor.black
            lblSecondValue.textColor = UIColor.black
            btnTrack.layer.cornerRadius = 5
            btnReupload.layer.cornerRadius = 5
            btnTrack.index = indexPath.section
            btnReupload.index = indexPath.section
            btnTrack.addTarget(self, action: #selector(btnTrackTouchUp(_:)), for: UIControlEvents.touchUpInside)
            btnReupload.addTarget(self, action: #selector(btnReuploadTouchUp(_:)), for: UIControlEvents.touchUpInside)
            
            if let status = order.object(forKey: "status") as? String {
                if "pending".elementsEqual(status.lowercased()) {
                    guard let processDate = order.object(forKey: "created_at") as? String else {
                        return cell
                    }
                    guard let estDate = order.object(forKey: "shipping_etd") as? String else {
                        return cell
                    }
                    self.shippingEtd = estDate
                    var etd: [NSString] = self.shippingEtd.split(separator: "-") as [NSString]
                    
                    if etd.count > 1 {
//                        lblSecondValue.text = GeneralHelper.calculateShipping(Date(), from: Int(etd[0].intValue), to: Int(etd[1].intValue))
                        lblSecondValue.text = GeneralHelper.calculateShipping(Date(), from: 3, to: 4)
                    }
                    else {
                        lblSecondValue.text = GeneralHelper.calculateShipping(Date(), from: 4, to: 7)
                    }
                    
                    lblFirstTitle.text = "PRODUCTION DATE"
                    lblSecondTitle.text = "EST. PRODUCTION DATE"
//                    firstValue  = Globals.dateFormat(processDate, format: "dd MMM yyyy")
                    
                    lblFirstValue.text = Globals.dateFormat(processDate, format: "dd MMM yyyy")
                    lblSecondTitle.font = lblSecondTitle.font.withSize(10)
                    btnTrack.isHidden = true
                    lblFirstTitle.isHidden = true
                    lblFirstValue.isHidden = true
                    btnReupload.isHidden = true
                    
                    if let height = Globals.getConstraint("height", view: btnTrack) {
                        height.constant = 0
                    }
                    if let height = Globals.getConstraint("height", view: lblFirstTitle) {
                        height.constant = 0
                    }
                    if let height = Globals.getConstraint("height", view: lblFirstValue) {
                        height.constant = 0
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondValue) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondTitle) {
                        height.constant = 17
                    }
                    
                } else if "process".elementsEqual(status.lowercased()) {
                    guard let createdAt = order.object(forKey: "process_date") as? String else {
                        return cell
                    }
                    
                    let firstValue = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd MMM yyyy"
                    let date = dateFormatter.date(from: firstValue)
                    
                    lblFirstTitle.text = "PRODUCTION DATE"
                    lblSecondTitle.text = "EST. DELIVERY DATE"
                    lblFirstValue.text = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                    lblSecondValue.text = GeneralHelper.calculateShipping(date!, from: 3, to: 4)
                    lblSecondTitle.font = lblSecondTitle.font.withSize(12)
                    lblFirstTitle.isHidden = false
                    lblSecondTitle.isHidden = false
                    lblFirstValue.isHidden = false
                    lblSecondValue.isHidden = false
                    btnTrack.isHidden = true
                    btnReupload.isHidden = true
                    
                    if let height = Globals.getConstraint("height", view: lblFirstTitle) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondTitle) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblFirstValue) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondValue) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: btnTrack) {
                        height.constant = 0
                    }
                } else if "shipping".elementsEqual(status.lowercased()) {
                    guard let createdAt = order.object(forKey: "ship_date") as? String else {
                        return cell
                    }
                    
                    lblFirstTitle.text = "DELIVERY DATE"
                    lblSecondTitle.text = "TRACKING CODE"
                    lblFirstValue.text = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                    if let shipping_number = order.object(forKey: "shipping_number") as? String {
                        lblSecondValue.text = shipping_number.uppercased()
                    }
                    else {
                        lblSecondValue.text = ""
                    }
                    lblSecondTitle.font = lblSecondTitle.font.withSize(12)
                    lblFirstTitle.isHidden = false
                    lblSecondTitle.isHidden = false
                    lblFirstValue.isHidden = false
                    lblSecondValue.isHidden = false
                    btnTrack.isHidden = false
                    btnReupload.isHidden = true
                    
                    if let height = Globals.getConstraint("height", view: lblFirstTitle) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondTitle) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblFirstValue) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondValue) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: btnTrack) {
                        height.constant = 25
                    }
                } else if "complete".elementsEqual(status.lowercased()) {
                    guard let createdAt = order.object(forKey: "complete_date") as? String else {
                        return cell
                    }
                    
                    lblFirstTitle.textColor = UIColor.colorPrimary
                    lblFirstValue.textColor = UIColor.colorPrimarySemiDark
                    
                    lblFirstTitle.text = "RECEIVED ON"
                    lblSecondTitle.text = ""
                    lblFirstValue.text = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                    lblSecondValue.text = ""
                    
                    lblSecondTitle.isHidden = true
                    lblSecondValue.isHidden = true
                    btnTrack.isHidden = true
                    btnReupload.isHidden = true
                    
                    if let height = Globals.getConstraint("height", view: lblSecondTitle) {
                        height.constant = 0
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondValue) {
                        height.constant = 0
                    }
                    if let height = Globals.getConstraint("height", view: btnTrack) {
                        height.constant = 0
                    }
                } else if "cancelled".elementsEqual(status.lowercased()) || "failed".elementsEqual(status.lowercased()) {
                    
                    guard let createdAt = order.object(forKey: "canceled_date") as? String else {
                        return cell
                    }
                    
                    guard let description = order.object(forKey: "reason_for_cancel") as? String else{
                        return cell
                    }
                    
                    
                    lblSecondTitle.textColor = UIColor.colorLightRed
                    lblSecondValue.textColor = UIColor.red
                
                    lblFirstTitle.text = "REASONS "
                    lblSecondTitle.text = "CANCELED ON"
                    lblSecondValue.text = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                    lblFirstValue.text = "\(description.uppercased())"
                    lblSecondTitle.font = lblSecondTitle.font.withSize(12)
                    lblSecondTitle.isHidden = false
                    lblSecondValue.isHidden = false
                    lblFirstValue.isHidden = false
                    lblFirstTitle.isHidden = false
                    btnTrack.isHidden = true
                    btnReupload.isHidden = true
                    
                    if let height = Globals.getConstraint("height", view: lblSecondTitle) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondValue) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblFirstValue) {
                        height.constant = 34
                    }
                    if let height = Globals.getConstraint("height", view: lblFirstTitle) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: btnTrack) {
                        height.constant = 0
                    }
                }
                else if "upload failed".elementsEqual(status.lowercased()){
                    btnReupload.isHidden = false
                    lblSecondTitle.isHidden = true
                    lblSecondValue.isHidden = true
                    lblFirstValue.isHidden = true
                    lblFirstTitle.isHidden = true
                    btnTrack.isHidden = true
                    
                    if let height = Globals.getConstraint("height", view: lblSecondTitle) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondValue) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblFirstValue) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: lblFirstTitle) {
                        height.constant = 17
                    }
                    if let height = Globals.getConstraint("height", view: btnTrack) {
                        height.constant = 0
                    }
                }
                else {
                    lblFirstTitle.isHidden = true
                    lblSecondTitle.isHidden = true
                    lblFirstValue.isHidden = true
                    lblSecondValue.isHidden = true
                    btnTrack.isHidden = true
                    if let height = Globals.getConstraint("height", view: lblFirstTitle) {
                        height.constant = 0
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondTitle) {
                        height.constant = 0
                    }
                    if let height = Globals.getConstraint("height", view: lblFirstValue) {
                        height.constant = 0
                    }
                    if let height = Globals.getConstraint("height", view: lblSecondValue) {
                        height.constant = 0
                    }
                    if let height = Globals.getConstraint("height", view: btnTrack) {
                        height.constant = 0
                    }
                }
            }
            else {
                lblFirstTitle.isHidden = true
                lblSecondTitle.isHidden = true
                lblFirstValue.isHidden = true
                lblSecondValue.isHidden = true
                btnTrack.isHidden = true
                if let height = Globals.getConstraint("height", view: lblFirstTitle) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: lblSecondTitle) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: lblFirstValue) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: lblSecondValue) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: btnTrack) {
                    height.constant = 0
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellInfoArray = self.purchasesDataSource.object(at: (indexPath as NSIndexPath).section) as! NSDictionary
        let order = cellInfoArray.object(forKey: "order") as! NSDictionary
        
        if let status = order.object(forKey: "status") as? String {
            if "upload failed".elementsEqual(status.trim().lowercased()) {
            }else{
                if let parent = self._parentViewController {
                    parent.showViewController("InCompleteDetailViewController", attributes: [
                        "order" : order
                        ])
                }
            }
        }
    }
    
    func btnTrackTouchUp(_ sender : AnyObject){
        if let btn = sender as? EditButton{
            let btnCart = btn.index
            if let cellArray = self.purchasesDataSource.object(at: btnCart) as? NSMutableDictionary{
                let controller = TrackingPopUpViewController()
                controller.purchaseOrder = cellArray.object(forKey: "order") as! NSDictionary
                customPresentViewController(presentr, viewController: controller, animated: true, completion: nil)
            }
        }
    }
    
    func btnReuploadTouchUp(_ sender : AnyObject){
        if let btn = sender as? EditButton{
            let btnCart = btn.index
            if let cellArray = self.purchasesDataSource.object(at: btnCart) as? NSMutableDictionary{
                
                if let order = cellArray.object(forKey: "order") as? NSDictionary{
                    if let id = order.object(forKey: "id") as? NSString{
                        orderId = id as String
                    }
                    if let number = order.object(forKey: "number") as? String{
                        orderNumber = number
                    }
                    if let grand_total = order.object(forKey: "grand_total") as? NSString{
                        grandTotal = grand_total as String
                    }
                    if let payment_type = order.object(forKey: "payment_type") as? String{
                        paymentType = payment_type
                    }
                }
                
                if let parent = self._parentViewController {
                    parent.showViewController("ProgressViewController", attributes: [
                        "orderId" : orderId,
                        "orderNumber" : orderNumber,
                        "grandTotal" : grandTotal,
                        "paymentType" : paymentType,
//                        "isUploadFailed" : true
                        ])
                }
            }
        }
    }
    
    // MARK: - actions
    func btnExpandTouched(_ sender: AnyObject){
        if let btn = sender as? EditButton {
            if let cellInfoArray = self.purchasesDataSource.object(at: btn.index) as? NSMutableDictionary {
                if let isExpanded = cellInfoArray.object(forKey: "isExpanded") as? Bool {
                    cellInfoArray.setValue(!isExpanded, forKey: "isExpanded")
                    self.purchasesDataSource.replaceObject(at: btn.index, with: cellInfoArray)
                }
                
                self.purchasesTableView.reloadSections(IndexSet(integer: btn.index), with: .fade)
            }
        }
    }
    
    func btnReorderTouchUpInside(_ sender:AnyObject){
        if let btn = sender as? EditButton{
            let btnCart = btn.index
            if let cellArray = self.purchasesDataSource.object(at: btnCart) as? NSMutableDictionary{
                let order = cellArray.object(forKey: "order") as? NSDictionary
                self.updatedData.setValue(order, forKey: "order")
                self.updatedData.setValue(true, forKey: "isReorder")
                if let parent = self._parentViewController {
                    parent.showViewController("CheckoutViewController", attributes: updatedData)
                }
            }
        }
    }
    
    // general methods
    func loadDataPurchases() {
        if !self.isLoading() {
            self.showLoading()
            
            var url: String = ""
            
            if self.isInProgress {
                url = Globals.getApiUrl("orders/getInProgressOrders")
            }
            else{
                url = Globals.getApiUrl("orders/getCompletedOrders")
            }
            
            
            Globals.getDataFromUrl(url,
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        self.purchasesDataSource.removeAllObjects()
                        
                        if let orders = result.object(forKey: "orders") as? NSArray {
                            
                            for order in orders {
                                
                                let updateData = NSMutableDictionary()
                                updateData.setValue(order, forKey: "order")
                                updateData.setValue(false, forKey: "isExpanded")
                                
                                let arrayData = NSMutableArray()
                                if let products = (order as! NSDictionary).object(forKey: "order_product") as? NSArray {
                                    for product in products {
                                        
                                        let dict = NSMutableDictionary()
                                        if let product_title = (product as! NSDictionary).object(forKey: "product_title") as? String {
                                            dict.setValue(product_title, forKey: "title")
                                        }
                                        if let total_price = (product as! NSDictionary).object(forKey: "total_price") as? String {
                                            dict.setValue(total_price, forKey: "value")
                                        }
                                        if let qty = (product as! NSDictionary).object(forKey: "qty") as? String {
                                            dict.setValue(qty, forKey: "quantity")
                                        }
                                        var name = ""
                                        if let title = (product as! NSDictionary).object(forKey: "title") as? String {
                                            name += title
                                        }
                                        
                                        if name.trim() != "" {
                                            dict.setValue(name.trim(), forKey: "name")
                                        }
                                        arrayData.add(dict)
                                    }
                                }
                                if let voucher_total = (order as! NSDictionary).object(forKey: "voucher_total") as? NSString {
                                        let dict = NSMutableDictionary()
                                        dict.setValue("Discount", forKey: "title")
                                        dict.setValue(voucher_total, forKey: "value")
                                        arrayData.add(dict)
                                }
                                if let shipping_price = (order as! NSDictionary).object(forKey: "shipping_price") as? String {
                                    let dict = NSMutableDictionary()
                                    
                                    if let shipping_type = (order as! NSDictionary).object(forKey: "shipping_type") as? String {
                                        dict.setValue("Shipping (\(shipping_type))", forKey: "title")
                                    }
                                    else {
                                        dict.setValue("Shipping", forKey: "title")
                                    }
                                    dict.setValue(shipping_price, forKey: "value")
                                    arrayData.add(dict)
                                }
                                
                               
                                updateData.setValue(arrayData, forKey: "expandData")
                                self.purchasesDataSource.add(updateData)
                            }
                        }
                        
                        self.purchasesTableView.reloadData()
                        
                    }
                    else{
                        Globals.showAlertWithTitle("My Orders Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
}
