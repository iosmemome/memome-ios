//
//  DefaultPopoverTransitioningDelegate.swift
//  Kreate
//
//  Created by Bobby Stenly on 9/22/15.
//  Copyright © 2015 Trio Digital Agency. All rights reserved.
//

import UIKit

class DefaultPopoverTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = DefaultPopoverPresentationController(presentedViewController:presented, presenting:presenting!)
        
        return presentationController
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = DefaultPopoverAnimation()
        animationController.isPresentation = true
        return animationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = DefaultPopoverAnimation()
        animationController.isPresentation = false
        return animationController
    }
    
}
