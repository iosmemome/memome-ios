//
//  Constant.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/11/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class Constant: NSObject {
    
    static func getPrefixUrl() -> String {
        return "memome.co.id"
    }
    
    static func getApiBaseUrl() -> String {
        return "http://memome.co.id/api/"
//        return "https://127.0.0.1/memome/api/"
//        return "http://192.168.0.39/memome/api/"
    }
    
    static func getPaymentUrl(_ orderId: String, grandTotal: String) -> String {
        return "http://memome.co.id/veritrans/index?id=\(orderId)&grand_total=\(grandTotal)"
    }

    static func getSearchLimit() -> Int {
        return 10
    }
    
    static var paymentCorporate = "corporate"
    static var paymentCorporateName = "Corporate"
    static var paymentTransferId = 1
    static var paymentTransfer = "bank transfer"
    static var paymentTransferName = "Bank Transfer"
    static var paymentCreditCardId = 2
    static var paymentCreditCard = "credit card"
    static var paymentCreditCardName = "Credit Card"
    
    static var shippingFree = "free"
}

struct INSTAGRAM_IDS {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
    static let INSTAGRAM_CLIENT_ID  = "134dca16622f4d58932d038320844706"
    
    static let INSTAGRAM_CLIENTSERCRET = "f29239f6069b47aaa35dfb7f108f869e"
    
    static let INSTAGRAM_REDIRECT_URI = "http://memome.co.id"
    
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
    
    static let INSTAGRAM_PREFIX = "https://scontent.cdninstagram.com"
    
}
