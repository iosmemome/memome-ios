//
//  ShippingViewController.swift
//  Memome
//
//  Created by Yuni on 29/03/18.
//  Copyright © 2018 Trio Digital Agency. All rights reserved.
//

import UIKit

class ShippingViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var shippingMethodTableView: UITableView!
    @IBOutlet weak var submitShipping: UIButton!
    
    var shippingMethodDataSource: NSMutableArray! = NSMutableArray()
    var shippingCost: Double = 0
    var selectedShippingMethod = -1
    var address: NSDictionary?
    var total: Double = 0
    
    var paymentType = Constant.paymentTransfer
    var paymentName = Constant.paymentTransferName
    var pDisc: Double = 0
    var selectedShipping: NSDictionary?
    
    var minTransaction: Double = 10000.0 // min transfer BCA
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.attributes != nil{
            if let addr = self.attributes.object(forKey: "address") as? NSDictionary{
                self.address = addr
            }
        }
        
        self.shippingMethodTableView.register(UINib(nibName: "ShippingMethodTableViewCell", bundle: nil), forCellReuseIdentifier: "shippingMethodTableViewCell")
        
        
        self.loadDataShippingMethod()
        
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismissSelf()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.shippingMethodTableView{
        return self.shippingMethodDataSource.count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          if tableView == self.shippingMethodTableView{
        let minHeight: CGFloat = 41
        var height: CGFloat = 16
        
        if let cellInfoArray = self.shippingMethodDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
            var cost = ""
            
            if let service = cellInfoArray.object(forKey: "service") as? String {
                cost = service
            }
            if cost != Constant.shippingFree.capitalized {
                cost += " "
                
                if let desc = cellInfoArray.object(forKey: "description") as? String {
                    cost += "(" + desc + ")"
                }
                
                cost += " - "
                
                if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                    if prices.count > 0 {
                        if let price = prices.object(at: 0) as? NSDictionary {
                            if let value = price.object(forKey: "value") as? NSNumber {
                                cost += "IDR " + Globals.numberFormat(value)
                            }
                        }
                    }
                }
            }
            
            
            height += NSString(string: cost).boundingRect(
                with: CGSize(width: self.shippingMethodTableView.frame.size.width - 49, height: 99999),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 14)!],
                context: nil
                ).size.height
            
        }
        return max(minHeight, height)
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          if tableView == self.shippingMethodTableView{
        let cell = self.shippingMethodTableView.dequeueReusableCell(withIdentifier: "shippingMethodTableViewCell", for: indexPath)
        
        let imgCheck: UIImageView = cell.viewWithTag(1) as! UIImageView
        let lblTitle: UILabel = cell.viewWithTag(2) as! UILabel
        
        if let cellInfoArray = self.shippingMethodDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
            var cost = ""
            
            if let service = cellInfoArray.object(forKey: "service") as? String {
                cost = service
                if cost == "REG" || cost == "CTC"{
                    cost = "REGULAR"
                }else if cost == "YES" || cost == "CTCYES"{
                    cost = "EXPRESS"
                }
            }
            
            if cost != Constant.shippingFree.capitalized {
                cost += " "
                
//                if let desc = cellInfoArray.object(forKey: "description") as? String {
//                    cost += "(" + desc + ")"
//                }
                
                cost += " - "
                
                if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                    if prices.count > 0 {
                        if let price = prices.object(at: 0) as? NSDictionary {
                            if let value = price.object(forKey: "value") as? NSNumber {
                                cost += "IDR " + Globals.numberFormat(value)
                            }
                        }
                    }
                }
                
            }
            
            lblTitle.text = cost
        }
        
        if (indexPath as NSIndexPath).row == self.selectedShippingMethod {
            imgCheck.image = UIImage(named: "icon-radio-checked.png")
        }
        else{
            imgCheck.image = UIImage(named: "icon-radio-unchecked.png")
        }
        
        return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.shippingMethodTableView {
            self.shippingCost = 0
            if (indexPath as NSIndexPath).row < self.shippingMethodDataSource.count {
                self.selectedShippingMethod = (indexPath as NSIndexPath).row
                self.reloadShippingMethodTableView()
                
                if let cellInfoArray = self.shippingMethodDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
                    if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                        if prices.count > 0 {
                            
                            if let price = prices.object(at: 0) as? NSDictionary {
                                
                                 let attributes: NSMutableDictionary = NSMutableDictionary()
                                
                                if let value = price.object(forKey: "value") as? NSNumber {
                                    
                                    self.shippingCost = value.doubleValue
                                    
                                   attributes.setValue(value, forKey: "value")
                                }}}}}}
        }
    }
    
    
    func reloadShippingMethodTableView() {
        self.shippingMethodTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.shippingMethodTableView) {
            height.constant = self.shippingMethodTableView.contentSize.height
        }
    }
    
    
    func loadDataShippingMethod() {
        if !self.isLoading() {
            self.showLoading()
            self.shippingMethodDataSource.removeAllObjects()
            
            var cityId = "0"
            if let addr = self.address {
                if let cId = addr.object(forKey: "city_id") as? String {
                    cityId = cId
                }
            }
            
//            if !self.isJustCorporate() {
                Globals.getDataFromUrl(
                    Globals.getApiUrl("orders/review"),
                    requestType: Globals.HTTPRequestType.http_POST,
                    params: [
                        "clientId" : Globals.getProperty("clientId"),
                        "productIds" : "-1",
                        "cityId" : cityId
                    ],
                    completion: { (result) -> Void in
                        self.hideLoading()
                        NSLog("loadDataShippingMethod")
                        if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                            
                            self.shippingMethodDataSource.removeAllObjects()
                            if let methods = result.object(forKey: "shippingCosts") as? NSArray {
                                
                                for method in methods {
                                    
                                    if let costs = (method as! NSDictionary).object(forKey: "costs") as? NSArray {
                                        for cost in costs {
                                            let c = NSMutableDictionary(dictionary: cost as! [AnyHashable: Any])
                                            let temp1 = c.object(forKey: "cost") as! NSArray
                                            let temp2 = temp1.object(at: 0) as! NSDictionary
                                            let dbTemp = temp2.object(forKey: "value") as! Double
                                            
                                            if(self.total + dbTemp - self.pDisc < self.minTransaction){
                                                let newFee: Double = self.minTransaction - (self.total - self.pDisc)
                                                ((c.object(forKey: "cost") as! NSArray).object(at: 0) as! NSDictionary).setValue(newFee, forKey: "value")
                                            }
                                            c.setValue((method as! NSDictionary).object(forKey: "code"), forKey: "code")
                                            
                                            self.shippingMethodDataSource.add(c)
                                        }
                                    }
                                    
                                }
                                //                            self.shippingMethodDataSource.addObjectsFromArray(methods as [AnyObject])
                                
                            }
                            
                            self.reloadShippingMethodTableView()
                        }
                        else{
                            Globals.showAlertWithTitle("Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                        }
                })
            }
            else{
                self.shippingMethodDataSource.add([
                    "code" : Constant.shippingFree,
                    "service" : Constant.shippingFree.capitalized
                    ])
                self.selectedShippingMethod = 0
                self.shippingCost = 0
                self.reloadShippingMethodTableView()
                self.hideLoading()
            
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func submitShippingTouchUpInside(_ sender: Any) {
        print("PRINT \(self.shippingCost)")
        
        self.dismissSelf()
        if let parent = self._parentViewController as? CheckoutViewController{
            
//            parent.viewDidLoad()
            parent.shippingValue = shippingCost
            parent.lblShipping.text = "IDR " + Globals.numberFormat(NSNumber(value: shippingCost as Double))
        }
    
    }
    
   
    
}
