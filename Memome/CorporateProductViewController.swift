//
//  CorporateProductViewController.swift
//  Memome
//
//  Created by Trio-1602 on 4/12/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class CorporateProductViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var productScrollView: UIScrollView!
    @IBOutlet weak var productContentScrollView: UIView!
    @IBOutlet weak var productTableView: UITableView!
    
    var productDataSource: NSMutableArray = NSMutableArray()
    var productTableViewAdapter: ProductTableViewAdapter = ProductTableViewAdapter()
    
    var corporateVoucherId: Int = 0
    var corporateVoucherName: String = ""
    var corporateVoucherQty: Int = 0
    var corporateVoucherCode: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.btnShoppingCart = self.btnCart
        self.productTableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "productTableViewCell")
        self.productTableViewAdapter._parentViewController = self
        self.productTableViewAdapter.tableView = self.productTableView
        
        self.productTableView.dataSource = self.productTableViewAdapter
        self.productTableView.delegate = self.productTableViewAdapter
        
        if let attributes = self.attributes {
            if let voucher = attributes.object(forKey: "voucher") as? NSDictionary {
                if let id = voucher.object(forKey: "id") as? NSString {
                    self.corporateVoucherId = id.integerValue
                }
                else if let id = voucher.object(forKey: "id") as? NSNumber {
                    self.corporateVoucherId = id.intValue
                }
                
                if let qty = voucher.object(forKey: "qty") as? NSString {
                    self.corporateVoucherQty = qty.integerValue
                }
                else if let qty = voucher.object(forKey: "qty") as? NSNumber {
                    self.corporateVoucherQty = qty.intValue
                }
                
                if let name = voucher.object(forKey: "name") as? String {
                    self.corporateVoucherName = name
                }
                
                self.productTableViewAdapter.voucher = voucher
            }
            
            if let code = attributes.object(forKey: "voucherCode") as? String {
                self.corporateVoucherCode = code
            }
            
        }
        
        self.productTableViewAdapter.voucherCode = self.corporateVoucherCode
        
        if self.corporateVoucherId == 0 {
            Globals.showAlertWithTitle("Redeem Voucher Error", message: "voucher not found", viewController: self)
            self.dismissSelf()
        }
        
        self.loadDataProducts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    // general methods
    func loadDataProducts(){
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(
                Globals.getApiUrl("corporates/searchProduct"),
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "corporateVoucherId" : String(self.corporateVoucherId)
                ],
                completion: { (result) in
                    self.hideLoading()
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let products = result.object(forKey: "products") as? NSArray {
                            self.productDataSource.addObjects(from: products as [AnyObject])
                        }
                        
                        self.productTableViewAdapter.setDataSource(self.productDataSource)
                        self.reloadProductTableView()
                    }
                    else{
                        Globals.showAlertWithTitle("Redeem Voucher Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    func reloadProductTableView(){
        self.productTableView.reloadData()
        
        if let height = Globals.getConstraint("height", view: self.productTableView) {
            height.constant = self.productTableView.contentSize.height
        }
    }
}
