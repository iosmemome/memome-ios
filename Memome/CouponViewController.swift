//
//  CouponViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 2/25/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import BSQRCodeReader

class CouponViewController: BaseViewController, BSQRCodeReaderDelegate{

    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var reader: BSQRCodeReader!
    
    @IBOutlet weak var txtCouponNumber: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.reader.delegate = self
        
        if let attributes = self.attributes {
            if let code = attributes.object(forKey: "couponCode") as? String {
                self.txtCouponNumber.text = code
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.rescan()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.reader.stopScanning()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func rescan() {
        self.reader.startScanning()
    }
    
    // BSQRCodeReaderDelegate
    func didCaptureQRCodeWithContent(_ content: String) -> Bool {
//        if content.substring(to: content.index(content.startIndex, offsetBy: 6)) == "MEMOME" {
            let code = content //content.substring(from: content.index(content.startIndex, offsetBy: 7))
            if !self.isLoading() {
                self.showLoading()
                
                Globals.getDataFromUrl(Globals.getApiUrl("vouchers/search"),
                   requestType: Globals.HTTPRequestType.http_POST,
                   params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String,
                    "voucherCode" : code
                    ],
                   completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let voucher = result.object(forKey: "voucher") as? NSDictionary {
                            
                            if let parent = self._parentViewController as? CheckoutViewController {
                                parent.setCoupon(voucher)
                                
                                self.dismissSelf()
                            }
                            else{
                                NSLog("error : %@", result)
                                Globals.showAlertWithTitle("Coupon Error", message: "Unknown Error Occurred", viewController: self, completion:  { (action) in
                                    self.rescan()
                                })
                            }
                            
                        }
                        else{
                            NSLog("error : %@", result)
                            Globals.showAlertWithTitle("Coupon Error", message: "Unknown Error Occurred", viewController: self, completion:  { (action) in
                                self.rescan()
                            })
                        }
                        
                    }
                    else{
                        Globals.showAlertWithTitle("Coupon Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self, completion:  { (action) in
                            self.rescan()
                        })
                    }
                })
                return true
            }
            else {
                return false
            }
//        }
//        else{
//            BSNotification.show("Code is undefined", view: self)
//            return false
//        }
    }

    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnSubmitTouched(_ sender: AnyObject) {
        
        if let _code = self.txtCouponNumber.text {
            let code = _code
            if code != "" {
//                if code.substring(to: code.index(code.startIndex, offsetBy: 6)) != "MEMOME" {
//                    code = String(format: "MEMOME-%@", code)
//                }
                let _ = self.didCaptureQRCodeWithContent(code)
            }
            else{
                Globals.showAlertWithTitle("Redeem Coupon Error", message: "Please enter a valid coupon code or scan it", viewController: self)
            }
        }
        else{
            Globals.showAlertWithTitle("Redeem Coupon Error", message: "Please enter a valid coupon code or scan it", viewController: self)
        }
        
    }
    
    
    
}
