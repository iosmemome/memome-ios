//
//  PaymentConfirmationViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 2/28/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import BSDropdown

class PaymentConfirmationViewController: BaseViewController, BSDropdownDataSource {

    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var paymentConfirmationScrollView: UIScrollView!
    @IBOutlet weak var paymentConfirmationContentScrollView: UIView!
    
    @IBOutlet weak var bsdOrder: BSDropdown!
    @IBOutlet weak var bsdTransferDate: BSDatePicker!
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtAccountName: UITextField!
    @IBOutlet weak var txtAccountNumber: UITextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtNotes: UITextField!
    @IBOutlet weak var btnConfirm: UIButton!
    
    @IBOutlet weak var lblInfo: UILabel!
    
    let dbDateFormat: DateFormatter! = DateFormatter()
    
    var orderDataSource: NSMutableArray! = NSMutableArray()
    
    var defaultOrderId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.defaultScrollView = self.paymentConfirmationScrollView
        self.defaultContentScrollView = self.paymentConfirmationContentScrollView
        
        let mainColor = UIColor.colorPrimary
        let titleFont = UIFont(name: "Montserrat-SemiBold", size: 15)
        let buttonFont = UIFont(name: "Montserrat-Regular", size: 14)
        
        self.bsdOrder.viewController = self
        self.bsdOrder.defaultTitle = "Order No."
        self.bsdOrder.title = "Order No."
        self.bsdOrder.headerBackgroundColor = mainColor
        self.bsdOrder.itemTintColor = mainColor
        self.bsdOrder.titleFont = titleFont
        self.bsdOrder.buttonFont = buttonFont
        self.bsdOrder.titleKey = "number"
        self.bsdOrder.dataSource = self
        self.bsdOrder.hideDoneButton = true
        self.bsdOrder.setup()
        
        self.bsdTransferDate.viewController = self
        self.bsdTransferDate.defaultTitle = "Transfer Date"
        self.bsdTransferDate.title = "Transfer Date"
        self.bsdTransferDate.headerBackgroundColor = mainColor
        self.bsdTransferDate.titleFont = titleFont
        self.bsdTransferDate.buttonFont = buttonFont
        self.bsdTransferDate.setup()
        
        self.dbDateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let attributes = self.attributes {
            if let orderId = attributes.object(forKey: "orderId") as? NSString {
                self.defaultOrderId = orderId.integerValue
            }
        }
        
        self.initRefreshControl(self.paymentConfirmationScrollView)
        
        self.txtAmount.addTarget(self, action: #selector(PaymentConfirmationViewController.txtAmountChange(_:)), for: UIControlEvents.editingChanged)
        
        self.loadDataConfirmation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadDataOrder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func refresh(_ sender: AnyObject) {
        super.refresh(sender)
        
        self.loadDataConfirmation()
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.txtBankName {
            self.txtAccountName.becomeFirstResponder()
        }
        else if textField == self.txtAccountName {
            self.txtAccountNumber.becomeFirstResponder()
        }
        else if textField == self.txtAccountNumber {
            self.txtAmount.becomeFirstResponder()
        }
        else if textField == self.txtAmount {
            self.txtNotes.becomeFirstResponder()
        }
        else if textField == self.txtNotes {
            self.btnConfirmClicked(self.btnConfirm)
        }
        
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //bsdropdown data source
    func itemHeightForRowAtIndexPath(_ dropdown: BSDropdown, tableView: UITableView, item: NSDictionary?, indexPath: IndexPath) -> CGFloat {
        if let cellInfoArray = item {
            var height: CGFloat = 30.0
            var number: NSString = ""
            var date: NSString = ""
            var total: NSString = ""
            
            if let orderNumber = cellInfoArray.object(forKey: "number") as? String {
                number = "ORDER #\(orderNumber)" as NSString
            }
            
            if let createdAt = cellInfoArray.object(forKey: "created_at") as? String {
                let calendar = Calendar.current
                let components: DateComponents = (calendar as NSCalendar).components([.month, .day, .year, .hour, .minute, .second], from: self.dbDateFormat.date(from: createdAt)!)
                
                date = String(format: "%i %@ %i", arguments: [
                    components.day!,
                    Globals.getMonthName(components.month!),
                    components.year!
                    ]) as NSString
            }
            
            if let tot = cellInfoArray.object(forKey: "grand_total") as? NSString {
                total = ("Total IDR " + Globals.numberFormat(NSNumber(value: tot.doubleValue as Double))) as NSString
            }
            
            let maxWidth = tableView.frame.size.width - 16
            let maxFloatHeight : CGFloat = 9999
            
            height += number.boundingRect(
                with: CGSize(width: maxWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-SemiBold", size: 14)!],
                context: nil
                ).size.height
            height += date.boundingRect(
                with: CGSize(width: maxWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 14)!],
                context: nil
                ).size.height
            height += total.boundingRect(
                with: CGSize(width: maxWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 14)!],
                context: nil
                ).size.height
            
            return height
        }
        else {
            return 0
        }
    }
    
    func itemForRowAtIndexPath(_ dropdown: BSDropdown, tableView: UITableView, item: NSDictionary?, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "OrderTableViewCell", bundle: nil), forCellReuseIdentifier: "orderTableViewCell")
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "orderTableViewCell", for: indexPath)
        if let cellInfoArray = item {
            let lblNumber = cell.viewWithTag(1) as! UILabel
            let lblDate = cell.viewWithTag(2) as! UILabel
            let lblTotal = cell.viewWithTag(3) as! UILabel
            
            if let number = cellInfoArray.object(forKey: "number") as? String {
                lblNumber.text = "ORDER #\(number)"
            }
            else{
                lblNumber.text = ""
            }
            
            if let createdAt = cellInfoArray.object(forKey: "created_at") as? String {
                let calendar = Calendar.current
                let components: DateComponents = (calendar as NSCalendar).components([.month, .day, .year, .hour, .minute, .second], from: self.dbDateFormat.date(from: createdAt)!)
                
                lblDate.text = String(format: "%i %@ %i", arguments: [
                    components.day!,
                    Globals.getMonthName(components.month!),
                    components.year!
                    ])
            }
            else{
                lblDate.text = ""
            }
            
            if let total = cellInfoArray.object(forKey: "grand_total") as? NSString {
                lblTotal.text = "Total IDR " + Globals.numberFormat(NSNumber(value: total.doubleValue as Double))
            }
            else{
                lblTotal.text = ""
            }    
        }
        
        return cell
    }

    // actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnConfirmClicked(_ sender: AnyObject) {
        if !self.isLoading() {
            self.showLoading()
            
            let params = NSMutableDictionary(dictionary: [
                "clientId" : Globals.getProperty("clientId"),
                "accessToken" : Globals.getDataSetting("accessToken") as! String,
                "orderId" : "0",
                "transferDate" : "",
                "bankName" : self.txtBankName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
                "accountName" : self.txtAccountName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
                "accountNumber" : self.txtAccountNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
                "notes" : self.txtNotes.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
                "total" : self.txtAmount.text!.replacingOccurrences(of: ".", with: "")
                ])
            if let order = self.bsdOrder.getSelectedValue() {
                params.setValue(order.object(forKey: "id") as! String, forKey: "orderId")
            }
            
            if let date = self.bsdTransferDate.getDate() {
                params.setValue(self.dbDateFormat.string(from: date), forKey: "transferDate")
            }
            
            if self.validateForm(params) {
                Globals.getDataFromUrl(Globals.getApiUrl("confirmations/add"),
                    requestType: Globals.HTTPRequestType.http_POST,
                    params: params,
                    completion: { (result) -> Void in
                        self.hideLoading()
                        
                        if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                            
                            if let confirmation = result.object(forKey: "confirmation") as? NSDictionary {
                                if let id = confirmation.object(forKey: "id") as? String {
                                    
                                    if id == "0" {
                                        Globals.showAlertWithTitle("Payment Confirmation Error", message: "Confirm payment failed. Please try again.", viewController: self)
                                    }
                                    else{
                                        BSNotification.show("Payment confirmation sent.", view: self)
                                        self.resetInput()
                                    }
                                    
                                }
                            }
                            
                        }
                        else{
                            Globals.showAlertWithTitle("Payment Confirmation Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                        }
                })
            }
            else{
                self.hideLoading()
            }
            
            
        }
    }
    
    func txtAmountChange(_ sender: AnyObject) {
        if let textField = sender as? UITextField {
            var val: String = (textField.text?.replacingOccurrences(of: ".", with: ""))!
            if val.length() > 3 {
                var length = val.length()
                var pos: Int = length % 3
                if pos == 0 {
                    pos = 3
                }
                while pos < length {
                    val = String(format: "%@.%@", arguments: [ val.substring( to: val.characters.index(val.startIndex, offsetBy: pos) ), val.substring( from: val.characters.index(val.startIndex, offsetBy: pos) ) ])
                    length += 1
                    pos += 4
                }
            }
            textField.text = val
        }
    }
    
    // general methods
    func loadDataOrder() {
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(Globals.getApiUrl("orders/getOrderNotYetConfirm"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        self.orderDataSource.removeAllObjects()
                        if let orders = result.object(forKey: "orders") as? NSArray {
                            self.orderDataSource.addObjects(from: orders as [AnyObject])
                        }
                        
                        self.bsdOrder.setDataSource(self.orderDataSource)
                        
                        if self.defaultOrderId > 0 {
                            for i in 0 ..< self.orderDataSource.count  {
                                if let order = self.orderDataSource.object(at: i) as? NSDictionary {
                                    var orderId = 0
                                    if let id = order.object(forKey: "id") as? NSString {
                                        orderId = id.integerValue
                                    }
                                    else if let id = order.object(forKey: "id") as? NSNumber {
                                        orderId = id.intValue
                                    }
                                    
                                    
                                    if orderId == self.defaultOrderId {
                                        self.bsdOrder.setSelectedIndex(i)
                                        break
                                    }
                                }
                            }
                            
                        }
                    }
                    else{
                        Globals.showAlertWithTitle("Payment Confirmation Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    func loadDataConfirmation() {
//        if !self.isLoading() {
//            self.showLoading()
            Globals.getDataFromUrl(Globals.getApiUrl("confirmations/search"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId")
                ],
                completion: { (result) -> Void in
//                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let confirmation = result.object(forKey: "confirmation") as? NSDictionary {
                            self.lblInfo.text = String(format: "By clicking confirm button, you confirmed to transfer above amount to memome %@ Account %@ %@", arguments: [
                                confirmation.object(forKey: "bank_name") as! String,
                                confirmation.object(forKey: "account_name") as! String,
                                confirmation.object(forKey: "account_number") as! String
                                ])
                        }
                        
                        
                    }
                    else{
                        Globals.showAlertWithTitle("Payment Confirmation Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
//        }
    }
    
    func validateForm(_ params: NSDictionary) -> Bool {
        var isGood = true
        
        if (params.object(forKey: "orderId") as! String) == "0" {
            Globals.showAlertWithTitle("Payment Confirmation Error", message: "Please select an order!", viewController: self)
            isGood = false
        }
        else if (params.object(forKey: "transferDate") as! String) == "" {
            Globals.showAlertWithTitle("Payment Confirmation Error", message: "Transfer date must not be empty!", viewController: self)
            isGood = false
        }
        else if (params.object(forKey: "bankName") as! String) == "" {
            Globals.showAlertWithTitle("Payment Confirmation Error", message: "Bank name must not be empty!", viewController: self)
            isGood = false
        }
        else if (params.object(forKey: "accountName") as! String) == "" {
            Globals.showAlertWithTitle("Payment Confirmation Error", message: "Account name must not be empty!", viewController: self)
            isGood = false
        }
        else if (params.object(forKey: "accountNumber") as! String) == "" {
            Globals.showAlertWithTitle("Payment Confirmation Error", message: "Account number must not be empty!", viewController: self)
            isGood = false
        }
        else if (params.object(forKey: "total") as! String) == "" {
            Globals.showAlertWithTitle("Payment Confirmation Error", message: "Amount must not be empty!", viewController: self)
            isGood = false
        }
        
        
        return isGood
    }
    
    func resetInput() {
        self.defaultOrderId = 0
        self.bsdOrder.setSelectedIndex(-1)
        self.bsdTransferDate.setDate(nil)
        
        self.loadDataOrder()
        
        self.txtBankName.text = ""
        self.txtAccountName.text = ""
        self.txtAccountNumber.text = ""
        self.txtAmount.text = ""
        self.txtNotes.text = ""
    }
}
