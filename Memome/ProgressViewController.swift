//
//  ProgressViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 2/27/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import Just

class ProgressViewController: BaseViewController {
    
    class UploadFile {
        internal var url: String!
        internal var scale: CGFloat!
        internal var rotation: Int!
        internal var cropRect: CGRect!
        internal var fileName: String!
        internal var productIdx: Int!
        internal var productDetailIdx: Int!
        
        init(url: String, scale: CGFloat, rotation: Int, cropRect: CGRect, fileName: String, productIdx: Int, productDetailIdx: Int) {
            self.url = url
            self.scale = scale
            self.rotation = rotation
            self.cropRect = cropRect
            self.fileName = fileName
            self.productIdx = productIdx
            self.productDetailIdx = productDetailIdx
        }
    }
    

    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var imgBankLogo: UIImageView!
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var bankTransferWrapper: UIView!
    
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    var orderId: Int = 0
    var orderNumber: String = ""
    var paymentType: String = ""
    var paymentGrandTotal: String = ""
    
//    var uploadFiles: [String:HTTPFile] = [String:HTTPFile]()
    var numberOfFilesToUpload: Int = 0
    var uploadQueue: [UploadFile] = []
    var uploadIndex: Int = 0
    var isUploading: Bool = false
    var isUploadFailed: Bool = false
//    var isReOrder: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnClose.isHidden = true
        if let height = Globals.getConstraint("height", view: btnClose){
            height.constant = 0
        }
        totalAmountLabel.isHidden = true
        
        // Do any additional setup after loading the view.
        ControllerHelper.setProgressViewController(self)
        
        if let attributes = self.attributes {
            if let id = attributes.object(forKey: "orderId") as? NSString {
                self.orderId = id.integerValue
                self.lblOrderNumber.text = "ORDER #\(self.orderId)"
            }
            if let number = attributes.object(forKey: "orderNumber") as? String {
                self.orderNumber = number
                self.lblOrderNumber.text = "ORDER #\(self.orderNumber)"
            }
            
            if let type = attributes.object(forKey: "paymentType") as? String {
                self.paymentType = type
            }
            
            if let total = attributes.object(forKey: "grandTotal") as? NSString {
                self.lblTotalAmount.text = "IDR " + Globals.numberFormat(NSNumber(value: total.doubleValue as Double))
                self.paymentGrandTotal = total as String
            }
            
//            if let uploadFailed = attributes.object(forKey: "isUploadFailed") as? Bool{
//                self.isUploadFailed = uploadFailed
//            }
        }
        
        self.isUploadFailed = false
        
        if self.paymentType == Constant.paymentTransfer {
            self.loadDataAccount()
        }
        else{
            if let height = Globals.getConstraint("height", view: self.bankTransferWrapper) {
                height.constant = 0.0
            }
            self.loadImages()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.isUploadFailed {
            self.retry()
        }
    }
    
//    deinit {
//        NSLog("Progress View Controller is being deinit")
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func showHomeViewController() {
        let navController: UINavigationController = self.revealViewController().frontViewController as! UINavigationController
        
        if let vc = navController.popViewController(animated: false) as? ProgressViewController {
            ControllerHelper.setProgressViewController(vc)
        }
        
        var i = navController.viewControllers.count - 1
        while i > 0 {
            navController.popViewController(animated: false)
            i -= 1
        }
        //        self.showViewController("MakePrintViewController", attributes: nil)
    }

    @IBAction func btnBackTouched(_ sender: AnyObject) {
//        self.dismissSelf()
        if !self.isUploading {
            ControllerHelper.setProgressViewController(nil)
        }
        self.showHomeViewController()
    }
    
    @IBAction func btnCloseTouched(_ sender: AnyObject) {
        self.btnBackTouched(sender)
    }
    
    // general method
    func loadDataAccount() {
        if !self.isLoading() {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("orders/getById"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "orderId" : String(self.orderId)
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let bank = result.object(forKey: "bankAccount") as? NSDictionary {
                            if let name = bank.object(forKey: "account_name") as? String {
                                self.lblAccountName.text = name
                            }
                            
                            if let number = bank.object(forKey: "account_number") as? String {
                                self.lblAccountNumber.text = number
                            }
                            
                            if let img = bank.object(forKey: "bank_image") as? String {
                                self.imgBankLogo.image = nil
                                Globals.setImageFromUrl(img, imageView: self.imgBankLogo, placeholderImage: nil)
                            }
                        }
                        
                        self.loadImages()
                        
                    }
                    else{
                        Globals.showAlertWithTitle("Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    func loadImages() {
//        NSLog("cart : %@", (ControllerHelper.getHomeViewController()?.uploadingCart)!)
        var i=0;
        for item in (ControllerHelper.getHomeViewController()?.uploadingCart)! {
            
            if let project = item as? Project {
//                NSLog("cart found")
//                if let project = ProjectHelper.getProject(cart.projectId) {
//                    NSLog("project found")
                    self.loadImage(project, cartIndex: i)
                    
//                }
                
            }
            
            i += 1
        }
        
    }
    
    func loadImage(_ project: Project, cartIndex: Int) {
        
//        let productData = NSKeyedUnarchiver.unarchiveObjectWithData(project.productData) as! NSDictionary
//        let productDetailData = NSKeyedUnarchiver.unarchiveObjectWithData(project.productDetailData) as! NSArray
        if let projectDetails = ProjectHelper.getAllProjectDetails(project.id) {
            
            self.numberOfFilesToUpload += projectDetails.count
            
            
            var i=0
            if let coverImage = ProjectHelper.getCoverImage(project.id) {
                var x: CGFloat = 0.0
                var y: CGFloat = 0.0
                var width: CGFloat = 0.0
                var height: CGFloat = 0.0
                let scale: CGFloat = 1.0 //CGFloat(coverImage.zoom)
                var rotation: Int = 0
                
                width = CGFloat(coverImage.width) * scale
                height = CGFloat(coverImage.height) * scale
                
                x = CGFloat(coverImage.offsetX) * scale
                y = CGFloat(coverImage.offsetY) * scale
                
                rotation = Int(coverImage.rotation)
                
                let cropRect = CGRect(x: x, y: y, width: width, height: height)
                
                self.uploadQueue.append( UploadFile(url: coverImage.image, scale: scale, rotation: rotation, cropRect: cropRect, fileName: "order_product_images", productIdx: cartIndex, productDetailIdx: -1) )
                
//                Globals.imageFromAssetLocalIdentifier(coverImage.image, scale: scale, isHighQuality: true, resultHandler: { (image) -> Void in
//                    if let img = image {
//                        autoreleasepool({ () -> () in
//                            if let croppedImage = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect) {
//                                
//                                self.uploadFiles["order_product_images[\(cartIndex)]"] = HTTPFile.Data("cover\(project.id).jpg", UIImageJPEGRepresentation(croppedImage, 1)!, nil)
//                                
//                            }
//                        })
//                    }
//                    
//                    self.numberOfFilesToUpload -= 1
//                    if self.numberOfFilesToUpload == 0 {
//                        //do upload
//                        self.uploadImage()
//                    }
//                })
                
//                if coverImage.imageData.length > 0 {
//                    NSLog("load cover image from image data database \(cartIndex)")
//                    self.uploadFiles["order_product_images[\(cartIndex)]"] = HTTPFile.Data("cover\(project.id).jpg", coverImage.imageData, nil)
//                    self.numberOfFilesToUpload -= 1
//                    
//                    if self.numberOfFilesToUpload == 0 {
//                        //do upload
//                        self.uploadImage()
//                    }
//                }
//                else{
//                    NSLog("load cover image from asset")
//                    Globals.imageFromAssetLocalIdentifier(coverImage.image, scale: scale, isHighQuality: true, resultHandler: { (image) -> Void in
//                        autoreleasepool({ 
//                            if let img = image {
//                                let imageData = UIImageJPEGRepresentation(img, 1)!
//                                NSLog("cover image data length : \(imageData.length)")
//                                self.uploadFiles["order_product_images[\(cartIndex)]"] = HTTPFile.Data("cover\(project.id).jpg", imageData, nil)
//                            }
//                            
//                            self.numberOfFilesToUpload -= 1
//                            if self.numberOfFilesToUpload == 0 {
//                                //do upload
//                                self.uploadImage()
//                            }
//                        })
//                        
//                    })
//                }
                
                i += 1
            }
            
            var j = 0
            while i < projectDetails.count {
                if let image = projectDetails.object(at: i) as? ProjectDetail {
//                    NSLog("project details \(j)")
                    var x: CGFloat = 0.0
                    var y: CGFloat = 0.0
                    var width: CGFloat = 0.0
                    var height: CGFloat = 0.0
                    let scale: CGFloat = 1.0 //CGFloat(image.zoom)
                    var rotation: Int = 0
                    let index = j
                    
                    width = CGFloat(image.width) * scale
                    height = CGFloat(image.height) * scale
                    
                    x = CGFloat(image.offsetX) * scale
                    y = CGFloat(image.offsetY) * scale
                    
                    rotation = Int(image.rotation)
                    
                    let cropRect = CGRect(x: x, y: y, width: width, height: height)
                    
                    self.uploadQueue.append( UploadFile(url: image.image, scale: scale, rotation: rotation, cropRect: cropRect, fileName: "order_product_detail_images", productIdx: cartIndex, productDetailIdx: index) )
                    
//                    Globals.imageFromAssetLocalIdentifier(image.image, scale: scale, isHighQuality: true, resultHandler: { (image) -> Void in
//                        if let img = image {
//                            autoreleasepool({ () -> () in
//                                if let croppedImage = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect) {
//                                    
////                                    NSLog("project details cropped image \(index)")
//                                    
//                                    self.uploadFiles["order_product_detail_images_\(cartIndex)[\(index)]"] = HTTPFile.Data("detail\(cartIndex)-\(index).jpg", UIImageJPEGRepresentation(croppedImage, 1)!, nil)
//                                    
//                                }
//                            })
//                        }
//                        
//                        self.numberOfFilesToUpload -= 1
//                        if self.numberOfFilesToUpload == 0 {
//                            //do upload
//                            self.uploadImage()
//                        }
//                    })
                    
                    
//                    if image.imageData.length > 0 {
//                        NSLog("load from image data database \(cartIndex) - \(index)")
//                        self.uploadFiles["order_product_detail_images_\(cartIndex)[\(index)]"] = HTTPFile.Data("detail\(cartIndex)-\(index).jpg", image.imageData, nil)
//                        self.numberOfFilesToUpload -= 1
//                        
//                        if self.numberOfFilesToUpload == 0 {
//                            //do upload
//                            self.uploadImage()
//                        }
//                    }
//                    else{
//                        NSLog("load cover image from asset")
//                        Globals.imageFromAssetLocalIdentifier(image.image, scale: scale, isHighQuality: true, resultHandler: { (image) -> Void in
//                            if let img = image {
//                                let imageData = UIImageJPEGRepresentation(img, 1)!
//                                self.uploadFiles["order_product_detail_images_\(cartIndex)[\(index)]"] = HTTPFile.Data("detail\(cartIndex)-\(index).jpg", imageData, nil)
//                            }
//                            
//                            self.numberOfFilesToUpload -= 1
//                            if self.numberOfFilesToUpload == 0 {
//                                //do upload
//                                self.uploadImage()
//                            }
//                        })
//                    }
                }
                else{
                    self.numberOfFilesToUpload -= 1
                }
                
                i += 1
                j += 1
            }
        }
        
        self.uploadIndex = 0
        self.performSelector(inBackground: #selector(ProgressViewController.uploadImageOneByOne), with: nil)
    }
    
    func uploadImage() {
        
//        NSLog("upload Image")
//        
//        for (key, value) in self.uploadFiles {
//            NSLog("upload file : \(key)")
//        }
//        
        
//        Just.post(Globals.getApiUrl("orders/uploadImages"),
//            params: [
//                "clientId" : Globals.getProperty("clientId"),
//                "accessToken" : Globals.getDataSetting("accessToken") as! String,
//                "orderId" : String(self.orderId)
//            ],
//            files: self.uploadFiles,
//            asyncProgressHandler: { (progress:HTTPProgress!) -> Void in
////                NSLog("percent upload : %f", progress.percent * 100)
//                dispatch_async(dispatch_get_main_queue(), {
//                    self.lblPercentage.text = String(format: "%.0f%%", arguments: [progress.percent * 100])
//                    
//                    ControllerHelper.getHomeViewController()?.updateUploadProgress(self.orderId, percent: progress.percent)
//                })
//            }) { (result:HTTPResult!) -> Void in
//                if result.ok {
////                    NSLog("Upload image complete")
//                    dispatch_async(dispatch_get_main_queue(), {
//                        if let _ = ControllerHelper.getActiveViewController() as? ProgressViewController {
//                            
//                            self.showViewController("PaymentConfirmationViewController", attributes: [
//                                "orderId" : String(self.orderId)
//                                ])
//                        }
//                        
//                        ControllerHelper.getHomeViewController()?.uploadComplete(self.orderId)
//                    })
//                }
//                else{
//                    dispatch_async(dispatch_get_main_queue(), {
//                        
//                        if let _ = ControllerHelper.getActiveViewController() as? ProgressViewController {
//                            Globals.showConfirmAlertWithTitle("Upload Error",
//                                message: "There is an error occurred while uploading your images. Do you want to try again?",
//                                viewController: self,
//                                completion: { (action) -> Void in
//                                    self.loadImages()
//                            })
//                        }
//                        
//                        ControllerHelper.getHomeViewController()?.uploadFailed(self.orderId)
//                    })
//                }
//        }
    }
    
    func uploadImageOneByOne() {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        if !isUploading {
            if self.uploadQueue.count > self.uploadIndex {
                let item = self.uploadQueue[self.uploadIndex]
                let isLast = self.uploadQueue.count - 1 == self.uploadIndex
                let basePercent: Float = Float(self.uploadIndex) / Float(self.uploadQueue.count) * 100.0
                let chunkPercent: Float = 1.0 / Float(self.uploadQueue.count)
                
                var filename = item.fileName
                var tmpname = ""
                if item.productDetailIdx > -1 {
                    filename = filename! + "_\(item.productIdx!)[\(item.productDetailIdx!)]"
                    tmpname = "detail\(item.productIdx!)-\(item.productDetailIdx!).jpg"
                }
                else{
                    filename = filename! + "[\(item.productIdx!)]"
                    tmpname = "cover\(item.productIdx!).jpg"
                }
                Globals.checkImageFrom(item.url, scale: item.scale, isHighQuality: true, resultHandler: { (image) -> Void in
                    if let img = image {
                        autoreleasepool(invoking: { () -> () in
                            if let croppedImage = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: item.rotation), rect: item.cropRect) {
                                
                                var quality : CGFloat = 1
                                var checkQualityDone : Bool = false
                                while !checkQualityDone {
                                    if let imgData = UIImageJPEGRepresentation(croppedImage, quality) {
                                        let fileSize = Double(imgData.count) / 1024.0
                                        
                                        if fileSize > 2048.0 {
                                            if quality <= 0.25 {
                                                checkQualityDone = true
                                            }else{
                                                quality -= 0.25
                                            }
                                        }else{
                                            checkQualityDone = true
                                        }
                                    }
                                }
                                
                                Just.post(
                                    Globals.getApiUrl("orders/uploadImage"),
                                    params: [
                                        "clientId" : Globals.getProperty("clientId"),
                                        "accessToken" : Globals.getDataSetting("accessToken") as! String,
                                        "orderId" : String(self.orderId),
                                        "productIdx" : String(item.productIdx),
                                        "productDetailIdx" : String(item.productDetailIdx)
                                    ],
                                    files: [
                                        filename!: HTTPFile.data(tmpname, UIImageJPEGRepresentation(croppedImage, quality)!, nil)
                                    ],
                                    asyncProgressHandler: { (progress:HTTPProgress!) in
                                        if progress.percent >= 0.0 {
//                                            NSLog("percent \(self.uploadIndex) upload : %f", progress.percent * 100)
//                                            NSLog("total percent upload : %f", basePercent + (chunkPercent * (progress.percent * 100)))
                                            DispatchQueue.main.async(execute: {
                                                self.lblPercentage.text = String(format: "%.0f%%", arguments: [ basePercent + (chunkPercent * (progress.percent * 100)) ])
                                                
                                                ControllerHelper.getHomeViewController()?.updateUploadProgress(self.orderId, orderNumber: self.orderNumber, percent: (basePercent / 100.0) + (chunkPercent * progress.percent))
                                            })
                                        }
                                    },
                                    asyncCompletionHandler: { (result:HTTPResult!) in
                                        if result.ok {
                                            if isLast {
                                                DispatchQueue.main.async(execute: {
//                                                    NSLog("last image uploaded")
                                                    self.isUploading = false
                                                    
                                                    ControllerHelper.setProgressViewController(nil)
                                                    self.dismissSelf({
                                                        if let _ = ControllerHelper.getActiveViewController() as? MakePrintViewController {
//                                                            if self.paymentType == Constant.paymentCreditCard {
//                                                                Globals.showWebViewWithUrl(Constant.getPaymentUrl(String(self.orderId), grandTotal: self.paymentGrandTotal), parentView: self, title: "PAYMENT")
//                                                            }
//                                                            else if self.paymentType == Constant.paymentTransfer {
//                                                                self._parentViewController.showViewController("PaymentConfirmationViewController", attributes: [
//                                                                    "orderId" : String(self.orderId)
//                                                                    ])
//                                                            }
                                                        }
                                                        else{
                                                            
                                                            if self.paymentType == Constant.paymentCreditCard {
                                                                Globals.showWebViewWithUrl(Constant.getPaymentUrl(String(self.orderId), grandTotal: self.paymentGrandTotal), parentView: self, title: "PAYMENT")
                                                            }
                                                            else if self.paymentType == Constant.paymentTransfer {
                                                                if let parent = self._parentViewController {
                                                                    parent.showViewController("PaymentConfirmationViewController", attributes: [
                                                                        "orderId" : String(self.orderId)
                                                                        ])
                                                                }
                                                                
                                                            }
                                                            
                                                        }
                                                    })
                                                    
                                                    
                                                    ControllerHelper.getHomeViewController()?.uploadComplete(self.orderId)
                                                })
                                            }
                                            else{
//                                                NSLog("image \(self.uploadIndex) uploaded")
                                                self.uploadIndex += 1
                                                self.isUploading = false
                                                self.uploadImageOneByOne()
                                            }
                                        }
                                        else{
                                            DispatchQueue.main.async(execute: {
//                                                NSLog("image \(self.uploadIndex) error")
                                                self.isUploadFailed = true
                                                if let _ = ControllerHelper.getActiveViewController() as? ProgressViewController {
                                                    self.retry()
                                                }
                                                
                                                ControllerHelper.getHomeViewController()?.uploadFailed(self.orderId)
                                            })
                                        }
                                })
                                
                                
                            }
                        })
                        
                    }
                })
                
            }
        }
    }
    
    func retry() {
        Globals.showConfirmAlertWithTitle("Upload Error",
                                          message: "There is an error occurred while uploading your images. Do you want to try again?",
                                          viewController: self,
                                          completion: { (action) -> Void in
                                            self.isUploading = false
                                            self.isUploadFailed = false
                                            self.uploadImageOneByOne()
                                            // self.loadImages()
        })
    }
}
