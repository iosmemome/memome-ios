//
//  ControllerHelper.swift
//  Kreate
//
//  Created by staff on 7/30/15.
//  Copyright (c) 2015 Trio Digital Agency. All rights reserved.
//

import UIKit

class ControllerHelper: NSObject {
   
    fileprivate static weak var activeViewController: BaseViewController? = nil
    fileprivate static weak var homeViewController: MakePrintViewController? = nil
    fileprivate static var progressViewController: ProgressViewController? = nil
    
    static func setActiveViewController(_ viewController: BaseViewController?){
        ControllerHelper.activeViewController = viewController
    }
    
    static func getActiveViewController() -> BaseViewController? {
        return ControllerHelper.activeViewController
    }
    
    static func setHomeViewController(_ viewController: MakePrintViewController){
        ControllerHelper.homeViewController = viewController
    }
    
    static func getHomeViewController() -> MakePrintViewController? {
        return ControllerHelper.homeViewController
    }
    
    static func setProgressViewController(_ viewController: ProgressViewController?){
        ControllerHelper.progressViewController = viewController
    }
    
    static func getProgressViewController() -> ProgressViewController? {
        return ControllerHelper.progressViewController
    }
    
}
