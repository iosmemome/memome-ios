//
//  FacebookSelectorViewController.swift
//  Memome
//
//  Created by iOS Developer on 16/11/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import FBSDKLoginKit

class FacebookSelectorViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var mainViewController: UIViewController?
    var photosDataSource: NSArray! = NSArray()
    let fbReadPermission = ["public_profile","email","user_photos"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.photosCollectionView.emptyDataSetDelegate = self
        self.photosCollectionView.emptyDataSetSource = self
        
        self.photosCollectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCollectionViewCell")
        
        if (FBSDKAccessToken.current() != nil) {
            self.showLoading()
            self.fetchListOfUserPhotos()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photosDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath)
        
        let imageView = cell.viewWithTag(1) as! UIImageView
        let lblTitle = cell.viewWithTag(2) as! UILabel
        let imageChecked = cell.viewWithTag(3) as! UIImageView
        let imageWarning = cell.viewWithTag(4) as! UIImageView
        
        let userPhotoObject = self.photosDataSource![indexPath.row] as! NSDictionary
        let userPhotoUrlString = userPhotoObject.value(forKey: "picture") as! String
        if userPhotoUrlString != nil && userPhotoUrlString != "" {
            Globals.setImageFromUrl(userPhotoUrlString, imageView: imageView, placeholderImage: nil, onSuccess: { (image) -> Void in
                imageView.backgroundColor = UIColor.white
            })
        }
//        let asset = self.photosDataSource.object(at: (indexPath as NSIndexPath).row) as! PHAsset
//        PHImageManager.default().requestImage(for: asset, targetSize: self.thumbImageSize, contentMode: .aspectFill, options: self.thumbImageOptions, resultHandler: { (image, info) -> Void in
//            imageView.image = image
//        })
        lblTitle.isHidden = true
        imageChecked.isHidden = true
        imageWarning.isHidden = true
        
        //        let selectedIdx = self.selectedPhotos.indexOfObject(indexPath.row)
        //        if selectedIdx != NSNotFound {
        //            lblTitle.hidden = false
        //            if self.numberOfPhotos > 1 {
        //                lblTitle.text = String(format: "%i", arguments: [selectedIdx + 1])
        //            }
        //            else{
        //                lblTitle.text = ""
        //            }
        //        }
        //        else{
        //            lblTitle.hidden = true
        //        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row < self.photosDataSource.count {
//            let asset = self.photosDataSource.object(at: (indexPath as NSIndexPath).row) as! PHAsset
//            if self.selectedPhotos.count < self.numberOfPhotos {
//                //                let copyAsset = asset.copy() as! PHAsset
//                self.selectedPhotos.add(asset)
//            }
//
//            if self.numberOfPhotos == 1 {
//                self.btnDoneTouched(self.btnDone)
//            }
//            else{
//                //                self.photosCollectionView.reloadData()
//                self.selectedPhotosCollectionView.reloadData()
//
//                self.selectedPhotosCollectionView.scrollToItem(at: IndexPath(row: self.selectedPhotos.count - 1, section: 0), at: UICollectionViewScrollPosition.right, animated: true)
//                self.updateAlbumDetailTitle()
//            }
        }
    }
    
    // Empty data set
//    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
//        let mainView = UIView.init()
//
//        let button = FBSDKLoginButton()
//        button.center = view.center
//        button.readPermissions = ["public_profile","email","user_photos"]
//        mainView.addSubview(button)
//
//        return mainView
//    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "Want to access your Facebook album?"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControlState) -> NSAttributedString? {
        let str = "Login With Facebook"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        let login = FBSDKLoginManager()
//        login.loginBehavior = FBSDKLoginBehavior.web
        login.logIn(withReadPermissions: self.fbReadPermission,
                    from: self) { (result, error) -> Void in
                        if (error != nil){
                            print("process error : \(error?.localizedDescription)")
                            Globals.showAlertWithTitle("Login Error", message: "There is an error occurred while logging in with your Facebook account", viewController: self)
                        }
                        else if (result?.isCancelled)! {
                            print("cancelled")
                            
                        }
                        else {
                            //                    NSLog("Logged in")
                            //                    NSLog("result : %@", result.token)
                            if (FBSDKAccessToken.current() != nil) {
                                self.showLoading()
                                self.fetchListOfUserPhotos()
                            }
                            
                        }
        }
    }
    
    // action
    func fetchListOfUserPhotos()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me/photos", parameters: ["fields":"picture"] )
        print("Token: \(FBSDKAccessToken.current().tokenString)")
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            self.hideLoading()
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                print("fetched user: \(result)")
                
                let fbResult:[String:AnyObject] = result as! [String : AnyObject]
                
                self.photosDataSource = fbResult["data"] as! NSArray?
                
                self.photosCollectionView.reloadData()
                
            }
        })
    }
}
