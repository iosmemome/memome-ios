//
//  StringExtension.swift
//  Memome
//
//  Created by iOS Developer on 30/11/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import Foundation

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
}
