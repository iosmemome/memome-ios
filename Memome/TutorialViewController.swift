//
//  TutorialViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 3/1/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class TutorialViewController: BaseViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pageViewController: UIPageViewController!
    var pageImages: NSMutableArray! = NSMutableArray()
    var sliderDataSource: NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.btnDone.isHidden = true
        
//        self.pageImages.add("tutorial_1.jpg")
//        self.pageImages.add("tutorial_2.jpg")
//        self.pageImages.add("tutorial_3.jpg")
//        self.pageImages.add("tutorial_4.jpg")
        
        loadTutorial()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    func viewControllerAtIndex(_ index: Int) -> TutorialPageContentViewController {
        if (self.pageImages.count == 0) || (index >= self.pageImages.count) {
            return TutorialPageContentViewController()
        }
        
        let vc: TutorialPageContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "TutorialPageContentViewController") as! TutorialPageContentViewController
        vc.imageFile = self.pageImages[index] as! String
        vc.pageIndex = index
        
        return vc
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! TutorialPageContentViewController
        var index = vc.pageIndex as Int
        
        if index >= self.pageImages.count-1 {
            self.btnDone.isHidden = false
        }
        else{
            self.btnDone.isHidden = true
        }
        
        if index == 0 || index == NSNotFound {
            return nil
        }
        
        index -= 1
        return self.viewControllerAtIndex(index)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! TutorialPageContentViewController
        var index = vc.pageIndex as Int
        
        if index >= self.pageImages.count-1 {
            self.btnDone.isHidden = false
        }
        else{
            self.btnDone.isHidden = true
        }
        
        if index == self.pageImages.count-1 || index == NSNotFound {
            return nil
        }
        
        index += 1
        return self.viewControllerAtIndex(index)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.pageImages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    // actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnDoneTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    func loadTutorial(){
        Globals.getDataFromUrl(Globals.getApiUrl("products/howtouse"),
                               requestType: Globals.HTTPRequestType.http_GET,
                               params: [
                                "clientId" : Globals.getProperty("clientId")
            ],
                               completion: { (result) -> Void in
                                
                                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                                    self.sliderDataSource.removeAllObjects()
                                    
                                    if let steps = result.object(forKey: "steps") as? NSArray {
                                        
                                        for step in steps{
                                            if let image = (step as! NSDictionary).object(forKey: "image") as? String{
                                                self.pageImages.add(image)
                                            }
                                        }
                                    }
                                    
                                    self.pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "TutorialPageViewController") as! TutorialPageViewController
                                    self.pageViewController.dataSource = self
                                    self.pageViewController.delegate = self
                                    
                                    let startVC = self.viewControllerAtIndex(0) as TutorialPageContentViewController
                                    let viewControllers = NSArray(object: startVC)
                                    
                                    self.pageViewController.setViewControllers(viewControllers as? [UIViewController], direction: .forward, animated: true, completion: nil)
                                    self.pageViewController.view.frame = CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: self.view.frame.size.height - 50)
                                    
                                    self.addChildViewController(self.pageViewController)
                                    self.view.addSubview(self.pageViewController.view)
                                    self.view.sendSubview(toBack: self.pageViewController.view)
                                    self.pageViewController.didMove(toParentViewController: self)
                                    
                                }
                                else{
                                    
                                    Globals.showAlertWithTitle("Tutorial Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                                    
                                }
        })
    }
    
}
