//
//  HomeViewController.swift
//  Memome
//
//  Created by staff on 12/18/15.
//  Copyright © 2015 Trio Digital Agency. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    
    // You're looking at the wrong place. this view is no longer used.
    // Please see MakePrintViewController. That's the new HOME

    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Globals.isLoggedIn() {
            self.showViewController("MakePrintViewController", attributes: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnRegisterTouched(_ sender: AnyObject) {
        self.showViewController("RegisterViewController", attributes: nil)
    }
    
    @IBAction func btnLoginTouched(_ sender: AnyObject) {
        self.showViewController("LoginViewController", attributes: nil)
    }
}
