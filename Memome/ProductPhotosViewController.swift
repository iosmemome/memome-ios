//
//  ProductPhotosViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/13/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class ProductPhotosViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblPageTitle: UILabel!
    
    var productDataSource: NSMutableArray! = NSMutableArray()
    
//    @IBOutlet weak var photoScrollView: UIScrollView!
//    @IBOutlet weak var photoContentScrollView: UIView!
//    @IBOutlet weak var photoTableView: UITableView!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var btnContinue: UIButton!
    
    var productData: NSDictionary! = NSDictionary()
    var productDetails: NSArray! = []
    
    var photosCount: Int = 0
    var hasCover = false
    var titleProduct: NSDictionary?
    
    var corporateVoucherId: Int = 0
    var corporateVoucherName: String = ""
    var corporateVoucherQty: Int = 0
    var corporateVoucherCode: String = ""
    var corporateVoucher: NSDictionary = NSDictionary()
    
    var project: Project?
    var projectDetails: NSArray?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let cover = MemomeHelper.coverImage
//        let title = cover?.object(forKey: "title") as? String
//        lblPageTitle.text = title
        
        // Do any additional setup after loading the view.
        
        if let attributes = self.attributes {
            
            if let product = attributes.object(forKey: "product") as? NSDictionary{
                if let title = product.object(forKey: "title") as? String{
                    lblPageTitle.text = title
                }
            }
            
//            if let title = attributes.object(forKey: "titleProduct") as? NSDictionary{
//                self.titleProduct = title
//
//                if let title = self.titleProduct{
//                    if let titleName = title.object(forKey: "title") as? String{
//                        lblPageTitle.text = titleName
//                    }
//                }
//            }
            
            if let voucher = attributes.object(forKey: "voucher") as? NSDictionary {
                
                if let id = voucher.object(forKey: "id") as? NSString {
                    self.corporateVoucherId = id.integerValue
                }
                else if let id = voucher.object(forKey: "id") as? NSNumber {
                    self.corporateVoucherId = id.intValue
                }
                
                if let qty = voucher.object(forKey: "qty") as? NSString {
                    self.corporateVoucherQty = qty.integerValue
                }
                else if let qty = voucher.object(forKey: "qty") as? NSNumber {
                    self.corporateVoucherQty = qty.intValue
                }
                
                if let name = voucher.object(forKey: "name") as? String {
                    self.corporateVoucherName = name
                }
                
                self.corporateVoucher = voucher
            }
            
            if let code = attributes.object(forKey: "voucherCode") as? String {
                self.corporateVoucherCode = code
            }
            
            if let proj = attributes.object(forKey: "project") as? Project{
                self.project = proj
                
                self.btnContinue.setTitle("Save Change", for: UIControlState())
            }
            if let details = attributes.object(forKey: "projectDetails") as? NSArray {
                self.projectDetails = details
            }
        }
        
        self.photoCollectionView.register(UINib(nibName: "CoverCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "coverCollectionViewCell")
        self.photoCollectionView.register(UINib(nibName: "ContentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "contentCollectionViewCell")
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
        if let attributes = self.attributes {
            self.productData = attributes.object(forKey: "product") as! NSDictionary
            self.productDetails = attributes.object(forKey: "productDetails") as! NSArray
        }
        
        if productData.object(forKey: "id") as? String == "20" || productData.object(forKey: "id") as? String == "22" || productData.object(forKey: "id") as? String == "24" || productData.object(forKey: "id") as? String == "27"{
            if self.photosCount == 0{
                self.photosCount += 1
            }
            self.hasCover = false
            self.reloadPhotoTableView()
        }
        else{
            self.photosCount = MemomeHelper.images.count //self.productDetails.count
            if let _ = ProjectHelper.getCoverImage(self.project?.id ?? 0) {
                self.photosCount += 1
                self.hasCover = true
            }
            else {
                self.photosCount += 1
                self.hasCover = false
            }
            self.reloadPhotoTableView()
                
            }
            
       
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func addedToMyProject() {
        self.dismissSelf(false){ () -> Void in
            ControllerHelper.getActiveViewController()?.addedToMyProject()
        }
    }
    
    override func addedToMyCart() {
        self.dismissSelf(false){ () -> Void in
            ControllerHelper.getActiveViewController()?.addedToMyCart()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photosCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell!
        
        if (indexPath as NSIndexPath).row == 0 {
            //cover
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coverCollectionViewCell", for: indexPath)
            self.setupCoverPreview(cell)
        }
        else{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contentCollectionViewCell", for: indexPath)
            if MemomeHelper.images.count >= (indexPath as NSIndexPath).row {
                self.setupContentPreview(cell, indexPath: indexPath)
            }
        }
        
        return cell
    }
    
    // collection view flow layout delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionViewCellSizeAtIndexPath(indexPath)
    }
    
    func collectionViewCellSizeAtIndexPath(_ indexPath: IndexPath) -> CGSize{
        var col: Int = 3
        if self.view.frame.size.width > 618 { // ipad
            col = 5
        }
        
        if (indexPath as NSIndexPath).row == 0 {
            col = 1
        }
        
        let width: CGFloat = (self.view.frame.size.width - 2.0) / CGFloat(col)
        var height: CGFloat = width
        
        let index = self.hasCover ? (indexPath as NSIndexPath).row - 1 : (indexPath as NSIndexPath).row - 1
        
        if (indexPath as NSIndexPath).row == 0 {
            let coverWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_width") as! NSString).floatValue)
            let coverHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_height") as! NSString).floatValue)
            let ratio: CGFloat = MemomeHelper.calculateRatio(coverWidth, targetSize: width)
            if self.productData.object(forKey: "cover_content_width") as? NSString == "0"{
                height = 0
            }
            else{
                height = coverHeight * ratio
            }
        }
        else {
            if let detail = self.productDetails.object(at: index) as? NSDictionary {
                let wrapperWidth: CGFloat = CGFloat((detail.object(forKey: "width") as! NSString).floatValue)
                let wrapperHeight: CGFloat = CGFloat((detail.object(forKey: "height") as! NSString).floatValue)
                let ratio: CGFloat = MemomeHelper.calculateRatio(wrapperWidth, targetSize: width)
                height = wrapperHeight * ratio
            }
        }
        //        NSLog("cell size : \(width) x \(height)")
        return CGSize(width: width, height: height)
    }
    
    // Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.photoCollectionView.removeFromSuperview()
        self.dismissSelf()
    }
    
    @IBAction func btnContinueTouched(_ sender: AnyObject) {
        
        var isEdit = false
        if let project = self.project {
            if let details = self.projectDetails {
                isEdit = true
                self.project = ProjectHelper.editMyProject(project, projectDetails: details)
            }
        }
        
        if !isEdit {
            //add to my project
            if self.corporateVoucherId > 0 {
                self.project = ProjectHelper.addToMyProject(self.productData, productDetail: self.productDetails, voucherCode: self.corporateVoucherCode, voucherId: self.corporateVoucherId, voucherName: self.corporateVoucherName, voucherQty: self.corporateVoucherQty)
            }
            else {
                self.project = ProjectHelper.addToMyProject(self.productData, productDetail: self.productDetails)
            }
        }
        
        // close photo preview, cover selector, product detail and back to home
        self.photoCollectionView.removeFromSuperview()
        if self.project != nil {
            if CartHelper.isProductExists(self.project!) {
            }
            else{
                CartHelper.addToCart(self.project!, qty: 1)
                self.updateShoppingCartButton()
            }
        }
        self.addedToMyCart()
    }
    
    func btnEditTouched(_ sender: AnyObject) {
        if let btnEdit = sender as? EditButton {
            if btnEdit.index == 0 {
                //cover
                let attributes: NSMutableDictionary = NSMutableDictionary()
                let coverContentWidth: Float = (self.productData.object(forKey: "cover_content_width") as! NSString).floatValue
                let coverContentHeight: Float = (self.productData.object(forKey: "cover_content_height") as! NSString).floatValue
                attributes.setValue(NSNumber(value: coverContentWidth/coverContentHeight as Float), forKey: "imageRatio")
                if let coverImage = MemomeHelper.coverImage {
                    attributes.setValue(coverImage, forKey: "initialImage")
                }
                attributes.setValue(NSNumber(value: btnEdit.index as Int), forKey: "index")
                self.showViewController("PhotoEditorViewController", attributes: attributes)
            }
            else{
                //content
                let attributes: NSMutableDictionary = NSMutableDictionary()
                if let detail = self.productDetails.object(at: btnEdit.index - 1) as? NSDictionary {
                    let coverContentWidth: Float = (detail.object(forKey: "content_width") as! NSString).floatValue
                    let coverContentHeight: Float = (detail.object(forKey: "content_height") as! NSString).floatValue
                    attributes.setValue(NSNumber(value: coverContentWidth/coverContentHeight as Float), forKey: "imageRatio")
                    if let image = MemomeHelper.images.object(at: btnEdit.index - 1) as? NSDictionary {
                        attributes.setValue(image, forKey: "initialImage")
                    }
                    attributes.setValue(NSNumber(value: btnEdit.index as Int), forKey: "index")
                    self.showViewController("PhotoEditorViewController", attributes: attributes)
                }
                
            }
        }
    }

    
    // General Methods
    func setupCoverPreview(_ cell: UICollectionViewCell) {
        let cellSize = self.collectionViewCellSizeAtIndexPath(IndexPath(row: 0, section: 0))
     
        
        let coverView: UIView! = cell.viewWithTag(1)
        let coverViewBackground: UIImageView! = cell.viewWithTag(2) as! UIImageView
        let coverImageView: UIImageView! = cell.viewWithTag(3) as! UIImageView
        let coverTitleView: UIView! = cell.viewWithTag(4)
        let coverTitleInnerView: UIView! = cell.viewWithTag(5)
        let lblCoverTitle: UILabel! = cell.viewWithTag(6) as! UILabel
        let lblCoverSubtitle: UILabel! = cell.viewWithTag(7) as! UILabel
        let btnEdit: EditButton = cell.viewWithTag(99) as! EditButton
        
        lblCoverTitle.text = lblCoverTitle.text?.uppercased()
        lblCoverSubtitle.text = lblCoverSubtitle.text?.uppercased()
        
        let coverWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_width") as! NSString).floatValue)
        let coverHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_height") as! NSString).floatValue)
        let coverBorderTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_border_top") as! NSString).floatValue)
        let coverBorderLeft: CGFloat = CGFloat((self.productData.object(forKey: "cover_border_left") as! NSString).floatValue)
        let coverContentWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_content_width") as! NSString).floatValue)
        let coverContentHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_content_height") as! NSString).floatValue)
        let coverTitleAreaWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_width") as! NSString).floatValue)
        let coverTitleAreaHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_height") as! NSString).floatValue)
        let coverTitleAreaTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_top") as! NSString).floatValue)
        let coverTitleAreaLeft: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_left") as! NSString).floatValue)
        let coverTitleTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_top") as! NSString).floatValue)
        let coverSubtitleTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_subtitle_top") as! NSString).floatValue)
        let coverTitleFontSize: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_font_size") as! NSString).floatValue)
        let coverSubtitleFontSize: CGFloat = CGFloat((self.productData.object(forKey: "cover_subtitle_font_size") as! NSString).floatValue)
        
        //        NSLog("cover width : \(coverView.frame.size.width)")
        //        let ratio: CGFloat = MemomeHelper.calculateRatio(coverWidth, targetSize: coverView.frame.size.width)
        //        NSLog("cover width : \(cellSize.width - 16)")
        let ratio: CGFloat = MemomeHelper.calculateRatio(coverWidth, targetSize: cellSize.width - 16)
        let coverTitleAreaBorder: CGFloat = MemomeHelper.coverTitleAreaBorder
        
        if let coverImages = self.productData.object(forKey: "cover_images") as? NSDictionary {
            if let image = coverImages.object(forKey: "thumb") as? String {
                Globals.setImageFromUrl(image, imageView: coverViewBackground, placeholderImage: nil)
            }
        }
        
        //cover size
        if let height = Globals.getConstraint("height", view: coverView) {
            height.constant = coverHeight * ratio
        }
        
        //cover content
        if let top = Globals.getConstraint("coverImageTop", view: coverView) {
            top.constant = coverBorderTop * ratio
        }
        if let left = Globals.getConstraint("coverImageLeft", view: coverView) {
            left.constant = coverBorderLeft * ratio
        }
        if let width = Globals.getConstraint("width", view: coverImageView) {
            width.constant = coverContentWidth * ratio
        }
        if let height = Globals.getConstraint("height", view: coverImageView) {
            height.constant = coverContentHeight * ratio
        }
        
        
        //title area
        if let top = Globals.getConstraint("titleViewTop", view: coverView) {
            top.constant = coverTitleAreaTop * ratio
        }
        if let left = Globals.getConstraint("titleViewLeft", view: coverView) {
            left.constant = coverTitleAreaLeft * ratio 
        }
        if let width = Globals.getConstraint("width", view: coverTitleView) {
            width.constant = coverTitleAreaWidth * ratio
        }
        if let height = Globals.getConstraint("height", view: coverTitleView) {
            height.constant = coverTitleAreaHeight * ratio
        }
        
        if let top = Globals.getConstraint("titleInnerTop", view: coverTitleView) {
            top.constant = coverTitleAreaBorder * ratio
        }
        if let left = Globals.getConstraint("titleInnerLeft", view: coverTitleView) {
            left.constant = coverTitleAreaBorder * ratio
        }
        if let bottom = Globals.getConstraint("titleInnerBottom", view: coverTitleView) {
            bottom.constant = coverTitleAreaBorder * ratio
        }
        if let right = Globals.getConstraint("titleInnerRight", view: coverTitleView) {
            right.constant = coverTitleAreaBorder * ratio
        }
        
        //title & subtitle
        if let top = Globals.getConstraint("titleTop", view: coverTitleInnerView) {
            top.constant = (coverTitleTop - coverTitleAreaBorder) * ratio
        }
        if let top = Globals.getConstraint("subtitleTop", view: coverTitleInnerView) {
            top.constant = (coverSubtitleTop - coverTitleAreaBorder) * ratio
        }
        
        let coverTitleFont = lblCoverTitle.font
//        lblCoverTitle.font = coverTitleFont?.withSize( coverTitleFontSize * ratio )
        lblCoverTitle.font = coverTitleFont?.withSize( 40 )
//        lblCoverTitle.text = lblCoverTitle.text?.uppercased()
        let coverSubtitleFont = lblCoverSubtitle.font
//        lblCoverSubtitle.font = coverSubtitleFont?.withSize( coverSubtitleFontSize * ratio )
        lblCoverSubtitle.font = coverSubtitleFont?.withSize( 15 )
//        lblCoverSubtitle.text = lblCoverSubtitle.text?.uppercased()
        
//        coverView.layer.borderColor = UIColor(white: 0.25, alpha: 1).cgColor
//        coverView.layer.borderWidth = 1.0
//        coverTitleView.layer.borderColor = UIColor(white: 0.57, alpha: 1).cgColor
//        coverTitleView.layer.borderWidth = 1.0
        
        let coverImage = MemomeHelper.coverImage
        if coverImage != nil {
            if let url = coverImage?.object(forKey: "url") as? String {
                var x: CGFloat = 0.0
                var y: CGFloat = 0.0
                var width: CGFloat = 0.0
                var height: CGFloat = 0.0
                var scale: CGFloat = 1
                var rotation: Int = 0
                if let zoomScale = coverImage?.object(forKey: "zoomScale") as? NSNumber {
                    scale = CGFloat(zoomScale.floatValue)
                }
                if let size = coverImage?.object(forKey: "size") as? NSValue {
                    width = size.cgSizeValue.width * scale
                    height = size.cgSizeValue.height * scale
                }
                if let offset = coverImage?.object(forKey: "offset") as? NSValue {
                    x = offset.cgPointValue.x * scale
                    y = offset.cgPointValue.y * scale
                }
                if let rotate = coverImage?.object(forKey: "rotation") as? NSNumber {
                    rotation = rotate.intValue
                }
                let screenScale = max( coverImageView.frame.size.width / width, coverImageView.frame.size.height / height )
                let cropRect = CGRect(x: x * screenScale, y: y * screenScale, width: width * screenScale, height: height * screenScale)

                Globals.checkImageFrom(url, scale: scale * screenScale, resultHandler: { (image) -> Void in
                    if let img = image {
                        coverImageView.image = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect)
                    }
                })
            }
            
            if let title = coverImage?.object(forKey: "title") as? String {
                lblCoverTitle.text = title
//                lblPageTitle.text = title
            }
            if let subtitle = coverImage?.object(forKey: "subtitle") as? String {
                lblCoverSubtitle.text = subtitle
            }
        }
        
        btnEdit.index = 0
        btnEdit.addTarget(self, action: #selector(ProductPhotosViewController.btnEditTouched(_:)), for: UIControlEvents.touchUpInside)
        
    }
    
    func setupContentPreview(_ cell: UICollectionViewCell, indexPath: IndexPath) {
        let cellSize = self.collectionViewCellSizeAtIndexPath(indexPath)
        let contentView: UIView! = cell.viewWithTag(1)
        let contentViewBackground: UIImageView! = cell.viewWithTag(2) as! UIImageView
        let contentImageView: UIImageView! = cell.viewWithTag(3) as! UIImageView
        let btnEdit: EditButton = cell.viewWithTag(99) as! EditButton
        
        let index = self.hasCover ? (indexPath as NSIndexPath).row - 1 : (indexPath as NSIndexPath).row - 1
//        let projectIndex = self.hasCover ? (indexPath as NSIndexPath).row : (indexPath as NSIndexPath).row - 1
        
        //        let index = indexPath.row - 1
        
        if let detail = self.productDetails.object(at: index) as? NSDictionary {
            let wrapperWidth: CGFloat = CGFloat((detail.object(forKey: "width") as! NSString).floatValue)
            let wrapperHeight: CGFloat = CGFloat((detail.object(forKey: "height") as! NSString).floatValue)
            let borderTop: CGFloat = CGFloat((detail.object(forKey: "border_top") as! NSString).floatValue)
            let borderLeft: CGFloat = CGFloat((detail.object(forKey: "border_left") as! NSString).floatValue)
            let borderBottom: CGFloat = CGFloat((detail.object(forKey: "border_top") as! NSString).floatValue)
            let contentWidth: CGFloat = CGFloat((detail.object(forKey: "content_width") as! NSString).floatValue)
            let contentHeight: CGFloat = CGFloat((detail.object(forKey: "content_height") as! NSString).floatValue)
            
            var parentWidth: CGFloat = cellSize.width
            //content size
            parentWidth -= 16
            //            if let leading = Globals.getConstraint("leading", view: contentView) {
            //                parentWidth -= leading.constant
            //            }
            //
            //            if let trailing = Globals.getConstraint("trailing", view: contentView) {
            //                parentWidth -= trailing.constant
            //            }
            
            NSLog("widht after : %f", parentWidth)
            
            let ratio: CGFloat = MemomeHelper.calculateRatio(wrapperWidth, targetSize: parentWidth)
            
            if let contentImages = detail.object(forKey: "images") as? NSDictionary {
                if let image = contentImages.object(forKey: "thumb") as? String {
                    Globals.setImageFromUrl(image, imageView: contentViewBackground, placeholderImage: nil)
                }
            }
            
            //content size
            if let height = Globals.getConstraint("height", view: contentView) {
                height.constant = wrapperHeight * ratio
            }
            
            //content content
            
            if let top = Globals.getConstraint("contentImageTop", view: contentView) {
                top.constant = borderTop * ratio
            }
            if let left = Globals.getConstraint("contentImageLeft", view: contentView) {
                left.constant = borderLeft * ratio
            }
            if let bottom = Globals.getConstraint("contentImageBottom", view: contentView) {
                bottom.constant = borderBottom * ratio
            }
            if let width = Globals.getConstraint("width", view: contentImageView) {
                width.constant = contentWidth * ratio
            }
            if let height = Globals.getConstraint("height", view: contentImageView) {
                height.constant = contentHeight * ratio
            }
            
            contentView.layer.borderColor = UIColor(white: 0.25, alpha: 1).cgColor
            contentView.layer.borderWidth = 1.0
            
            if let image = MemomeHelper.images.object(at: (indexPath as NSIndexPath).row - 1) as? NSDictionary {
                if let url = image.object(forKey: "url") as? String {
                    var x: CGFloat = 0.0
                    var y: CGFloat = 0.0
                    var width: CGFloat = 0.0
                    var height: CGFloat = 0.0
                    var scale: CGFloat = 1
                    var rotation: Int = 0
                    if let zoomScale = image.object(forKey: "zoomScale") as? NSNumber {
                        scale = CGFloat(zoomScale.floatValue)
                    }
                    if let size = image.object(forKey: "size") as? NSValue {
                        width = size.cgSizeValue.width * scale
                        height = size.cgSizeValue.height * scale
                    }
                    if let offset = image.object(forKey: "offset") as? NSValue {
                        x = offset.cgPointValue.x * scale
                        y = offset.cgPointValue.y * scale
                    }
                    if let rotate = image.object(forKey: "rotation") as? NSNumber {
                        rotation = rotate.intValue
                    }
                    let screenScale = max( contentImageView.frame.size.width / width, contentImageView.frame.size.height / height)
                    let cropRect = CGRect(x: x * screenScale, y: y * screenScale, width: width * screenScale, height: height * screenScale)

                    Globals.checkImageFrom(url, scale: scale * screenScale, resultHandler: { (image) -> Void in
                        if let img = image {
                            autoreleasepool(invoking: { () -> () in
                                contentImageView.image = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect)
                            })
                        }
                    })
                }
            }
        }
        
        btnEdit.index = (indexPath as NSIndexPath).row
        btnEdit.addTarget(self, action: #selector(ProductPhotosViewController.btnEditTouched(_:)), for: UIControlEvents.touchUpInside)

    }
    
    func reloadPhotoTableView() {
        self.photoCollectionView.reloadData()
        
        if let height = Globals.getConstraint("height", view: self.photoCollectionView) {
            height.constant = self.photoCollectionView.contentSize.height
        }
    }
}
