//
//  CartHelper.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/26/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import CoreData

class CartHelper: NSObject {

    static func isProductExists(_ project: Project) -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cart", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(projectId = %i)", project.id)
        request.predicate = pred
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count > 0 {
            return true
        }
        
        return false
    }
    
    static func addToCart(_ project: Project, qty: Int32) -> Int32{
        return addToCart(project, qty: qty, reorder: 0)
    }
    
    static func addToCart(_ project: Project, qty: Int32, reorder: Int32) -> Int32{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        var cart: Cart
        cart = NSEntityDescription.insertNewObject(forEntityName: "Cart", into: context) as! Cart
        
        let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData as Data) as! NSDictionary
        
        cart.projectId = project.id
        cart.qty = qty
        cart.reorderId = reorder
        if let price = productData.object(forKey: "price") as? NSString {
            cart.price = price.doubleValue
            cart.priceNormal = price.doubleValue
        }
        if let price = productData.object(forKey: "special_price") as? NSString {
            if price.doubleValue > 0 {
                cart.price = price.doubleValue
            }
        }
        
        cart.total = Double(cart.qty) * cart.price
        
        do {
            try context.save()
        } catch let error1 as NSError {
            error??.pointee = error1
        }
        if error != nil {
            print("Cannot store Cart")
            return 0
        }
        else{
            return cart.id
        }
    }
    
    static func updateCart(_ cartId: Int32, qty: Int32) -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cart", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(id = %i)", cartId)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count > 0 {
            let cart: Cart = objects[0] as! Cart
            cart.qty = qty
            cart.total = Double(cart.qty) * cart.price
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
            if error != nil {
                print("Error updating \(entityDesc) - error:\(error)");
                return false
            }
        }
        
        return true
    }
    
    static func deleteFromCart(_ cartId: Int32) -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cart", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(id = %i)", cartId)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count > 0 {
            context.delete(objects[0] as! NSManagedObject)
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
            if error != nil {
                print("Error deleting \(entityDesc) - error:\(error)");
                return false
            }
        }
        
        return true
    }
    
    static func clearCart() -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cart", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        for object in objects {
            context.delete(object as! NSManagedObject)
        }
        
        do {
            try context.save()
        } catch let error1 as NSError {
            error??.pointee = error1
        }
        if error != nil {
            print("Error deleting \(entityDesc) - error:\(error)");
            return false
        }
        
        return true
    }
    
    static func getCart() -> NSArray {
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cart", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        return objects as NSArray
    }
    
}
