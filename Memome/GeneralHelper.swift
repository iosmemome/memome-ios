//
//  GeneralHelper.swift
//  Memome
//
//  Created by iOS Developer on 19/12/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import Foundation

class GeneralHelper: NSObject {
    static func calculateShipping(_ date: Date, from: Int, to: Int) -> String {
        let today = date
        let minDate = Calendar.current.date(byAdding: .day, value: from, to: today)
        let maxDate = Calendar.current.date(byAdding: .day, value: to, to: today)
        
        let isInSameYear = Calendar.current.isDate(minDate ?? today, equalTo: maxDate ?? today, toGranularity: .year)
        let isInSameMonth = Calendar.current.isDate(minDate ?? today, equalTo: maxDate ?? today, toGranularity: .month)
        
        let minFormatter = DateFormatter()
        let maxFormatter = DateFormatter()
        
        if isInSameYear {
            if isInSameMonth {
                minFormatter.dateFormat = "dd"
                maxFormatter.dateFormat = "dd MMM yyyy"
            }
            else {
                minFormatter.dateFormat = "dd MMM"
                maxFormatter.dateFormat = "dd MMM yyyy"
            }
        }
        else {
            minFormatter.dateFormat = "dd MMM yyyy"
            maxFormatter.dateFormat = "dd MMM yyyy"
        }
        
        return minFormatter.string(from: minDate ?? today) + " - " + maxFormatter.string(from: maxDate ?? today)
    }
}
