//
//  AddressViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/27/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import BSDropdown

class AddressViewController: BaseViewController, BSDropdownDelegate, BSDropdownDataSource {
    @IBOutlet weak var addressScrollView: UIScrollView!
    @IBOutlet weak var addressContentScrollView: UIView!

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var useAddressWrapper: UIView!
    @IBOutlet weak var btnAddressChoose: BSDropdown!
    
    @IBOutlet weak var newAddressWrapper: UIView!
    @IBOutlet weak var txtFirstWrapper: UIView!
    @IBOutlet weak var txtLastWrapper: UIView!
    @IBOutlet weak var txtAddressWrapper: UIView!
    @IBOutlet weak var btnProvinceWrapper: UIView!
    @IBOutlet weak var btnCityWrapper: UIView!
    @IBOutlet weak var txtPostalCodeWrapper: UIView!
    @IBOutlet weak var txtPhoneWrapper: UIView!
    @IBOutlet weak var txtEmailWrapper: UIView!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var btnProvince: BSDropdown!
    @IBOutlet weak var btnCity: BSDropdown!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var chkSaveAddress: UISwitch!
    @IBOutlet weak var saveAddressWrapper: UIView!
    
    var provinceDataSource: NSMutableArray! = NSMutableArray()
    var cityDataSource: NSMutableArray! = NSMutableArray()
    var addressDataSource: NSMutableArray! = NSMutableArray()
    
    var addressId: Int = 0
    var dataAddress: NSDictionary?
    var selectedProvinceId: Int = 0
    var selectedCityId: Int = 0
    var email = ""
    
    var isManualAddress: Bool = false // for check out - input manually shipping address
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        self.useAddressWrapper.layer.cornerRadius = 5
//        self.btnAddressChoose.layer.cornerRadius = 5
        self.newAddressWrapper.layer.cornerRadius = 5
        self.txtFirstWrapper.layer.cornerRadius = 5
        self.txtFirstWrapper.layer.borderWidth = 1
        self.txtFirstWrapper.layer.borderColor = UIColor.lightGray.cgColor
        self.txtLastWrapper.layer.cornerRadius = 5
        self.txtLastWrapper.layer.borderWidth = 1
        self.txtLastWrapper.layer.borderColor = UIColor.lightGray.cgColor
        self.txtAddressWrapper.layer.cornerRadius = 5
        self.txtAddressWrapper.layer.borderWidth = 1
        self.txtAddressWrapper.layer.borderColor = UIColor.lightGray.cgColor
        self.btnProvinceWrapper.layer.cornerRadius = 5
        self.btnProvinceWrapper.layer.borderWidth = 1
        self.btnProvinceWrapper.layer.borderColor = UIColor.lightGray.cgColor
        self.btnCityWrapper.layer.cornerRadius = 5
        self.btnCityWrapper.layer.borderWidth = 1
        self.btnCityWrapper.layer.borderColor = UIColor.lightGray.cgColor
        self.txtPostalCodeWrapper.layer.cornerRadius = 5
        self.txtPostalCodeWrapper.layer.borderWidth = 1
        self.txtPostalCodeWrapper.layer.borderColor = UIColor.lightGray.cgColor
        self.txtPhoneWrapper.layer.cornerRadius = 5
        self.txtPhoneWrapper.layer.borderWidth = 1
        self.txtPhoneWrapper.layer.borderColor = UIColor.lightGray.cgColor
        self.txtEmailWrapper.layer.cornerRadius = 5
        self.txtEmailWrapper.layer.borderWidth = 1
        self.txtEmailWrapper.layer.borderColor = UIColor.lightGray.cgColor
        
        self.defaultScrollView = self.addressScrollView
        self.defaultContentScrollView = self.addressContentScrollView
        
//        if let _ = self._parentViewController as? CheckoutViewController {
//            self.isManualAddress = true
//        }
        
        if let address = self.attributes.object(forKey: "address") as? NSDictionary {
            self.dataAddress = address
            self.loadAddress(address)
            
            if let addressId = address.object(forKey: "id") as? NSString {
                self.addressId = addressId.integerValue
            }

            self.lblTitle.text = "EDIT ADDRESS"
            self.btnSubmit.setTitle("EDIT ADDRESS", for: UIControlState())
            self.btnDelete.isHidden = false
//            self.useAddressWrapper.isHidden = true
//            self.viewHeader.isHidden = true
            
        }
        else {
            self.lblTitle.text = "ADD ADDRESS"
            self.btnSubmit.setTitle("ADD ADDRESS", for: UIControlState())
            self.btnDelete.isHidden = true
        }
        
        if self.isManualAddress {
            self.lblTitle.text = "INPUT ADDRESS"
            self.btnSubmit.setTitle("SUBMIT", for: UIControlState())
            self.btnDelete.isHidden = true
            
            self.addressId = 0
            
            self.saveAddressWrapper.isHidden = false
            self.chkSaveAddress.isOn = false
        }
        else{
            self.saveAddressWrapper.isHidden = true
            if let height = Globals.getConstraint("height", view: self.saveAddressWrapper) {
                height.constant = 0
            }
        }
        
        let mainColor = UIColor.colorPrimary
        let titleFont = UIFont(name: "Montserrat-SemiBold", size: 15)
        let buttonFont = UIFont(name: "Montserrat-Regular", size: 14)
        
//        self.btnAddressChoose.viewController = self
//        self.btnAddressChoose.defaultTitle = "Add"
//        self.btnAddressChoose.title = "Address"
//        self.btnAddressChoose.headerBackgroundColor = mainColor
//        self.btnAddressChoose.itemTintColor = mainColor
//        self.btnAddressChoose.titleFont = titleFont
//        self.btnAddressChoose.buttonFont = buttonFont
//        self.btnAddressChoose.titleKey = "name"
//        self.btnAddressChoose.fixedDisplayedTitle = true
//        self.btnAddressChoose.delegate = self
//        self.btnAddressChoose.dataSource = self
//        self.btnAddressChoose.hideDoneButton = true
//        self.btnAddressChoose.setup()
        
        self.btnProvince.viewController = self
        self.btnProvince.defaultTitle = "Province"
        self.btnProvince.title = "Province"
        self.btnProvince.headerBackgroundColor = mainColor
        self.btnProvince.itemTintColor = mainColor
        self.btnProvince.titleFont = titleFont
        self.btnProvince.buttonFont = buttonFont
        self.btnProvince.titleKey = "name"
        self.btnProvince.delegate = self
        self.btnProvince.hideDoneButton = true
        self.btnProvince.setup()
        
        self.btnCity.viewController = self
        self.btnCity.defaultTitle = "City"
        self.btnCity.title = "City"
        self.btnCity.headerBackgroundColor = mainColor
        self.btnCity.itemTintColor = mainColor
        self.btnCity.titleFont = titleFont
        self.btnCity.buttonFont = buttonFont
        self.btnCity.titleKey = "name"
        self.btnCity.delegate = self
        self.btnCity.hideDoneButton = true
        self.btnCity.setup()
        
        self.initRefreshControl(self.addressScrollView)
        self.txtEmailWrapper.isHidden = true
        
        if let height = Globals.getConstraint("height", view: txtEmailWrapper){
            height.constant = 0
        }
        
        self.loadDataAddress()
        self.loadDataProvince()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func refresh(_ sender: AnyObject) {
        super.refresh(sender)
        
        self.loadDataProvince()
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.txtFirstName {
            self.txtLastName.becomeFirstResponder()
        }
        else if textField == self.txtLastName {
            self.txtPostalCode.becomeFirstResponder()
        }
        else if textField == self.txtPostalCode {
            self.txtAddress.becomeFirstResponder()
        }
        else if textField == self.txtAddress {
            self.txtPhone.becomeFirstResponder()
        }
        else if textField == self.txtPhone {
            self.txtEmail.becomeFirstResponder()
        }
        else if textField == self.txtEmail {
            self.btnSubmitTouched(self.btnSubmit)
        }
        
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // bsdropdown datasource
    func itemHeightForRowAtIndexPath(_ dropdown: BSDropdown, tableView: UITableView, item: NSDictionary?, indexPath: IndexPath) -> CGFloat {
        if let cellInfoArray = item {
            var height: CGFloat = 30.0
            var name: String = ""
            var lastname: String = ""
            var address: String = ""
            var area: String = ""
            var phone: String = ""
            if let firstName = cellInfoArray.object(forKey: "first_name") as? String {
                name = firstName
            }
            else{
                name = ""
            }
            
            if let lastName = cellInfoArray.object(forKey: "last_name") as? String {
                lastname = lastName
            } else{
                lastname = ""
            }
            
            if let a = cellInfoArray.object(forKey: "address") as? String {
                address = a
            }
            
            var city = ""
            var province = ""
            var zipCode = ""
            if let item = cellInfoArray.object(forKey: "cityName") as? String {
                city = item
            }
            if let item = cellInfoArray.object(forKey: "provinceName") as? String {
                province = item
            }
            
            if let item = cellInfoArray.object(forKey: "postal_code") as? String {
                zipCode = item
            }
            
            if city != "" {
                area = area + city
            }
            if province != "" {
                if area != "" {
                    area = area + ", "
                }
                area = area + province
            }
            if zipCode != "" {
                if area != "" {
                    area = area + " - "
                }
                area = area + zipCode
            }
            
            if let p = cellInfoArray.object(forKey: "phone") as? String {
                phone = p
            }
            
            
            let nameWidth = tableView.frame.size.width - 54
            let maxWidth = tableView.frame.size.width - 16
            let maxFloatHeight : CGFloat = 9999
            
            height += name.boundingRect(
                with: CGSize(width: nameWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 14)!],
                context: nil
                ).size.height
            height += address.boundingRect(
                with: CGSize(width: maxWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 13)!],
                context: nil
                ).size.height
            height += area.boundingRect(
                with: CGSize(width: maxWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 13)!],
                context: nil
                ).size.height
            height += phone.boundingRect(
                with: CGSize(width: maxWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 13)!],
                context: nil
                ).size.height
            
            return height
        }
        else {
            return 0
        }
    }
    
    func itemForRowAtIndexPath(_ dropdown: BSDropdown, tableView: UITableView, item: NSDictionary?, indexPath: IndexPath) -> UITableViewCell {
        if let cellInfoArray = item {
            tableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "addressTableViewCell")
            let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "addressTableViewCell", for: indexPath)
            
            let lblName = cell.viewWithTag(1) as! UILabel
            let lblAddress = cell.viewWithTag(2) as! UILabel
            let lblCity = cell.viewWithTag(3) as! UILabel
            let lblPhone = cell.viewWithTag(4) as! UILabel
            let btnEdit = cell.viewWithTag(5) as! EditButton
            
            if let firstName = cellInfoArray.object(forKey: "first_name") as? String {
                lblName.text = firstName
            }
            else{
                lblName.text = ""
            }
            
            if let lastName = cellInfoArray.object(forKey: "last_name") as? String {
                lblName.text = lastName
            }
            
            if let address = cellInfoArray.object(forKey: "address") as? String {
                lblAddress.text = address
            }
            else{
                lblAddress.text = ""
            }
            
            var city = ""
            var province = ""
            var zipCode = ""
            if let item = cellInfoArray.object(forKey: "cityName") as? String {
                city = item
            }
            if let item = cellInfoArray.object(forKey: "provinceName") as? String {
                province = item
            }
            
            if let item = cellInfoArray.object(forKey: "postal_code") as? String {
                zipCode = item
            }
            
            lblCity.text = ""
            if city != "" {
                lblCity.text = lblCity.text! + city
            }
            if province != "" {
                if lblCity.text != "" {
                    lblCity.text = lblCity.text! + ", "
                }
                lblCity.text = lblCity.text! + province
            }
            if zipCode != "" {
                if lblCity.text != "" {
                    lblCity.text = lblCity.text! + " - "
                }
                lblCity.text = lblCity.text! + zipCode
            }
            
            if let phone = cellInfoArray.object(forKey: "phone") as? String {
                lblPhone.text = phone
            }
            else{
                lblPhone.text = ""
            }
            
            btnEdit.isHidden = true
            
            return cell
        }
        else {
            return tableView.dequeueReusableCell(withIdentifier: "addressTableViewCell", for: indexPath)
        }
    }
    
    // BSDropdown delegate
    func onDropdownSelectedItemChange(_ dropdown: BSDropdown, selectedItem: NSDictionary?) {
        if dropdown == self.btnAddressChoose {
            if let parent = self._parentViewController as? CheckoutViewController {
                parent.setupSelectedAddress(selectedItem)
            }
            
            self.dismissSelf()
        }
        else
        if dropdown == self.btnProvince {
            if let selectedProvince = selectedItem {
                if let id = selectedProvince.object(forKey: "id") as? NSString {
                    self.selectedProvinceId = id.integerValue
                    self.btnProvince.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
                    self.selectedCityId = 0
                    self.btnCity.setSelectedIndex(-1)
                }
                else{
                    self.selectedProvinceId = 0
                    self.btnProvince.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                }
            }
            else{
                self.selectedProvinceId = 0
                self.btnProvince.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
            }
            
            self.loadDataCity()
        }
        else{
            if let selectedCity = selectedItem {
                if let id = selectedCity.object(forKey: "id") as? NSString {
                    self.selectedCityId = id.integerValue
                    self.btnCity.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
                }
                else{
                    self.selectedCityId = 0
                    self.btnCity.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                }
            }
            else{
                self.selectedCityId = 0
                self.btnCity.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
            }
        }
    }

    // Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnSubmitTouched(_ sender: AnyObject) {
        if self.isManualAddress && !self.chkSaveAddress.isOn {
            self.submitManualInputAddress()
        }
        else {
            if !self.isLoading() {
                self.showLoading()
                let params = NSMutableDictionary()
                var url: String = Globals.getApiUrl("users/addAddress")
                if self.addressId > 0 {
                    url = Globals.getApiUrl("users/updateAddress")
                    params.setValue(String(self.addressId), forKey: "userAddressId")
                }
                
                let arrayName = self.txtFirstName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).components(separatedBy: " ")
                
                
                let arrayLastName = self.txtLastName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).components(separatedBy: " ")
                var firstName = ""
                var lastName = ""
                if arrayName != nil && (arrayName?.count)! > 0 {
                    for i in 0 ..< (arrayName?.count)! {
                        if i == 0 {
                            firstName = (arrayName?[i])!
                        }
                        
                    }
                }
                if arrayLastName != nil && (arrayLastName?.count)! > 0 {
                    for i in 0 ..< (arrayLastName?.count)! {
                        if i == 0 {
                            lastName = (arrayLastName?[i])!
                        }
                        
                    }
                }
                
                params.setValue(Globals.getProperty("clientId"), forKey: "clientId")
                params.setValue(Globals.getDataSetting("accessToken"), forKey: "accessToken")
                params.setValue(firstName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "firstName")
                params.setValue(lastName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "lastName")
                params.setValue(self.txtPhone.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "phone")
                params.setValue(self.txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "email")
                params.setValue(self.txtAddress.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "address")
                params.setValue(self.txtPostalCode.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "postalCode")
                params.setValue(String(self.selectedCityId), forKey: "cityId")
                params.setValue(String(self.selectedProvinceId), forKey: "provinceId")
                
                Globals.getDataFromUrl(url,
                    requestType: Globals.HTTPRequestType.http_POST,
                    params: params,
                    completion: { (result) -> Void in
                        self.hideLoading()
                        
                        if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                            
                            if self.isManualAddress {
                                self.submitManualInputAddress()
                            }
                            else {
                                if let parent = self._parentViewController as? CheckoutViewController{
                                    BSNotification.show("Your address has been saved successfully", view: parent)
//                                    self.showViewController("CheckoutViewController", attributes: nil)
                                    parent.loadDataAddress()
//                                    parent.showUseAddress()
                                }
                                
                                self.dismissSelf()
                                
                                if let parent = self._parentViewController as? ProfileViewController {
                                    parent.loadData()
                                }
                            }
                            
                            
                        }
                        else{
                            Globals.showAlertWithTitle("Address Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                        }
                })
            }
        }
    }
    
    @IBAction func btnDeleteTouched(_ sender: AnyObject) {
        if !self.isLoading() {
            Globals.showConfirmAlertWithTitle("Delete Address", message: "Are you sure want to delete this address?\nYou can't undo this action.", viewController: self, completion: { (action) -> Void in
                
                
                self.showLoading()
                
                self.provinceDataSource.removeAllObjects()
                
                Globals.getDataFromUrl(Globals.getApiUrl("users/deleteAddress"),
                    requestType: Globals.HTTPRequestType.http_GET,
                    params: [
                        "clientId" : Globals.getProperty("clientId"),
                        "accessToken" : Globals.getDataSetting("accessToken") as! String,
                        "userAddressId" : String(self.addressId)
                    ],
                    completion: { (result) -> Void in
                        self.hideLoading()
                        
                        if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                            
                            self.btnBackTouched(sender)
                            
                            if let parent = self._parentViewController as? ProfileViewController {
                                parent.loadData()
                            }
                            
                        }
                        else{
                            Globals.showAlertWithTitle("Delete Address Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                        }
                })
                
            })
        }
    }
    
    // general method
    func loadAddress(_ address: NSDictionary) {
        if let firstName = address.object(forKey: "first_name") as? String {
            self.txtFirstName.text = firstName
        }
        
        if let lastName = address.object(forKey: "last_name") as? String {
            self.txtLastName.text = lastName
        }
        
        if let address = address.object(forKey: "address") as? String {
            self.txtAddress.text = address
        }
        
        if let postalCode = address.object(forKey: "postal_code") as? String {
            self.txtPostalCode.text = postalCode
        }
        
        if let phone = address.object(forKey: "phone") as? String {
            self.txtPhone.text = phone
        }
        
//        if let email = address.object(forKey: "email") as? String {
//            self.txtEmail.text = email
//        }
        self.txtEmail.text = email
        
        if let province = address.object(forKey: "province_id") as? NSString {
            self.selectedProvinceId = province.integerValue
        }
        
        if let city = address.object(forKey: "city_id") as? NSString {
            self.selectedCityId = city.integerValue
        }
        
        if self.provinceDataSource.count > 0 {
            var i = 0
            for item in self.provinceDataSource {
                if let province = item as? NSDictionary {
                    if let provinceId = province.object(forKey: "id") as? NSString {
                        if provinceId.integerValue == self.selectedProvinceId {
                            self.btnProvince.setSelectedIndex(i)
                            self.btnProvince.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
                        }
                    }
                }
                i += 1
            }
        }
    }
    
    func loadDataAddress() {
//        if !self.isLoading() {
//            self.showLoading()
            Globals.getDataFromUrl(Globals.getApiUrl("users/view"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken")!
                ],
                completion: { (result) -> Void in
                    self.hideLoading()

                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){

                        self.addressDataSource.removeAllObjects()
                        if let user = result.object(forKey: "user") as? NSDictionary{
                            if let userEmail = user.object(forKey: "email") as? String{
                                self.email = userEmail
                                self.txtEmail.text = self.email
                            }
                        }
                        if let addresses = result.object(forKey: "userAddresses") as? NSArray {
                            self.addressDataSource.addObjects(from: addresses as [AnyObject])
                            
//                            self.showUseAddress()
//                            self.btnAddressChoose.setDataSource(self.addressDataSource)
                        }
                    }
                    else{
                        Globals.showAlertWithTitle("Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
//        }
    }
    
    
    
//    func showUseAddress() {
//        if addressDataSource.count > 0 {
//            if let height = Globals.getConstraint("height", view: self.useAddressWrapper) {
//                height.constant = 60
//            }
//            if let top = Globals.getConstraint("useAddressTop", view: self.useAddressWrapper) {
//                top.constant = 8
//            }
//        }
//        else {
//            if let height = Globals.getConstraint("height", view: self.useAddressWrapper) {
//                height.constant = 0
//            }
//            if let top = Globals.getConstraint("useAddressTop", view: self.useAddressWrapper) {
//                top.constant = 0
//            }
//        }
//    }
    
    func loadDataProvince() {
        if !self.isLoading() {
            self.showLoading()
            
            self.provinceDataSource.removeAllObjects()
            
            Globals.getDataFromUrl(Globals.getApiUrl("regions/getProvinces"),
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId")
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){

                        if let provinces = result.object(forKey: "provinces") as? NSArray {
                            self.provinceDataSource.addObjects(from: provinces as [AnyObject])
                        }
                        
                        self.btnProvince.setDataSource(self.provinceDataSource)
                        
                        if self.selectedProvinceId != 0 {
                            var i = 0
                            for item in self.provinceDataSource {
                                if let province = item as? NSDictionary {
                                    if let provinceId = province.object(forKey: "id") as? NSString {
                                        if provinceId.integerValue == self.selectedProvinceId {
                                            self.btnProvince.setSelectedIndex(i)
                                            self.btnProvince.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
                                        }
                                    }
                                }
                                i += 1
                            }
                            
                            self.loadDataCity()
                        }
                        
                    }
                    else{
                        Globals.showAlertWithTitle("Province Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    func loadDataCity() {
        if !self.isLoading() {
            self.showLoading()
            
            self.cityDataSource.removeAllObjects()
            
            Globals.getDataFromUrl(Globals.getApiUrl("regions/getCities"),
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "provinceId" : String(self.selectedProvinceId)
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let cities = result.object(forKey: "cities") as? NSArray {
                            self.cityDataSource.addObjects(from: cities as [AnyObject])
                        }
                        
                        self.btnCity.setDataSource(self.cityDataSource)
                        
                        if self.selectedCityId != 0 {
                            var i = 0
                            for item in self.cityDataSource {
                                if let city = item as? NSDictionary {
                                    if let cityId = city.object(forKey: "id") as? NSString {
                                        if cityId.integerValue == self.selectedCityId {
                                            self.btnCity.setSelectedIndex(i)
                                            self.btnCity.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
                                        }
                                    }
                                }
                                i += 1
                            }
                        }
                    }
                    else{
                        Globals.showAlertWithTitle("City Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
        
    }
    
    func submitManualInputAddress(){
        let params = NSMutableDictionary()
        let arrayName = self.txtFirstName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).components(separatedBy: " ")
         let arrayLastName = self.txtLastName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).components(separatedBy: " ")
        var firstName = ""
        var lastName = ""
        if arrayName != nil && (arrayName?.count)! > 0 {
            for i in 0 ..< (arrayName?.count)! {
                if i == 0 {
                    firstName = (arrayName?[i])!
                }
                
            }
        }
        if arrayLastName != nil && (arrayLastName?.count)! > 0 {
            for i in 0 ..< (arrayLastName?.count)! {
                if i == 0 {
                    lastName = (arrayLastName?[i])!
                }
                
            }
        }
        
        params.setValue(self.txtFirstName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "first_name")
        params.setValue(self.txtLastName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "last_name")
        params.setValue(self.txtAddress.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "address")
        params.setValue(self.txtPostalCode.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "postal_code")
        params.setValue(self.txtPhone.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "phone")
        params.setValue(self.txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "email")
        params.setValue(String(self.selectedCityId), forKey: "city_id")
        params.setValue(String(self.selectedProvinceId), forKey: "province_id")
        if let city = self.btnCity.getSelectedValue() {
            params.setValue(city.object(forKey: "name"), forKey: "cityName")
        }
        if let province = self.btnProvince.getSelectedValue() {
            params.setValue(province.object(forKey: "name"), forKey: "provinceName")
        }
        
        if (params.object(forKey: "first_name") as! String) == "" {
            Globals.showAlertWithTitle("Address Error", message: "First Name must not be empty!", viewController: self)
        }
        else if (params.object(forKey: "last_name") as! String) == "" {
            Globals.showAlertWithTitle("Address Error", message: "Last Name must not be empty!", viewController: self)
        }
        else if (params.object(forKey: "address") as! String) == "" {
            Globals.showAlertWithTitle("Address Error", message: "Address must not be empty!", viewController: self)
        }
        else if (params.object(forKey: "postal_code") as! String) == "" {
            Globals.showAlertWithTitle("Address Error", message: "Postal Code must not be empty!", viewController: self)
        }
        else if (params.object(forKey: "phone") as! String) == "" {
            Globals.showAlertWithTitle("Address Error", message: "Phone must not be empty!", viewController: self)
        }
        else if (params.object(forKey: "email") as! String) == "" {
            Globals.showAlertWithTitle("Address Error", message: "Email must not be empty!", viewController: self)
        }
        else if !Globals.isValidEmail(params.object(forKey: "email") as! String) {
            Globals.showAlertWithTitle("Address Error", message: "Please enter a valid email address", viewController: self)
        }
        else if (params.object(forKey: "province_id") as! String) == "" || (params.object(forKey: "province_id") as! String) == "0" {
            Globals.showAlertWithTitle("Address Error", message: "Province must not be empty!", viewController: self)
        }
        else if (params.object(forKey: "city_id") as! String) == "" || (params.object(forKey: "city_id") as! String) == "0" {
            Globals.showAlertWithTitle("Address Error", message: "City must not be empty!", viewController: self)
        }
        else {
            
            if let parent = self._parentViewController as? CheckoutViewController {
                parent.setupSelectedAddress(params)
            }
            
            self.dismissSelf()
        }
        
    }
}
