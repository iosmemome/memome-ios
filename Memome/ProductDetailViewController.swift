//
//  ProductDetailViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/12/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class ProductDetailViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, BSSliderDelegate {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var productSlider: BSSlider!
    @IBOutlet weak var detailTableView: UITableView!
    @IBOutlet weak var btnBuy: UIButton!
    
    @IBOutlet weak var productScrollView: UIScrollView!
    @IBOutlet weak var productContentScrollView: UIView!
    
    
    var productData: NSDictionary! = NSDictionary()
    var productDetailsDataSource: NSMutableArray! = NSMutableArray()
    var detailDataSource: NSMutableArray! = NSMutableArray()
    var sliderDataSource: NSMutableArray! = NSMutableArray()
    
    var corporateVoucherId: Int = 0
    var corporateVoucherName: String = ""
    var corporateVoucherQty: Int = 0
    var corporateVoucherCode: String = ""
    var corporateVoucher: NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.btnShoppingCart = self.btnCart
        self.initShoppingCartButton()
        
        if let attributes = self.attributes {
            if let data = attributes.object(forKey: "product") as? NSDictionary {
                self.productData = data
            }
            
            if let voucher = attributes.object(forKey: "voucher") as? NSDictionary {
                if let id = voucher.object(forKey: "id") as? NSString {
                    self.corporateVoucherId = id.integerValue
                }
                else if let id = voucher.object(forKey: "id") as? NSNumber {
                    self.corporateVoucherId = id.intValue
                }
                
                if let qty = voucher.object(forKey: "qty") as? NSString {
                    self.corporateVoucherQty = qty.integerValue
                }
                else if let qty = voucher.object(forKey: "qty") as? NSNumber {
                    self.corporateVoucherQty = qty.intValue
                }
                
                if let name = voucher.object(forKey: "name") as? String {
                    self.corporateVoucherName = name
                }
                
                self.corporateVoucher = voucher
            }
            
            if let code = attributes.object(forKey: "voucherCode") as? String {
                self.corporateVoucherCode = code
            }
        }
        
        self.productSlider.setDelegate(self)
        
        self.detailTableView.register(UINib(nibName: "ProductDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "productDetailTableViewCell")
        
//        self.initRefreshControl(self.productScrollView)
        
        self.assignData()
        self.loadProductData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.productSlider.reloadSlides()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func addedToMyProject() {
        self.productSlider.removeFromSuperview()
        super.addedToMyProject()
    }
    
    override func addedToMyCart() {
        self.productSlider.removeFromSuperview()
        super.addedToMyCart()
        self.showViewController("CartViewController", attributes: nil)
    }
    
    override func refresh(_ sender: AnyObject) {
        super.refresh(sender)
        self.assignData()
        self.loadProductData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // BSSlider delegate
    func setupSlides(_ slider: BSSlider) {
        slider.setScrollView(self.productScrollView)
        if let slidesContainer = slider.getSlidesContainer() {
            let width: CGFloat = slider.bounds.size.width
            let height: CGFloat = slider.bounds.size.height
            var i: CGFloat = 0
            for item in self.sliderDataSource {
                let contents = Bundle.main.loadNibNamed("GallerySlide", owner: self, options: nil)
                let slide: UIView = contents!.last as! UIView
                slide.frame = CGRect(x: i * width, y: 0, width: width, height: height)
                slide.clipsToBounds = true
                
                let imgView: UIImageView = slide.viewWithTag(1) as! UIImageView
                var type = "large"
                if self.view.frame.size.width > 700 {
                    type = "large"
                }
                imgView.image = UIImage(named: "image-default.png")
                if let image = (item as AnyObject).object(forKey: type) as? String {
                    Globals.setImageFromUrl(image, imageView: imgView, placeholderImage: UIImage(named: "image-default.png"))
                }
                
                slidesContainer.addSubview(slide)
                
                i += 1
            }
        }
    }
    
    // table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.detailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let maxFloatHeight : CGFloat = 9999
        let height : CGFloat = 32
        let labelWidth : CGFloat = tableView.frame.size.width - 92
        let cellInfoArray : NSDictionary = self.detailDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        var title : NSString! = ""
        if let temp = cellInfoArray.object(forKey: "text") as? NSString {
             title = temp
        }
        else if let temp = cellInfoArray.object(forKey: "attributedText") as? NSAttributedString {
            title = temp.string as NSString
        }
        
        let titleHeight : CGFloat = title.boundingRect(
            with: CGSize(width: labelWidth, height: maxFloatHeight),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 14)!],
            context: nil
            ).size.height
        
        return max(height + titleHeight, 52)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "productDetailTableViewCell", for: indexPath) 
        
        
        let imgIcon = cell.viewWithTag(1) as! UIImageView
        let lblText = cell.viewWithTag(2) as! UILabel
        
        let cellInfoArray = self.detailDataSource.object(at: (indexPath as NSIndexPath).row)
        
        if let icon = (cellInfoArray as AnyObject).object(forKey: "icon") as? String {
            imgIcon.image = UIImage(named: icon)
        }
        else{
            imgIcon.image = nil
        }
        
        if let text = (cellInfoArray as AnyObject).object(forKey: "text") as? String {
            lblText.text = text
        }
        else{
            lblText.text = ""
        }
        
        if let text = (cellInfoArray as AnyObject).object(forKey: "attributedText") as? NSAttributedString {
            lblText.attributedText = text
        }
        
        return cell
    }

    
    //Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.productSlider.removeFromSuperview()
        self.dismissSelf()
    }
    
    @IBAction func btnBuyTouched(_ sender: AnyObject) {
        let attributes = NSMutableDictionary(dictionary: [
            "product" : self.productData,
            "productDetails" : self.productDetailsDataSource
            ])
        if self.corporateVoucherId > 0 {
            attributes.setValue(self.corporateVoucherCode, forKeyPath: "voucherCode")
            attributes.setValue(self.corporateVoucher, forKey: "voucher")
        }
        self.showViewController("ProductCoverViewController", attributes: attributes)
    }
    
    //general method
    func assignData(){
        if let title = self.productData.object(forKey: "title") as? String {
            self.lblTitle.text = title
        }
        
        if let images = self.productData.object(forKey: "gallery_images") as? NSArray {
            self.sliderDataSource.addObjects(from: images as [AnyObject])
        }
        
        self.productSlider.reloadSlides()
    }
    
    func assignDetails(){
        // details ( photos, size, price, info )
        let coverContentWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_content_width") as! NSString).floatValue)
        let coverContentHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_content_height") as! NSString).floatValue)
        
        var count = self.productDetailsDataSource.count
        if coverContentWidth != 0 && coverContentHeight != 0 {
            count += 1
        }
        
        if self.corporateVoucherId > 0 {
            self.detailDataSource.add([
                "icon" : "icon-about-us.png",
                "text" : String(format: "Free \(self.corporateVoucherQty) print - \(self.corporateVoucherName)")
                ])
        }
        
        self.detailDataSource.add([
            "icon" : "icon-images.png",
            "text" : String(format: "%i photos", arguments: [count])
            ])
        
        if let size = self.productData.object(forKey: "size") as? String {
            self.detailDataSource.add([
                "icon" : "icon-size.png",
                "text" : size
                ])
        }
        
        var hasDiscount: Bool = false
        if let priceDiscount = self.productData.object(forKey: "special_price") as? NSString {
            if priceDiscount.doubleValue > Double(0) {
                
                hasDiscount = true
                let discountPrice = Globals.currencyFormat(NSNumber(value: priceDiscount.doubleValue as Double))
                var normalPrice = ""
                if let price = self.productData.object(forKey: "price") as? NSString {
                    normalPrice = Globals.currencyFormat(NSNumber(value: price.doubleValue as Double))
                }
                
                let price = NSMutableAttributedString(string: String(format: "%@     %@", arguments: [normalPrice, discountPrice]))
                
                let normalPriceLength = NSString(string: normalPrice).length
                price.addAttribute(NSStrikethroughStyleAttributeName,
                    value: NSUnderlineStyle.styleSingle.rawValue,
                    range: NSRange(location: 0, length: normalPriceLength))
                price.addAttribute(NSForegroundColorAttributeName,
                    value: UIColor(white: 0.3, alpha: 1),
                    range: NSRange(location: 0, length: normalPriceLength))
                
                self.detailDataSource.add([
                    "icon" : "icon-payment.png",
                    "attributedText" : price
                    ])
            }
        }
        
        if !hasDiscount {
            if let price = self.productData.object(forKey: "price") as? NSString {
                self.detailDataSource.add([
                    "icon" : "icon-payment.png",
                    "text" : Globals.currencyFormat(NSNumber(value: price.doubleValue as Double))
                    ])
            }
        }
        
        if let info = self.productData.object(forKey: "info") as? String {
            self.detailDataSource.add([
                "icon" : "icon-about-us.png",
                "text" : info
                ])
        }
        
        self.reloadDetailTableView()
    }
    
    func reloadDetailTableView(){
        self.detailTableView.reloadData()
        
        if let height = Globals.getConstraint("height", view: self.detailTableView) {
            height.constant = self.detailTableView.contentSize.height
        }
    }
    
    func loadProductData(){
        if !self.isLoading() {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("products/view"),
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "productId" : self.productData.object(forKey: "id") as! String
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        self.productDetailsDataSource.removeAllObjects()
                        
                        if let productDetails = result.object(forKey: "details") as? NSArray {
                            self.productDetailsDataSource.addObjects(from: productDetails as [AnyObject])
                        }
                        
                        if let product = result.object(forKey: "product") as? NSDictionary {
                            self.productData = product
                        }
                        
                        self.assignDetails()
                    }
                    else{
                        Globals.showAlertWithTitle("Products Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
}
