//
//  Setting.swift
//  Kreate
//
//  Created by staff on 5/12/15.
//  Copyright (c) 2015 Trio Digital Agency. All rights reserved.
//

import Foundation
import CoreData

@objc(Setting)
class Setting: NSManagedObject {
   
    @NSManaged var name: String
    @NSManaged var value: String
    
}
