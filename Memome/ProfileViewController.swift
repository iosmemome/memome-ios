//
//  ProfileViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/13/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import Just

class ProfileViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var profileScrollView: UIScrollView!
    @IBOutlet weak var profileContentScrollView: UIView!
    
    @IBOutlet weak var labelAddress: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnName: UIButton!
    @IBOutlet weak var iconEditView: UIView!
    @IBOutlet weak var btnProfilePicture: UIButton!
    
    @IBOutlet weak var btnAddAddress: UIButton!
    @IBOutlet weak var addressTableView: UITableView!
    
    @IBOutlet weak var btnLogout: UIButton!
    
    var addressDataSource: NSMutableArray! = NSMutableArray()
    
    var responseData = NSMutableData()
    var isUpdatingProfilePicture = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelAddress.isHidden = true
        
        // Do any additional setup after loading the view.
        
        self.btnAddAddress.imageView?.contentMode = .scaleAspectFit
        
//        self.btnAddAddress.contentHorizontalAlignment = .center
        
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width / 2
        
        self.lblName.text = Globals.getDataSetting("userName") as! String + " - Edit"
        
        self.addressTableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "addressTableViewCell")
        
        self.iconEditView.clipsToBounds = true
        self.iconEditView.layer.cornerRadius = self.iconEditView.frame.size.width / 2
        
        self.initRefreshControl(self.profileScrollView)
        
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func refresh(_ sender: AnyObject) {
        super.refresh(sender)
        
        self.loadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addressDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 30.0
        let cellInfoArray = self.addressDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        var name: String = ""
        var address: String = ""
        var area: String = ""
        var phone: String = ""
        if let firstName = cellInfoArray.object(forKey: "first_name") as? String {
            name = firstName
        }
        else{
            name = ""
        }
        
        if let lastName = cellInfoArray.object(forKey: "last_name") as? String {
            name = name.appendingFormat(" %@", lastName)
        }
        
        if let a = cellInfoArray.object(forKey: "address") as? String {
            address = a
        }
        
        var city = ""
        var province = ""
        var zipCode = ""
        if let item = cellInfoArray.object(forKey: "cityName") as? String {
            city = item
        }
        if let item = cellInfoArray.object(forKey: "provinceName") as? String {
            province = item
        }
        
        if let item = cellInfoArray.object(forKey: "postal_code") as? String {
            zipCode = item
        }
        
        if city != "" {
            area = area + city
        }
        if province != "" {
            if area != "" {
                area = area + ", "
            }
            area = area + province
        }
        if zipCode != "" {
            if area != "" {
                area = area + " - "
            }
            area = area + zipCode
        }
        
        if let p = cellInfoArray.object(forKey: "phone") as? String {
            phone = p
        }
        
        
        let nameWidth = tableView.frame.size.width - 54
        let maxWidth = tableView.frame.size.width - 16
        let maxFloatHeight : CGFloat = 9999
        
        height += name.boundingRect(
            with: CGSize(width: nameWidth, height: maxFloatHeight),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 14)!],
            context: nil
            ).size.height
        height += address.boundingRect(
            with: CGSize(width: maxWidth, height: maxFloatHeight),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 13)!],
            context: nil
            ).size.height
        height += area.boundingRect(
            with: CGSize(width: maxWidth, height: maxFloatHeight),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 13)!],
            context: nil
            ).size.height
        height += phone.boundingRect(
            with: CGSize(width: maxWidth, height: maxFloatHeight),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 13)!],
            context: nil
            ).size.height
        
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "addressTableViewCell", for: indexPath)
        
        let lblName = cell.viewWithTag(1) as! UILabel
        let lblAddress = cell.viewWithTag(2) as! UILabel
        let lblCity = cell.viewWithTag(3) as! UILabel
        let lblPhone = cell.viewWithTag(4) as! UILabel
        let btnEdit = cell.viewWithTag(5) as! EditButton
        
        let cellInfoArray = self.addressDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        
        if let firstName = cellInfoArray.object(forKey: "first_name") as? String {
            lblName.text = firstName
        }
        else{
            lblName.text = ""
        }
        
        if let lastName = cellInfoArray.object(forKey: "last_name") as? String {
            lblName.text = lblName.text?.appendingFormat(" %@", lastName)
        }
        
        if let address = cellInfoArray.object(forKey: "address") as? String {
            lblAddress.text = address
        }
        else{
            lblAddress.text = ""
        }
        
        var city = ""
        var province = ""
        var zipCode = ""
        if let item = cellInfoArray.object(forKey: "cityName") as? String {
            city = item
        }
        if let item = cellInfoArray.object(forKey: "provinceName") as? String {
            province = item
        }
        
        if let item = cellInfoArray.object(forKey: "postal_code") as? String {
            zipCode = item
        }
        
        lblCity.text = ""
        if city != "" {
            lblCity.text = lblCity.text! + city
        }
        if province != "" {
            if lblCity.text != "" {
                lblCity.text = lblCity.text! + ", "
            }
            lblCity.text = lblCity.text! + province
        }
        if zipCode != "" {
            if lblCity.text != "" {
                lblCity.text = lblCity.text! + " - "
            }
            lblCity.text = lblCity.text! + zipCode
        }
        
        if let phone = cellInfoArray.object(forKey: "phone") as? String {
            lblPhone.text = phone
        }
        else{
            lblPhone.text = ""
        }
        
        btnEdit.index = (indexPath as NSIndexPath).row
        btnEdit.addTarget(self, action: #selector(ProfileViewController.btnEditAddressTouched(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }

    // Actions
    
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnAddAddressTouched(_ sender: AnyObject) {
        self.showViewController("AddressViewController", attributes: NSDictionary())
    }
    
    @IBAction func btnLogoutTouched(_ sender: AnyObject) {
        Globals.logOut(self) { () -> Void in
            self.showHomeViewController()
        }
    }
    
    func btnEditAddressTouched(_ sender: AnyObject) {
        if let button = sender as? EditButton {
            let cellInfoArray = self.addressDataSource.object(at: button.index) as! NSDictionary
            self.showViewController("AddressViewController", attributes: [
                "address" : cellInfoArray
                ])
        }
    }
    
    @IBAction func btnProfilePictureTouched(_ sender: AnyObject) {
        if !self.isUpdatingProfilePicture {
            self.showViewController("MainPhotoSelectorViewController", attributes: [
                "numberOfPhotos" : 1
                ])
        }
        else{
            
        }
    }
    
    @IBAction func btnNameTouched(_ sender: AnyObject) {
        self.showViewController("EditProfileViewController", attributes: NSDictionary())
    }
    
    // General Methods
    
    func loadData() {
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(Globals.getApiUrl("users/view"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken")!
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        if let user = result.object(forKey: "user") as? NSDictionary {
                            self.assignDataUser(user)
                        }
                        
                        self.addressDataSource.removeAllObjects()
                        if let addresses = result.object(forKey: "userAddresses") as? NSArray {
                            self.addressDataSource.addObjects(from: addresses as [AnyObject])
                            
                            self.reloadAddressTableView()
                        }
                    }
                    else{
                        Globals.showAlertWithTitle("Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    func assignDataUser(_ data: NSDictionary){
        if let name = data.object(forKey: "name") as? String {
            Globals.setDataSetting("userName", value: name)
        }
        self.lblName.text = Globals.getDataSetting("userName") as! String + " - Edit"
        
        self.imgProfile.backgroundColor = UIColor.lightGray
        if let images = data.object(forKey: "images") as? NSDictionary {
            if let image = images.object(forKey: "original") as? String {
                Globals.setDataSetting("userProfilePicture", value: image)
                Globals.setImageFromUrl(image, imageView: self.imgProfile, placeholderImage: nil, onSuccess: { (image) -> Void in
                    self.imgProfile.backgroundColor = UIColor.white
                })
            }
        }
    }
    
    func reloadAddressTableView(){
        self.addressTableView.reloadData()
        
        if let height = Globals.getConstraint("height", view: self.addressTableView) {
            height.constant = self.addressTableView.contentSize.height
        }
    }
    
    func updateProfilePicture(_ image: UIImage){
        if !self.isUpdatingProfilePicture {
            self.isUpdatingProfilePicture = true
            self.showLoading()
            
            Just.post(
                Globals.getApiUrl("users/uploadProfilePicture"),
                data: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String
                ],
                files: [
                    "profile_picture": .data("profile-picture", UIImagePNGRepresentation(image)!, nil)
                ],
                asyncCompletionHandler: { (r) -> Void in
                    DispatchQueue.main.async(execute: {
                        self.hideLoading()
                        if r.ok {
                            
                            if let result = r.json as? NSDictionary {
                                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                                    self.imgProfile.image = image
                                    
                                    if let user = result.object(forKey: "user") as? NSDictionary {
                                        if let images = user.object(forKey: "images") as? NSDictionary {
                                            if let image = images.object(forKey: "original") as? String {
                                                Globals.setDataSetting("userProfilePicture", value: image)
                                            }
                                        }
                                    }
                                    
                                }
                                else{
                                    Globals.showAlertWithTitle("Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                                }
                            }
                            
                        }
                        else{
                            
                        }
                    })
                })
        }
    }
    
}
