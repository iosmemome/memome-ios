//
//  ImageCache.swift
//
//  Created by Bobby Stenly on 6/15/15.
//  Copyright (c) 2015 Kreate. All rights reserved.
//

import Foundation
import CoreData

@objc(ImageCache)
class ImageCache: NSManagedObject {
    
    @NSManaged var url: String
    @NSManaged var value: Data
    @NSManaged var lastUpdated: Date
    
}
