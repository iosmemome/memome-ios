//
//  EditProfileViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 3/2/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var bsdBirthday: BSDatePicker!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnChangePassword: UIButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var editProfileScrollView: UIScrollView!
    @IBOutlet weak var editProfileContentScrollView: UIView!
    var dbDateFormat: DateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.defaultScrollView = self.editProfileScrollView
        self.defaultContentScrollView = self.editProfileContentScrollView
        
        self.dbDateFormat.dateFormat = "yyyy-MM-dd"
        
        let mainColor = UIColor.colorPrimary
        let titleFont = UIFont(name: "Montserrat-SemiBold", size: 15)
        let buttonFont = UIFont(name: "Montserrat-Regular", size: 14)
        
        self.bsdBirthday.viewController = self
        self.bsdBirthday.defaultTitle = "Birthday"
        self.bsdBirthday.title = "Birthday"
        self.bsdBirthday.headerBackgroundColor = mainColor
        self.bsdBirthday.titleFont = titleFont
        self.bsdBirthday.buttonFont = buttonFont
        self.bsdBirthday.setup()

        self.initRefreshControl(self.editProfileScrollView)
        
        self.loadDataUser()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func refresh(_ sender: AnyObject) {
        super.refresh(sender)
        self.loadDataUser()
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.txtName {
            self.txtEmail.becomeFirstResponder()
        }
        else if textField == self.txtEmail {
            self.btnSubmitTouched(self.btnSubmit)
        }
        
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnSubmitTouched(_ sender: AnyObject) {
        let params = NSMutableDictionary()
        params.setValue(self.txtName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "name")
        params.setValue(self.txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "email")
        params.setValue(self.dbDateFormat.string(from: self.bsdBirthday.getDate()! as Date), forKey: "birthdate")
        params.setValue(Globals.getProperty("clientId"), forKey: "clientId")
        params.setValue(Globals.getDataSetting("accessToken") as! String, forKey: "accessToken")
        
        
        if !self.isLoading() && self.validateForm(params) {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("users/update"),
                requestType: Globals.HTTPRequestType.http_POST,
                params: params,
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        Globals.setDataSetting("userName", value: params.object(forKey: "name") as! String)
                        Globals.setDataSetting("userEmail", value: params.object(forKey: "email") as! String)
                        
                        self.dismissSelf()
                        if let parent = self._parentViewController {
                            BSNotification.show("Update user profile success.", view: parent)
                        }
                    }
                    else{
                        Globals.showAlertWithTitle("My Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    @IBAction func btnChangePasswordTouched(_ sender: AnyObject) {
        self.showViewController("ChangePasswordViewController", attributes: nil)
    }
    
    //general methods
    func validateForm(_ params: NSDictionary) -> Bool {
        var isValid = true
        if params.object(forKey: "name") as! String == "" {
            Globals.showAlertWithTitle("Register Error", message: "Name must not be empty!", viewController: self)
            isValid = false
        }
        else if params.object(forKey: "email") as! String == "" || !Globals.isValidEmail(params.object(forKey: "email") as! String) {
            Globals.showAlertWithTitle("Register Error", message: "Email must not be empty and must be a valid email address!", viewController: self)
            isValid = false
        }
        
        return isValid
    }
    
    
    func loadDataUser() {
        if !self.isLoading() {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("users/view"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let user = result.object(forKey: "user") as? NSDictionary {
                            
                            if let name = user.object(forKey: "name") as? String {
                                self.txtName.text = name
                            }
                            
                            if let email = user.object(forKey: "email") as? String {
                                self.txtEmail.text = email
                            }
                            
                            if let date = user.object(forKey: "birthdate") as? String {
                                self.bsdBirthday.setDate(self.dbDateFormat.date(from: date))
                            }
                        }
                        
                    }
                    else{
                        Globals.showAlertWithTitle("My Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
}
