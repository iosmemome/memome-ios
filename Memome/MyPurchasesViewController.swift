//
//  MyPurchasesViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 2/29/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import PageMenu

class MyPurchasesViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var topBarView: UIView!
    
    var pageMenu: CAPSPageMenu?
    var inProgressController: PurchasesListViewController!
    var completedController: PurchasesListViewController!
    var controllerArray : [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.btnShoppingCart = self.btnCart
        self.initShoppingCartButton()
        
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        self.inProgressController = PurchasesListViewController(nibName: "PurchasesListViewController", bundle: nil)
        self.inProgressController.title = "IN PROGRESS"
        self.inProgressController._parentViewController = self
        self.inProgressController.isInProgress = true
        self.controllerArray.append(self.inProgressController)
        
        self.completedController = PurchasesListViewController(nibName: "PurchasesListViewController", bundle: nil)
        self.completedController.title = "COMPLETED"
        self.completedController._parentViewController = self
        self.completedController.isInProgress = false
        controllerArray.append(self.completedController)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(white: 0, alpha: 0)),
            .selectionIndicatorColor(UIColor.colorPrimarySemiDark),
//            .SelectionIndicatorHeight(35.0),
            .selectedMenuItemLabelColor(UIColor.colorPrimarySemiDark),
            .unselectedMenuItemLabelColor(UIColor.lightGray),//(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1)),
            .bottomMenuHairlineColor(UIColor.lightGray),//(red: 212/255, green: 212/255, blue: 212/255, alpha: 1)),
            .menuItemFont(UIFont(name: "Montserrat-SemiBold", size: 14.0)!),
            .menuMargin(0),
            .menuHeight(35.0),
            .menuItemSeparatorWidth(0),
            .menuItemSeparatorColor(UIColor.lightGray),//(red: 212/255, green: 212/255, blue: 212/255, alpha: 1)),
            .menuItemWidth((self.view.frame.size.width) / 2),
            .centerMenuItems(true)
        ]
        
        // Initialize page menu with controller array, frame, and optional parameters
        self.pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: self.topBarView.frame.height + 16, width: self.view.frame.width, height: self.view.frame.height - 100), pageMenuOptions: parameters)
//        self.pageMenu?.delegate = self
        
        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        self.view.addSubview(pageMenu!.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
}
