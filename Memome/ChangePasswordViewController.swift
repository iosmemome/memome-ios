//
//  ChangePasswordViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 3/2/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmNewPassword: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var changePasswordScrollView: UIScrollView!
    @IBOutlet weak var changePasswordContentScrollView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.defaultScrollView = self.changePasswordScrollView
        self.defaultContentScrollView = self.changePasswordContentScrollView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.txtOldPassword {
            self.txtNewPassword.becomeFirstResponder()
        }
        else if textField == self.txtNewPassword {
            self.txtConfirmNewPassword.becomeFirstResponder()
        }
        else if textField == self.txtConfirmNewPassword {
            self.btnSubmitTouched(self.btnSubmit)
        }
        
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnSubmitTouched(_ sender: AnyObject) {
        
        if !self.isLoading() && self.validateForm() {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("users/changePassword"),
                requestType: Globals.HTTPRequestType.http_POST,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String,
                    "oldPassword" : self.txtOldPassword.text!,
                    "newPassword" : self.txtNewPassword.text!
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        self.dismissSelf()
                        if let parent = self._parentViewController {
                            BSNotification.show("Change password success.", view: parent)
                        }
                    }
                    else{
                        Globals.showAlertWithTitle("Change Password Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
        
    }
    
    func validateForm() -> Bool {
        let requiredTextFields = [
            ["field" : self.txtNewPassword, "name" : "New Password"],
            ["field" : self.txtConfirmNewPassword, "name" : "Confirm New Password"]
        ]
        
        for item in requiredTextFields {
            let requiredTextField: NSDictionary = item as NSDictionary
            if (requiredTextField.object(forKey: "field") as? UITextField)?.text == "" {
                let message: String = String(format: "%@ must not be empty", arguments: [requiredTextField.object(forKey: "name") as! String])
                Globals.showAlertWithTitle("Change Password Error", message: message, viewController: self)
                (requiredTextField.object(forKey: "field") as? UITextField)?.becomeFirstResponder()
                return false
            }
        }
        
        if self.txtNewPassword.text != self.txtConfirmNewPassword.text {
            Globals.showAlertWithTitle("Change Password Error", message: "Your new password mismatch", viewController: self)
            self.txtConfirmNewPassword.becomeFirstResponder()
            return false
        }
        
        return true
    }
}
