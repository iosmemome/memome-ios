//
//  TrackingPopUpViewController.swift
//  Memome
//
//  Created by Yuni on 23/05/18.
//  Copyright © 2018 Trio Digital Agency. All rights reserved.
//

import UIKit
import Presentr

class TrackingPopUpViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var resiNumb: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var historyView: UIView!
    @IBOutlet weak var manifestTableView: UITableView!
    
    @IBOutlet weak var trackingScrollView: UIScrollView!
    @IBOutlet weak var trackingContainerView: UIView!
    var trackingIncomplete: Bool = false
    
    var purchaseOrder: NSDictionary = NSDictionary()
    var orderId = ""
    var manifestDataSource: NSMutableArray! = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.defaultScrollView = self.trackingScrollView
        self.defaultContentScrollView = self.trackingContainerView
        self.initRefreshControl(self.trackingScrollView)
        
        self.manifestTableView.register(UINib(nibName: "ManifestTableViewCell", bundle: nil), forCellReuseIdentifier: "manifestTableViewCell")
        
        self.renderPurchaseWaybill(purchaseOrder)
        loadWaybillData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissBtnTouchUp(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func renderPurchaseWaybill(_ order: NSDictionary){
        if let id = order.object(forKey: "id") as? String{
            self.orderId = id
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == manifestTableView{
        return self.manifestDataSource.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == manifestTableView{
        let cellInfoArray = self.manifestDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        
        var height: CGFloat = 20.0
        let minHeight: CGFloat = 80.0
        let maxFloat: CGFloat = 99999.0
        
        if let description = cellInfoArray.object(forKey: "manifest_description") as? NSString {
            height += description.boundingRect(
                with: CGSize(width: tableView.frame.size.width, height: maxFloat),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "MyriadPro-Regular", size: 14)!],
                context: nil
                ).size.height
        }
        
        if let date = cellInfoArray.object(forKey: "manifest_date") as? String {
            height += date.boundingRect(
                with: CGSize(width: tableView.frame.size.width, height: maxFloat),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "MyriadPro-Bold", size: 14)!],
                context: nil
                ).size.height
        }
        
        return max(height, minHeight)
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == manifestTableView{
         let cell = self.manifestTableView.dequeueReusableCell(withIdentifier: "manifestTableViewCell", for: indexPath)
        
        let cellInfoArray = self.manifestDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        
        let lblDate: UILabel = cell.viewWithTag(1) as! UILabel
        let lblLocation: UILabel = cell.viewWithTag(2) as! UILabel
        
        var location = ""
        if let manifest_description = cellInfoArray.object(forKey: "description") as? String {
            location = location + manifest_description + "\n"
        }
        if let manifest_city = cellInfoArray.object(forKey: "city") as? String {
            location = location + manifest_city
           lblLocation.text = location
        }
        
        var date = ""
        if let manifest_date = cellInfoArray.object(forKey: "date") as? String {
           
            date = "\(manifest_date) , "
        }
        
        if let manifest_time = cellInfoArray.object(forKey: "time") as? String {
           date = date + manifest_time
            lblDate.text = date
        }
        
        return cell
        }
        return UITableViewCell()
    }
    
    
    func loadWaybillData(){
        if !self.isLoading(){
            self.showLoading()
            Globals.getDataFromUrl(
                Globals.getApiUrl("orders/getWaybill"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "orderId" : self.orderId
                ],
                completion: {(result) in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as!
                        NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        self.manifestDataSource.removeAllObjects()
                        
                        guard let order = result.object(forKey: "order") as? NSDictionary else {return}
                        
                            if let waybill = order.object(forKey: "waybill") as? NSDictionary{
                                
                                if let summary = waybill.object(forKey: "summary") as? NSDictionary{
                                    
                                    if let resiNumber = summary.object(forKey: "waybill_number") as? String{
                                        self.resiNumb.attributedText = NSAttributedString(string: resiNumber, attributes: [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
                                    }
                                    
                                } //summary
                
                                if let delivery_status = waybill.object(forKey: "delivery_status") as? NSDictionary{
                                    if let status = delivery_status.object(forKey: "status") as? String{
                                       self.lblStatus.text = status
                                    }
                                    
                                } //delivery status
                                
                               
                                let arrayData = NSMutableArray()
                                if let manifest = waybill.object(forKey: "manifest") as? NSArray{
                                    for history in manifest{
                                        
                                        let manifestWaybill = NSMutableDictionary()
                                        
                                        if let manifest_description = (history as! NSDictionary).object(forKey: "manifest_description") as? String {
                                            manifestWaybill.setValue(manifest_description, forKey: "description")
                                        }
                                        if let manifest_city = (history as! NSDictionary).object(forKey: "city_name") as? String {
                                            manifestWaybill.setValue(manifest_city, forKey: "city")
                                            
                                        }
                                        
                                        if let manifest_date = (history as! NSDictionary).object(forKey: "manifest_date") as? String {
                                            manifestWaybill.setValue(manifest_date, forKey: "date")
                                        }
                                        
                                        if let manifest_time = (history as! NSDictionary).object(forKey: "manifest_time") as? String {
                                            manifestWaybill.setValue(manifest_time, forKey: "time")
                                        }
                                        arrayData.add(manifestWaybill)
                                    } //loop
                                    
                                    self.manifestDataSource.addObjects(from: arrayData as [AnyObject])
                                
                                    self.reloadManifestTableView()
                                } //manifest
                                
                            } // waybill
                    }
                    else{
                        Globals.showAlertWithTitle("Tracking Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func reloadManifestTableView(){
        self.manifestTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.manifestTableView){
            height.constant = self.manifestTableView.contentSize.height
        }
    }
    
}
