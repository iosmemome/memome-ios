//
//  PhotoEditorViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/14/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class PhotoEditorViewController: BaseViewController{

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnBrowse: UIButton!
//    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var photoCrop: BSPhotoCrop!
    
    var imageRatio: CGFloat! = 1
    
    var imagePicker: UIImagePickerController!
    var image: UIImage? = nil
    var imageUrl: String = ""
    var rotation: Int = 0
    
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let ratio = self.attributes.object(forKey: "imageRatio") as? NSNumber {
            self.imageRatio = CGFloat(ratio.floatValue)
        }
        
        self.photoCrop.gridColor = UIColor.darkGray//(red: 80.0/255.0, green: 210.0/255.0, blue: 194.0/255.0, alpha: 1)
        self.photoCrop.imageRatio = self.imageRatio
        
        self.btnBrowse.imageView?.contentMode = .scaleAspectFit
//        self.btnEdit.imageView?.contentMode = .scaleAspectFit
        
        if let idx = self.attributes.object(forKey: "index") as? NSNumber {
            self.index = idx.intValue
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.photoCrop.isInitialized() {
            if let initialImage = self.attributes.object(forKey: "initialImage") as? NSDictionary {
                if let url = initialImage.object(forKey: "url") as? String {
                    self.imageUrl = url
                    
                    var size: CGSize = CGSize.zero
                    var offset: CGPoint = CGPoint.zero
                    if let s = initialImage.object(forKey: "size") as? NSValue {
                        size = s.cgSizeValue
                    }
                    if let o = initialImage.object(forKey: "offset") as? NSValue {
                        offset = o.cgPointValue
                    }
                    if let rotate = initialImage.object(forKey: "rotation") as? NSNumber {
                        self.rotation = rotate.intValue
                    }
                    
                    Globals.checkImageFrom(url, scale: 1, resultHandler: { (image) -> Void in
                        if let img = image {
                            self.image = img
                            self.photoCrop.initWithImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: self.rotation), cropSize: size, cropOffset: offset)
                        }
                        else{
                            self.photoCrop.initWithImage(nil)
                        }
                    })
                }
//                Globals.imageFromAssetLocalIdentifier(initialImageUrl, scale: 1, resultHandler: { (image) -> Void in
//                    self.photoCrop.initWithImage(image)
//                })
            }
            else{
                self.photoCrop.initWithImage(nil)
            }
        }
//        self.photoCrop.initWithImage(UIImage(named: "test.jpg"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    //actions
    
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.photoCrop.removeFromSuperview()
        self.dismissSelf()
    }

    @IBAction func btnBrowseTouched(_ sender: AnyObject) {
        self.showViewController("MainPhotoSelectorViewController", attributes: [
            "numberOfPhotos" : 1
            ])
    }
    
    @IBAction func btnEditTouched(_ sender: AnyObject) {
        if let img = self.photoCrop.getOriginalImage() {
            self.rotation += 90
            self.rotation = self.rotation % 360
            self.photoCrop.setImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: self.rotation))
        }
    }
    
    @IBAction func btnSaveTouched(_ sender: AnyObject) {
        if self.index == 0 {
            if !self.imageUrl.isEmpty {
                MemomeHelper.coverImage = NSMutableDictionary()
                MemomeHelper.coverImage.setValue(self.imageUrl, forKey: "url")
                MemomeHelper.coverImage.setValue(NSValue(cgSize: self.photoCrop.getSize()), forKey: "size")
                MemomeHelper.coverImage.setValue(NSValue(cgPoint: self.photoCrop.getOffset()), forKey: "offset")
                MemomeHelper.coverImage.setValue(NSNumber(value: Float(self.photoCrop.getZoomScale()) as Float), forKey: "zoomScale")
                MemomeHelper.coverImage.setValue(NSNumber(value: self.rotation as Int), forKey: "rotation")
            }
//            MemomeHelper.coverImage.setValue(UIImageJPEGRepresentation(self.photoCrop.getCroppedImage()!, 1.0), forKey: "imageData")
        }
        else{
            if let image = MemomeHelper.images.object(at: self.index - 1) as? NSMutableDictionary {
                NSLog("images \(self.index - 1)")
                image.setValue(self.imageUrl, forKey: "url")
                image.setValue(NSValue(cgSize: self.photoCrop.getSize()), forKey: "size")
                image.setValue(NSValue(cgPoint: self.photoCrop.getOffset()), forKey: "offset")
                image.setValue(NSNumber(value: Float(self.photoCrop.getZoomScale()) as Float), forKey: "zoomScale")
                image.setValue(NSNumber(value: self.rotation as Int), forKey: "rotation")
//                image.setValue(UIImageJPEGRepresentation(self.photoCrop.getCroppedImage()!, 1.0), forKey: "imageData")
            }
            else{
                NSLog("index \(self.index) not found");
            }
        }
        
        self.btnBackTouched(sender)
    }
}
