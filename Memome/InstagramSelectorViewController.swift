//
//  InstagramSelectorViewController.swift
//  Memome
//
//  Created by iOS Developer on 17/11/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class InstagramSelectorViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var mainViewController: UIViewController?
    var photosDataSource: NSArray! = NSArray()
    
    var numberOfPhotos: Int = 0
    
    var productData: NSDictionary!
    var productDetails: NSArray!
    
    var selectMode = false
    var swipeSelect = false
    var firstSelectedCell = IndexPath()
    var lastSelectedCell = IndexPath()
    var firstLocation: CGPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.photosCollectionView.emptyDataSetDelegate = self
        self.photosCollectionView.emptyDataSetSource = self
        
        self.photosCollectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCollectionViewCell")
        
        if let number = self.attributes.object(forKey: "numberOfPhotos") as? NSNumber{
            self.numberOfPhotos = number.intValue
        }
        
//        if self.numberOfPhotos > 1 {
//            selectMode = true
            setupCollectionView()
//        }
        
        if let attributes = self.attributes {
            if let data = attributes.object(forKey: "product") as? NSDictionary {
                self.productData = data
            }
            if let details = attributes.object(forKey: "productDetails") as? NSArray {
                self.productDetails = details
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view did appear instagram")
        if let setting: AnyObject = Globals.getDataSetting("instagramAuthToken") {
            let token : String! = setting as! String
            if token != "" {
                fetchListOfUserPhotos(token)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // general function
    func setupCollectionView() {
        self.photosCollectionView.canCancelContentTouches = false
        self.photosCollectionView.allowsMultipleSelection = true
//        longpressGesture = UILongPressGestureRecognizer(target: self, action: #selector(didLongpress))
//        longpressGesture?.minimumPressDuration = 0.15
//        longpressGesture?.delaysTouchesBegan = true
//        longpressGesture?.delegate = self
//        self.photosCollectionView.addGestureRecognizer(longpressGesture!)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(didPan(toSelectCells:)))
        panGesture.delegate = self
        self.photosCollectionView.addGestureRecognizer(panGesture)
    }
    
    func selectCell(_ indexPath: IndexPath, before lastIndexPath: IndexPath) {
        selectCell(indexPath, before: lastIndexPath, first: false)
    }
    
    func selectCell(_ indexPath: IndexPath, before lastIndexPath: IndexPath, first selection: Bool) {
        if !lastIndexPath.isEmpty && abs(indexPath.item - lastIndexPath.item) > 1 {
            if (indexPath.item >= firstSelectedCell.item) {
                if (indexPath.item >= lastIndexPath.item) {
                    for i in (lastIndexPath.item+1)...indexPath.item {
                        let index = IndexPath(item: i, section: indexPath.section)
                        if (!selection && index.item != firstSelectedCell.item) {
                            self.selectionCell(index)
                        }
                    }
                }
                else {
                    for i in (indexPath.item+1)...lastIndexPath.item {
                        let index = IndexPath(item: i, section: indexPath.section)
                        if (!selection && index.item != firstSelectedCell.item) {
                            self.selectionCell(index)
                        }
                    }
                }
            }
            else {
                if (indexPath.item >= lastIndexPath.item) {
                    for i in lastIndexPath.item..<indexPath.item {
                        let index = IndexPath(item: i, section: indexPath.section)
                        if (!selection && index.item != firstSelectedCell.item) {
                            self.selectionCell(index)
                        }
                    }
                }
                else {
                    for i in indexPath.item..<lastIndexPath.item {
                        let index = IndexPath(item: i, section: indexPath.section)
                        if (!selection && index.item != firstSelectedCell.item) {
                            self.selectionCell(index)
                        }
                    }
                }
            }
        }
        else {
            if (selection || indexPath.item != firstSelectedCell.item) {
                self.selectionCell(indexPath)
            }
        }
        
        self.photosCollectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
    }
    
    func selectionCell(_ indexPath: IndexPath) {
        if let parent = self.mainViewController as? MainPhotoSelectorViewController {
            if (indexPath as NSIndexPath).row < self.photosDataSource.count {
                let asset = self.photosDataSource![indexPath.row] as! NSDictionary
                //                let copyAsset = asset.copy() as! PHAsset
                parent.currentselected(asset, completionHandler: { (isHidden, index) in
                    if isHidden {
                        if parent.selectedPhotos.count < parent.numberOfPhotos {
                            parent.selectedPhotos.add(asset)
                        }
                    }
                    else {
                        if index != nil {
                            parent.selectedPhotos.removeObject(at: index!)
                        }
                    }
                    
                    if self.numberOfPhotos == 1 {
                        parent.btnDoneTouched(parent)
                    }
                    else{
                        self.photosCollectionView.reloadItems(at: [indexPath])
                        //                    self.selectedPhotosCollectionView.reloadData()
                        
                        //                    self.selectedPhotosCollectionView.scrollToItem(at: IndexPath(row: parent.selectedPhotos.count - 1, section: 0), at: UICollectionViewScrollPosition.right, animated: true)
                        parent.updateAlbumDetailTitle()
                    }
                })
                //                    parent.selectedPhotos.add(asset)
            }
        }
    }
    
    func didPan(toSelectCells panGesture: UIPanGestureRecognizer) {
        if panGesture.state == .began {
            firstLocation = panGesture.location(in: self.photosCollectionView)
        }
        else if panGesture.state == .changed {
            let location: CGPoint = panGesture.location(in: self.photosCollectionView)
            print("did pan \(abs(location.x - (firstLocation?.x)!))")
            print("did pan \(swipeSelect)")
            if !swipeSelect {
                if abs(location.x - (firstLocation?.x)!) >= 20 {
                    print("did pan got it")
                    self.photosCollectionView.isUserInteractionEnabled = false
                    self.photosCollectionView.isScrollEnabled = false
                    swipeSelect = true
                    
                    if let indexPath: IndexPath = self.photosCollectionView.indexPathForItem(at: location) {
                        firstSelectedCell = indexPath
                        lastSelectedCell = indexPath
                        self.selectCell(indexPath, before: lastSelectedCell, first: true)
                    }
                }
            }
            else {
                if let indexPath: IndexPath = self.photosCollectionView.indexPathForItem(at: location) {
                    if indexPath != lastSelectedCell {
                        self.selectCell(indexPath, before: lastSelectedCell)
                        lastSelectedCell = indexPath
                    }
                }
            }
        } else if panGesture.state == .ended {
            self.photosCollectionView.isScrollEnabled = true
            self.photosCollectionView.isUserInteractionEnabled = true
            swipeSelect = false
        }
    }
    
    func didLongpress() {
        if !swipeSelect {
            swipeSelect = true
            print("did long press")
        }
    }
    
    // uigesture
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    //collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photosDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let parent = self.mainViewController as? MainPhotoSelectorViewController {
            let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath)
            
            let imageView = cell.viewWithTag(1) as! UIImageView
            let lblTitle = cell.viewWithTag(2) as! UILabel
            let imageChecked = cell.viewWithTag(3) as! UIImageView
            let imageWarning = cell.viewWithTag(4) as! UIImageView
            
            let asset = self.photosDataSource![indexPath.row] as! NSDictionary
            if let images = asset.value(forKey: "images") as? NSDictionary {
                if let thumbnail = images.value(forKey: "thumbnail") as? NSDictionary {
                    let userPhotoUrlString = thumbnail.value(forKey: "url") as! String
                    if userPhotoUrlString != nil && userPhotoUrlString != "" {
                        Globals.setImageFromUrl(userPhotoUrlString, imageView: imageView, placeholderImage: nil, onSuccess: { (image) -> Void in
                            imageView.backgroundColor = UIColor.white
                        })
                    }
                }
            }
            //        let asset = self.photosDataSource.object(at: (indexPath as NSIndexPath).row) as! PHAsset
            //        PHImageManager.default().requestImage(for: asset, targetSize: self.thumbImageSize, contentMode: .aspectFill, options: self.thumbImageOptions, resultHandler: { (image, info) -> Void in
            //            imageView.image = image
            //        })
            lblTitle.isHidden = true
            imageChecked.isHidden = true
            imageWarning.isHidden = true
            parent.currentselected(asset, completionHandler: { (isHidden, index) in
                imageChecked.isHidden = isHidden
            })
            //        let selectedIdx = self.selectedPhotos.indexOfObject(indexPath.row)
            //        if selectedIdx != NSNotFound {
            //            lblTitle.hidden = false
            //            if self.numberOfPhotos > 1 {
            //                lblTitle.text = String(format: "%i", arguments: [selectedIdx + 1])
            //            }
            //            else{
            //                lblTitle.text = ""
            //            }
            //        }
            //        else{
            //            lblTitle.hidden = true
            //        }
            
            return cell
        }
        else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectionCell(indexPath)
    }
    
    // Empty data set
    //    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
    //        let mainView = UIView.init()
    //
    //        let button = FBSDKLoginButton()
    //        button.center = view.center
    //        button.readPermissions = ["public_profile","email","user_photos"]
    //        mainView.addSubview(button)
    //
    //        return mainView
    //    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "Want to access your Instagram photos?"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControlState) -> NSAttributedString? {
        let str = "Login With Instagram"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        let vc = InstagramLoginViewController()
        self.showViewController(vc, parent: mainViewController ?? self)
    }
    
    // action
    func fetchListOfUserPhotos(_ token: String)
    {
        print("token instagram \(token)")
        Globals.getDataFromUrl("https://api.instagram.com/v1/users/self/media/recent/",
           noCache: true,
           requestType: Globals.HTTPRequestType.http_GET,
           params: [
            "access_token" : token
            ],
           completion: { (result) -> Void in
            let igResult:[String:AnyObject] = result as! [String : AnyObject]
            
            self.photosDataSource = igResult["data"] as! NSArray?
            
            self.photosCollectionView.reloadData()
        })
    }
}
