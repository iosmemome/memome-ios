//
//  MakePrintDetailViewController.swift
//  Memome
//
//  Created by iOS Developer on 08/12/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import UIKit

class MakePrintDetailViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var productTableView: UITableView!
//    @IBOutlet weak var productSlider: BSSlider!
    
    @IBOutlet weak var productScrollView: UIScrollView!
    @IBOutlet weak var productContentScrollView: UIView!
    
    
    @IBOutlet weak var uploadProgressView: UIView!
    @IBOutlet weak var lblUploadOrderId: UILabel!
    @IBOutlet weak var pgbUpload: UIProgressView!
    @IBOutlet weak var lblUploadPercent: UILabel!
    @IBOutlet weak var btnUploadProgress: UIButton!
    
    var itemsToLoad: Int! = 0
    
    var isProductLast: Bool! = false
    var lastProductIndex: Int! = 0
    
    var productDataSource: NSMutableArray = NSMutableArray()
    var sliderDataSource: NSMutableArray = NSMutableArray()
    var categoryId: String! = ""
    
    var uploadingOrderId = 0
    var uploadingOrderNumber = ""
    var isUploading = false
    var uploadingCart: NSMutableArray = NSMutableArray()
    var uploadingPaymentType = ""
    var uploadingGrandTotal: Double = 0
    
    var productTableViewAdapter: ProductTableViewAdapter = ProductTableViewAdapter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        
//        self.btnToggleSideNav = self.btnLeftMenu
        self.initRevealViewController()
        self.btnShoppingCart = self.btnCart
        self.initShoppingCartButton()
        
        self.productTableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "productTableViewCell")
        self.productTableView.register(UINib(nibName: "NewProductDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "productCell")
        
        self.productTableViewAdapter._parentViewController = self
        self.productTableViewAdapter.tableView = self.productTableView
        self.productTableView.dataSource = self.productTableViewAdapter
        self.productTableView.delegate = self.productTableViewAdapter
        self.productTableView.backgroundColor = UIColor.init(white: 239/255, alpha: 1)
        
        if let height = Globals.getConstraint("height", view: self.uploadProgressView) {
            height.constant = 0
        }
        
        if let attributes = self.attributes {
            if let title = attributes.object(forKey: "title") as? String {
                self.lblTitle.text = title
            }
            else {
                self.lblTitle.text = "Detail"
            }
            if let categoryId = attributes.object(forKey: "categoryId") as? String  {
                self.categoryId = categoryId
            }
            
            if let dataArray = attributes.object(forKey: "productList") as? NSArray {
                self.productDataSource.removeAllObjects()
                self.productDataSource.addObjects(from: dataArray as [AnyObject])
                print("total product \(self.productDataSource.count)")
            }
        }
        
//        if let height = Globals.getConstraint("height", view: self.productSlider) {
//            height.constant = 0
//        }
        
//        self.productSlider.setDelegate(self)
        
        self.isLoadingUnconfirmedOrder = false
        
        self.initRefreshControl(self.productScrollView)
        
//        self.loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func addedToMyProject() {
        //        BSNotification.show("Project has been added successfully", view: self)
        self.showViewController("MyProjectViewController", attributes: nil)
    }
    
    override func addedToMyCart() {
        //        BSNotification.show("Project has been added successfully", view: self)
        self.showViewController("CartViewController", attributes: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func refresh(_ sender: AnyObject) {
        super.refresh(sender)
        self.loadData()
    }
    
    //Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    //general functions
    
    func loadData(){
        //        self.itemsToLoad = 2
        
        if (categoryId != "") {
            self.loadDataProducts()
        }
    }
    
    func loadDataProducts(){
        if !self.isLoading() && !self.isProductLast {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("products/searchByCategory"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "limit" : String(Constant.getSearchLimit()),
                    "start" : String(self.lastProductIndex),
                    "categoryId" : categoryId
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        if let products = result.object(forKey: "products") as? NSArray {
                            self.productDataSource.removeAllObjects()
                            self.productDataSource.addObjects(from: products as [AnyObject])
                        }
                        self.productTableViewAdapter.setDataSource(self.productDataSource)
                        self.reloadProductTableView()
                    }
                    else{
                        Globals.showAlertWithTitle("Products Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
                })
        }
    }
    
    func reloadProductTableView(){
        self.productTableView.reloadData()
        
        if let height = Globals.getConstraint("height", view: self.productTableView) {
            height.constant = self.productTableView.contentSize.height + 100
        }
    }
    
    
    func updateUploadProgress(_ orderId: Int, orderNumber: String, percent: Float){
        self.uploadingOrderId = orderId
        self.uploadingOrderNumber = orderNumber
        self.isUploading = true
        
        //        if let _ = ControllerHelper.getActiveViewController() as? MakePrintViewController {
        
        if let height = Globals.getConstraint("height", view: self.uploadProgressView) {
            if height.constant == 0 {
                
                UIView.animate(withDuration: 0.3,
                               animations: { () -> Void in
                                height.constant = 56
                }, completion: { (finished:Bool) -> Void in
                    
                })
                
            }
        }
        
        self.lblUploadOrderId.text = "ORDER #\(orderNumber)"
        self.lblUploadPercent.text = String(format: "%.0f%%", arguments: [percent * 100])
        self.lblUploadPercent.textColor = UIColor.black
        self.pgbUpload.progress = percent
        //            BSNotification.show("upload progress : \(percent)%", view: self)
        //        }
    }
    
    func uploadComplete(_ orderId: Int) {
        self.isUploading = false
        self.uploadingCart.removeAllObjects()
        
        //        if let _ = ControllerHelper.getActiveViewController() as? MakePrintViewController {
        self.lblUploadPercent.text = "Completed"
        //        }
    }
    
    func uploadFailed(_ orderId: Int) {
        self.isUploading = false
        
        if let _ = ControllerHelper.getActiveViewController() as? MakePrintViewController {
            self.lblUploadPercent.text = "Failed"
            self.lblUploadPercent.textColor = UIColor.red
        }
        
    }
    
    @IBAction func btnUploadProgressTouched(_ sender: AnyObject) {
        if self.isUploading {
            //            self.showViewController("ProgressViewController", attributes: [
            //                "orderId" : String(self.uploadingOrderId),
            //                "orderNumber" : self.uploadingOrderNumber,
            //                "grandTotal" : String(self.uploadingGrandTotal),
            //                "paymentType" : self.uploadingPaymentType
            //                ])
            self.showProgressViewController()
        }
        else{
            
            if self.lblUploadPercent.text == "Failed" {
                self.showProgressViewController()
                //                self.showViewController("ProgressViewController", attributes: [
                //                    "orderId" : String(self.uploadingOrderId),
                //                    "orderNumber" : self.uploadingOrderNumber,
                //                    "grandTotal" : String(self.uploadingGrandTotal),
                //                    "paymentType" : self.uploadingPaymentType
                //                    ])
            }
            else{
                if let height = Globals.getConstraint("height", view: self.uploadProgressView) {
                    if height.constant > 0 {
                        
                        UIView.animate(withDuration: 0.3,
                                       animations: { () -> Void in
                                        height.constant = 0
                        }, completion: { (finished:Bool) -> Void in
                            self.continueToPayment()
                            
                            self.uploadingOrderId = 0
                            self.uploadingOrderNumber = ""
                        })
                        
                    }
                }
            }
            
            
        }
    }
    
    func continueToPayment() {
        if self.uploadingPaymentType == Constant.paymentCreditCard {
            Globals.showWebViewWithUrl(Constant.getPaymentUrl(String(self.uploadingOrderId), grandTotal: String(self.uploadingGrandTotal)), parentView: self, title: "PAYMENT")
        }
        else if self.uploadingPaymentType == Constant.paymentTransfer {
            self.showViewController("PaymentConfirmationViewController", attributes: [
                "orderId" : String(self.uploadingOrderId)
                ])
        }
    }
    
    
    func showProgressViewController() {
        if let _ = ControllerHelper.getProgressViewController() {
            var delay: TimeInterval = 0.6
            
            if !self.keyboardVisible {
                delay = 0
            }
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(MakePrintViewController.showProgressViewControllerAfterDelayed(_:)), userInfo: nil, repeats: false)
        }
        
    }
    
    func showProgressViewControllerAfterDelayed(_ timer: Timer) {
        if let vc = ControllerHelper.getProgressViewController() {
            let navController: UINavigationController = self.revealViewController().frontViewController as! UINavigationController
            navController.pushViewController(vc, animated: true)
            ControllerHelper.setActiveViewController(vc)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
    }
}
