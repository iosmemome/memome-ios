//
//  LeftMenuViewController.swift
//  Memome
//
//  Created by staff on 12/18/15.
//  Copyright © 2015 Trio Digital Agency. All rights reserved.
//

import UIKit

class LeftMenuViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    
    @IBOutlet weak var menuTableView: UITableView!
    var generalMenu: NSMutableArray = NSMutableArray()
    
//    @IBOutlet weak var btnContactUsPhone: UIButton!
    @IBOutlet weak var btnContactUsEmail: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.generalMenu.add("MyProjects")
//        self.generalMenu.add("ConfirmPayment")
        self.generalMenu.add("MyPurchases")
//        self.generalMenu.addObject("ShippingFee")
        self.generalMenu.add("MyProfile")
        self.generalMenu.add("HowToUse")
        self.generalMenu.add("AboutUs")
//        self.generalMenu.add("RedeemVoucher")
        
        self.lblName.text = ""
        self.imgProfile.image = nil
        self.imgProfile.layer.borderColor = UIColor.darkGray.cgColor //(red: 109.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1).cgColor
//        self.imgProfile.layer.borderWidth = 1.0
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width / 2.0
        self.imgProfile.clipsToBounds = true
        
//        self.btnContactUsPhone.imageView?.contentMode = .scaleAspectFit
        self.btnContactUsEmail.imageView?.contentMode = .scaleAspectFit
//        self.btnContactUsPhone.setTitle(Globals.getProperty("contactPhone") , for: UIControlState())
        self.btnContactUsEmail.setTitle(Globals.getProperty("contactEmail"), for: UIControlState())
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
        
        self.lblName.text = String(format: "Hi, %@", arguments: [Globals.getDataSetting("userName") as! String])
//        self.imgProfile.backgroundColor = UIColor.lightGray
        let image = Globals.getDataSetting("userProfilePicture") as! String
        if image != "" {
            Globals.setImageFromUrl(image, imageView: self.imgProfile, placeholderImage: nil, onSuccess: { (image) -> Void in
                self.imgProfile.backgroundColor = UIColor.white
            })
        }
        else{
            self.imgProfile.backgroundColor = UIColor.lightGray
            self.imgProfile.image = nil
        }

        self.menuTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.menuTableView){
            height.constant = self.menuTableView.contentSize.height
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.generalMenu.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        cell = tableView.dequeueReusableCell(withIdentifier: self.generalMenu.object(at: (indexPath as NSIndexPath).row) as! String, for: indexPath)
        
        if let menuName = self.generalMenu.object(at: (indexPath as NSIndexPath).row) as? String {
            if menuName == "ConfirmPayment" {
                
                let lblCounter = cell.viewWithTag(9) as! UILabel
                
                lblCounter.isHidden = true
                lblCounter.layer.cornerRadius = lblCounter.frame.size.width / 2.0
                lblCounter.clipsToBounds = true
                
                if let controller = ControllerHelper.getActiveViewController() {
                    if controller.unconfirmedOrder > 0 {
                        lblCounter.isHidden = false
                        lblCounter.text = String(controller.unconfirmedOrder)
                    }
                }
                
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menuName = self.generalMenu.object(at: (indexPath as NSIndexPath).row) as? String {
            
            switch menuName {
            case "MyProjects" :
                ControllerHelper.getActiveViewController()!.showViewController("MyProjectViewController", attributes: nil)
                break
            case "ConfirmPayment" :
//                ControllerHelper.getActiveViewController()!.showViewController("PaymentConfirmationViewController", attributes: nil)
                ControllerHelper.getActiveViewController()!.showViewController("PaymentUnpaidViewController", attributes: nil)
                break
            case "MyPurchases" :
                ControllerHelper.getActiveViewController()!.showViewController("MyPurchasesViewController", attributes: nil)
                break
            case "ShippingFee" :
                break
            case "MyProfile" :
                ControllerHelper.getActiveViewController()!.showViewController("ProfileViewController", attributes: nil)
                break
            case "HowToUse" :
                ControllerHelper.getActiveViewController()!.showViewController("TutorialViewController", attributes: nil)
                break
            case "AboutUs" :
                ControllerHelper.getActiveViewController()!.showViewController("AboutViewController", attributes: nil)
                break
//            case "RedeemVoucher" :
//                ControllerHelper.getActiveViewController()!.showViewController("RedeemVoucherViewController", attributes: nil)
//                break
            default :
                break
            }
            
        }
    }

    //actions
    @IBAction func btnTermsAndConditionsTouched(_ sender: AnyObject) {
        ControllerHelper.getActiveViewController()!.showViewController("TermsViewController", attributes: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func btnPrivacyPolicyTouched(_ sender: AnyObject) {
        ControllerHelper.getActiveViewController()!.showViewController("PrivacyViewController", attributes: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func btnContactUsPhoneTouched(_ sender: AnyObject) {
        let phoneNumber = Globals.getProperty("contactPhone")
        if let url = URL(string: "tel:\(phoneNumber)") {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btnContactUsEmailTouched(_ sender: AnyObject) {
        if let url = URL(string: String(format: "mailto:%@", arguments: [Globals.getProperty("contactEmail")])) {
            UIApplication.shared.openURL(url)
        }
    }
}
