//
//  ResetPasswordViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 3/2/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class ResetPasswordViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmNewPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Text Fields Delegate
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtNewPassword{
            self.txtConfirmNewPassword.becomeFirstResponder()
        }
        else if textField == self.txtConfirmNewPassword{
            self.btnSendTouched(self.btnSend)
        }
        
        return true
    }
    
    // MARK: - Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnSendTouched(_ sender: AnyObject) {
        if self.validateForm() {
            if !self.isLoading() {
                self.showLoading()
                
                let params: NSMutableDictionary = NSMutableDictionary()
                params.setValue(Globals.getProperty("clientId"), forKey: "clientId")
                params.setValue(self.txtNewPassword.text, forKey: "newPassword")
                
                if let activationKey: String = self.attributes.object(forKey: "activationKey") as? String {
                    params.setValue(activationKey, forKey: "activationKey")
                }
                
                Globals.getDataFromUrl(Globals.getApiUrl("users/resetPassword"),
                    requestType: Globals.HTTPRequestType.http_POST,
                    params: params,
                    completion: { (result) -> Void in
                        self.hideLoading()
                        
                        if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                            self.dismissSelf()
                            if let parent = self._parentViewController {
                                Globals.showAlertWithTitle("Reset Password Success", message: "Your password has been updated successfully.", viewController: parent)
                            }
                        }
                        else{
                            Globals.showAlertWithTitle("Reset Password Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                        }
                })
            }
        }
    }
    
    func validateForm() -> Bool {
        let requiredTextFields = [
            ["field" : self.txtNewPassword, "name" : "New Password"],
            ["field" : self.txtConfirmNewPassword, "name" : "Confirm New Password"]
        ]
        
        for item in requiredTextFields {
            let requiredTextField: NSDictionary = item as NSDictionary
            if (requiredTextField.object(forKey: "field") as? UITextField)?.text == "" {
                let message: String = String(format: "%@ must not be empty", arguments: [requiredTextField.object(forKey: "name") as! String])
                Globals.showAlertWithTitle("Reset Password Error", message: message, viewController: self)
                (requiredTextField.object(forKey: "field") as? UITextField)?.becomeFirstResponder()
                return false
            }
        }
        
        if self.txtNewPassword.text != self.txtConfirmNewPassword.text {
            Globals.showAlertWithTitle("Reset Password Error", message: "Your new password mismatch", viewController: self)
            self.txtConfirmNewPassword.becomeFirstResponder()
            return false
        }
        
        return true
    }
}
