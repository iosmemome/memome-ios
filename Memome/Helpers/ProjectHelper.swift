//
//  ProjectHelper.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/25/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import CoreData

class ProjectHelper: NSObject {

    static func addToMyProject(_ product: NSDictionary, productDetail: NSArray) -> Project? {
        return ProjectHelper.addToMyProject(product, productDetail: productDetail, voucherCode: "", voucherId: 0, voucherName: "", voucherQty: 0)
    }
    static func addToMyProject(_ product: NSDictionary, productDetail: NSArray, voucherCode: String, voucherId: Int, voucherName: String, voucherQty: Int) -> Project? {
        let context = Globals.getManagedObjectContext()//appDelegate.managedObjectContext
        let error: NSErrorPointer? = nil
        
        let project: Project = NSEntityDescription.insertNewObject(forEntityName: "Project", into: context) as! Project
        
        if MemomeHelper.coverImage != nil {
            if let title = MemomeHelper.coverImage.object(forKey: "title") as? String {
                project.title = title
            }
            if let subtitle = MemomeHelper.coverImage.object(forKey: "subtitle") as? String {
                project.subtitle = subtitle
            }
        }
        project.productId = (product.object(forKey: "id") as! NSString).intValue
        project.productData = NSKeyedArchiver.archivedData(withRootObject: product)
        project.productDetailData = NSKeyedArchiver.archivedData(withRootObject: productDetail)
        
        project.corporateVoucherCode = voucherCode
        project.corporateVoucherQty = Int32(voucherQty)
        project.corporateVoucherName = voucherName
        project.corporateVoucherId = Int32(voucherId)
        
        do {
            try context.save()
        } catch let error1 as NSError {
            error??.pointee = error1
        }
        
        if error != nil {
            NSLog("Cannot store project %@", error.debugDescription)
            return nil
        }
        
        if MemomeHelper.coverImage != nil {
            if let cover = MemomeHelper.coverImage.object(forKey: "url") as? String {
                let coverImage: ProjectDetail = NSEntityDescription.insertNewObject(forEntityName: "ProjectDetail", into: context) as! ProjectDetail
                coverImage.projectId = project.id
                coverImage.image = cover
                coverImage.type = ProjectDetail.TYPE_COVER
                if let size = MemomeHelper.coverImage.object(forKey: "size") as? NSValue {
                    coverImage.width = Float(size.cgSizeValue.width)
                    coverImage.height = Float(size.cgSizeValue.height)
                }
                if let offset = MemomeHelper.coverImage.object(forKey: "offset") as? NSValue {
                    coverImage.offsetX = Float(offset.cgPointValue.x)
                    coverImage.offsetY = Float(offset.cgPointValue.y)
                }
                if let zoom = MemomeHelper.coverImage.object(forKey: "zoomScale") as? NSNumber {
                    coverImage.zoom = zoom.floatValue
                }
                if let rotation = MemomeHelper.coverImage.object(forKey: "rotation") as? NSNumber {
                    coverImage.rotation = rotation.floatValue
                }
                if let imageData = MemomeHelper.coverImage.object(forKey: "imageData") as? Data {
                    coverImage.imageData = imageData
                }
                
                do {
                    try context.save()
                } catch let error1 as NSError {
                    error??.pointee = error1
                }
            }
        }
        
        if MemomeHelper.images != nil {
            for item in MemomeHelper.images {
                if let content = item as? NSDictionary {
                    if let imageUrl = content.object(forKey: "url") as? String {
                        let image: ProjectDetail = NSEntityDescription.insertNewObject(forEntityName: "ProjectDetail", into: context) as! ProjectDetail
                        image.projectId = project.id
                        image.image = imageUrl
                        image.type = ProjectDetail.TYPE_CONTENT
                        if let size = content.object(forKey: "size") as? NSValue {
                            image.width = Float(size.cgSizeValue.width)
                            image.height = Float(size.cgSizeValue.height)
                        }
                        if let offset = content.object(forKey: "offset") as? NSValue {
                            image.offsetX = Float(offset.cgPointValue.x)
                            image.offsetY = Float(offset.cgPointValue.y)
                        }
                        if let zoom = content.object(forKey: "zoomScale") as? NSNumber {
                            image.zoom = zoom.floatValue
                        }
                        if let rotation = content.object(forKey: "rotation") as? NSNumber {
                            image.rotation = rotation.floatValue
                        }
                        if let imageData = content.object(forKey: "imageData") as? Data {
                            image.imageData = imageData
                        }
                        
                        do {
                            try context.save()
                        } catch let error1 as NSError {
                            error??.pointee = error1
                        }
                    }
                    
                }
            }
        }
        
        
        if error != nil {
            NSLog("Cannot store project %@", error.debugDescription)
            return nil
        }
        
        
        MemomeHelper.coverImage = nil
        if MemomeHelper.images != nil {
            MemomeHelper.images.removeAllObjects()
        }
        return project
    }
    
    static func editMyProject(_ oldProject: Project, projectDetails: NSArray)  -> Project?{
        var context = Globals.getManagedObjectContext()//appDelegate.managedObjectContext
        let error: NSErrorPointer? = nil
        if let ctx = oldProject.managedObjectContext {
            context = ctx
        }
        
//        let project: Project = NSEntityDescription.insertNewObjectForEntityForName("Project", inManagedObjectContext: context) as! Project
        let project = oldProject
        
        if let title = MemomeHelper.coverImage.object(forKey: "title") as? String {
            project.title = title
        }
        if let subtitle = MemomeHelper.coverImage.object(forKey: "subtitle") as? String {
            project.subtitle = subtitle
        }
//        project.productId = (product.objectForKey("id") as! NSString).intValue
//        project.productData = NSKeyedArchiver.archivedDataWithRootObject(product)
//        project.productDetailData = NSKeyedArchiver.archivedDataWithRootObject(productDetail)
//        
//        project.corporateVoucherCode = voucherCode
//        project.corporateVoucherQty = Int32(voucherQty)
//        project.corporateVoucherName = voucherName
//        project.corporateVoucherId = Int32(voucherId)
        
        do {
            try context.save()
        } catch let error1 as NSError {
            error??.pointee = error1
        }
        
        if error != nil {
            NSLog("Cannot store project %@", error.debugDescription)
            return nil
        }
        
        var i = 0
        for item in projectDetails {
            if let detail = item as? ProjectDetail {
                
                if let ctx = detail.managedObjectContext {
                    context = ctx
                }
                
                if detail.type == ProjectDetail.TYPE_COVER {
                    
                    if let cover = MemomeHelper.coverImage.object(forKey: "url") as? String {
                        
//                        let coverImage: ProjectDetail = NSEntityDescription.insertNewObjectForEntityForName("ProjectDetail", inManagedObjectContext: context) as! ProjectDetail
                        detail.image = cover
                        if let size = MemomeHelper.coverImage.object(forKey: "size") as? NSValue {
                            detail.width = Float(size.cgSizeValue.width)
                            detail.height = Float(size.cgSizeValue.height)
                        }
                        if let offset = MemomeHelper.coverImage.object(forKey: "offset") as? NSValue {
                            detail.offsetX = Float(offset.cgPointValue.x)
                            detail.offsetY = Float(offset.cgPointValue.y)
                        }
                        if let zoom = MemomeHelper.coverImage.object(forKey: "zoomScale") as? NSNumber {
                            detail.zoom = zoom.floatValue
                        }
                        if let rotation = MemomeHelper.coverImage.object(forKey: "rotation") as? NSNumber {
                            detail.rotation = rotation.floatValue
                        }
                        if let imageData = MemomeHelper.coverImage.object(forKey: "imageData") as? Data {
                            detail.imageData = imageData
                        }
                        
                        do {
                            try context.save()
                        } catch let error1 as NSError {
                            error??.pointee = error1
                        }
                    }
                    
                }
                else {
                    if let content = MemomeHelper.images.object(at: i) as? NSDictionary {
                        if let imageUrl = content.object(forKey: "url") as? String {
//                            let image: ProjectDetail = NSEntityDescription.insertNewObjectForEntityForName("ProjectDetail", inManagedObjectContext: context) as! ProjectDetail
                            detail.image = imageUrl
                            if let size = content.object(forKey: "size") as? NSValue {
                                detail.width = Float(size.cgSizeValue.width)
                                detail.height = Float(size.cgSizeValue.height)
                            }
                            if let offset = content.object(forKey: "offset") as? NSValue {
                                detail.offsetX = Float(offset.cgPointValue.x)
                                detail.offsetY = Float(offset.cgPointValue.y)
                            }
                            if let zoom = content.object(forKey: "zoomScale") as? NSNumber {
                                detail.zoom = zoom.floatValue
                            }
                            if let rotation = content.object(forKey: "rotation") as? NSNumber {
                                detail.rotation = rotation.floatValue
                            }
                            if let imageData = content.object(forKey: "imageData") as? Data {
                                detail.imageData = imageData
                            }
                            
                            do {
                                try context.save()
                            } catch let error1 as NSError {
                                error??.pointee = error1
                            }
                        }
                        
                    }
                    
                    i += 1
                    
                }
                
            }
        }
        
        
        
        if error != nil {
            NSLog("Cannot store project %@", error.debugDescription)
            return nil
        }
        
        
        MemomeHelper.coverImage = nil
        MemomeHelper.images.removeAllObjects()
        return project
    }
    
    static func getAllProjects() -> NSArray? {
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Project", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        return objects as NSArray?
    }
    
    static func getProject(_ projectId: Int32) -> Project? {
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Project", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(id = %i)", projectId)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count == 0{
            return nil
        }
        else{
            return objects[0] as? Project
        }
    }
    
    static func getAllProjectDetails(_ projectId: Int32) -> NSArray? {
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "ProjectDetail", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(projectId = %i)", projectId)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        return objects as NSArray?
    }
    
    static func getCoverImage(_ projectId: Int32) -> ProjectDetail? {
        return getCoverImage(projectId, false)
    }
    
    static func getCoverImage(_ projectId: Int32, _ fromOther: Bool) -> ProjectDetail? {
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "ProjectDetail", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(projectId = %i and type = %@ )", projectId, ProjectDetail.TYPE_COVER)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
            if objects.count > 0 {
                if let detail = objects[0] as? ProjectDetail{
                    return detail
                }
            }
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if fromOther {
            if objects == nil || objects.count == 0 {
                let pred: NSPredicate = NSPredicate(format: "(projectId = %i and type = %@ )", projectId, ProjectDetail.TYPE_CONTENT)
                request.predicate = pred
                request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
                var objects: [AnyObject]!
                do {
                    objects = try context.fetch(request)
                    if objects.count > 0 {
                        if let detail = objects[objects.count - 1] as? ProjectDetail{
                            return detail
                        }
                    }
                } catch let error1 as NSError {
                    error??.pointee = error1
                    objects = nil
                }
            }
        }
        return nil
    }
    
    static func deleteProject(_ projectId: Int32) -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Project", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(id = %i)", projectId)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count > 0 {
            context.delete(objects[0] as! NSManagedObject)
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
            if error != nil {
                print("Error deleting \(entityDesc) - error:\(error)");
                return false
            }
        }
        
        ProjectHelper.deleteProjectDetailByProjectId(projectId)
        
        return true
    }
    
    static func deleteProjectDetailByProjectId(_ projectId: Int32) -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "ProjectDetail", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(projectId = %i)", projectId)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "projectId", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count > 0 {
            for i in 0 ..< objects.count {
                context.delete(objects[i] as! NSManagedObject)
            }
            
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
            if error != nil {
                print("Error deleting \(entityDesc) - error:\(error)");
                return false
            }
        }
        
        return true
    }
    
    static func deleteProjectFromCart(_ projectId: Int32) -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Cart", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(projectId = %i)", projectId)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count > 0 {
            context.delete(objects[0] as! NSManagedObject)
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
            if error != nil {
                print("Error deleting \(entityDesc) - error:\(error)");
                return false
            }
        }
        
        ProjectHelper.deleteProjectDetailByProjectId(projectId)
        
        return true
    }
    
    static func clearProject() -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Project", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        for object in objects {
            context.delete(object as! NSManagedObject)
        }
        
        do {
            try context.save()
        } catch let error1 as NSError {
            error??.pointee = error1
        }
        if error != nil {
            print("Error clear project \(entityDesc) - error:\(error)");
            return false
        }
        
        return ProjectHelper.clearProjectDetail()
    }
    
    fileprivate static func clearProjectDetail() -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "ProjectDetail", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        for object in objects {
            context.delete(object as! NSManagedObject)
        }
        
        do {
            try context.save()
        } catch let error1 as NSError {
            error??.pointee = error1
        }
        if error != nil {
            print("Error Clear Project Detail \(entityDesc) - error:\(error)");
            return false
        }
        
        return true
    }
    
    static func deleteCorporateVoucher(_ projectId: Int32) -> Bool{
        let context = Globals.getManagedObjectContext()
        let error: NSErrorPointer? = nil
        
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Project", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        let pred: NSPredicate = NSPredicate(format: "(id = %i)", projectId)
        request.predicate = pred
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var objects: [AnyObject]!
        do {
            objects = try context.fetch(request)
        } catch let error1 as NSError {
            error??.pointee = error1
            objects = nil
        }
        if objects.count > 0 {
            
            if let project = objects[0] as? Project {
                project.corporateVoucherCode = ""
                project.corporateVoucherId = 0
                project.corporateVoucherQty = 0
                project.corporateVoucherName = ""
            }
            
            do {
                try context.save()
            } catch let error1 as NSError {
                error??.pointee = error1
            }
            if error != nil {
                print("Error deleting corporate voucher \(entityDesc) - error:\(error)");
                return false
            }
        }
        
        return true
    }
}
