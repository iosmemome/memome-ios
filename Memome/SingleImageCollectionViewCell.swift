//
//  SingleImageCollectionViewCell.swift
//  IRG
//
//  Created by Stillalive on 22/10/17.
//  Copyright © 2017 Stillalive. All rights reserved.
//

import UIKit

class SingleImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        imageView.image = nil
    }
}
