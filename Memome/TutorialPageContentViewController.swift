//
//  TutorialPageContentViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 3/1/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class TutorialPageContentViewController: BaseViewController {
    
    @IBOutlet weak var tutorialImageView: UIImageView!
    
    var pageIndex: Int!
    var imageFile: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if imageFile != nil{
//            let cover_image = (cover as! NSDictionary).object(forKey: "image") as? NSDictionary
//            if let image_large = cover_image?.object(forKey: "large") as? String{
            
//                print("IMAGE \(image_large)")
                let urlImage = URL(string : imageFile)
                let session = URLSession.shared
                let getImageFromUrl = session.dataTask(with: urlImage!){(data, response, error) in
                    
                    if let e = error{
                        print("Error Occurred : \(e)")
                    } else{
                        if (response as? HTTPURLResponse) != nil{
                            if let imageData = data{
                                DispatchQueue.main.async {
                                    let images = UIImage(data: imageData)
                                    self.tutorialImageView.image = images
                                }
                            } else{
                                print("Image file is corrupted")
                            }
                        }
                        else{
                            print("No response from server")
                        }
                    }
                }
                getImageFromUrl.resume()
//            }
        }
        
//        self.tutorialImageView.image = UIImage(named: self.imageFile)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
