//
//  RegisterViewController.swift
//  Memome
//
//  Created by staff on 1/4/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    
    @IBOutlet weak var registerScrollView: UIScrollView!
    @IBOutlet weak var registerContentScrollView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.defaultScrollView = self.registerScrollView
        self.defaultContentScrollView = self.registerContentScrollView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.txtName {
            self.txtEmail.becomeFirstResponder()
        }
        else if textField == self.txtEmail {
            self.txtPassword.becomeFirstResponder()
        }
        else if textField == self.txtPassword {
            self.txtConfirmPassword.becomeFirstResponder()
        }
        else if textField == self.txtConfirmPassword {
            self.btnRegisterTouched(textField)
        }
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    @IBAction func btnRegisterTouched(_ sender: AnyObject) {
        let params = NSMutableDictionary()
        params.setValue(self.txtName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "name")
        params.setValue(self.txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKey: "email")
        params.setValue(self.txtPassword.text, forKey: "password")
        params.setValue(Globals.getProperty("clientId"), forKey: "clientId")
        
        
        if !self.isLoading() && self.validateForm(params) {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("users/register"),
                requestType: Globals.HTTPRequestType.http_POST,
                params: params,
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        self.dismissSelf()
                        if let parent = self._parentViewController {
                            Globals.showAlertWithTitle("Register Success", message: "Your profile has been created. Please login with your registered email and password.", viewController: parent)
                        }
                    }
                    else{
                        Globals.showAlertWithTitle("Register Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
        
    }
    
    //general methods
    func validateForm(_ params: NSDictionary) -> Bool {
        var isValid = true
        if params.object(forKey: "name") as! String == "" {
            Globals.showAlertWithTitle("Register Error", message: "Name must not be empty!", viewController: self)
            isValid = false
        }
        else if params.object(forKey: "email") as! String == "" || !Globals.isValidEmail(params.object(forKey: "email") as! String) {
                Globals.showAlertWithTitle("Register Error", message: "Email must not be empty and must be a valid email address!", viewController: self)
            isValid = false
        }
        else if params.object(forKey: "password") as! String == "" {
            Globals.showAlertWithTitle("Register Error", message: "Password must not be empty!", viewController: self)
            isValid = false
        }
        else if params.object(forKey: "password") as? String != self.txtConfirmPassword.text {
            Globals.showAlertWithTitle("Register Error", message: "Password mismatch!", viewController: self)
            isValid = false
        }
        
        
        return isValid
    }
}
