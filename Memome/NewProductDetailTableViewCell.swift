//
//  NewProductDetailTableViewCell.swift
//  Memome
//
//  Created by Stillalive on 25/07/18.
//  Copyright © 2018 Trio Digital Agency. All rights reserved.
//

import UIKit

import Kingfisher


protocol NewProductDetailTableViewCellDelegate {
    func newProductDetailCell(cell: NewProductDetailTableViewCell, didSelectItemAt indexPath: IndexPath)
}

class NewProductDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dimensionLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var qualityLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    var delegate: NewProductDetailTableViewCellDelegate?
    var imageObjects = [NSObject]()
    var imageCellSize: CGSize!
    private var timer: Timer!
    
    
    // MARK: - View Lifecycles
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        
        cardView.layer.cornerRadius = 8
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "SingleImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imageCell")
        
        pageControl.backgroundColor = .clear
        pageControl.pageIndicatorTintColor = .white
        pageControl.currentPageIndicatorTintColor = .colorPrimary
        
        imageCellSize = CGSize(width: UIScreen.main.bounds.width-16, height: UIScreen.main.bounds.width-16)
        timer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)
    }
    
    override func prepareForReuse() {
        collectionView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: - Action Handlers
    @objc fileprivate func timerFired() {
        if imageObjects.count > 0 {
            var nextPosition = pageControl.currentPage + 1
            if nextPosition >= imageObjects.count { nextPosition = 0 }
            
            pageControl.currentPage = nextPosition
            pageControlValueChanged(pageControl)
        }
    }
    
    @IBAction func pageControlValueChanged(_ sender: Any) {
        collectionView.scrollToItem(at: IndexPath.init(row: pageControl.currentPage, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    
    // MARK: - Public Methods
    func setupPageControl() {
        if imageObjects.count > 1 {
            pageControl.isHidden = false
        }else {
            pageControl.isHidden = true
        }
        
        pageControl.numberOfPages = imageObjects.count
    }
}


// MARK: -
extension NewProductDetailTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: Collection View Data Source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageObjects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createImageCell(indexPath)
    }
    
    
    // MARK: Collection View Delegate
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let imageCell = cell as! SingleImageCollectionViewCell
        let imgObj = imageObjects[indexPath.row]
        if let image = imgObj as? UIImage {
            imageCell.imageView.image = image
        }else if let urlString = imgObj as? String {
            if let url = URL(string: urlString) {
                imageCell.imageView.kf.setImage(with: url, options: [.transition(.fade(0.2))])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.newProductDetailCell(cell: self, didSelectItemAt: indexPath)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let xAxis = scrollView.contentOffset.x
        
        if xAxis > 0 {
            let position = xAxis/scrollView.bounds.width
            pageControl.currentPage = Int(position)
        }else {
            pageControl.currentPage = 0
        }
    }
    
    
    // MARK: Collection View Flow Layout Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return imageCellSize
    }
    
    
    // MARK: Cell Creation
    private func createImageCell(_ indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath)
        return cell
    }
}
