//
//  AboutViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 3/1/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class AboutViewController: BaseViewController, BSSliderDelegate {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var aboutSlider: BSSlider!
    @IBOutlet weak var txtAbout: UITextView!
    
    var aboutSliderDataSource: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.aboutSlider.setDelegate(self)
        self.loadDataAbout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // bs slider delegate
    func setupSlides(_ slider: BSSlider) {
        if let slidesContainer = slider.getSlidesContainer(){
            let width: CGFloat = slider.bounds.size.width
            let height: CGFloat = slider.bounds.size.height
            var i: CGFloat = 0
            for item in self.aboutSliderDataSource {
                let contents = Bundle.main.loadNibNamed("AboutSlide", owner: self, options: nil)
                let slide: UIView = contents!.last as! UIView
                slide.frame = CGRect(x: i * width, y: 0, width: width, height: height)
                slide.clipsToBounds = true
                
                let imgAbout: UIImageView = slide.viewWithTag(1) as! UIImageView
                
                let type = "large"
                //            if self.view.frame.size.width > 700 {
                //                type = "medium"
                //            }
                imgAbout.image = nil
                if let image = (item as AnyObject).object(forKey: type) as? String {
                    Globals.setImageFromUrl(image, imageView: imgAbout, placeholderImage: nil)
                }
                
                slidesContainer.addSubview(slide)
                
                i += 1
            }
        }
    }

    // actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    //general methods
    func loadDataAbout(){
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(Globals.getApiUrl("about/search"),
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId")
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let about = result.object(forKey: "about") as? NSDictionary {
                            if let content = about.object(forKey: "about_content") as? String {
                                var htmlText = content
                                htmlText += "<style>"
                                htmlText += "* {"
                                htmlText += "font-family: '\(self.txtAbout.font?.fontName)';"
                                htmlText += "font-size: \(self.txtAbout.font?.pointSize)px;"
                                htmlText += "text-align: center;"
                                htmlText += "}"
                                htmlText += "</style>"
                                
                                do {
                                    let str = try NSAttributedString(data: htmlText.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                                    self.txtAbout.attributedText = str
                                    
                                } catch {
                                    print(error)
                                }
                                
                                
                            }
                            
                            self.aboutSliderDataSource.removeAllObjects()
                            if let images = about.object(forKey: "about_images") as? NSArray {
                                self.aboutSliderDataSource.addObjects(from: images as [AnyObject])
                            }
                            
                            self.aboutSlider.reloadSlides()
                        }
                        
                    }
                    else{
                        Globals.showAlertWithTitle("About Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
}
