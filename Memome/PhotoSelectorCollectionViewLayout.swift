//
//  PhotoSelectorCollectionViewLayout.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/18/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class PhotoSelectorCollectionViewLayout: UICollectionViewFlowLayout {
    var itemOffset : UIOffset = UIOffsetMake(1, 1)
    var itemAttributes : NSMutableArray = NSMutableArray()
    var contentSize : CGSize = CGSize(width: 0, height: 0)
    var dataLength: Int = 0
    
    override func prepare() {
        self.itemAttributes.removeAllObjects()
        self.contentSize = CGSize(width: 0, height: 0)
        
        var column : Int = 0    // Current column inside row
        
        self.itemOffset = UIOffsetMake(1.0, 1.0)
        
        var xOffset : CGFloat = self.itemOffset.horizontal
        var yOffset : CGFloat = self.itemOffset.vertical
        var rowHeight : CGFloat = 0.0
        
        var contentWidth : CGFloat = 0.0         // Used to determine the contentSize
        var contentHeight : CGFloat = 0.0        // Used to determine the contentSize
        
        // We'll create a dynamic layout. Each row will have a random number of columns
        var numberOfColumnsInRow : Int = 3
        var coef : CGFloat = 0.3333
        if(self.collectionView!.frame.size.width > 618){
            numberOfColumnsInRow = 6
            coef = 0.166667
        }
        
        // Loop through all items and calculate the UICollectionViewLayoutAttributes for each one
        let numberOfItems : Int = self.collectionView!.numberOfItems(inSection: 0)
        self.dataLength = numberOfItems
        //        NSLog("number of items : %i", numberOfItems)
        for index:Int in 0 ..< numberOfItems
        {
            let itemSize: CGSize = CGSize(
                width: floor(self.collectionView!.frame.size.width * coef) - (self.itemOffset.horizontal * 1.5),
                height: floor(self.collectionView!.frame.size.width * coef) - (self.itemOffset.vertical * 1.5))
            
            if itemSize.height > rowHeight {
                rowHeight = itemSize.height
            }
            
            // Create the actual UICollectionViewLayoutAttributes and add it to your array. We'll use this later in layoutAttributesForItemAtIndexPath:
            let indexPath: IndexPath = IndexPath(item: index, section: 0)
            //            NSLog("Index : %i", index)
            let attributes: UICollectionViewLayoutAttributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemSize.width, height: itemSize.height).integral
            self.itemAttributes.add(attributes)
            
            xOffset = xOffset + itemSize.width + (self.itemOffset.horizontal * 1)
            column += 1
            
            // Create a new row if this was the last column
            if column == numberOfColumnsInRow
            {
                if xOffset > contentWidth {
                    contentWidth = xOffset
                }
                
                // Reset values
                column = 0
                xOffset = self.itemOffset.horizontal
                yOffset += rowHeight + (self.itemOffset.vertical * 1)
            }
        }
        
        if self.itemAttributes.count > 0 {
            // Get the last item to calculate the total height of the content
            //            NSLog("item attributes count : %i", self.itemAttributes.count)
            let attributes: UICollectionViewLayoutAttributes = self.itemAttributes.lastObject as! UICollectionViewLayoutAttributes
            contentHeight = attributes.frame.origin.y + attributes.frame.size.height;
        }
        
        if contentWidth == 0 {
            contentWidth = self.collectionView!.frame.size.width;
        }
        
        // Return this in collectionViewContentSize
        self.contentSize = CGSize(width: contentWidth, height: contentHeight);
    }
    
    override var collectionViewContentSize : CGSize
    {
        return self.contentSize
    }
    
    override func layoutAttributesForItem(at indexPath:IndexPath) -> UICollectionViewLayoutAttributes {
        if (indexPath as NSIndexPath).row < self.itemAttributes.count {
            return self.itemAttributes.object(at: (indexPath as NSIndexPath).row) as! UICollectionViewLayoutAttributes
        }
        else {
            return UICollectionViewLayoutAttributes(forCellWith: indexPath)
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return self.itemAttributes.filtered(using: NSPredicate(block: { (evaluatedObject, _: [String : Any]?) -> Bool in
            return rect.intersects((evaluatedObject as! UICollectionViewLayoutAttributes).frame)
        })) as? [UICollectionViewLayoutAttributes]
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return false
    }
}
