//
//  RedeemVoucherViewController.swift
//  Memome
//
//  Created by Trio-1602 on 4/11/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import BSQRCodeReader


class RedeemVoucherViewController: BaseViewController,  BSQRCodeReaderDelegate{

    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var reader: BSQRCodeReader!
    
    @IBOutlet weak var txtVoucherCode: UITextField!
    @IBOutlet weak var btnApplyVoucherCode: UIButton!
    var isVoucher: Bool = false
    var selectedCoupon: NSDictionary = NSDictionary()
    var lblVoucher: String = ""
    var codes_id = ""
    var codes_name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtVoucherCode.text = lblVoucher
        
        // Do any additional setup after loading the view.
        if self.attributes != nil{
            if let code = self.attributes.object(forKey: "coupon") as? NSDictionary{
                selectedCoupon = code
            }
            if let isvoucher = self.attributes.object(forKey: "isVoucher") as? Bool{
                isVoucher = isvoucher
            }
            if let labelVoucher = self.attributes.object(forKey: "lblVoucher") as? String{
                if labelVoucher == "0.0"{
                    txtVoucherCode.text = ""
                }else{
                txtVoucherCode.text = labelVoucher
                }
            }
        }
        
        self.reader.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.reader.startScanning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.reader.stopScanning()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    // BSQRCodeReaderDelegate
//    func didCaptureQRCodeWithContent(_ content: String) -> Bool {
//        if content.substring(to: content.index(content.startIndex, offsetBy: 6)) == "MEMOME" {
//            let code = content.substring(from: content.index(content.startIndex, offsetBy: 7))
//            self.showLoading()
//
//            var isExist = false
//
//            for item in CartHelper.getCart() {
//                if let cart = item as? Cart {
//                    if let project = ProjectHelper.getProject(cart.projectId) {
//                        if project.corporateVoucherCode == code && project.corporateVoucherId > 0 {
//                            isExist = true
//                        }
//                    }
//                }
//            }
//
//            if isExist {
//                self.hideLoading()
//                Globals.showAlertWithTitle("Redeem Voucher Error", message: "Code is already used.", viewController: self)
//                return false
//            }
//            else {
//                Globals.getDataFromUrl(
//                    Globals.getApiUrl("corporates/search"),
//                    noCache: true,
//                    requestType: Globals.HTTPRequestType.http_GET,
//                    params: [
//                        "clientId" : Globals.getProperty("clientId"),
//                        "accessToken" : Globals.getDataSetting("accessToken") as! String,
//                        "code" : code
//                ]) { (result) in
//                    self.hideLoading()
//                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
//                        if let voucher = result.object(forKey: "corporateVoucher") as? NSDictionary {
//                            self.showViewController("CorporateProductViewController", attributes: [
//                                "voucher" : voucher,
//                                "voucherCode" : code
//                                ])
//                        }
//                        else{
//                            Globals.showAlertWithTitle("Redeem Voucher Error", message: "Voucher not found", viewController: self)
//                            self.reader.startScanning()
//                        }
//
//
//                    }
//                    else{
//                        Globals.showAlertWithTitle("Redeem Voucher Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
//                        self.reader.startScanning()
//                    }
//                }
//                return true
//            }
//
//
//        }
//        else{
//            BSNotification.show("Code is undefined", view: self)
//            return false
//        }
//    }
    
    // BSQRCodeReaderDelegate
    func didCaptureQRCodeWithContent(_ content: String) -> Bool {
        
        var code = content
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(
                Globals.getApiUrl("corporates/search"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String,
                    "code" : code
                ],
                completion: { (result) -> Void in
                    
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let corporate_voucher = result.object(forKey: "corporateVoucher") as? NSDictionary{
                            if let qty = corporate_voucher.object(forKey: "qty") as? String{
                                code = qty
                            }
                            
                            if let id = corporate_voucher.object(forKey: "id") as? String{
                                self.codes_id = id
                            }
                            
                            if let name = corporate_voucher.object(forKey: "name") as? String{
                                self.codes_name = name
                            }
                        }
                        
                        if let parent = self._parentViewController as? CheckoutViewController{
                            
                            parent.isVoucher = false
                            parent.codes = code
                            parent.codes_name = self.codes_name
                            parent.codes_id = self.codes_id
                            
                        }
                        
                        self.dismissSelf()
                        
                        
                    }
                    else{
                        Globals.showAlertWithTitle("Code Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self, completion:  nil)
                    }
            })
            return true
        }
            
        else {
            BSNotification.show("Code is undefined", view: self)
            return false
            
        }
        
//    }
    
    }

    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnApplyVoucherCodeTouched(_ sender: AnyObject) {
        if let _code = self.txtVoucherCode.text {
            var code = _code
            if code != "" {
//                if code.substring(to: code.index(code.startIndex, offsetBy: 6)) != "MEMOME" {
//                    code = String(format: "MEMOME-%@", code)
//                }
//                let _ = self.didCaptureQRCodeWithContent(code)
                  // dicomment dulu sementara
                
                if isVoucher == true{
                     Globals.showAlertWithTitle("Sorry...", message: "You can not apply for both PROMO CODE and GIFT CARD", viewController: self)
                }else{
                    
                    let _ = self.didCaptureQRCodeWithContent(code)
                    
//                    if let parent = self._parentViewController as? CheckoutViewController{
////                        self.showViewController("CheckoutViewController", attributes: ["code" : code, "isVoucher" : false])
//                        parent.isVoucher = false
//                        parent.codes = code
////                        parent.viewDidLoad()
//                    }
//
//                    self.dismissSelf()
                    
                }
                
            }
            else{

                if isVoucher == true{
                    Globals.showAlertWithTitle("Sorry...", message: "You can not apply for both PROMO CODE and GIFT CARD", viewController: self)
                }else{
                    
                    if let parent = self._parentViewController as? CheckoutViewController{
                        parent.isVoucher = false
                        parent.codes = code
//                        parent.viewDidLoad()
                    }
                    
                    self.dismissSelf()
                    
                }
                
            }
        }
        else{
            Globals.showAlertWithTitle("Redeem Voucher Error", message: "Please enter a valid barcode number or scan it", viewController: self)
        }
    }
    
}
