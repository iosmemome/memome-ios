//
//  AppDelegate.swift
//  Memome
//
//  Created by staff on 12/10/15.
//  Copyright © 2015 Trio Digital Agency. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import GoogleSignIn
import MidtransKit

//MidtransConfig.shared().setClientKey("_siqvfNYd5JwmCe3", environment: .sandbox, merchantServerURL: "merchant server url")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate{
    var window: UIWindow?

    var pendingStoryboardName: String?
    var pendingAttributes: NSDictionary?
    
    var googleUser: GIDGoogleUser?
    var pendingGoogleLogin = false
    var loginViewController: LoginViewController?
    
    override class func initialize () {
        super.initialize()
        
        MidtransConfig.shared().setClientKey("VT-client-LQ5AL1YEpSoH7DdJ", environment: .production, merchantServerURL: "https://memome.co.id/transactions/index.php")
        
        //configure iRate
//        iRate.sharedInstance.daysUntilPrompt = 1;
//        iRate.sharedInstance.usesUntilPrompt = 2;
    }
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.dbMigrate()
        
        let pageController = UIPageControl.appearance()
        pageController.pageIndicatorTintColor = UIColor.lightGray
        pageController.currentPageIndicatorTintColor = UIColor.black
        pageController.backgroundColor = UIColor.white
        
        // Initialize sign-in
//        var configureError: NSError?
//        GGLContext.sharedInstance().configureWithError(&configureError)
//        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().clientID = "846589451901-62vh5ti290onprj3lhhtig9jtn88udt4.apps.googleusercontent.com"
        
        // Push notification
        if (application.responds(to: #selector(UIApplication.registerUserNotificationSettings(_:)))){
            application.registerUserNotificationSettings(UIUserNotificationSettings.init(types: [UIUserNotificationType.badge, UIUserNotificationType.sound, UIUserNotificationType.alert], categories: nil))
        }
        else{
            application.registerForRemoteNotifications(matching: [UIRemoteNotificationType.badge, UIRemoteNotificationType.sound, UIRemoteNotificationType.alert])
        }
        
        Globals.registerDeviceId()
        
        // Set Minimum Background Fetch Inteval //
        if ProcessInfo().isOperatingSystemAtLeast(OperatingSystemVersion(majorVersion: 7, minorVersion: 0, patchVersion: 0)) {
            UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        }
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.triodigitalagency.Memome" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "memome", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        let mOptions = [NSMigratePersistentStoresAutomaticallyOption: true,
                        NSInferMappingModelAutomaticallyOption: true]
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: mOptions)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

    // MARK: - Set Default Value DB
    func dbMigrate () {
        let error: NSErrorPointer? = nil
        let dataPath : String = Bundle.main.path(forResource: "Settings", ofType: "json")!
        var settings : NSArray!
        do {
            try settings = JSONSerialization.jsonObject(with: Data(contentsOf: URL(fileURLWithPath: dataPath)), options: [JSONSerialization.ReadingOptions.allowFragments, JSONSerialization.ReadingOptions.mutableContainers]) as? NSArray
        }
        catch {
            
        }
        
        let context = self.managedObjectContext
        let entityDesc: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Setting", in: context)!
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        request.entity = entityDesc
        
        for obj in settings{
            //check if exists
            let pred: NSPredicate = NSPredicate(format: "(name = %@)", (obj as! NSDictionary).object(forKey: "name") as! String)
            request.predicate = pred
            var objects: [AnyObject]!
            do {
                objects = try context.fetch(request)
            } catch let error1 as NSError {
                error??.pointee = error1
                objects = nil
            }
            
            if (objects.count  == 0){
                //create new setting
                let setting = NSEntityDescription.insertNewObject(forEntityName: "Setting", into: context) as! Setting
                setting.name = (obj as! NSDictionary).object(forKey: "name") as! String
                setting.value = (obj as! NSDictionary).object(forKey: "value") as! String
                do {
                    try context.save()
                }
                catch let error1 as NSError {
                    NSLog("Whoops, cannot save setting : %@", error1.debugDescription)
                }
                catch {
                    
                }
            }
            else{
                //already exist. Skip it
            }
        }
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        self.openViewControllerWithUrl(url)
        
//        let options: [String: AnyObject] = [UIApplicationOpenURLOptionsSourceApplicationKey: sourceApplication!,
//            UIApplicationOpenURLOptionsAnnotationKey: annotation]
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation) || GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    //google sign in
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        if #available(iOS 9.0, *) {
            self.openViewControllerWithUrl(url)
            
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation]) || GIDSignIn.sharedInstance().handle(url,
                sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,
                annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        } else {
            // Fallback on earlier versions
            return false
        }
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
            if (error == nil) {
                // Perform any operations on signed in user here.
                self.googleUser = user
                print("login google from appdelegate")
//                if self.pendingGoogleLogin {
                    self.loginViewController?.googleLogin(self.googleUser)
//                }
            } else {
                print("\(error.localizedDescription)")
            }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!, withError error: Error!) {
            // Perform any operations when the user disconnects from app here.
            // ...
    }
    
    // open controller with url
    func openViewControllerWithUrl(_ url: URL) {
        
        var path: [String]? = url.path.components(separatedBy: "/")
        if path != nil && path!.count > 0 {
            let activeViewController: BaseViewController? = ControllerHelper.getActiveViewController()
            if path!.count > 3 && path![1] == "pages" && path![2] == "forgetPassword" && !Globals.isLoggedIn() {
                if let vc = activeViewController {
                    vc.showViewController("ResetPasswordViewController", attributes: [
                        "activationKey" : path![3]
                        ])
                }
                else {
                    self.pendingStoryboardName = "ResetPasswordViewController"
                    self.pendingAttributes = [
                        "activationKey" : path![3]
                    ]
                }
            }
        }
        
    }
    
    func registerForPushNotifications(_ application: UIApplication) {
        let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        //        print("Device Token:", tokenString)
        Globals.setDataSetting("deviceId", value: tokenString)
        
        Globals.registerDeviceId()
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        let state = application.applicationState
        var isActive = true
        if state == UIApplicationState.inactive || state == UIApplicationState.background {
            isActive = false
        }
        self.addMessageFromRemoteNotification(userInfo as NSDictionary, isActive: isActive)
    }
    
    func addMessageFromRemoteNotification(_ userInfo: NSDictionary, isActive: Bool) {
        if let vc = ControllerHelper.getActiveViewController() {
            vc.showHomeViewController()
        }
        
//        if let data = userInfo.object(forKey: "CustomData") as? NSDictionary {
//            
//            if let type = data.object(forKey: "Type") as? String {
//                
//                switch(type){
//                case "HOME" :
//                    if let vc = ControllerHelper.getActiveViewController() {
//                        vc.showHomeViewController()
//                    }
//                    break
//                case "ORDER_DETAIL" :
//                    var orderId = ""
//                    if let id = data.object(forKey: "OrderID") as? NSNumber {
//                        orderId = String(id.intValue)
//                    }
//                    else if let id = data.object(forKey: "OrderID") as? String {
//                        orderId = id
//                    }
//                    let params = [
//                        "BookingId" : orderId
//                    ]
//                    
//                    if let vc = ControllerHelper.getActiveViewController() {
//                        vc.showViewController("MyBookingDetailViewController", attributes: params)
//                    }
//                    else{
//                        self.pendingAttributes = params as NSDictionary?
//                        self.pendingStoryboardName = "MyBookingDetailViewController"
//                    }
//                    break
//                case "LINK" :
//                    var link = ""
//                    if let l = data.object(forKey: "OrderID") as? String {
//                        link = l
//                    }
//                    let params = [
//                        "Url" : link,
//                        "Title": "",
//                        "BackConfirmationMessage" : "",
//                        "CloseToHome" : "0"
//                    ]
//                    
//                    if let vc = ControllerHelper.getActiveViewController() {
//                        Globals.showWebViewWithUrl(link, parentView: vc, title: "")
//                    }
//                    else{
//                        self.pendingAttributes = params as NSDictionary?
//                        self.pendingStoryboardName = "WebViewController"
//                    }
//                    break
//                case "HOTEL_DETAIL" :
//                    var hotelId = ""
//                    if let id = data.object(forKey: "HotelID") as? NSNumber {
//                        hotelId = String(id.intValue)
//                    }
//                    else if let id = data.object(forKey: "HotelID") as? String {
//                        hotelId = id
//                    }
//                    let params = [
//                        "hotelId" : hotelId
//                    ]
//                    
//                    if let vc = ControllerHelper.getActiveViewController() {
//                        vc.showViewController("HotelDetailViewController", attributes: params)
//                    }
//                    else{
//                        self.pendingAttributes = params as NSDictionary?
//                        self.pendingStoryboardName = "HotelDetailViewController"
//                    }
//                    break
//                default:
//                    if let vc = ControllerHelper.getActiveViewController() {
//                        vc.showHomeViewController()
//                    }
//                    break
//                }
//                
//            }
//            
//        }
    }
}

