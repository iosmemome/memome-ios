//
//  ProgressBar.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 3/3/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class ProgressBar: UIProgressView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    @IBInspectable var height : CGFloat! {
        didSet{
            if(self.height != nil){
                
            }
        }
    }
    
    #if !TARGET_INTERFACE_BUILDER
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    #endif
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    private func setup() {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 10.0)
    }
}
