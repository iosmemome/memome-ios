//
//  MakePrintViewController.swift
//  Memome
//
//  Created by staff on 1/4/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class MakePrintViewController: BaseViewController, BSSliderDelegate {

    @IBOutlet weak var btnLeftMenu: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var productTableView: UITableView!
    @IBOutlet weak var productSlider: BSSlider!
    
    @IBOutlet weak var productScrollView: UIScrollView!
    @IBOutlet weak var productContentScrollView: UIView!
    
    
    @IBOutlet weak var uploadProgressView: UIView!
    @IBOutlet weak var lblUploadOrderId: UILabel!
    @IBOutlet weak var pgbUpload: UIProgressView!
    @IBOutlet weak var lblUploadPercent: UILabel!
    @IBOutlet weak var btnUploadProgress: UIButton!
    
    var itemsToLoad: Int! = 0
    
    var isProductLast: Bool! = false
    var lastProductIndex: Int! = 0
    var isCategoryLast: Bool! = false
    var lastCategoryIndex: Int! = 0
    
    var sliderDataSource: NSMutableArray = NSMutableArray()
    var productDataSource: NSMutableArray = NSMutableArray()
    var categoryDataSource: NSMutableArray = NSMutableArray()
    
    var uploadingOrderId = 0
    var uploadingOrderNumber = ""
    var isUploading = false
    var uploadingCart: NSMutableArray = NSMutableArray()
    var uploadingPaymentType = ""
    var uploadingGrandTotal: Double = 0
    
    var categoryTableViewAdapter: CategoryTableViewAdapter = CategoryTableViewAdapter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ControllerHelper.setHomeViewController(self)

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        
        self.btnToggleSideNav = self.btnLeftMenu
        self.initRevealViewController()
        self.btnShoppingCart = self.btnCart
        self.initShoppingCartButton()
        
        self.productTableView.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "categoryTableViewCell")
        
        self.categoryTableViewAdapter._parentViewController = self
        self.categoryTableViewAdapter.tableView = self.productTableView
        self.productTableView.dataSource = self.categoryTableViewAdapter
        self.productTableView.delegate = self.categoryTableViewAdapter
        
        if let height = Globals.getConstraint("height", view: self.uploadProgressView) {
            height.constant = 0
        }
        
        if let height = Globals.getConstraint("height", view: self.productSlider) {
            height.constant = 0
        }
        
        self.productSlider.setDelegate(self)
        
        self.isLoadingUnconfirmedOrder = false
        
        self.initRefreshControl(self.productScrollView)
        
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func addedToMyProject() {
//        BSNotification.show("Project has been added successfully", view: self)
        self.showViewController("MyProjectViewController", attributes: nil)
    }
    
    override func addedToMyCart() {
        //        BSNotification.show("Project has been added successfully", view: self)
        self.showViewController("CartViewController", attributes: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func refresh(_ sender: AnyObject) {
        super.refresh(sender)
        self.loadData()
    }
    
    //BSSlider delegate
    func setupSlides(_ slider: BSSlider) {
        slider.setScrollView(self.productScrollView)
        if let slidesContainer = slider.getSlidesContainer() {
            
            let width: CGFloat = slidesContainer.bounds.size.width
            let height: CGFloat = slidesContainer.bounds.size.height
//            NSLog("width \(width) height \(height)")
            var i: CGFloat = 0
            for item in self.sliderDataSource {
                let contents = Bundle.main.loadNibNamed("GallerySlide", owner: self, options: nil)
                let slide: UIView = contents!.last as! UIView
                slide.frame = CGRect(x: i * width, y: 0, width: width, height: height)
                slide.clipsToBounds = true
                
                let imgView: UIImageView = slide.viewWithTag(1) as! UIImageView
                let type = "original"
                imgView.image = UIImage(named: "image-default.png")
                if let images = (item as AnyObject).object(forKey: "images") as? NSDictionary{
                    if let image = images.object(forKey: type) as? String {
                        Globals.setImageFromUrl(image, imageView: imgView, placeholderImage: UIImage(named: "image-default.png"))
                    }
                }
                
                slidesContainer.addSubview(slide)
                
                i += 1
            }
        }
    }
    
    //general functions
    
    func loadData(){
//        self.itemsToLoad = 2
        
        self.loadDataCategories()
        self.loadDataSlider()
    }
    
    func loadDataCategories(){
        if !self.isLoading() && !self.isCategoryLast {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("category/search"),
               requestType: Globals.HTTPRequestType.http_GET,
               params: [
                "clientId" : Globals.getProperty("clientId"),
                "limit" : String(Constant.getSearchLimit()),
                "start" : String(self.lastCategoryIndex)
               ],
               completion: { (result) -> Void in
                self.hideLoading()
                
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    if let category = result.object(forKey: "category") as? NSArray {
                        self.categoryDataSource.removeAllObjects()
                        self.categoryDataSource.addObjects(from: category as [AnyObject])
                    }
                    self.categoryTableViewAdapter.setDataSource(self.categoryDataSource)
                    self.reloadProductTableView()
                }
                else{
                    Globals.showAlertWithTitle("Categories Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
            })
        }
    }
    
    func loadDataProducts(){
        if !self.isLoading() && !self.isProductLast {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("products/search"),
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "limit" : String(Constant.getSearchLimit()),
                    "start" : String(self.lastProductIndex)
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        if let products = result.object(forKey: "products") as? NSArray {
                            self.productDataSource.removeAllObjects()
//                            self.productDataSource.addObjects(from: products as [AnyObject])
                            let serverProduct: NSMutableArray = NSMutableArray()
                            serverProduct.addObjects(from: products as [AnyObject])
                            let filteredBook = serverProduct.filter() {
                                if let type = ($0 as AnyObject).object(forKey: "title") as? String {
                                    return type.lowercased().range(of: "book") != nil
                                } else {
                                    return false
                                }
                            }
                            if filteredBook.count > 0 {
                                for obj in filteredBook {
                                    serverProduct.remove(obj)
                                }
                                let attributes = NSMutableDictionary(dictionary: [
                                    "id" : ((filteredBook[0] as AnyObject).object(forKey: "id") as? NSString ?? "0").intValue,
                                    "title" : "PHOTOBOOKS",
                                    "product" : filteredBook[0],
                                    "productList" : filteredBook
                                    ])
                                self.productDataSource.add(attributes)
                            }
                            
                            let filteredCard = serverProduct.filter() {
                                if let type = ($0 as AnyObject).object(forKey: "title") as? String {
                                    return type.lowercased().range(of: "card") != nil
                                } else {
                                    return false
                                }
                            }
                            if filteredCard.count > 0 {
                                for obj in filteredCard {
                                    serverProduct.remove(obj)
                                }
                                let attributes = NSMutableDictionary(dictionary: [
                                    "id" : ((filteredCard[0] as AnyObject).object(forKey: "id") as? NSString ?? "0").intValue,
                                    "title" : "CARDS",
                                    "product" : filteredCard[0],
                                    "productList" : filteredCard
                                    ])
                                self.productDataSource.add(attributes)
                            }
                            
                            for obj in serverProduct {
                                var title = (obj as AnyObject).object(forKey: "title") as? String ?? ""
                                if "BUNKO" == title {
                                    title = "BOXES"
                                }
                                let attributes = NSMutableDictionary(dictionary: [
                                    "id" : ((obj as AnyObject).object(forKey: "id") as? NSString ?? "0").intValue,
                                    "title" : title,
                                    "product" : obj,
                                    "productList" : [obj]
                                    ])
                                self.productDataSource.add(attributes)
                            }
                        }
                        self.categoryTableViewAdapter.setDataSource(self.productDataSource)
                        self.reloadProductTableView()
                    }
                    else{
                        Globals.showAlertWithTitle("Products Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    func loadDataSlider(){
        Globals.getDataFromUrl(Globals.getApiUrl("products/banner"),
            requestType: Globals.HTTPRequestType.http_GET,
            params: [
                "clientId" : Globals.getProperty("clientId")
            ],
            completion: { (result) -> Void in
                
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    self.sliderDataSource.removeAllObjects()
                    
                    if let banners = result.object(forKey: "banners") as? NSArray {
                        self.sliderDataSource.addObjects(from: banners as [AnyObject])
                    }
                    
                    if self.sliderDataSource.count == 0 {
                        
                    }
                    else{
                        if let height = Globals.getConstraint("height", view: self.productSlider) {
                            height.constant = self.productSlider.frame.size.width * 109.0 / 300.0
//                            NSLog("height : \(height.constant)")
                        }
                        self.productSlider.layoutSubviews()
                        self.productSlider.reloadSlides()
                    }
                    
                }
                else{
                    Globals.showAlertWithTitle("Banners Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
        })
    }
    
    func reloadProductTableView(){
        self.productTableView.reloadData()
        
        if let height = Globals.getConstraint("height", view: self.productTableView) {
            height.constant = self.productTableView.contentSize.height
        }
    }
    
    
    func updateUploadProgress(_ orderId: Int, orderNumber: String, percent: Float){
        self.uploadingOrderId = orderId
        self.uploadingOrderNumber = orderNumber
        self.isUploading = true
        
//        if let _ = ControllerHelper.getActiveViewController() as? MakePrintViewController {
        
            if let height = Globals.getConstraint("height", view: self.uploadProgressView) {
                if height.constant == 0 {
                    
                    UIView.animate(withDuration: 0.3,
                        animations: { () -> Void in
                            height.constant = 56
                        }, completion: { (finished:Bool) -> Void in
                            
                    })
                    
                }
            }
            
            self.lblUploadOrderId.text = "ORDER #\(orderNumber)"
            self.lblUploadPercent.text = String(format: "%.0f%%", arguments: [percent * 100])
            self.lblUploadPercent.textColor = UIColor.black
            self.pgbUpload.progress = percent
//            BSNotification.show("upload progress : \(percent)%", view: self)
//        }
    }
    
    func uploadComplete(_ orderId: Int) {
        self.isUploading = false
        self.uploadingCart.removeAllObjects()
        
//        if let _ = ControllerHelper.getActiveViewController() as? MakePrintViewController {
            self.lblUploadPercent.text = "Completed"
//        }
    }
    
    func uploadFailed(_ orderId: Int) {
        self.isUploading = false
        
        if let _ = ControllerHelper.getActiveViewController() as? MakePrintViewController {
            self.lblUploadPercent.text = "Failed"
            self.lblUploadPercent.textColor = UIColor.red
        }
        
    }
    
    @IBAction func btnUploadProgressTouched(_ sender: AnyObject) {
        if self.isUploading {

            self.showProgressViewController()
        }
        else{
            
            if self.lblUploadPercent.text == "Failed" {
                self.showProgressViewController()

            }
            else{
                if let height = Globals.getConstraint("height", view: self.uploadProgressView) {
                    if height.constant > 0 {
                        
                        UIView.animate(withDuration: 0.3,
                            animations: { () -> Void in
                                height.constant = 0
                            }, completion: { (finished:Bool) -> Void in
                                self.continueToPayment()
                                
                                self.uploadingOrderId = 0
                                self.uploadingOrderNumber = ""
                        })
                        
                    }
                }
            }
            
            
        }
    }
    
    func continueToPayment() {
        if self.uploadingPaymentType == Constant.paymentCreditCard {
            Globals.showWebViewWithUrl(Constant.getPaymentUrl(String(self.uploadingOrderId), grandTotal: String(self.uploadingGrandTotal)), parentView: self, title: "PAYMENT")
        }
        else if self.uploadingPaymentType == Constant.paymentTransfer {
            self.showViewController("PaymentConfirmationViewController", attributes: [
                "orderId" : String(self.uploadingOrderId)
                ])
        }
    }
    
    
    func showProgressViewController() {
        if let _ = ControllerHelper.getProgressViewController() {
            var delay: TimeInterval = 0.6
            
            if !self.keyboardVisible {
                delay = 0
            }
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(MakePrintViewController.showProgressViewControllerAfterDelayed(_:)), userInfo: nil, repeats: false)
        }
        
    }
    
    func showProgressViewControllerAfterDelayed(_ timer: Timer) {
        if let vc = ControllerHelper.getProgressViewController() {
            let navController: UINavigationController = self.revealViewController().frontViewController as! UINavigationController
            navController.pushViewController(vc, animated: true)
            ControllerHelper.setActiveViewController(vc)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
    }
}
