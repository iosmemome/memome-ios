//
//  BaseViewController.swift
//  Kreate
//
//  Created by staff on 5/11/15.
//  Copyright (c) 2015 Trio Digital Agency. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UITextFieldDelegate{

    
    
    var keyboardSize = CGSize(width: 0, height: 0)
    var keyboardVisible = false
    var tapper: UITapGestureRecognizer!
    var defaultViewHeight: CGFloat = 0
    
    let limitLength = 8
    
    weak var defaultScrollView: UIScrollView?
    weak var defaultContentScrollView: UIView?
    weak var activeTextField: UITextField?
    var loadingView: UIView!
    var attributes: NSDictionary!
    weak var _parentViewController: BaseViewController?
    
    let defaultPopoverTransitionDelegate = DefaultPopoverTransitioningDelegate()
    
    var refreshControl: UIRefreshControl!
    
    weak var btnToggleSideNav: UIButton? = nil
    weak var btnShoppingCart: UIButton? = nil
    
    var isLoadingUnconfirmedOrder = false
    var unconfirmedOrder = 0
    
    deinit {
        NSLog("deinit view controller")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        Globals.initialize()
        
        defaultViewHeight = 0
        tapper = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.handleSingleTap(_:)))
        tapper.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapper)
        
        self.setupLoadingView();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let scrollView = self.defaultScrollView {
            self.defaultViewHeight = scrollView.frame.size.height
        }
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.keyboardDidChangeFrame(_:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
        
        ControllerHelper.setActiveViewController(self)
        
        self.setBtnToggleSideNavTarget()
        
        self.updateShoppingCartButton()
        
        //check if app delegate has pending storyboard and attributes
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if appDelegate.pendingStoryboardName != nil {
                self.showViewController(appDelegate.pendingStoryboardName!, attributes: appDelegate.pendingAttributes)
                
                appDelegate.pendingStoryboardName = nil
                appDelegate.pendingAttributes = nil
            }
        }
        
        
    }
    
    func initShoppingCartButton(){
        if let btn = self.btnShoppingCart {
            btn.addTarget(self, action: #selector(BaseViewController.btnShoppingCartTouched(_:)), for: UIControlEvents.touchUpInside)
        }
    }
    
    func initRevealViewController(){
        if self.revealViewController() != nil {
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().shouldUseFrontViewOverlay = true
            
            self.setBtnToggleSideNavTarget()
        }
    }
    
    func setBtnToggleSideNavTarget() {
        if self.revealViewController() != nil {
            
            if let btn = self.btnToggleSideNav {
                if Globals.isLoggedIn() {
                    btn.removeTarget(self, action: #selector(BaseViewController.btnToggleSideNavTouched(_:)), for: UIControlEvents.touchUpInside)
                    btn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
                }
                else{
                    btn.removeTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
                    btn.addTarget(self, action: #selector(BaseViewController.btnToggleSideNavTouched(_:)), for: UIControlEvents.touchUpInside)
                }
                
                self.checkUnconfirmedOrder()
            }
        }
    }
    
    func btnToggleSideNavTouched(_ sender: AnyObject) {
        Globals.login(self) { () -> Void in
            self.setBtnToggleSideNavTarget()
        }
    }
    
    func initRefreshControl(_ scrollView: UIScrollView){
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: Globals.localized("Pull to refresh"))
        self.refreshControl.addTarget(self, action: #selector(BaseViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        scrollView.addSubview(self.refreshControl)
    }
    
    func refresh(_ sender: AnyObject) {
        self.refreshControl.endRefreshing()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.activeTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func handleSingleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        let maxAllowedCharactersPerLine = 30
//        let lines = (textField.text! as NSString).replacingCharacters(in: range, with: string).components(separatedBy: .newlines)
//        for line in lines{
//            if line.count > maxAllowedCharactersPerLine{
//                return false
//            }
//        }
//        return true
//        
//    }
    
    
    func keyboardDidChangeFrame(_ notification: Notification) {
        if (self.defaultScrollView != nil && self.activeTextField != nil) {
            let constraint : NSLayoutConstraint! = Globals.getFirstConstraint(NSLayoutAttribute.bottom, view: self.defaultScrollView!)
            
            let keyboardEndFrame: CGRect = ((notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let screenRect: CGRect = UIScreen.main.bounds
            var viewFrame: CGRect = self.defaultScrollView!.frame
            
            var isChange = false
            var isKeyboardVisible = false
            if (keyboardEndFrame.intersects(screenRect)){
                if !keyboardVisible {
                    isChange = true
                }
                //keyboard is visible
                keyboardSize = keyboardEndFrame.size
                viewFrame.size.height = defaultViewHeight - keyboardSize.height
                //keyboard is now visible
                isKeyboardVisible = true
                var posY: CGFloat!
                if self.defaultContentScrollView != nil {
//                    NSLog("active text field bounds origin : %f, %f", activeTextField.bounds.origin.x, activeTextField.bounds.origin.y)
                    posY = self.activeTextField!.convert(self.activeTextField!.bounds.origin, to: self.defaultContentScrollView).y
                    print("keyboard handler \(posY)")
                }
                else{
                    posY = self.activeTextField!.bounds.origin.y - 50
                }
                if posY < 0{
                    posY = 0
                }
                self.defaultScrollView!.scrollRectToVisible(CGRect(x: 0, y: posY, width: self.defaultScrollView!.frame.width, height: viewFrame.size.height+150), animated: true)
            }
            else {
                if keyboardVisible {
                    isChange = true
                }
                //keyboard is hidden
                viewFrame.size.height = defaultViewHeight
                isKeyboardVisible = false
            }
            if isChange {
                UIView.animate(withDuration: 0.3,
                    animations: { () -> Void in
                        if constraint == nil {
                            self.defaultScrollView!.frame = viewFrame
                        }
                        else{
                            constraint.constant = self.defaultViewHeight - viewFrame.height
                        }
                    }, completion: { (finished:Bool) -> Void in
                        self.keyboardVisible = isKeyboardVisible
                })
//                UIView.beginAnimations(nil, context: nil)
//                UIView.setAnimationBeginsFromCurrentState(true)
//                UIView.setAnimationDuration(0.3)
//                UIView.setAnimationDelay(0)
//                if constraint == nil {
//                    defaultScrollView.frame = viewFrame
//                }
//                else{
//                    constraint.constant = defaultViewHeight - viewFrame.height
//                }
//                UIView.commitAnimations()
            }
        }
    }

    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    func setupLoadingView() {
        let width:CGFloat = 60.0
        let height: CGFloat = 60.0
        
        loadingView = UIView()
//        loadingView.autoresizingMask = UIViewAutoresizing.None
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
//        loadingView.frame = CGRectMake((self.view.frame.size.width - width) / 2, (self.view.frame.size.height - height) / 2, width, height)
        
        loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        loadingView.layer.cornerRadius = 5.0
        self.view.addSubview(loadingView)
        
        self.view.addConstraint(NSLayoutConstraint(
            item: loadingView,
            attribute: NSLayoutAttribute.width,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1,
            constant: width
        ))
        
        self.view.addConstraint(NSLayoutConstraint(
            item: loadingView,
            attribute: NSLayoutAttribute.height,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1,
            constant: height
            ))
        
        self.view.addConstraint(NSLayoutConstraint(
            item: loadingView,
            attribute: NSLayoutAttribute.centerX,
            relatedBy: NSLayoutRelation.equal,
            toItem: self.view,
            attribute: NSLayoutAttribute.centerX,
            multiplier: 1,
            constant: 0
            ))
        
        self.view.addConstraint(NSLayoutConstraint(
            item: loadingView,
            attribute: NSLayoutAttribute.centerY,
            relatedBy: NSLayoutRelation.equal,
            toItem: self.view,
            attribute: NSLayoutAttribute.centerY,
            multiplier: 1,
            constant: 0
            ))
        
        let loadingProcess: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        loadingProcess.startAnimating()
        loadingView.addSubview(loadingProcess)
        
        loadingView.isHidden = true
    }
    
    func showLoading() {
        loadingView.isHidden = false;
        self.view.bringSubview(toFront: loadingView)
    }
    
    func hideLoading() {
        loadingView.isHidden = true
        self.view.sendSubview(toBack: loadingView)
    }
    
    func isLoading() -> Bool {
        return loadingView.isHidden ? false : true
    }
    
    func showViewController(_ storyboardId: String, attributes: NSDictionary?) {
        var delay: TimeInterval = 0.6
        
        if !self.keyboardVisible {
            delay = 0
        }
        
        if let unWrappedAttributes = attributes {
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(BaseViewController.showViewControllerAfterDelayed(_:)), userInfo: ["storyboardId": storyboardId, "attributes": unWrappedAttributes], repeats: false)
        }
        else {
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(BaseViewController.showViewControllerAfterDelayed(_:)), userInfo: ["storyboardId": storyboardId, "attributes": []], repeats: false)
        }
        

    }
    
    func showViewController(_ viewController: UIViewController, parent: UIViewController) {
        var delay: TimeInterval = 0.6
        
        if !self.keyboardVisible {
            delay = 0
        }
        
        if let unWrappedAttributes = attributes {
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(BaseViewController.showViewControllerAfterDelayed(_:)), userInfo: ["viewController": viewController, "parent": parent, "attributes": unWrappedAttributes], repeats: false)
        }
        else {
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(BaseViewController.showViewControllerAfterDelayed(_:)), userInfo: ["viewController": viewController, "parent": parent, "attributes": []], repeats: false)
        }
        
        
    }
    
    func showViewControllerAfterDelayed(_ timer: Timer) {
        let args: NSDictionary = timer.userInfo as! NSDictionary
        let parent = args.object(forKey: "parent") as? BaseViewController ?? self

        if let storyboardId = args.object(forKey: "storyboardId") as? String{
            let storyboardName = "Main"
            let storyboard: UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
            let vc: BaseViewController = storyboard.instantiateViewController(withIdentifier: args.object(forKey: "storyboardId") as! String) as! BaseViewController
            if let attributes: NSDictionary = args.object(forKey: "attributes") as? NSDictionary {
                vc.attributes = attributes
            }
            vc._parentViewController = parent
    //        self.presentViewController(vc, animated: true, completion: nil)
            let navController: UINavigationController = parent.revealViewController().frontViewController as! UINavigationController
            navController.interactivePopGestureRecognizer?.isEnabled = false
            navController.pushViewController(vc, animated: true)
    //        navController.setViewControllers([vc], animated: true)
            
            
            ControllerHelper.setActiveViewController(vc)
            parent.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
        else if let vc = args.object(forKey: "viewController") as? BaseViewController {
        
            if let attributes: NSDictionary = args.object(forKey: "attributes") as? NSDictionary {
                vc.attributes = attributes
            }
            vc._parentViewController = parent
            //        self.presentViewController(vc, animated: true, completion: nil)
            let navController: UINavigationController = parent.revealViewController().frontViewController as! UINavigationController
            navController.interactivePopGestureRecognizer?.isEnabled = false
            navController.pushViewController(vc, animated: true)
            //        navController.setViewControllers([vc], animated: true)
            ControllerHelper.setActiveViewController(vc)
            parent.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
    }
    
    func showModalViewController(_ storyboardId: String, attributes: NSDictionary?){
        var delay: TimeInterval = 0.6
        
        if !self.keyboardVisible {
            delay = 0
        }
        
        if let unWrappedAttributes = attributes {
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(BaseViewController.showModalViewControllerAfterDelayed(_:)), userInfo: ["storyboardId": storyboardId, "attributes": unWrappedAttributes], repeats: false)
        }
        else {
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(BaseViewController.showModalViewControllerAfterDelayed(_:)), userInfo: ["storyboardId": storyboardId, "attributes": []], repeats: false)
        }
    }
    
    func showModalViewController(_ viewController: UIViewController){
        var delay: TimeInterval = 0.6
        
        if !self.keyboardVisible {
            delay = 0
        }
        
        if let unWrappedAttributes = attributes {
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(BaseViewController.showModalViewControllerAfterDelayed(_:)), userInfo: ["viewController": viewController, "attributes": unWrappedAttributes], repeats: false)
        }
        else {
            _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(BaseViewController.showModalViewControllerAfterDelayed(_:)), userInfo: ["viewController": viewController, "attributes": []], repeats: false)
        }
    }
    
    func showModalViewControllerAfterDelayed(_ timer: Timer){
        let args: NSDictionary = timer.userInfo as! NSDictionary
        if let storyboardId = args.object(forKey: "storyboardId") as? String{
            let popoverContent:BaseViewController = self.storyboard?.instantiateViewController(withIdentifier: storyboardId) as! BaseViewController
            
            
            if let attributes: NSDictionary = args.object(forKey: "attributes") as? NSDictionary {
                popoverContent.attributes = attributes
            }
            popoverContent._parentViewController = self
            
            self.addChildViewController(popoverContent)
            popoverContent.view.frame = self.view.bounds
            self.view.addSubview(popoverContent.view)
            popoverContent.view.alpha = 0
            popoverContent.didMove(toParentViewController: self)
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
                
                popoverContent.view.alpha = 1
                
                }){ (Bool) -> Void in
                    self.modalViewControllerDidAppear(popoverContent, storyboardId: args.object(forKey: "storyboardId") as! String)
            }
        }
        else if let popoverContent = args.object(forKey: "viewController") as? UIViewController {
            
            self.addChildViewController(popoverContent)
            popoverContent.view.frame = self.view.bounds
            self.view.addSubview(popoverContent.view)
            popoverContent.view.alpha = 0
            popoverContent.didMove(toParentViewController: self)
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
                
                popoverContent.view.alpha = 1
                
                }){ (Bool) -> Void in
//                    self.modalViewControllerDidAppear(popoverContent, storyboardId: args.objectForKey("storyboardId") as! String)
            }
        }
        
    }
    
    func modalViewControllerDidAppear(_ modalViewController: UIViewController, storyboardId: String) {
        
    }
    
    
    func showWebViewController(_ url: String, title: String) {
        self.showWebViewController(url, title: title, requestCode: 1)
    }
    
    func showWebViewController(_ url: String, title: String, requestCode: Int) {
        var delay: TimeInterval = 0.6
        
        if !self.keyboardVisible {
            delay = 0
        }
        
        let attributes = ["url" : url, "title": title, "requestCode" : requestCode] as [String : Any]
        _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(BaseViewController.showWebViewControllerAfterDelayed(_:)), userInfo: ["attributes": attributes], repeats: false)
        
    }
    
    func showWebViewControllerAfterDelayed(_ timer: Timer) {
        let args: NSDictionary = timer.userInfo as! NSDictionary
        let storyboardName = "Main"
        let storyboard: UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc: WebViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        if let attributes: NSDictionary = args.object(forKey: "attributes") as? NSDictionary {
            vc.attributes = attributes
        }
        vc._parentViewController = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func webViewDidLoad(_ webViewController: WebViewController, webView: UIWebView, requestCode: Int){
        
    }
    
    
    func dismissSelf() {
        self.dismissSelf(nil)
    }
    
    func dismissSelf(_ completion: (() -> Void)? ) {
        self.dismissSelf(true, completion: completion)
    }
    
    func dismissSelf(_ animated: Bool, completion: (() -> Void)? ) {
        ControllerHelper.setActiveViewController(self._parentViewController)
        
        let navController: UINavigationController = self.revealViewController().frontViewController as! UINavigationController
//        self.dismissViewControllerAnimated(animated, completion: nil)
        navController.popViewController(animated: animated)
//        navController.setViewControllers([self._parentViewController], animated: animated)
        if(completion != nil) {
            completion!()
        }
    }
    
    func dismissSelfModal() {
        self.dismissSelfModal(nil)
    }
    
    func dismissSelfModal(_ completion: (() -> Void)? ) {
        if let parent = self._parentViewController {
            parent.viewDidAppear(true)
        }
        
        if(completion != nil) {
            completion!()
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in

            self.view.alpha = 0
            
            }, completion: { (Bool) -> Void in
                if let parent = self._parentViewController {
                    parent.modalViewControllerDidRemoved(self)
                }
                self.removeFromParentViewController()
        })
    }
    
    func modalViewControllerDidRemoved(_ modalViewController: UIViewController) {
        
    }
    
    func showHomeViewController() {
        let navController: UINavigationController = self.revealViewController().frontViewController as! UINavigationController
        var i = navController.viewControllers.count - 1
        while i > 0 {
            navController.popViewController(animated: false)
            i -= 1
        }
//        self.showViewController("MakePrintViewController", attributes: nil)
    }
    
    func updateShoppingCartButton() {
        if let btn = self.btnShoppingCart {
            btn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            
            //check cart
            self.setCartIcons(btn)
        }
    }
    
    func addedToMyProject() {
        self.dismissSelf(false){ () -> Void in
            ControllerHelper.getActiveViewController()?.addedToMyProject()
        }
    }
    
    func addedToMyCart() {
        self.dismissSelf(false){ () -> Void in
            ControllerHelper.getActiveViewController()?.addedToMyCart()
        }
    }
    
    func btnShoppingCartTouched(_ sender: AnyObject) {
        self.showViewController("CartViewController", attributes: nil)
    }
    
    
    //cart icons
    func setCartIcons(_ btnCart:UIButton) {
        let carts = CartHelper.getCart()
        
        var badge: UILabel
        if let view = btnCart.viewWithTag(99) {
            badge = view as! UILabel
        }
        else{
            badge = UILabel(frame: CGRect(x: btnCart.frame.size.width - 15, y: 0, width: 15, height: 15))
            badge.tag = 99
            badge.layer.cornerRadius = 7.5
            badge.backgroundColor = UIColor.colorPrimary
            badge.textColor = UIColor(white: 1, alpha: 1)
            badge.textAlignment = NSTextAlignment.center
            badge.clipsToBounds = true
            badge.font = UIFont(name: badge.font.fontName, size: 8)
            btnCart.addSubview(badge)
        }
        
        badge.text = String(format: "%i", arguments: [carts.count])
        
        if carts.count > 0 {
            badge.isHidden = false
        }
        else{
            badge.isHidden = true
        }
    }
    
    
    // side menu
    func checkUnconfirmedOrder() {
        if !self.isLoadingUnconfirmedOrder {
            self.isLoadingUnconfirmedOrder = true
            
            Globals.getDataFromUrl(Globals.getApiUrl("orders/getOrderNotYetPaid"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String
                ],
                completion: { (result) -> Void in
                    self.isLoadingUnconfirmedOrder = false
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let orders = result.object(forKey: "orders") as? NSArray {
                            self.unconfirmedOrder = orders.count
                        }
                        else{
                            self.unconfirmedOrder = 0
                        }
                        
                    }
                    else{
                        self.unconfirmedOrder = 0
                    }
                    
                    self.setupSideButtonNotifCount(self.unconfirmedOrder)
            })
        }
    }
    
    func setupSideButtonNotifCount(_ count: Int) {
        
        if let btnSideNav = self.btnToggleSideNav {
            var badge: UILabel
            if let view = btnSideNav.viewWithTag(99) {
                badge = view as! UILabel
            }
            else{
                badge = UILabel(frame: CGRect(x: btnSideNav.frame.size.width - 15, y: 0, width: 15, height: 15))
                badge.tag = 99
                badge.layer.cornerRadius = 7.5
                badge.backgroundColor = UIColor.colorPrimary
                badge.textColor = UIColor(white: 1, alpha: 1)
                badge.textAlignment = NSTextAlignment.center
                badge.clipsToBounds = true
                badge.font = UIFont(name: badge.font.fontName, size: 8)
                btnSideNav.addSubview(badge)
            }
            
            badge.text = String(format: "%i", arguments: [count])
            
            if count > 0 {
                badge.isHidden = false
            }
            else{
                badge.isHidden = true
            }
        }
        
    }
}
