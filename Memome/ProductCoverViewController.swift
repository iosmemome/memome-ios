//
//  ProductCoverViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/13/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class ProductCoverViewController: BaseViewController{
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var coverScrollView: UIScrollView!
    @IBOutlet weak var coverContentScrollView: UIView!

    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var coverViewBackground: UIImageView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var btnCoverImage: UIButton!
    @IBOutlet weak var coverTitleView: UIView!
    @IBOutlet weak var coverTitleInnerView: UIView!
    @IBOutlet weak var lblCoverTitle: UILabel!
    @IBOutlet weak var lblCoverSubtitle: UILabel!
    
    @IBOutlet weak var coverTitle: UILabel!
    @IBOutlet weak var coverDescription: UILabel!
    
    @IBOutlet weak var txtTitleWrapper: UIView!
    @IBOutlet weak var txtSubtitleWrapper: UIView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtSubtitle: UITextField!
    
    @IBOutlet weak var btnChangeCoverImage: UIButton!
    @IBOutlet weak var btnDirectToLayoutInstagram: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    var productData: NSDictionary!
    var productDetails: NSArray!
    var productDetailsDataSource: NSMutableArray! = NSMutableArray()
    var ratio: CGFloat = 1
    var isTitleRequired: Bool = true
    var isCoverImageRequired: Bool = true
    
    var corporateVoucherId: Int = 0
    var corporateVoucherName: String = ""
    var corporateVoucherQty: Int = 0
    var corporateVoucherCode: String = ""
    var corporateVoucher: NSDictionary = NSDictionary()
    var categoryId: String = ""
    
    var project: Project?
    var projectDetails: NSArray?
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var collageView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        lblCoverTitle.text = lblCoverTitle.text?.uppercased()
        lblCoverSubtitle.text = lblCoverSubtitle.text?.uppercased()
       
//        titleView.isHidden = true
//        collageView.isHidden = true
//        titleLabel.isHidden = true
//        coverView.isHidden = true
        
        // Do any additional setup after loading the view.
        
        txtTitle.delegate = self
        
        self.defaultScrollView = self.coverScrollView
        self.defaultContentScrollView = self.coverContentScrollView
        
        if #available(iOS 11.0, *) {
            coverScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        } else {
            // Fallback on earlier versions
        }
    
        
        if let attributes = self.attributes {
            self.productData = attributes.object(forKey: "product") as! NSDictionary
            self.productDetails = attributes.object(forKey: "productDetails") as? NSArray
            
            if let voucher = attributes.object(forKey: "voucher") as? NSDictionary {
                if let id = voucher.object(forKey: "id") as? NSString {
                    self.corporateVoucherId = id.integerValue
                }
                else if let id = voucher.object(forKey: "id") as? NSNumber {
                    self.corporateVoucherId = id.intValue
                }
                
                if let qty = voucher.object(forKey: "qty") as? NSString {
                    self.corporateVoucherQty = qty.integerValue
                }
                else if let qty = voucher.object(forKey: "qty") as? NSNumber {
                    self.corporateVoucherQty = qty.intValue
                }
                
                if let name = voucher.object(forKey: "name") as? String {
                    self.corporateVoucherName = name
                }
                
                self.corporateVoucher = voucher
            }
            
            if let code = attributes.object(forKey: "voucherCode") as? String {
                self.corporateVoucherCode = code
            }
            
            if let category = productData.object(forKey: "id") as? String{
                self.categoryId = category
            }
            
            
            if let proj = attributes.object(forKey: "project") as? Project{
                self.project = proj
            }
            if let details = attributes.object(forKey: "projectDetails") as? NSArray {
                self.projectDetails = details
            }
            if let title = attributes.object(forKey: "title") as? String {
                self.txtTitle.text = title
                self.lblCoverTitle.text = title
            }
            if let subtitle = attributes.object(forKey: "subtitle") as? String {
                self.txtSubtitle.text = subtitle
                self.lblCoverSubtitle.text = subtitle
            }
        }
        
//        self.coverView.layer.borderColor = UIColor(white: 0.25, alpha: 1).cgColor
//        self.coverView.layer.borderWidth = 1
        
//        self.coverTitleInnerView.layer.borderColor = UIColor(white: 0.57, alpha: 1).cgColor
//        self.coverTitleInnerView.layer.borderWidth = 1
        
        self.btnChangeCoverImage.layer.borderColor = UIColor.darkGray.cgColor//UIColor(red: 80.0/255.0, green: 210.0/255.0, blue: 194.0/255.0, alpha: 1).cgColor
        self.btnChangeCoverImage.layer.borderWidth = 1.0
        self.btnChangeCoverImage.layer.cornerRadius = 5//self.btnChangeCoverImage.frame.size.height / 2.0
        
        if let isRequireTitle = self.productData.object(forKey: "is_require_title") as? NSNumber {
            self.isTitleRequired = isRequireTitle.intValue == 1
            if !isTitleRequired{
                lblCoverTitle.isHidden = true
                lblCoverSubtitle.isHidden = true
            }
        }
        else if let isRequireTitle = self.productData.object(forKey: "is_require_title") as? NSString {
            self.isTitleRequired = isRequireTitle.integerValue == 1
            if !isTitleRequired{
                lblCoverTitle.isHidden = true
                lblCoverSubtitle.isHidden = true
            }
        }
        
        self.coverView.isHidden = true
        
        if self.categoryId == "27" {
            coverTitle.text = "DESIGN YOUR CASE HERE"
            coverDescription.text = "Create your own case here"
            collageView.isHidden = true
        }
        if self.categoryId == "20" || self.categoryId == "22" || self.categoryId == "24"{
            if let title = productData.object(forKey: "title") as? String{
                titleLabel.text = title
            }
            coverDescription.isHidden = true
            coverTitle.text = "DESIGN YOUR PRODUCT HERE"
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let coverWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_width") as! NSString).floatValue)
        self.ratio = MemomeHelper.calculateRatio(coverWidth, targetSize: self.coverView.frame.size.width)
        
        self.setupCoverView()
        
        if let coverImage = MemomeHelper.coverImage {
            if let url = coverImage.object(forKey: "url") as? String {
                var x: CGFloat = 0.0
                var y: CGFloat = 0.0
                var width: CGFloat = 0.0
                var height: CGFloat = 0.0
                var rotation: Int = 0
                var scale: CGFloat = 1
                if let zoomScale = coverImage.object(forKey: "zoomScale") as? NSNumber {
                    scale = CGFloat(zoomScale.floatValue)
                }
                if let size = coverImage.object(forKey: "size") as? NSValue {
                    width = size.cgSizeValue.width * scale
                    height = size.cgSizeValue.height * scale
                }
                if let offset = coverImage.object(forKey: "offset") as? NSValue {
                    x = offset.cgPointValue.x * scale
                    y = offset.cgPointValue.y * scale
                }
                if let rotate = coverImage.object(forKey: "rotation") as? NSNumber {
                    rotation = rotate.intValue
                }
                let screenScale = max( self.coverImageView.frame.size.width / width, self.coverImageView.frame.size.height / height )
                let cropRect = CGRect(x: x * screenScale, y: y * screenScale, width: width * screenScale, height: height * screenScale)
                
                Globals.checkImageFrom(url, scale: scale * screenScale, resultHandler: { (image) -> Void in
                    if let img = image {
                        self.coverImageView.image = BSPhotoCrop.cropImage(BSPhotoCrop.rotateImageBy90Degree(img, degree: rotation), rect: cropRect)
                    }
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtTitle {
            self.txtSubtitle.becomeFirstResponder()
        }
        else if textField == self.txtSubtitle {
            self.btnNextTouched(self.btnNext)
        }
        
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func txtTitleEditingChanged(_ sender: AnyObject) {
        self.lblCoverTitle.text = self.txtTitle.text
    }

    @IBAction func txtSubtitleEditingChanged(_ sender: AnyObject) {
        self.lblCoverSubtitle.text = self.txtSubtitle.text
    }
    
    // Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        MemomeHelper.coverImage = nil
        self.dismissSelf()
    }
    
    @IBAction func btnCoverImageTouched(_ sender: AnyObject) {
        let attributes: NSMutableDictionary = NSMutableDictionary()
        let coverContentWidth: Float = (self.productData.object(forKey: "cover_content_width") as! NSString).floatValue
        let coverContentHeight: Float = (self.productData.object(forKey: "cover_content_height") as! NSString).floatValue
        attributes.setValue(NSNumber(value: coverContentWidth/coverContentHeight as Float), forKey: "imageRatio")
        if let coverImage = MemomeHelper.coverImage {
            attributes.setValue(coverImage, forKey: "initialImage")
        }
        self.showViewController("PhotoEditorViewController", attributes: attributes)
    }
    
    @IBAction func btnDirectToLayoutInstagramTouched(_ sender: AnyObject) {
        guard let url = URL(string: "https://itunes.apple.com/id/app/layout-from-instagram/id967351793") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btnChangeCoverImageTouched(_ sender: AnyObject) {
        self.btnCoverImageTouched(sender)
    }
    
    @IBAction func btnNextTouched(_ sender: AnyObject) {
        
        if self.isCoverImageRequired && MemomeHelper.coverImage == nil{
            if self.categoryId == "27"{
                Globals.showAlertWithTitle("Image is Required", message: "Please choose an image for your case", viewController: self)
            }
            else{
            Globals.showAlertWithTitle("Image is Required", message: "Please choose an image for your cover", viewController: self)
            }
        }
        else if self.isTitleRequired && self.txtTitle.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
            Globals.showAlertWithTitle("Title is Required", message: "Please set a title", viewController: self)
        }
//        else if self.isTitleRequired && self.txtSubtitle.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
//            Globals.showAlertWithTitle("Subtitle is Required", message: "Please set a subtitle", viewController: self)
//        }
        else{
            if MemomeHelper.coverImage == nil {
                MemomeHelper.coverImage = NSMutableDictionary()
            }
            MemomeHelper.coverImage.setValue(self.txtTitle.text, forKey: "title")
            MemomeHelper.coverImage.setValue(self.txtSubtitle.text, forKey: "subtitle")
            
            
//            if self.productDetails == nil {
                self.loadProductData()
//            }
//            else {
//                if let _ = self.project {
//                    self.goToPhotoPreview()
//                }
//                else{
//                    let attributes = NSMutableDictionary(dictionary: [
//                        "product" : self.productData,
//                        "productDetails" : self.productDetails,
//                        "numberOfPhotos" : NSNumber(value: self.productDetails.count as Int)
//                        ])
//                    if let proj = self.project {
//                        attributes.setValue(proj, forKey: "project")
//                    }
//                    if let details = self.projectDetails {
//                        attributes.setValue(details, forKey: "projectDetails")
//                    }
//                    if self.corporateVoucherId > 0 {
//                        attributes.setValue(self.corporateVoucherCode, forKey: "voucherCode")
//                        attributes.setValue(self.corporateVoucher, forKey: "voucher")
//                    }
//                    self.showViewController("MainPhotoSelectorViewController", attributes: attributes)
//                }
//            }
            
        }
    }
    
    // General Method
    func setupCoverView() {
//        let coverWidth: CGFloat = CGFloat((self.productData.objectForKey("cover_width") as! NSString).floatValue)
        let coverHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_height") as! NSString).floatValue)
        let coverBorderTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_border_top") as! NSString).floatValue)
        let coverBorderLeft: CGFloat = CGFloat((self.productData.object(forKey: "cover_border_left") as! NSString).floatValue)
        let coverContentWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_content_width") as! NSString).floatValue)
        let coverContentHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_content_height") as! NSString).floatValue)
        let coverTitleAreaWidth: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_width") as! NSString).floatValue)
        let coverTitleAreaHeight: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_height") as! NSString).floatValue)
        let coverTitleAreaTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_top") as! NSString).floatValue)
        let coverTitleAreaLeft: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_area_left") as! NSString).floatValue)
        let coverTitleTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_top") as! NSString).floatValue)
        let coverSubtitleTop: CGFloat = CGFloat((self.productData.object(forKey: "cover_subtitle_top") as! NSString).floatValue)
        let coverTitleFontSize: CGFloat = CGFloat((self.productData.object(forKey: "cover_title_font_size") as! NSString).floatValue)
        let coverSubtitleFontSize: CGFloat = CGFloat((self.productData.object(forKey: "cover_subtitle_font_size") as! NSString).floatValue)
        
        
        let coverTitleAreaBorder: CGFloat = MemomeHelper.coverTitleAreaBorder
        
        if let coverImages = self.productData.object(forKey: "cover_images") as? NSDictionary {
            if let image = coverImages.object(forKey: "thumb") as? String {
                Globals.setImageFromUrl(image, imageView: self.coverViewBackground, placeholderImage: nil)
            }
        }
        
        if coverTitleAreaWidth == 0 || coverTitleAreaHeight == 0 {
            
            if let height = Globals.getConstraint("height", view: self.txtTitleWrapper) {
                height.constant = 0
            }
            if let height = Globals.getConstraint("height", view: self.txtSubtitleWrapper) {
                height.constant = 0
            }
            
//            self.isTitleRequired = false
        }
        
        if coverContentWidth == 0 || coverContentHeight == 0 {
            
            if let height = Globals.getConstraint("height", view: self.btnChangeCoverImage) {
                height.constant = 0
            }
            
            self.btnChangeCoverImage.isHidden = true
            
            self.isCoverImageRequired = false
        }
        else{
            self.btnChangeCoverImage.isHidden = false
        }
        
        //cover size
        if let height = Globals.getConstraint("height", view: self.coverView) {
            height.constant = coverHeight * self.ratio
        }
        
        //cover content
        if let top = Globals.getConstraint("coverImageTop", view: self.coverView) {
            top.constant = coverBorderTop * self.ratio
        }
        if let left = Globals.getConstraint("coverImageLeft", view: self.coverView) {
            left.constant = coverBorderLeft * self.ratio
        }
        if let width = Globals.getConstraint("width", view: self.coverImageView) {
            width.constant = coverContentWidth * self.ratio
        }
        if let height = Globals.getConstraint("height", view: self.coverImageView) {
            height.constant = coverContentHeight * self.ratio
        }
        
        if let top = Globals.getConstraint("btnCoverImageTop", view: self.coverView) {
            top.constant = coverBorderTop * self.ratio
        }
        if let left = Globals.getConstraint("btnCoverImageLeft", view: self.coverView) {
            left.constant = coverBorderLeft * self.ratio
        }
        if let width = Globals.getConstraint("width", view: self.btnCoverImage) {
            width.constant = coverContentWidth * self.ratio
        }
        if let height = Globals.getConstraint("height", view: self.btnCoverImage) {
            height.constant = coverContentHeight * self.ratio
        }
        
        
        //title area
        if let top = Globals.getConstraint("titleViewTop", view: self.coverView) {
            top.constant = coverTitleAreaTop * self.ratio
        }
        if let left = Globals.getConstraint("titleViewLeft", view: self.coverView) {
            left.constant = coverTitleAreaLeft * self.ratio
        }
        if let width = Globals.getConstraint("width", view: self.coverTitleView) {
            width.constant = coverTitleAreaWidth * self.ratio
        }
        if let height = Globals.getConstraint("height", view: self.coverTitleView) {
            height.constant = coverTitleAreaHeight * self.ratio
        }
        
        if let top = Globals.getConstraint("titleInnerTop", view: self.coverTitleView) {
            top.constant = coverTitleAreaBorder * self.ratio
        }
        if let left = Globals.getConstraint("titleInnerLeft", view: self.coverTitleView) {
            left.constant = coverTitleAreaBorder * self.ratio
        }
        if let bottom = Globals.getConstraint("titleInnerBottom", view: self.coverTitleView) {
            bottom.constant = coverTitleAreaBorder * self.ratio
        }
        if let right = Globals.getConstraint("titleInnerRight", view: self.coverTitleView) {
            right.constant = coverTitleAreaBorder * self.ratio
        }
        
        //title & subtitle
        if let top = Globals.getConstraint("titleTop", view: self.coverTitleInnerView) {
            top.constant = (coverTitleTop - coverTitleAreaBorder) * self.ratio
        }
        if let top = Globals.getConstraint("subtitleTop", view: self.coverTitleInnerView) {
            top.constant = (coverSubtitleTop - coverTitleAreaBorder) * self.ratio
        }
        if let top = Globals.getConstraint("txtTitleTop", view: self.coverTitleInnerView) {
            top.constant = (coverTitleTop - coverTitleAreaBorder) * self.ratio
        }
        if let top = Globals.getConstraint("txtSubtitleTop", view: self.coverTitleInnerView) {
            top.constant = (coverSubtitleTop - coverTitleAreaBorder) * self.ratio
        }
        
        let coverTitleFont = self.txtTitle.font
        self.txtTitle.font = coverTitleFont?.withSize( 50 )
        let coverSubtitleFont = self.txtSubtitle.font
        self.txtSubtitle.font = coverSubtitleFont?.withSize( 9 )
        
        let cvTitleFont = self.lblCoverTitle.font
        self.lblCoverTitle.font = cvTitleFont?.withSize( 40 )
        let cvSubtitleFont = self.lblCoverSubtitle.font
        self.lblCoverSubtitle.font = cvSubtitleFont?.withSize( 15 )
        
        
        self.coverView.isHidden = false
    }
    
    func setPhotos(_ photos: NSArray){
        if MemomeHelper.images == nil {
            MemomeHelper.images = NSMutableArray()
        }
        MemomeHelper.images.removeAllObjects()
        MemomeHelper.images.addObjects(from: photos as [AnyObject])
        
        self.goToPhotoPreview()
    }
    
    func goToPhotoPreview(){
        let attributes = NSMutableDictionary(dictionary: [
            "product" : self.productData,
            "productDetails" : self.productDetails
            ])
        if let proj = self.project {
            attributes.setValue(proj, forKey: "project")
        }
        if let details = self.projectDetails {
            attributes.setValue(details, forKey: "projectDetails")
        }
        if self.corporateVoucherId > 0 {
            attributes.setValue(self.corporateVoucherCode, forKey: "voucherCode")
            attributes.setValue(self.corporateVoucher, forKey: "voucher")
        }
        self.showViewController("ProductPhotosViewController", attributes: attributes)
    }
    
    func loadProductData(){
        if !self.isLoading() {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("products/view"),
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "productId" : self.productData.object(forKey: "id") as! String
                ],
                completion: { (result) -> Void in
                self.hideLoading()
                
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    self.productDetailsDataSource.removeAllObjects()
                    
                    if let productDetails = result.object(forKey: "details") as? NSArray {
                        self.productDetailsDataSource.addObjects(from: productDetails as [AnyObject])
                    }
                    
                    if let product = result.object(forKey: "product") as? NSDictionary {
                        self.productData = product
                    }
                    
                    self.productDetails = self.productDetailsDataSource
                    
                    if self.productData.object(forKey: "id") as? String == "20" || self.productData.object(forKey: "id") as? String == "22" || self.productData.object(forKey: "id") as? String == "24" || self.productData.object(forKey: "id") as? String == "27"{
                        self.goToPhotoPreview()
                    }
                    else{
                    
                    if let _ = self.project {
                        self.goToPhotoPreview()
                    }
                    else{
                        let attributes = NSMutableDictionary(dictionary: [
                            "product" : self.productData,
                            "productDetails" : self.productDetails,
                            "numberOfPhotos" : NSNumber(value: self.productDetails.count as Int),
                            "minimumNumberOfPhotos" : Int(self.productData.object(forKey: "min_image") as? String ?? "0") ?? 0
                            ])
                        if let proj = self.project {
                            attributes.setValue(proj, forKey: "project")
                        }
                        if let details = self.projectDetails {
                            attributes.setValue(details, forKey: "projectDetails")
                        }
                        if self.corporateVoucherId > 0 {
                            attributes.setValue(self.corporateVoucherCode, forKey: "voucherCode")
                            attributes.setValue(self.corporateVoucher, forKey: "voucher")
                        }
                        self.showViewController("MainPhotoSelectorViewController", attributes: attributes)
                    }
                    }
                }
                else{
                    Globals.showAlertWithTitle("Products Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
            })
        }
    }
}
