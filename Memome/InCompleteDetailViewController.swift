//
//  InCompleteDetailViewController.swift
//  Memome
//
//  Created by Yuni on 30/04/18.
//  Copyright © 2018 Trio Digital Agency. All rights reserved.
//

import UIKit
import Presentr

class InCompleteDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var checkoutTableView: UITableView!
    @IBOutlet weak var checkoutView: UIView!
    @IBOutlet weak var productTableView: UITableView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var pageTitleLabel: UILabel!
    
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    
    @IBOutlet weak var trackBtn: UIButton!
    
    @IBOutlet weak var btnReorder: UIButton!
    @IBOutlet weak var firstTitle: UILabel!
    @IBOutlet weak var secondTitle: UILabel!
    @IBOutlet weak var firstValue: UILabel!
    @IBOutlet weak var secondValue: UILabel!
    @IBOutlet weak var deliveryView: UIView!
    
    @IBOutlet weak var discountValue: UILabel!
    @IBOutlet weak var shippingName: UILabel!
    @IBOutlet weak var shippingCost: UILabel!
    
    var cartDataSource: NSMutableArray! = NSMutableArray()
    var listProductDataSource = NSMutableArray()
    var productDataSource = NSMutableArray()
    var orderId = ""
    var shippingEtd: String = "1-3"
    var quantity = 0
    let dbDateFormat: DateFormatter! = DateFormatter()
    var orderStatus: NSDictionary = NSDictionary()
    var reOrder: NSDictionary = NSDictionary()
    var purchaseOrder: NSDictionary = NSDictionary()
    var isReOrder: NSDictionary = NSDictionary()
    
    var updatedData = NSMutableDictionary()
    var productDetails: NSArray!
    var productDetailsDataSource: NSMutableArray! = NSMutableArray()
    var coverImage: String = ""
    
    @IBOutlet weak var inCompleteScrollView: UIScrollView!
    @IBOutlet weak var inCompleteContentView: UIView!

    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .popup)
        presenter.transitionType = .crossDissolve
        return presenter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.defaultScrollView = self.inCompleteScrollView
        self.defaultContentScrollView = self.inCompleteContentView
        self.initRefreshControl(self.inCompleteScrollView)
        
        self.checkoutTableView.register(UINib(nibName: "PurchaseProductTableViewCell", bundle: nil), forCellReuseIdentifier: "purchaseProductTableViewCell")
        
        self.productTableView.register(UINib(nibName: "PurchaseListTableViewCell", bundle: nil), forCellReuseIdentifier: "purchaseListTableViewCell")
        
        self.dbDateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.trackBtn.layer.cornerRadius = 5
        
        if self.attributes != nil{
            if let order = self.attributes.object(forKey: "order") as? NSDictionary{
                self.renderOrderData(order)
                orderStatus = order
                self.purchaseOrder = order
            }
        }
        
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func refresh(_ sender: AnyObject){
        super.refresh(sender)
        self.loadData()
    }
    
    @IBAction func backButtonYouchUpInside(_ sender: Any) {
        self.dismissSelf()
    }
    
    @IBAction func btnReorderTouchUpInside(_ sender: Any) {
        if self.productDataSource.count == 0 {
            Globals.showAlertWithTitle("Memome is under development", message: "Sorry, currently this feature is under development", viewController: self)
        }
        else{

            self.showViewController("CheckoutViewController", attributes: updatedData)
     
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

//    func numberOfSections(in tableView: UITableView) -> Int {
//        if tableView == self.checkoutTableView{
//            return self.productDataSource.count
//        }
//        else if tableView == self.productTableView{
//            return self.listProductDataSource.count
//        }
//        return 0
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.checkoutTableView{
            return self.productDataSource.count
        }
        else if tableView == self.productTableView{
            return self.listProductDataSource.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.checkoutTableView{
            
            let cellInfoArray = self.productDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
            
            let minHeight: CGFloat = 167.0
            var heightByPhoto: CGFloat = 82.0
            var heightByTitle: CGFloat = 145.0
            
            var title = ""
                        if let productTitle = cellInfoArray.object(forKey: "title") as? String {
                            title = (productTitle as NSString!) as String
                        }
            
                    
                    let titleHeight : CGFloat = title.boundingRect(
                        with: CGSize(width: self.checkoutTableView.frame.size.width - 145, height: 99999),
                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
                        attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 16)!],
                        context: nil
                        ).size.height
                    
                    heightByTitle += titleHeight
            return max(minHeight, max(heightByTitle, heightByPhoto))
            
        }
            
        else
            if tableView == self.productTableView{
                
            var height: CGFloat = 15.0
            let minHeight: CGFloat = 30
            let maxFloat: CGFloat = 99999.0
            
            let cellInfoArray = self.listProductDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
            
            if let title = cellInfoArray.object(forKey: "product_title") as? NSString {
                height += title.boundingRect(
                    with: CGSize(width: tableView.frame.size.width, height: maxFloat),
                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: UIFont(name: "MyriadPro-Regular", size: 14)!],
                    context: nil
                    ).size.height
            }
            
            if let title = cellInfoArray.object(forKey: "title") as? String {
                height += title.boundingRect(
                    with: CGSize(width: tableView.frame.size.width, height: maxFloat),
                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: UIFont(name: "MyriadPro-Bold", size: 14)!],
                    context: nil
                    ).size.height
            }
            
            return max(height, minHeight)
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.checkoutTableView{
            
            let cell = self.checkoutTableView.dequeueReusableCell(withIdentifier: "purchaseProductTableViewCell", for: indexPath)
            
            let coverWrapper: UIView = cell.viewWithTag(1)!
            let lbhPhotoType: UILabel = cell.viewWithTag(2) as! UILabel
            let lblTitle: UILabel = cell.viewWithTag(3) as! UILabel
            let lblPhotoCount: UILabel = cell.viewWithTag(4) as! UILabel
            let lblTotalPrice: UILabel = cell.viewWithTag(5) as! UILabel
            let lblQty: UILabel = cell.viewWithTag(6) as! UILabel
            let btnAdd: EditButton = cell.viewWithTag(7) as! EditButton
            let imageView: UIImageView = cell.viewWithTag(8) as! UIImageView
            
            btnAdd.index = indexPath.row
            
            imageView.layer.borderColor = UIColor.black.cgColor
            imageView.layer.borderWidth = 1
            
            coverWrapper.isHidden = true
            btnAdd.isHidden = true
            
            btnAdd.addTarget(self, action: #selector(btnAddtoCart(_:)), for: UIControlEvents.touchUpInside)
            
            let cellInfoArray = self.productDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
            
            if let status = orderStatus.object(forKey: "status") as? String {
                
                if "complete".elementsEqual(status.trim().lowercased()){
                   
                    if orderStatus.object(forKey: "keterangan_expired") as? String == "1" || orderStatus.object(forKey: "keterangan_expired") as? NSNumber == 1{
                        btnAdd.isHidden = true
                        btnAdd.layer.cornerRadius = 5
                    }else{
                        btnAdd.isHidden = false
                        btnAdd.layer.cornerRadius = 5
                    }
                    
                }
                else{
                    btnAdd.isHidden = true
                }
            }
            
            if let cover_images = cellInfoArray.object(forKey: "cover_image") as? NSArray{
                
                for cover in cover_images {
                    let image = (cover as! NSDictionary).object(forKey: "image") as? NSDictionary
                    
                    if image != nil{
                        let cover_image = (cover as! NSDictionary).object(forKey: "image") as? NSDictionary
                        if let image_large = cover_image?.object(forKey: "large") as? String{
                            
                            print("IMAGE \(image_large)")
                            let urlImage = URL(string : image_large)
                            let session = URLSession.shared
                            let getImageFromUrl = session.dataTask(with: urlImage!){(data, response, error) in
                                
                                if let e = error{
                                    print("Error Occurred : \(e)")
                                } else{
                                    if (response as? HTTPURLResponse) != nil{
                                        if let imageData = data{
                                            DispatchQueue.main.async {
                                                let images = UIImage(data: imageData)
                                                imageView.image = images
                                                self.coverImage = image_large
                                            }
                                        } else{
                                            print("Image file is corrupted")
                                        }
                                    }
                                    else{
                                        print("No response from server")
                                    }
                                }
                            }
                            getImageFromUrl.resume()
                        }
                    }
                    
                    else{
                        
                        if let order_product_details = cellInfoArray.object(forKey: "order_product_detail") as? NSArray{
                            if order_product_details.count != 0{
                            if let order_product = order_product_details.object(at: 0) as? NSDictionary{
                                if let image = order_product.object(forKey: "image") as? NSDictionary{
                                    if let image_large = image.object(forKey: "large") as? String{
                                        
                                        print("IMAGE \(image_large)")
                                        let urlImage = URL(string : image_large)
                                        let session = URLSession.shared
                                        let getImageFromUrl = session.dataTask(with: urlImage!){(data, response, error) in
                                            
                                            if let e = error{
                                                print("Error Occurred : \(e)")
                                            } else{
                                                if (response as? HTTPURLResponse) != nil{
                                                    if let imageData = data{
                                                        DispatchQueue.main.async {
                                                            let images = UIImage(data: imageData)
                                                            imageView.image = images
                                                            self.coverImage = image_large
                                                        }
                                                    } else{
                                                        print("Image file is corrupted")
                                                    }
                                                }
                                                else{
                                                    print("No response from server")
                                                }
                                            }
                                        }
                                        getImageFromUrl.resume()
                                    }
                                }
                            }
                            
                            }
                            else{
                                imageView.backgroundColor = UIColor.white
                            }
                            
                        }
                    } //else
                    
                } //for
            } // if
            
            if let title = cellInfoArray.object(forKey: "product_title") as? String {
                lbhPhotoType.text = title
            }
            else {
                lbhPhotoType.text = ""
            }
            
            if let count_photo = cellInfoArray.object(forKey: "count_photo") as? String{
                   lblPhotoCount.text = count_photo + " Photos"
            }
            else if let count_photo = cellInfoArray.object(forKey: "count_photo") as? NSNumber{
                   lblPhotoCount.text = "\(count_photo) Photos"
            }
            
            
            if let title = cellInfoArray.object(forKey: "title") as? String {
                lblTitle.text = title
            }
            else{
                lblTitle.text = title
            }
            
            if let price = cellInfoArray.object(forKey: "total_price") as? NSString {
                if price.doubleValue == 0 {
                    lblTotalPrice.text = "FREE"
                }
                else{
                    lblTotalPrice.text = "IDR " + Globals.numberFormat(NSNumber(value: price.doubleValue as Double))
                }
            }
            else{
                lblTotalPrice.text = ""
            }
            
            if let qty = cellInfoArray.object(forKey: "qty") as? NSString {
                
                lblQty.text = Globals.numberFormat(NSNumber(value: qty.integerValue as Int)) + " X"
                
            }
            else {
                lblQty.text = ""
            }

            return cell
            
        }
        
        else if tableView == self.productTableView{
            let cell = self.productTableView.dequeueReusableCell(withIdentifier: "purchaseListTableViewCell", for: indexPath)
            
            let cellInfoArray = self.listProductDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
            let lblProductTitle = cell.viewWithTag(1) as! UILabel
            let lblTitle = cell.viewWithTag(4) as! UILabel
            let lblPrice = cell.viewWithTag(2) as! UILabel
            let lblQty = cell.viewWithTag(3) as! UILabel
            
            if let title = cellInfoArray.object(forKey: "title") as? String {
                lblProductTitle.text = title
            }
            else {
                lblProductTitle.text = ""
            }
            
            if let title = cellInfoArray.object(forKey: "name") as? String {
                lblTitle.text = title
            }
            else{
                lblTitle.text = ""
            }
            
            if let price = cellInfoArray.object(forKey: "value") as? NSString {
                lblPrice.text = "IDR " + Globals.numberFormat(NSNumber(value: price.doubleValue as Double))
            }
            else{
                lblPrice.text = ""
            }
            
            if let qty = cellInfoArray.object(forKey: "quantity") as? NSString {
                
                let quantitys =  qty.integerValue
                
                lblQty.text = Globals.numberFormat(NSNumber(value: qty.integerValue as Int)) + "x"
                
                quantity += quantitys
                qtyLabel.text = "TOTAL \(quantity) PRODUCTS"
            }
            else {
                lblQty.text = ""
            }
            
            return cell
        }
        
        return UITableViewCell()
        
    }
    
    
    func renderOrderData(_ order: NSDictionary){
        if let number = order.object(forKey: "number") as? String {
            self.pageTitleLabel.text = "ORDER #\(number)"
        }
        else {
            self.pageTitleLabel.text = ""
        }
        
        if let orderNumber = order.object(forKey: "number") as? String {
            self.orderNumberLabel.text = "#\(orderNumber)"
        }
        else {
            self.orderNumberLabel.text = ""
        }
        
        if let number = order.object(forKey: "id") as? String {
            self.orderId = number
        }
        
        if let createdAt = order.object(forKey: "created_at") as? String {
            let calendar = Calendar.current
            let components: DateComponents = (calendar as NSCalendar).components([.month, .day, .year, .hour, .minute, .second], from: self.dbDateFormat.date(from: createdAt)!)
            
            self.orderDateLabel.text = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
//                String(format: "%i %@ %i", arguments: [
//                components.day!,
//                Globals.getMonthName(components.month!),
//                components.year!
//                ])
        }
        else{
            self.orderDateLabel.text = ""
        }
        
        if let grandTotal = order.object(forKey: "grand_total") as? NSString {
            self.totalLabel.text = "IDR " + Globals.numberFormat(NSNumber(value: grandTotal.doubleValue as Double))
        }
        else{
            self.totalLabel.text = ""
        }
        
        if let voucher_total = order.object(forKey: "voucher_total") as? NSString {
            self.discountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: voucher_total.doubleValue as Double))
        }
        else{
            self.discountValue.text = ""
        }
        if let shipping_price = order.object(forKey: "shipping_price") as? String {
            self.shippingCost.text = shipping_price
        }
        else{
            self.shippingCost.text = ""
        }
        
        if let shipping_type = order.object(forKey: "shipping_type") as? String {
            self.shippingName.text = "Shipping " + shipping_type.uppercased()
        }
        else {
            self.shippingName.text = ""
        }
        
        var shipping = ""
        if let item = order.object(forKey: "shipping_first_name") as? String {
            shipping = shipping + item + " "
        }
        
        if let item = order.object(forKey: "shipping_last_name") as? String {
            shipping = shipping + item
        }
        
        if let item = order.object(forKey: "shipping_address") as? String {
            if shipping != "" {
                shipping = shipping + "\n"
            }
            shipping = shipping + item + "\n"
        }
        
        if let item = order.object(forKey: "shipping_city") as? String {
            shipping = shipping + item + ", "
        }
        
        if let item = order.object(forKey: "shipping_province") as? String {
            shipping = shipping + item + " - "
        }
        
        if let item = order.object(forKey: "shipping_postal_code") as? String {
            shipping = shipping + item
        }
        
        if let item = order.object(forKey: "shipping_phone") as? String {
            if shipping != "" {
                shipping = shipping + "\n"
            }
            shipping = shipping + item
        }
        
        self.shippingLabel.text = shipping
        
        if let status = order.object(forKey: "status") as? String {
            if "pending".elementsEqual(status.trim().lowercased()) {
                
                self.statusLabel.text = "ON PROGRESS"
                self.statusLabel.textColor = UIColor.gray
                secondTitle.text = "EST. PRODUCTION DATE"
                secondTitle.font = secondTitle.font.withSize(10)
                secondValue.text = GeneralHelper.calculateShipping(Date(), from: 3, to: 4)

                btnReorder.isHidden = true
                firstTitle.isHidden = true
                secondTitle.isHidden = false
                firstValue.isHidden = true
                secondValue.isHidden = false
                trackBtn.isHidden = true
                if let height = Globals.getConstraint("height", view: btnReorder) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: firstTitle) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: secondTitle) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: firstValue) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: secondValue) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: trackBtn) {
                    height.constant = 0
                }
            } else if "process".elementsEqual(status.trim().lowercased()) {
                guard let createdAt = order.object(forKey: "process_date") as? String else {
                    return
                }
                
                let dateValue = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM yyyy"
                let date = dateFormatter.date(from: dateValue)
                
                self.statusLabel.text = "IN PRODUCTION"
                self.statusLabel.textColor = UIColor(red: 233/255.0, green: 179/255.0, blue: 67/255.0, alpha: 1)
                
                firstTitle.text = "PRODUCTION DATE"
                secondTitle.text = "EST. DELIVERY DATE"
                firstValue.text = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                secondValue.text = GeneralHelper.calculateShipping(date!, from: 3, to: 4)
                
                btnReorder.isHidden = true
                firstTitle.isHidden = false
                secondTitle.isHidden = false
                firstValue.isHidden = false
                secondValue.isHidden = false
                trackBtn.isHidden = true
                if let height = Globals.getConstraint("height", view: btnReorder) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: firstTitle) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: secondTitle) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: firstValue) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: secondValue) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: trackBtn) {
                    height.constant = 0
                }
            }
            else if "cancelled".elementsEqual(status.lowercased()) || "failed".elementsEqual(status.lowercased()) {
                
                guard let createdAt = order.object(forKey: "canceled_date") as? String else {
                    return
                }
                guard let description = order.object(forKey: "reason_for_cancel") as? String else{
                    return
                }
                
                statusLabel.text = "CANCELLED"
                statusLabel.textColor = UIColor.red
//                statusLabel.backgroundColor = UIColor.red
                secondTitle.textColor = UIColor.colorLightRed
                secondValue.textColor = UIColor.red
                
                firstTitle.text = "REASONS "
                secondTitle.text = "CANCELLED ON "
                firstValue.text = "\(description.uppercased())"
                secondValue.text = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                
                btnReorder.isHidden = true
                firstTitle.isHidden = false
                secondTitle.isHidden = false
                firstValue.isHidden = false
                secondValue.isHidden = false
                trackBtn.isHidden = true
                
                if let height = Globals.getConstraint("height", view: btnReorder) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: firstTitle) {
                    height.constant = 17
                }
//                if let height = Globals.getConstraint("height", view: statusLabel) {
//                    height.constant = 19
//                }
//                if let width = Globals.getConstraint("width", view: statusLabel) {
//                    width.constant = 85
//                }
                if let height = Globals.getConstraint("height", view: secondTitle) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: firstValue) {
                    height.constant = 80
                }
                if let height = Globals.getConstraint("height", view: secondValue) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: trackBtn) {
                    height.constant = 0
                }
            }
            else if "shipping".elementsEqual(status.trim().lowercased()) {
                guard let createdAt = order.object(forKey: "ship_date") as? String else {
                    return
                }
                self.statusLabel.text = "ON DELIVERY"
                statusLabel.textColor = UIColor(red: 69/255.0, green: 208/255.0, blue: 58/255.0, alpha: 1)
                firstTitle.text = "DELIVERY DATE"
                secondTitle.text = "TRACKING CODE"
                firstValue.text = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                if let shipping_number = order.object(forKey: "shipping_number") as? String {
                    secondValue.text = shipping_number.uppercased()
                }
                else {
                    secondValue.text = ""
                }
                
                btnReorder.isHidden = true
                firstTitle.isHidden = false
                secondTitle.isHidden = false
                firstValue.isHidden = false
                secondValue.isHidden = false
                trackBtn.isHidden = false
                if let height = Globals.getConstraint("height", view: btnReorder) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: firstTitle) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: secondTitle) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: firstValue) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: secondValue) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: trackBtn) {
                    height.constant = 25
                }
            } else if "complete".elementsEqual(status.trim().lowercased()) {
                guard let createdAt = order.object(forKey: "complete_date") as? String else {
                    return
                }
                self.statusLabel.text = status.uppercased()
                statusLabel.isHidden = false
                statusLabel.textColor = UIColor.colorPrimary
                firstTitle.textColor = UIColor.colorPrimary
                firstValue.textColor = UIColor.colorPrimarySemiDark
                
                firstTitle.text = "RECEIVED ON"
                secondTitle.text = ""
                firstValue.text = Globals.dateFormat(createdAt, format: "dd MMM yyyy")
                secondValue.text = ""
                
                btnReorder.isHidden = false
                firstTitle.isHidden = false
                secondTitle.isHidden = true
                firstValue.isHidden = false
                secondValue.isHidden = true
                trackBtn.isHidden = true
                if let height = Globals.getConstraint("height", view: firstTitle) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: secondTitle) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: firstValue) {
                    height.constant = 17
                }
                if let height = Globals.getConstraint("height", view: secondValue) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: trackBtn) {
                    height.constant = 0
                }
                
                if order.object(forKey: "keterangan_expired") as? String == "1" || order.object(forKey: "keterangan_expired") as? NSNumber == 1{
                    btnReorder.isHidden = true
                    if let height = Globals.getConstraint("height", view: btnReorder) {
                        height.constant = 0
                    }
                }
                
            }
            else {
                
                btnReorder.isHidden = false
                statusLabel.textColor = UIColor.orange
                firstTitle.isHidden = true
                secondTitle.isHidden = true
                firstValue.isHidden = true
                secondValue.isHidden = true
                trackBtn.isHidden = true
                if let height = Globals.getConstraint("height", view: firstTitle) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: secondTitle) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: firstValue) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: secondValue) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("height", view: trackBtn) {
                    height.constant = 0
                }
            }
        }
//        else{
//            btnReorder.isHidden = false
//            self.statusLabel.text = ""
//            firstTitle.isHidden = true
//            secondTitle.isHidden = true
//            firstValue.isHidden = true
//            secondValue.isHidden = true
//            trackBtn.isHidden = true
//            if let height = Globals.getConstraint("height", view: firstTitle) {
//                height.constant = 0
//            }
//            if let height = Globals.getConstraint("height", view: secondTitle) {
//                height.constant = 0
//            }
//            if let height = Globals.getConstraint("height", view: firstValue) {
//                height.constant = 0
//            }
//            if let height = Globals.getConstraint("height", view: secondValue) {
//                height.constant = 0
//            }
//            if let height = Globals.getConstraint("height", view: trackBtn) {
//                height.constant = 0
//            }
//        }
    }
    
    func loadData(){
        
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(
                Globals.getApiUrl("orders/getById"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken") as! String,
                    "orderId" : self.orderId
                ],
                completion: { (result) in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        self.listProductDataSource.removeAllObjects()
                        self.productDataSource.removeAllObjects()
                        
                        guard let order = result.object(forKey: "order") as? NSDictionary else { return }
                        
                        
                        self.updatedData.setValue(order, forKey: "order")
                        self.updatedData.setValue(true, forKey: "isReorder")
                        self.updatedData.setValue(true, forKey: "isAddToCart")
//                        self.updatedData.setValue(self.orderId, forKey: "order_id")
                        
                        self.reOrder = order
                        let arrayData = NSMutableArray()
                        if let products = order.object(forKey: "order_product") as? NSArray {
                            for product in products {
                               
                                let dict = NSMutableDictionary()
                                
                                if let product_title = (product as! NSDictionary).object(forKey: "product_title") as? String {
                                    dict.setValue(product_title, forKey: "title")
                                }
                                if let total_price = (product as! NSDictionary).object(forKey: "total_price") as? String {
                                    dict.setValue(total_price, forKey: "value")
                                }
                                if let qty = (product as! NSDictionary).object(forKey: "qty") as? String {
                                    dict.setValue(qty, forKey: "quantity")
                                }
                                var name = ""
                                if let title = (product as! NSDictionary).object(forKey: "title") as? String {
                                    name += title
                                }
                                if name.trim() != "" {
                                    dict.setValue(name.trim(), forKey: "name")
                                }
                                arrayData.add(dict)
                            }
                            
//                            updatedData.setValue(arrayData, forKey: "expandData")
//                            self.productDataSource.add(self.updatedData)
                            self.productDataSource.addObjects(from: products as [AnyObject])
                        }
                        
                        
                        self.listProductDataSource.addObjects(from: arrayData as [AnyObject])
                        
                        self.reloadProductTableView()
                        self.reloadOrderDetailTableView()
//                        self.orderStatus = order
//                        self.renderOrderData(order)
                        
                    }
                    else{
                        Globals.showAlertWithTitle("My Orders Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
        
    }
    
    func loadProductData(_ product: NSDictionary, order: NSDictionary){
        if !self.isLoading() {
            self.showLoading()
            var productData = product
            Globals.getDataFromUrl(Globals.getApiUrl("products/view"),
                                   requestType: Globals.HTTPRequestType.http_GET,
                                   params: [
                                    "clientId" : Globals.getProperty("clientId"),
                                    "productId" : productData.object(forKey: "id") as! String
                ],
               completion: { (result) -> Void in
                self.hideLoading()
                
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    self.productDetailsDataSource.removeAllObjects()
                    
                    if let productDetails = result.object(forKey: "details") as? NSArray {
                        self.productDetailsDataSource.addObjects(from: productDetails as [AnyObject])
                    }
                    
                    if let product = result.object(forKey: "product") as? NSDictionary {
                        productData = product
                    }
                    
                    self.productDetails = self.productDetailsDataSource
                    
                    //add cover image
                    if MemomeHelper.coverImage == nil {
                        MemomeHelper.coverImage = NSMutableDictionary()
                    }
                    MemomeHelper.coverImage.setValue(order.object(forKey: "title") as? String ?? "", forKey: "title")
                    MemomeHelper.coverImage.setValue(order.object(forKey: "subtitle") as? String ?? "", forKey: "subtitle")
                    if let cover_images = order.object(forKey: "cover_image") as? NSArray {
                        for cover in cover_images {
                            let image = (cover as! NSDictionary).object(forKey: "image") as? NSDictionary
                            
                            if image != nil{
                              
                                let cover_image = (cover as! NSDictionary).object(forKey: "image") as? NSDictionary
                                if let image_large = cover_image?.object(forKey: "large") as? String{
                                MemomeHelper.coverImage.setValue(cover_image?.object(forKey: "large") as? String ?? "", forKey: "url")
                                    
                                    let userPhotoWidth = CGFloat((productData.object(forKey: "cover_width") as? NSString ?? "0").floatValue)
                                    let userPhotoHeight = CGFloat((productData.object(forKey: "cover_height") as? NSString ?? "0").floatValue)
                                    if image_large != nil && image_large != "" {
                                        var offsetLeft: CGFloat = 0.0
                                        var offsetTop: CGFloat = 0.0
                                        let width: CGFloat = CGFloat((productData.object(forKey: "cover_content_width") as! NSString).floatValue)
                                        let height: CGFloat = CGFloat((productData.object(forKey: "cover_content_height") as! NSString).floatValue)
                                        let imgWidth: CGFloat = CGFloat(userPhotoWidth)
                                        let imgHeight: CGFloat = CGFloat(userPhotoHeight)
                                        let scale: CGFloat = max(width / imgWidth, height / imgHeight)
                                        
                                        offsetLeft = (((imgWidth * scale) - width) / 2.0) / scale
                                        offsetTop = (((imgHeight * scale) - height) / 2.0) / scale
                                        
                                        MemomeHelper.coverImage.setValue(NSValue(cgSize: CGSize(width: width / scale, height: height / scale)), forKey: "size")
                                        MemomeHelper.coverImage.setValue(NSValue(cgPoint: CGPoint(x: offsetLeft, y: offsetTop)), forKey: "offset")
                                        MemomeHelper.coverImage.setValue(NSNumber(value: Float(scale) as Float), forKey: "zoomScale")
                                        MemomeHelper.coverImage.setValue(NSNumber(value: 0.0 as Float), forKey: "rotation")
                                    }
                                    }
                            }
                        }
                    }
                    
                    //add detail image
                    let photos = NSMutableArray()
                    var i = 0
                    if let images = order.object(forKey: "order_product_detail") as? NSArray {
                        for item in images {
                            let productDetail = self.productDetails.object(at: i) as! NSDictionary
                            let asset = item as! NSDictionary
                            if let images = asset.value(forKey: "image") as? NSDictionary {
//                                if let thumbnail = images.value(forKey: "large") as? NSDictionary {
                                    let userPhotoUrlString = images.value(forKey: "large") as! String
                                    let userPhotoWidth = CGFloat((productDetail.object(forKey: "width") as? NSString ?? "0").floatValue)
                                    let userPhotoHeight = CGFloat((productDetail.object(forKey: "height") as? NSString ?? "0").floatValue)
                                    if userPhotoUrlString != nil && userPhotoUrlString != "" {
                                        let temp = NSMutableDictionary()
                                        var offsetLeft: CGFloat = 0.0
                                        var offsetTop: CGFloat = 0.0
                                        let width: CGFloat = CGFloat((productDetail.object(forKey: "content_width") as! NSString).floatValue)
                                        let height: CGFloat = CGFloat((productDetail.object(forKey: "content_height") as! NSString).floatValue)
                                        let imgWidth: CGFloat = CGFloat(userPhotoWidth)
                                        let imgHeight: CGFloat = CGFloat(userPhotoHeight)
                                        let scale: CGFloat = max(width / imgWidth, height / imgHeight)
                                        
                                        offsetLeft = (((imgWidth * scale) - width) / 2.0) / scale
                                        offsetTop = (((imgHeight * scale) - height) / 2.0) / scale
                                        
                                        temp.setValue(userPhotoUrlString, forKey: "url")
                                        temp.setValue(NSValue(cgSize: CGSize(width: width / scale, height: height / scale)), forKey: "size")
                                        temp.setValue(NSValue(cgPoint: CGPoint(x: offsetLeft, y: offsetTop)), forKey: "offset")
                                        temp.setValue(NSNumber(value: Float(scale) as Float), forKey: "zoomScale")
                                        temp.setValue(NSNumber(value: 0.0 as Float), forKey: "rotation")
                                        
                                        photos.add( temp )
                                    }
//                                }
                            }
                            i += 1
                        }
                    }
                    if MemomeHelper.images == nil {
                        MemomeHelper.images = NSMutableArray()
                    }
                    MemomeHelper.images.removeAllObjects()
                    MemomeHelper.images.addObjects(from: photos as [AnyObject])
                    
                    //add to my project
                    let project = ProjectHelper.addToMyProject(productData, productDetail: self.productDetails)
                    
                    // close photo preview, cover selector, product detail and back to home
                    
                    if project != nil {
                        if CartHelper.isProductExists(project!) {
                        }
                        else{
                            CartHelper.addToCart(project!, qty: 1, reorder: Int32(order.object(forKey: "id") as? String ?? "0")!)
                            self.updateShoppingCartButton()
                        }
                        
                        self.showViewController("CartViewController", attributes: ["order" : order])
                    }
                }
                else{
                    Globals.showAlertWithTitle("Products Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
            })
        }
    }
    
    func reloadOrderDetailTableView() {
        self.checkoutTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.checkoutTableView) {
            height.constant = self.checkoutTableView.contentSize.height
        }
    }
 
    
    func reloadProductTableView(){
        
        self.productTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.productTableView){
            height.constant = self.productTableView.contentSize.height
        }
        
    }
    
    
    func btnAddtoCart(_ sender: AnyObject){
        if let btn = sender as? EditButton{
            let btncart = btn.index
            if let orderProduct = self.productDataSource.object(at: btncart) as? NSMutableDictionary{
                if let products = orderProduct.object(forKey: "product") as? NSArray {
                    if let product = products.object(at: 0) as? NSDictionary {
                        self.loadProductData(product, order: orderProduct)
                    }
                }
            }
        }
    }
    
    
    @IBAction func trackBtnTouchUpInside(_ sender: Any) {
        let controller = TrackingPopUpViewController()
        controller.purchaseOrder = self.purchaseOrder
        customPresentViewController(presenter, viewController: controller, animated: true, completion: nil)

    }
    
    
}
