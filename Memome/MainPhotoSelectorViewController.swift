//
//  MainPhotoSelectorViewController.swift
//  Memome
//
//  Created by iOS Developer on 16/11/17.
//  Copyright © 2017 Trio Digital Agency. All rights reserved.
//

import UIKit
import Segmentio
import Photos

class MainPhotoSelectorViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblPageTitle: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var segmentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentioView: Segmentio!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    fileprivate lazy var viewControllers: [UIViewController] = {
        return self.preparedViewControllers()
    }()
    
    var selectedPhotos: NSMutableArray! = NSMutableArray()
    var numberOfPhotos: Int = 0
    var minimumNumberOfPhotos: Int = 0
    
    var productData: NSDictionary!
    var productDetails: NSArray!
    
    var corporateVoucherId: Int = 0
    var corporateVoucherName: String = ""
    var corporateVoucherQty: Int = 0
    var corporateVoucherCode: String = ""
    var corporateVoucher: NSDictionary = NSDictionary()
    
    var project: Project?
    var projectDetails: NSArray?
    
    var segmentioStyle = SegmentioStyle.onlyImage
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScrollView()
        
        if let number = self.attributes.object(forKey: "numberOfPhotos") as? NSNumber{
            self.numberOfPhotos = number.intValue
        }
        if let number = self.attributes.object(forKey: "minimumNumberOfPhotos") as? Int{
            self.minimumNumberOfPhotos = number
            
            if (self.minimumNumberOfPhotos == 0) {
                self.minimumNumberOfPhotos = numberOfPhotos
            }
        }
        if let attributes = self.attributes {
            if let data = attributes.object(forKey: "product") as? NSDictionary {
                self.productData = data
            }
            if let details = attributes.object(forKey: "productDetails") as? NSArray {
                self.productDetails = details
            }
            
            if let voucher = attributes.object(forKey: "voucher") as? NSDictionary {
                if let id = voucher.object(forKey: "id") as? NSString {
                    self.corporateVoucherId = id.integerValue
                }
                else if let id = voucher.object(forKey: "id") as? NSNumber {
                    self.corporateVoucherId = id.intValue
                }
                
                if let qty = voucher.object(forKey: "qty") as? NSString {
                    self.corporateVoucherQty = qty.integerValue
                }
                else if let qty = voucher.object(forKey: "qty") as? NSNumber {
                    self.corporateVoucherQty = qty.intValue
                }
                
                if let name = voucher.object(forKey: "name") as? String {
                    self.corporateVoucherName = name
                }
                
                self.corporateVoucher = voucher
            }
            
            if let code = attributes.object(forKey: "voucherCode") as? String {
                self.corporateVoucherCode = code
            }
            
            if let proj = attributes.object(forKey: "project") as? Project{
                self.project = proj
            }
            if let details = attributes.object(forKey: "projectDetails") as? NSArray {
                self.projectDetails = details
            }
        }
        
        segmentioView.setup(
            content: segmentioContent(),
            style: segmentioStyle,
            options: segmentioOptions(segmentioStyle: segmentioStyle)
        )
        
        segmentioView.selectedSegmentioIndex = 0
        
        segmentioView.valueDidChange = { [weak self] _, segmentIndex in
            if let scrollViewWidth = self?.scrollView.frame.width {
                let contentOffsetX = scrollViewWidth * CGFloat(segmentIndex)
                self?.scrollView.setContentOffset(
                    CGPoint(x: contentOffsetX, y: 0),
                    animated: true
                )
            }
        }
        
        updateAlbumDetailTitle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // segmentio setup
    func segmentioContent() -> [SegmentioItem] {
        return [
            SegmentioItem(title: "Photo", image: UIImage(named: "icon-cam.png")),
//            SegmentioItem(title: "Facebook", image: UIImage(named: "icon-fb.png")),
//            SegmentioItem(title: "Instagram", image: UIImage(named: "icon-ig.png"))
        ]
    }
    
    func segmentioOptions(segmentioStyle: SegmentioStyle) -> SegmentioOptions {
        var imageContentMode = UIViewContentMode.center
        switch segmentioStyle {
        case .imageBeforeLabel, .imageAfterLabel:
            imageContentMode = .scaleAspectFit
        default:
            break
        }
        
        return SegmentioOptions(
            backgroundColor: .white,
            maxVisibleItems: 3,
            scrollEnabled: false,
            indicatorOptions: segmentioIndicatorOptions(),
            horizontalSeparatorOptions: nil,
            verticalSeparatorOptions: nil,
            imageContentMode: imageContentMode,
            labelTextAlignment: .center,
            labelTextNumberOfLines: 1,
            segmentStates: segmentioStates(),
            animationDuration: 0.3
        )
    }
    
    func segmentioStates() -> SegmentioStates {
        return SegmentioStates(
            defaultState: SegmentioState(
                backgroundColor: .clear
            ),
            selectedState: SegmentioState(
                backgroundColor: .clear
            ),
            highlightedState: SegmentioState(
                backgroundColor: .clear
            )
        )
    }
    
    func segmentioIndicatorOptions() -> SegmentioIndicatorOptions {
        return SegmentioIndicatorOptions(
            type: .bottom,
            ratio: 1,
            height: 5,
            color: .darkGray
        )
    }
    
    //general methods
    fileprivate func preparedViewControllers() -> [UIViewController] {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let photoSelectorViewController = board.instantiateViewController(withIdentifier: "PhotoSelectorViewController") as! PhotoSelectorViewController
        if let attributes: NSDictionary = attributes {
            photoSelectorViewController.attributes = attributes
        }
        photoSelectorViewController.mainViewController = self
        
        let facebookSelectorViewController = board.instantiateViewController(withIdentifier: "FacebookSelectorViewController") as! FacebookSelectorViewController
        if let attributes: NSDictionary = attributes {
            facebookSelectorViewController.attributes = attributes
        }
        facebookSelectorViewController.mainViewController = self
        
        let instagramSelectorViewController = board.instantiateViewController(withIdentifier: "InstagramSelectorViewController") as! InstagramSelectorViewController
        if let attributes: NSDictionary = attributes {
            instagramSelectorViewController.attributes = attributes
        }
        instagramSelectorViewController.mainViewController = self
        
        return [
            photoSelectorViewController,
//            facebookSelectorViewController,
            instagramSelectorViewController
        ]
    }
    
    fileprivate func setupScrollView() {
        scrollView.contentSize = CGSize(
            width: UIScreen.main.bounds.width * CGFloat(viewControllers.count),
            height: containerView.frame.height
        )
        
        for (index, viewController) in viewControllers.enumerated() {
            viewController.view.frame = CGRect(
                x: UIScreen.main.bounds.width * CGFloat(index),
                y: 0,
                width: scrollView.frame.width,
                height: scrollView.frame.height
            )
            addChildViewController(viewController)
            scrollView.addSubview(viewController.view, options: .useAutoresize) // module's extension
            viewController.didMove(toParentViewController: self)
        }
    }
    
    func updateAlbumDetailTitle(){
        if (self.selectedPhotos.count < self.minimumNumberOfPhotos) {
            self.lblPageTitle.text = String(format: "%i/%i", arguments: [self.selectedPhotos.count, self.minimumNumberOfPhotos])
        }
        else {
            self.lblPageTitle.text = String(format: "%i/%i", arguments: [self.selectedPhotos.count, self.numberOfPhotos])
        }
    }
    
    func currentselected(_ item: Any, completionHandler : @escaping ((_ response : Bool, _ position : Int?) -> Void)) {
        if let asset = item as? PHAsset {
            for i in 0 ..< self.selectedPhotos.count {
                if let savedAsset = self.selectedPhotos[i] as? PHAsset {
                    if asset.localIdentifier == savedAsset.localIdentifier {
                        print("data asset get \(i) \(savedAsset.localIdentifier)")
                        completionHandler(false, i)
                        return
                    }
                }
                
                if i >= (self.selectedPhotos.count-1) {
                    completionHandler(true, nil)
                    return
                }
            }
            
            if self.selectedPhotos.count == 0 {
                completionHandler(true, nil)
                return
            }
            
        }
        else if let asset = item as? NSDictionary {
            for i in 0 ..< self.selectedPhotos.count {
                if let savedAsset = self.selectedPhotos[i] as? NSDictionary {
                    if (savedAsset.value(forKey: "images") as? NSDictionary) != nil {
                        if (savedAsset.value(forKey: "images") as? NSDictionary) != nil {
                            if asset.value(forKey: "id") as? String == savedAsset.value(forKey: "id") as? String {
                                print("data asset get \(i) \(savedAsset.value(forKey: "id") as? String)")
                                completionHandler(false, i)
                                return
                            }
                        }
                    }
                }
                
                if i >= (self.selectedPhotos.count-1) {
                    completionHandler(true, nil)
                    return
                }
            }
            
            if self.selectedPhotos.count == 0 {
                completionHandler(true, nil)
                return
            }
        }
        else {
            completionHandler(true, nil)
            return
        }
    }
    
    func setPhotos(_ photos: NSArray){
        if MemomeHelper.images == nil {
            MemomeHelper.images = NSMutableArray()
        }
        MemomeHelper.images.removeAllObjects()
        MemomeHelper.images.addObjects(from: photos as [AnyObject])
        
        self.goToPhotoPreview()
    }
    
    func goToPhotoPreview(){
        let attributes = NSMutableDictionary(dictionary: [
            "product" : self.productData,
            "productDetails" : self.productDetails
            ])
        if let proj = self.project {
            attributes.setValue(proj, forKey: "project")
        }
        if let details = self.projectDetails {
            attributes.setValue(details, forKey: "projectDetails")
        }
        if self.corporateVoucherId > 0 {
            attributes.setValue(self.corporateVoucherCode, forKey: "voucherCode")
            attributes.setValue(self.corporateVoucher, forKey: "voucher")
        }
        self.showViewController("ProductPhotosViewController", attributes: attributes)
    }
    
    // actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    @IBAction func btnDoneTouched(_ sender: AnyObject) {
        if self.selectedPhotos.count < self.minimumNumberOfPhotos {
            let remaining = self.minimumNumberOfPhotos - self.selectedPhotos.count

            Globals.showAlertWithTitle("Select More Photos", message: String(format: "Please select %i more photo%@", arguments: [remaining, remaining > 1 ? "s" : ""]), viewController: self)
        }
        else{
            if let parent = self._parentViewController as? ProductCoverViewController {
                let photos = NSMutableArray()
                var i = 0
                let productDetail = self.productDetails.object(at: i) as! NSDictionary
                for item in self.selectedPhotos {
                    if let asset = item as? PHAsset {
                        let temp = NSMutableDictionary()
                        var offsetLeft: CGFloat = 0.0
                        var offsetTop: CGFloat = 0.0
                        let width: CGFloat = CGFloat((productDetail.object(forKey: "content_width") as! NSString).floatValue)
                        let height: CGFloat = CGFloat((productDetail.object(forKey: "content_height") as! NSString).floatValue)
                        let imgWidth: CGFloat = CGFloat(asset.pixelWidth)
                        let imgHeight: CGFloat = CGFloat(asset.pixelHeight)
                        let scale: CGFloat = max(width / imgWidth, height / imgHeight)

                        offsetLeft = (((imgWidth * scale) - width) / 2.0) / scale
                        offsetTop = (((imgHeight * scale) - height) / 2.0) / scale

                        temp.setValue(asset.localIdentifier, forKey: "url")
                        temp.setValue(NSValue(cgSize: CGSize(width: width / scale, height: height / scale)), forKey: "size")
                        temp.setValue(NSValue(cgPoint: CGPoint(x: offsetLeft, y: offsetTop)), forKey: "offset")
                        temp.setValue(NSNumber(value: Float(scale) as Float), forKey: "zoomScale")
                        temp.setValue(NSNumber(value: 0.0 as Float), forKey: "rotation")

                        photos.add( temp )
                    }
                    else if let asset = item as? NSDictionary {
                        if let images = asset.value(forKey: "images") as? NSDictionary {
                            if let thumbnail = images.value(forKey: "standard_resolution") as? NSDictionary {
                                let userPhotoUrlString = thumbnail.value(forKey: "url") as! String
                                let userPhotoWidth = thumbnail.value(forKey: "width") as? Float ?? 0
                                let userPhotoHeight = thumbnail.value(forKey: "height") as? Float ?? 0
                                if userPhotoUrlString != nil && userPhotoUrlString != "" {
                                    let temp = NSMutableDictionary()
                                    var offsetLeft: CGFloat = 0.0
                                    var offsetTop: CGFloat = 0.0
                                    let width: CGFloat = CGFloat((productDetail.object(forKey: "content_width") as! NSString).floatValue)
                                    let height: CGFloat = CGFloat((productDetail.object(forKey: "content_height") as! NSString).floatValue)
                                    let imgWidth: CGFloat = CGFloat(userPhotoWidth)
                                    let imgHeight: CGFloat = CGFloat(userPhotoHeight)
                                    let scale: CGFloat = max(width / imgWidth, height / imgHeight)
                                    
                                    offsetLeft = (((imgWidth * scale) - width) / 2.0) / scale
                                    offsetTop = (((imgHeight * scale) - height) / 2.0) / scale
                                    
                                    temp.setValue(userPhotoUrlString, forKey: "url")
                                    temp.setValue(NSValue(cgSize: CGSize(width: width / scale, height: height / scale)), forKey: "size")
                                    temp.setValue(NSValue(cgPoint: CGPoint(x: offsetLeft, y: offsetTop)), forKey: "offset")
                                    temp.setValue(NSNumber(value: Float(scale) as Float), forKey: "zoomScale")
                                    temp.setValue(NSNumber(value: 0.0 as Float), forKey: "rotation")
                                    
                                    photos.add( temp )
                                }
                            }
                        }
                    }
                    i += 1
                }
                self.setPhotos(photos)
//                self.dismissSelf()
            }
            else if let parent = self._parentViewController as? PhotoEditorViewController {
                for item in self.selectedPhotos {
                    if let asset = item as? PHAsset {
                        Globals.imageFromAsset(asset, scale: 1.0, resultHandler: { (image) -> Void in
                            DispatchQueue.main.async(execute: { () -> Void in
                                parent.photoCrop.setImage(image)
                                parent.imageUrl = asset.localIdentifier
                                parent.image = image
                                parent.rotation = 0
                            })
                        })
                        self.dismissSelf()
                    }
                    else if let asset = item as? NSDictionary {
                        if let images = asset.value(forKey: "images") as? NSDictionary {
                            if let thumbnail = images.value(forKey: "standard_resolution") as? NSDictionary {
                                let userPhotoUrlString = thumbnail.value(forKey: "url") as! String
                                if userPhotoUrlString != nil && userPhotoUrlString != "" {
                                    Globals.imageFromUrl(userPhotoUrlString, onSuccess: { (image) in
                                        DispatchQueue.main.async(execute: { () -> Void in
                                            parent.photoCrop.setImage(image)
                                            parent.imageUrl = userPhotoUrlString
                                            parent.image = image
                                            parent.rotation = 0
                                        })
                                        self.dismissSelf()
                                    })
                                }
                            }
                        }
                    }
                }
            }
            else if let parent = self._parentViewController as? ProfileViewController {
                for item in self.selectedPhotos {
                    if let asset = item as? PHAsset {
                        Globals.imageFromAsset(asset, scale: 1.0, resultHandler: { (image) -> Void in
                            DispatchQueue.main.async(execute: { () -> Void in
                                parent.updateProfilePicture(image!)
                            })
                        })
                        self.dismissSelf()
                    }
                    else if let asset = item as? NSDictionary {
                        if let images = asset.value(forKey: "images") as? NSDictionary {
                            if let thumbnail = images.value(forKey: "standard_resolution") as? NSDictionary {
                                let userPhotoUrlString = thumbnail.value(forKey: "url") as! String
                                if userPhotoUrlString != nil && userPhotoUrlString != "" {
                                    Globals.imageFromUrl(userPhotoUrlString, onSuccess: { (image) in
                                        DispatchQueue.main.async(execute: { () -> Void in
                                            parent.updateProfilePicture(image!)
                                        })
                                        self.dismissSelf()
                                    })
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if let parent = self._parentViewController as? MakePrintDetailViewController {
            let photos = NSMutableArray()
            var i = 0
            let productDetail = self.productDetails.object(at: i) as! NSDictionary
            for item in self.selectedPhotos {
                if let asset = item as? PHAsset {
                    let temp = NSMutableDictionary()
                    var offsetLeft: CGFloat = 0.0
                    var offsetTop: CGFloat = 0.0
                    let width: CGFloat = CGFloat((productDetail.object(forKey: "content_width") as! NSString).floatValue)
                    let height: CGFloat = CGFloat((productDetail.object(forKey: "content_height") as! NSString).floatValue)
                    let imgWidth: CGFloat = CGFloat(asset.pixelWidth)
                    let imgHeight: CGFloat = CGFloat(asset.pixelHeight)
                    let scale: CGFloat = max(width / imgWidth, height / imgHeight)
                    
                    offsetLeft = (((imgWidth * scale) - width) / 2.0) / scale
                    offsetTop = (((imgHeight * scale) - height) / 2.0) / scale
                    
                    temp.setValue(asset.localIdentifier, forKey: "url")
                    temp.setValue(NSValue(cgSize: CGSize(width: width / scale, height: height / scale)), forKey: "size")
                    temp.setValue(NSValue(cgPoint: CGPoint(x: offsetLeft, y: offsetTop)), forKey: "offset")
                    temp.setValue(NSNumber(value: Float(scale) as Float), forKey: "zoomScale")
                    temp.setValue(NSNumber(value: 0.0 as Float), forKey: "rotation")
                    
                    photos.add( temp )
                }
                else if let asset = item as? NSDictionary {
                    if let images = asset.value(forKey: "images") as? NSDictionary {
                        if let thumbnail = images.value(forKey: "standard_resolution") as? NSDictionary {
                            let userPhotoUrlString = thumbnail.value(forKey: "url") as! String
                            let userPhotoWidth = thumbnail.value(forKey: "width") as? Float ?? 0
                            let userPhotoHeight = thumbnail.value(forKey: "height") as? Float ?? 0
                            if userPhotoUrlString != nil && userPhotoUrlString != "" {
                                let temp = NSMutableDictionary()
                                var offsetLeft: CGFloat = 0.0
                                var offsetTop: CGFloat = 0.0
                                let width: CGFloat = CGFloat((productDetail.object(forKey: "content_width") as! NSString).floatValue)
                                let height: CGFloat = CGFloat((productDetail.object(forKey: "content_height") as! NSString).floatValue)
                                let imgWidth: CGFloat = CGFloat(userPhotoWidth)
                                let imgHeight: CGFloat = CGFloat(userPhotoHeight)
                                let scale: CGFloat = max(width / imgWidth, height / imgHeight)
                                
                                offsetLeft = (((imgWidth * scale) - width) / 2.0) / scale
                                offsetTop = (((imgHeight * scale) - height) / 2.0) / scale
                                
                                temp.setValue(userPhotoUrlString, forKey: "url")
                                temp.setValue(NSValue(cgSize: CGSize(width: width / scale, height: height / scale)), forKey: "size")
                                temp.setValue(NSValue(cgPoint: CGPoint(x: offsetLeft, y: offsetTop)), forKey: "offset")
                                temp.setValue(NSNumber(value: Float(scale) as Float), forKey: "zoomScale")
                                temp.setValue(NSNumber(value: 0.0 as Float), forKey: "rotation")
                                
                                photos.add( temp )
                            }
                        }
                    }
                }
                i += 1
            }
            self.setPhotos(photos)
            //                self.dismissSelf()
        }
        
        
    }
}

extension MainPhotoSelectorViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = floor(scrollView.contentOffset.x / scrollView.frame.width)
        segmentioView.selectedSegmentioIndex = Int(currentPage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 0)
    }
    
}
