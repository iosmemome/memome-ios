//
//  TermsViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 3/1/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class TermsViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtAbout: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.loadDataAbout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    //general methods
    func loadDataAbout(){
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(Globals.getApiUrl("about/search"),
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId")
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        if let about = result.object(forKey: "about") as? NSDictionary {
                            if let content = about.object(forKey: "about_images") as? String {
                                var htmlText = content
                                htmlText += "<style>"
                                htmlText += "* {"
                                htmlText += "font-family: '\(self.txtAbout.font?.fontName ?? UIFont.systemFont(ofSize: 14).fontName)';"
                                htmlText += "font-size: \(self.txtAbout.font?.pointSize ?? 14)px;"
                                htmlText += "text-align: center;"
                                htmlText += "}"
                                htmlText += "</style>"
                                
                                do {
                                    let str = try NSAttributedString(data: htmlText.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                                    self.txtAbout.attributedText = str
                                    
                                } catch {
                                    print(error)
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                    else{
                        Globals.showAlertWithTitle("Terms & Conditions Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
}
