//
//  CheckoutViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 2/24/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit
import BSDropdown
import MidtransKit
import Presentr

class CheckoutViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, BSDropdownDelegate, BSDropdownDataSource, MidtransUIPaymentViewControllerDelegate {
  
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var checkoutTableView: UITableView!
    @IBOutlet weak var listCartView: UIView!
    @IBOutlet weak var totalView: UIView!
    
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var txtCoupun: UITextField!
    @IBOutlet weak var discountResultView: UIView!
    @IBOutlet weak var lbldiscountDesc: UILabel!
    @IBOutlet weak var lbldiscountValue: UILabel!
    @IBOutlet weak var btnScanGC: UIButton!
    @IBOutlet weak var txtGC: UITextField!
    
    @IBOutlet weak var shippingTableView: UITableView!
    @IBOutlet weak var pickedAddressTableView: UITableView!
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var discountView: UIView!
    
    var shippingValue: Double = 0
    
    @IBOutlet weak var buttonAdd: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var couponWrapperView: UIView!
    
    @IBOutlet weak var checkoutScrollView: UIScrollView!
    @IBOutlet weak var checkoutContentScrollView: UIView!
   
    @IBOutlet weak var paymentDetailTitleWrapper: UIView!
    @IBOutlet weak var paymentMethodWrapper: UIView!
    @IBOutlet weak var shippingEstimateWrapper: UIView!
    @IBOutlet weak var btnShippingEstimate: UIButton!
//    @IBOutlet weak var shippingArrivalWrapper: UIView!
    @IBOutlet weak var btnArrivalEstimate: UIButton!
    @IBOutlet weak var bsdPaymentMethod: BSDropdown!
    
    // address
    @IBOutlet weak var useAddressWrapper: UIView!
    @IBOutlet weak var btnAddressChoose: BSDropdown!
    @IBOutlet weak var btnNewAddress: UIButton!
    @IBOutlet weak var btnAddressEdit: BSDropdown!
    
    @IBOutlet weak var shippingMethod: UIView!
    @IBOutlet weak var lblShippingMethod: UILabel!
    @IBOutlet weak var btnShippingMethod: UIButton! //20.04.2018
    
    @IBOutlet weak var newAddressWrapper: UIView!
    @IBOutlet weak var txtFirstWrapper: UIView!
    @IBOutlet weak var txtLastWrapper: UIView!
    @IBOutlet weak var txtAddressWrapper: UIView!
    @IBOutlet weak var btnProvinceWrapper: UIView!
    @IBOutlet weak var btnCityWrapper: UIView!
    @IBOutlet weak var txtPostalCodeWrapper: UIView!
    @IBOutlet weak var txtPhoneWrapper: UIView!
    @IBOutlet weak var txtEmailWrapper: UIView!
    
    @IBOutlet weak var lblShipping: UILabel!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var btnProvince: BSDropdown!
    @IBOutlet weak var btnCity: BSDropdown!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var chkSaveAddress: UISwitch!
    @IBOutlet weak var saveAddressWrapper: UIView!
    
    @IBOutlet weak var pickedAddressView: UIView!
    @IBOutlet weak var namePickedAddress: UILabel!
    @IBOutlet weak var addressPickedAddress: UILabel!
    @IBOutlet weak var phonePickedAddress: UILabel!
    @IBOutlet weak var cityPickedAddress: UILabel!
    @IBOutlet weak var shippingMethodView: UIView!
    @IBOutlet weak var discountDesc: UILabel!
    
    var cartDataSource: NSMutableArray! = NSMutableArray()
    var addressDataSource: NSMutableArray! = NSMutableArray()
    var shippingMethodDataSource: NSMutableArray! = NSMutableArray()
    
    var selectedAddress: NSDictionary?
    var selectedCoupon: NSDictionary?
    
    var selectedPaymentType: String = ""
    var selectedPaymentName: String = ""
    var selectedName: String = ""
    var selectedShippingMethod = -1
    
    let mainColor = UIColor.colorPrimary
    let titleFont = UIFont(name: "Montserrat-SemiBold", size: 15)
    let buttonFont = UIFont(name: "Montserrat-Regular", size: 14)
    
     let addButton = UIButton(frame: CGRect(x: 25, y: 680, width: 364, height: 30))
    
    var provinceDataSource: NSMutableArray! = NSMutableArray()
    var cityDataSource: NSMutableArray! = NSMutableArray()
    var productReorderDataSource = NSMutableArray()
    
    var addressId: Int = 0
    var dataAddress: NSDictionary?
    var selectedProvinceId: Int = 0
    var orderQuantity: Int = 0
    var selectedCityId: Int = 0
    var shippingCost: Double = 0
    var shippingCost1: Double = 0
    var shippingEtd: String = "1-3"
    var totalShipping: Double = 0
    var pDisc: Double = 0
    var minTransaction: Double = 10000.0 //min transfer BCA
    var total: Double = 0
    var grandTotal: Double = 0
    var disc: Double = 0
    var paymentType = Constant.paymentTransfer
    var paymentName = Constant.paymentTransferName
    var transactionId = ""
    var orderId = ""
    var finalSum: Double = 0
    var qtyProduct: Int = 0
    var qtyProduct1: Int = 0
    
    var isManualAddress: Bool = false // for check out - input manually shipping address
    var isSelectedAddress: Bool = false
    var isAddButton: Bool = false
    var isReOrder: Bool = false// for check out - reorder product
    var reOrder: NSArray = NSArray()
    var order_attribute: NSDictionary = NSDictionary()
    var quantityArray: [Int] = []
    var quantityOrder: Int = 0
    var totalProduct: Double = 0
    var couponString = ""
    var sub: Double = 0
    var isVoucher: Bool = true
    var codes: String = ""
    
    var codes_id = ""
    var codes_name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MidtransConfig.shared().setClientKey("VT-client-LQ5AL1YEpSoH7DdJ", environment: .production, merchantServerURL: "https://memome.co.id/transactions/index.php")
        
//        MidtransConfig.shared().setClientKey("VT-client-xbjKOW6X6k3LWjQ5", environment: .sandbox, merchantServerURL: "https://memome.co.id/transactions/index.php")
        
        NotificationCenter.default.addObserver(self, selector: #selector(CheckoutViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CheckoutViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnNewAddress.isHidden = false
        btnNewAddress.layer.cornerRadius = 5
        
        shippingMethod.isHidden = true
        reloadShippingMethodTableView()
        setCoupon(nil)
        
        useAddressWrapper.isHidden = false
        
        btnApply.layer.cornerRadius = 5
        btnScanGC.layer.cornerRadius = 5
        
        
        // Do any additional setup after loading the view.
        
        self.checkoutTableView.register(UINib(nibName: "CheckoutTableViewCell", bundle: nil), forCellReuseIdentifier: "checkoutTableViewCell")
        
        
        self.shippingTableView.register(UINib(nibName: "ShippingMethodTableViewCell", bundle: nil), forCellReuseIdentifier: "shippingMethodTableViewCell")
        
        listCartView.layer.cornerRadius = 5
        totalView.layer.cornerRadius = 5
        shippingEstimateWrapper.layer.cornerRadius = 5
        
        btnShippingEstimate.setTitle(GeneralHelper.calculateShipping(Date(), from: 3, to: 4), for: .normal)
        var etd: [NSString] = self.shippingEtd.split(separator: "-") as [NSString]
        if etd.count > 1 {
            btnArrivalEstimate.setTitle(GeneralHelper.calculateShipping(Date(), from: 5+Int(etd[0].intValue), to: 4+(2*Int(etd[1].intValue))), for: .normal)
        }
        else {
            btnArrivalEstimate.setTitle(GeneralHelper.calculateShipping(Date(), from: 4, to: 7), for: .normal)
        }
        
        if self.isManualAddress {
            self.addressId = 0
            
        }
        else{
//            self.saveAddressWrapper.isHidden = true
//            if let height = Globals.getConstraint("height", view: self.saveAddressWrapper) {
//                height.constant = 0
//            }
        }
        
        self.defaultScrollView = self.checkoutScrollView
        self.defaultContentScrollView = self.checkoutContentScrollView
        
        self.useAddressWrapper.layer.cornerRadius = 5
        self.btnAddressEdit.layer.cornerRadius = 5
        
        self.btnAddressEdit.viewController = self
        self.btnAddressEdit.defaultTitle = "EDIT"
        self.btnAddressEdit.title = "Address"
        self.btnAddressEdit.headerBackgroundColor = mainColor
        self.btnAddressEdit.itemTintColor = mainColor
        self.btnAddressEdit.buttonFont = buttonFont
        self.btnAddressEdit.titleKey = "name"
        self.btnAddressEdit.fixedDisplayedTitle = true
        self.btnAddressEdit.delegate = self
        self.btnAddressEdit.dataSource = self
        self.btnAddressEdit.hideDoneButton = true
        self.btnAddressEdit.showExtraButton = true
        self.btnAddressEdit.setup()

        let gesture = UITapGestureRecognizer(target: self, action: #selector(txtAddressWrapperTouched(_:)))
//        self.txtAddressWrapper.addGestureRecognizer(gesture)
        
        self.initRefreshControl(self.checkoutScrollView)
        
        if self.isJustCorporate() {

//            self.paymentDetailTitleWrapper.isHidden = true
//
//            if let height = Globals.getConstraint("height", view: self.paymentDetailTitleWrapper) {
//                height.constant = 0
//            }
            self.selectedPaymentType = Constant.paymentCorporate
            self.selectedPaymentName = Constant.paymentCorporateName
        }
        else {
            self.selectedPaymentType = Constant.paymentTransfer
            self.selectedPaymentName = Constant.paymentTransferName

            self.loadDataPayment()
        }
        
//        if codes != ""{
//        if isVoucher == false{
//            if codes != ""{
//                disc = Double(codes)!
//                couponWrapperView.isHidden = false
//                if let height = Globals.getConstraint("height", view: self.couponWrapperView) {
//                    height.constant = 35
//                }
//                if let height = Globals.getConstraint("discountBottom", view: self.couponWrapperView) {
//                    height.constant = 8
//                }
//                couponString += " (IDR " + Globals.numberFormat(NSNumber(value: disc)) + ")"
//                discountDesc.text = "Discount Gift Card"
//                self.lbldiscountDesc.text = "Discount"
//                self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
//                self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: -disc))
//                self.lbldiscountValue.textColor = UIColor.red
//            }
//            else{
//                disc = 0
//                couponWrapperView.isHidden = true
//                if let height = Globals.getConstraint("height", view: self.couponWrapperView) {
//                    height.constant = 0
//                }
//                if let height = Globals.getConstraint("discountBottom", view: self.couponWrapperView) {
//                    height.constant = 0
//                }
//                isVoucher = true
//                couponString += " (IDR " + Globals.numberFormat(NSNumber(value: disc)) + ")"
////                discountDesc.text = "Discount Gift Card"
////                self.lbldiscountDesc.text = "Discount"
////                self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
////                self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: -disc))
////                self.lbldiscountValue.textColor = UIColor.red
//                discountView.isHidden = true
//            }
//
//        }
//        }
        
        if self.attributes != nil{
            if let isReorder = self.attributes.object(forKey: "isReorder") as? Bool{
                self.isReOrder = isReorder;
                if let order = self.attributes.object(forKey: "order") as? NSDictionary{
                    order_attribute = order
                    self.renderOrderData(order)
                    
                    if let reorder = order.object(forKey: "order_product") as? NSArray{
                            self.productReorderDataSource.addObjects(from: reorder as [AnyObject])
                        self.reOrder = reorder
                        for product in reorder{
                            if let qty = (product as! NSDictionary).object(forKey: "qty") as? String{
                                quantityArray.append(Int(qty)!)
                            }
                        }
                    }
                }
            }
            
//            if let isvoucher = self.attributes.object(forKey: "isVoucher") as? Bool{
//                isVoucher = isvoucher
//            }
            
//            if let code = self.attributes.object(forKey: "code") as? String{
            
//                disc = Double(code)!
//                couponWrapperView.isHidden = false
//                if let height = Globals.getConstraint("height", view: self.couponWrapperView) {
//                    height.constant = 35
//                }
//                if let height = Globals.getConstraint("discountBottom", view: self.couponWrapperView) {
//                    height.constant = 8
//                }
//                couponString += " (IDR " + Globals.numberFormat(NSNumber(value: disc)) + ")"
//                discountDesc.text = "Discount Gift Card"
//                self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
//                self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
//            }
        }
        
        self.showUseAddress()
        self.loadDataAddress()
//        self.loadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isVoucher == false{
            if codes != ""{
                disc = Double(codes)!
                couponWrapperView.isHidden = false
                if let height = Globals.getConstraint("height", view: self.couponWrapperView) {
                    height.constant = 35
                }
                if let height = Globals.getConstraint("discountBottom", view: self.couponWrapperView) {
                    height.constant = 8
                }
                couponString += " (IDR " + Globals.numberFormat(NSNumber(value: disc)) + ")"
                discountDesc.text = "Discount Gift Card"
                self.lbldiscountDesc.text = self.codes_name
                self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: -disc))
                self.lbldiscountValue.textColor = UIColor.red
            }
            else{
                disc = 0
                couponWrapperView.isHidden = true
                if let height = Globals.getConstraint("height", view: self.couponWrapperView) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("discountBottom", view: self.couponWrapperView) {
                    height.constant = 0
                }
                isVoucher = true
                couponString += " (IDR " + Globals.numberFormat(NSNumber(value: disc)) + ")"
                //                discountDesc.text = "Discount Gift Card"
                //                self.lbldiscountDesc.text = "Discount"
                //                self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                //                self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: -disc))
                //                self.lbldiscountValue.textColor = UIColor.red
                discountView.isHidden = true
            }
            
        }
        
        self.loadDataAddress()
        self.loadDataCart()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func refresh(_ sender: AnyObject) {
        super.refresh(sender)
        
        self.loadDataAddress()
        self.loadDataCart()
//        self.loadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @objc func keyboardWillShow(notification: NSNotification){
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    // bsdropdown datasource
    func itemHeightForRowAtIndexPath(_ dropdown: BSDropdown, tableView: UITableView, item: NSDictionary?, indexPath: IndexPath) -> CGFloat {
        if let cellInfoArray = item {
            var height: CGFloat = 30.0
            var name: String = ""
            var address: String = ""
            var area: String = ""
            var phone: String = ""
            if let firstName = cellInfoArray.object(forKey: "first_name") as? String {
                name = firstName
            }
            else{
                name = ""
            }
            
            if let lastName = cellInfoArray.object(forKey: "last_name") as? String {
                name = name.appendingFormat(" %@", lastName)
            }
            
            if let a = cellInfoArray.object(forKey: "address") as? String {
                address = a
            }
            
            var city = ""
            var province = ""
            var zipCode = ""
            if let item = cellInfoArray.object(forKey: "cityName") as? String {
                city = item
            }
            if let item = cellInfoArray.object(forKey: "provinceName") as? String {
                province = item
            }
            
            if let item = cellInfoArray.object(forKey: "postal_code") as? String {
                zipCode = item
            }
            
            if city != "" {
                area = area + city
            }
            if province != "" {
                if area != "" {
                    area = area + ", "
                }
                area = area + province
            }
            if zipCode != "" {
                if area != "" {
                    area = area + " - "
                }
                area = area + zipCode
            }
            
            if let p = cellInfoArray.object(forKey: "phone") as? String {
                phone = p
            }
            
            
            let nameWidth = tableView.frame.size.width - 54
            let maxWidth = tableView.frame.size.width - 16
            let maxFloatHeight : CGFloat = 9999
            
            height += name.boundingRect(
                with: CGSize(width: nameWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 14)!],
                context: nil
                ).size.height
            height += address.boundingRect(
                with: CGSize(width: maxWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 13)!],
                context: nil
                ).size.height
            height += area.boundingRect(
                with: CGSize(width: maxWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 13)!],
                context: nil
                ).size.height
            height += phone.boundingRect(
                with: CGSize(width: maxWidth, height: maxFloatHeight),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 13)!],
                context: nil
                ).size.height
            
            return height
        }
        else {
            return 0
        }
    }
    
    func itemForRowAtIndexPath(_ dropdown: BSDropdown, tableView: UITableView, item: NSDictionary?, indexPath: IndexPath) -> UITableViewCell {
        if let cellInfoArray = item {
            tableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "addressTableViewCell")
            let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "addressTableViewCell", for: indexPath)
            
            let lblName = cell.viewWithTag(1) as! UILabel
            let lblAddress = cell.viewWithTag(2) as! UILabel
            let lblCity = cell.viewWithTag(3) as! UILabel
            let lblPhone = cell.viewWithTag(4) as! UILabel
            let btnEdit = cell.viewWithTag(5) as! EditButton
            
            btnEdit.index = (indexPath as NSIndexPath).row
            btnEdit.addTarget(self, action: #selector(CheckoutViewController.btnEditAddressTouched(_:)), for: UIControlEvents.touchUpInside)
            
            if let firstName = cellInfoArray.object(forKey: "first_name") as? String {
                lblName.text = firstName
            }
            else{
                lblName.text = ""
            }
            
            if let lastName = cellInfoArray.object(forKey: "last_name") as? String {
                lblName.text = lblName.text?.appendingFormat(" %@", lastName)
            }
            
            if let address = cellInfoArray.object(forKey: "address") as? String {
                lblAddress.text = address
            }
            else{
                lblAddress.text = ""
            }
            
            var city = ""
            var province = ""
            var zipCode = ""
            if let item = cellInfoArray.object(forKey: "cityName") as? String {
                city = item
            }
            if let item = cellInfoArray.object(forKey: "provinceName") as? String {
                province = item
            }
            
            if let item = cellInfoArray.object(forKey: "postal_code") as? String {
                zipCode = item
            }
            
            lblCity.text = ""
            if city != "" {
                lblCity.text = lblCity.text! + city
            }
            if province != "" {
                if lblCity.text != "" {
                    lblCity.text = lblCity.text! + ", "
                }
                lblCity.text = lblCity.text! + province
            }
            if zipCode != "" {
                if lblCity.text != "" {
                    lblCity.text = lblCity.text! + " - "
                }
                lblCity.text = lblCity.text! + zipCode
            }
            
            if let phone = cellInfoArray.object(forKey: "phone") as? String {
                lblPhone.text = phone
            }
            else{
                lblPhone.text = ""
            }
            loadDataAddress()
            return cell
        }
        else {
            return tableView.dequeueReusableCell(withIdentifier: "addressTableViewCell", for: indexPath)
            
        }
    }
    
    func btnEditAddressTouched(_ sender: AnyObject) {
        if let button = sender as? EditButton {
            let cellInfoArray = self.addressDataSource.object(at: button.index) as! NSDictionary
            self.showViewController("AddressViewController", attributes: [
                "address" : cellInfoArray
                ])
        }
    }
    
    //bsdropdown delegate
    func onDropdownSelectedItemChange(_ dropdown: BSDropdown, selectedItem: NSDictionary?) {
        
//        if dropdown == self.btnAddressChoose {
//
//            addButton.isHidden = false
//
//            selectedAddress = selectedItem
//            showUseAddress()
//            lblShipping.text = "IDR 0"
//            loadAddress(self.selectedAddress!)
//
//        }
//        else
            if dropdown == self.btnAddressEdit {
            
            selectedShippingMethod = -1
            selectedAddress = selectedItem
            showUseAddress()
            grandTotal -= shippingCost
            shippingCost = 0
            shippingEtd = "1-3"
            self.lblShipping.text = "IDR " + Globals.numberFormat(NSNumber(value: shippingCost as Double))
            
            loadDataCart()
            loadAddress(self.selectedAddress!)
        }
        else
//            if dropdown == self.bsdPaymentMethod {
//                if let item = selectedItem {
//                    self.selectedPaymentType = item.object(forKey: "value") as! String
//                    self.selectedPaymentName = item.object(forKey: "title") as! String
//                }
//            }
//            else
                if dropdown == self.btnProvince {
                    if let selectedProvince = selectedItem {
                        if let id = selectedProvince.object(forKey: "id") as? NSString {
                            self.selectedProvinceId = id.integerValue
                            self.btnProvince.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
                            self.selectedCityId = 0
                            self.btnCity.setSelectedIndex(-1)
                        }
                        else{
                            self.selectedProvinceId = 0
                            self.btnProvince.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                        }
                    }
                    else{
                        self.selectedProvinceId = 0
                        self.btnProvince.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                    }
                    
                    self.loadDataCity()
                }
                else{
                    if let selectedCity = selectedItem {
                        if let id = selectedCity.object(forKey: "id") as? NSString {
                            self.selectedCityId = id.integerValue
                            self.btnCity.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
                        }
                        else{
                            self.selectedCityId = 0
                            self.btnCity.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                        }
                    }
                    else{
                        self.selectedCityId = 0
                        self.btnCity.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                    }
        }
        
    }
    
    func onBtnExtraSelected(_ dropdown: BSDropdown) {
       self.showViewController("AddressViewController", attributes: NSDictionary())
    }
    
    func loadDataShippingMethod(){
        
        if !self.isLoading() {
            self.showLoading()
            self.shippingCost = 0
            self.shippingMethodDataSource.removeAllObjects()
            self.selectedShippingMethod = -1
            var cityId = "0"
            if let addr = self.selectedAddress {
                if let cId = addr.object(forKey: "city_id") as? String {
                    cityId = cId
                }
            }
            
            //            if !self.isJustCorporate() {
            Globals.getDataFromUrl(
                Globals.getApiUrl("orders/review"),
                requestType: Globals.HTTPRequestType.http_POST,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "productIds" : "-1",
                    "cityId" : cityId
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    NSLog("loadDataShippingMethod")
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        self.shippingMethodDataSource.removeAllObjects()
                        if let methods = result.object(forKey: "shippingCosts") as? NSArray {
                            
                            for method in methods {
                                
                                if let costs = (method as! NSDictionary).object(forKey: "costs") as? NSArray {
                                    for cost in costs {
                                        let c = NSMutableDictionary(dictionary: cost as! [AnyHashable: Any])
                                        if let temp1 = c.object(forKey: "cost") as? NSArray {
                                            if temp1.count > 0 {
                                                if let temp2 = temp1.object(at: 0) as? NSDictionary {
                                                    if let dbTemp = temp2.object(forKey: "value") as? Double {

//                                                        if(self.totalShipping + dbTemp - self.pDisc < self.minTransaction){
//                                                            let newFee: Double = self.minTransaction - (self.totalShipping - self.pDisc)
//                                                            ((c.object(forKey: "cost") as! NSArray).object(at: 0) as! NSDictionary).setValue(newFee, forKey: "value")
//                                                        }
                                                        c.setValue((method as! NSDictionary).object(forKey: "code"), forKey: "code")

                                                        self.shippingMethodDataSource.add(c)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
//                                                        self.shippingMethodDataSource.addObjectsFromArray(methods as [AnyObject])
                            
                        }
                        
                        self.reloadShippingMethodTableView()
                    }
                    else{
                        Globals.showAlertWithTitle("Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
        else{
            self.shippingMethodDataSource.add([
                "code" : Constant.shippingFree,
                "service" : Constant.shippingFree.capitalized
                ])
            self.selectedShippingMethod = -1
            self.shippingCost = 0
            self.shippingEtd = "1-3"
            self.reloadShippingMethodTableView()
            self.hideLoading()
            
        }
        
        
        
    }
    
    
    //Table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == checkoutTableView{
            if isReOrder{
                return self.productReorderDataSource.count
            }
            else{
            return self.cartDataSource.count
        }
        }
        if tableView == shippingTableView {
            return self.shippingMethodDataSource.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == checkoutTableView{
        let minHeight: CGFloat = 100.0
        var heightByPhoto: CGFloat = 82.0
        var heightByTitle: CGFloat = 145.0
            if isReOrder{
            
                let cellInfoArray = self.productReorderDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
            var title = ""
            if let productTitle = cellInfoArray.object(forKey: "title") as? String {
                title = (productTitle as NSString!) as String
            }


            let titleHeight : CGFloat = title.boundingRect(
                with: CGSize(width: self.checkoutTableView.frame.size.width - 145, height: 99999),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 16)!],
                context: nil
                ).size.height

            heightByTitle += titleHeight
            return max(minHeight, max(heightByTitle, heightByPhoto))
            
            
            
            if self.productReorderDataSource.count > indexPath.row{

                let cellInfoArray = self.productReorderDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary

                var title = ""
                if let productTitle = cellInfoArray.object(forKey: "title") as? String {
                    title = (productTitle as NSString!) as String
                }


                let titleHeight : CGFloat = title.boundingRect(
                    with: CGSize(width: self.checkoutTableView.frame.size.width - 145, height: 99999),
                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 16)!],
                    context: nil
                    ).size.height

                heightByTitle += titleHeight
                return max(minHeight, max(heightByTitle, heightByPhoto))

            }
            }
            else{
            
        if self.cartDataSource.count > indexPath.row {
        if let cart = self.cartDataSource.object(at: (indexPath as NSIndexPath).row) as? Cart {
            if let project = ProjectHelper.getProject(cart.projectId){
                let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData) as! NSDictionary

                let coverWidth: CGFloat = CGFloat((productData.object(forKey: "cover_width") as! NSString).floatValue)
                let coverHeight: CGFloat = CGFloat((productData.object(forKey: "cover_height") as! NSString).floatValue)

                let wrapperWidth: CGFloat = 85.0
                let wrapperHeight = CGFloat(coverHeight / coverWidth) * wrapperWidth

                heightByPhoto += wrapperHeight

                var title : NSString!
                if project.title == ""{
                    if let productTitle = productData.object(forKey: "title") as? String {
                        title = productTitle as NSString?
                    }
                }
                else{
                    title = String(format: "\"%@\"", arguments: [project.title]) as NSString?
                }

                let titleHeight : CGFloat = title.boundingRect(
                    with: CGSize(width: tableView.frame.size.width - 145, height: 99999),
                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 16)!],
                    context: nil
                    ).size.height

                heightByTitle += titleHeight
            }

        }
        }
        return max(minHeight, max(heightByTitle, heightByPhoto))

        } //isReorder
            
        }
        if tableView == shippingTableView{
            let minHeight: CGFloat = 60
            var height: CGFloat = 8
            
            if self.shippingMethodDataSource.count > indexPath.row {
                if let cellInfoArray = self.shippingMethodDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
                    var cost = ""
                    var priceShipping = ""
                    var delivery = ""
                    
                    if let service = cellInfoArray.object(forKey: "service") as? String {
                        cost = service
                    }
                    if cost != Constant.shippingFree.capitalized {
                        cost += " "
                        
                        if let desc = cellInfoArray.object(forKey: "description") as? String {
                            cost += "(" + desc + ")"
                        }
                        
                        cost += " - "
                        
                        if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                            if prices.count > 0 {
                                if let price = prices.object(at: 0) as? NSDictionary {
                                    if let value = price.object(forKey: "value") as? NSNumber {
                                        //                                    cost += "IDR " + Globals.numberFormat(value)
                                        priceShipping = Globals.numberFormat(value)
                                    }
                                    if let etd = price.object(forKey: "etd") as? NSNumber {
                                        delivery = "\(etd)"
                                    }
                                }
                            }
                        }
                    }
                    
                    
                    height += NSString(string: cost).boundingRect(
                        with: CGSize(width: self.shippingTableView.frame.size.width - 49, height: 99999),
                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
                        attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 14)!],
                        context: nil
                        ).size.height
                    
                }
            }
            return max(minHeight, height)
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == checkoutTableView{
            
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkoutTableViewCell", for: indexPath)
        
        let coverWrapper: UIView = cell.viewWithTag(100)!
        let lblTitle: UILabel = cell.viewWithTag(200) as! UILabel
        let lblPhotoType: UILabel = cell.viewWithTag(300) as! UILabel
        let lblPrice: UILabel = cell.viewWithTag(400) as! UILabel
        let lblPhotoCount: UILabel = cell.viewWithTag(401) as! UILabel
        let lblQty: UILabel = cell.viewWithTag(500) as! UILabel
        let btnPlus: EditButton = cell.viewWithTag(600) as! EditButton
        let btnMinus: EditButton = cell.viewWithTag(700) as! EditButton
        let btnDelete: EditButton = cell.viewWithTag(900) as! EditButton
        let imageView: UIImageView = cell.viewWithTag(800) as! UIImageView
        
            if isReOrder{
                
                btnDelete.isHidden = true
                coverWrapper.isHidden = true
                imageView.isHidden = false
                
            if self.productReorderDataSource.count > indexPath.row{
                let cellInfoArray = self.productReorderDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
                if let title = cellInfoArray.object(forKey: "title") as? String{
                    lblTitle.text = title
                }

                if let cover_images = cellInfoArray.object(forKey: "cover_image") as? NSArray{
                    
                    for cover in cover_images {
                        let image = (cover as! NSDictionary).object(forKey: "image") as? NSDictionary
                        
                        if image != nil{
                            let cover_image = (cover as! NSDictionary).object(forKey: "image") as? NSDictionary
                            if let image_large = cover_image?.object(forKey: "large") as? String{
                                
                                print("IMAGE \(image_large)")
                                let urlImage = URL(string : image_large)
                                let session = URLSession.shared
                                let getImageFromUrl = session.dataTask(with: urlImage!){(data, response, error) in
                                    
                                    if let e = error{
                                        print("Error Occurred : \(e)")
                                    } else{
                                        if (response as? HTTPURLResponse) != nil{
                                            if let imageData = data{
                                                DispatchQueue.main.async {
                                                    let images = UIImage(data: imageData)
                                                    imageView.image = images
                                                }
                                            } else{
                                                print("Image file is corrupted")
                                            }
                                        }
                                        else{
                                            print("No response from server")
                                        }
                                    }
                                }
                                getImageFromUrl.resume()
                            }
                        }
                            
                        else{
                            
                            if let order_product_details = cellInfoArray.object(forKey: "order_product_detail") as? NSArray{
                                if let order_product = order_product_details.object(at: 0) as? NSDictionary{
                                    if let image = order_product.object(forKey: "image") as? NSDictionary{
                                        if let image_large = image.object(forKey: "large") as? String{
                                            
                                            print("IMAGE \(image_large)")
                                            let urlImage = URL(string : image_large)
                                            let session = URLSession.shared
                                            let getImageFromUrl = session.dataTask(with: urlImage!){(data, response, error) in
                                                
                                                if let e = error{
                                                    print("Error Occurred : \(e)")
                                                } else{
                                                    if (response as? HTTPURLResponse) != nil{
                                                        if let imageData = data{
                                                            DispatchQueue.main.async {
                                                                let images = UIImage(data: imageData)
                                                                imageView.image = images
                                                            }
                                                        } else{
                                                            print("Image file is corrupted")
                                                        }
                                                    }
                                                    else{
                                                        print("No response from server")
                                                    }
                                                }
                                            }
                                            getImageFromUrl.resume()
                                        }
                                    }
                                }
                            }
                        } //else
                        
                    } //for
                } // if
                
                if let photo_count = cellInfoArray.object(forKey: "count_photo") as? String{
                    lblPhotoCount.text = photo_count + " Photos"
                }
                if let photo_count = cellInfoArray.object(forKey: "count_photo") as? NSNumber{
                    lblPhotoCount.text = "\(photo_count) Photos"
                }

            if let photoType = cellInfoArray.object(forKey: "product_title") as? String{
                lblPhotoType.text = photoType
            } else{
                lblPhotoType.text = ""
            }

            if let price = cellInfoArray.object(forKey: "price") as? NSString {
                if price.doubleValue == 0 {
                    lblPrice.text = "FREE"
                }
                else{
                    lblPrice.text = "IDR " + Globals.numberFormat(NSNumber(value: price.doubleValue as Double))
                }
            }
            else{
                lblPrice.text = ""
            }

            if let qty = cellInfoArray.object(forKey: "qty") as? NSString {

//                var quantityDouble: Double = 0
                
                quantityOrder = Int(qty as String)!
//                quantityDouble = Double(quantityOrder)
                
                lblQty.text = "\(quantityOrder) "
                
//                totalProduct = total * quantityDouble
//
//                self.lblSubTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: totalProduct as Double))
//
                finalSum = grandTotal
                
//                loadDataCart()
//                grandTotal = 0
//                grandTotal = grandTotal + totalProduct
            
            }
            else {
                lblQty.text = ""
            }

                btnPlus.layer.cornerRadius = 5
                btnMinus.layer.cornerRadius = 5

            btnPlus.index = (indexPath as NSIndexPath).row
            btnMinus.index = (indexPath as NSIndexPath).row
//            btnDelete.index = (indexPath as NSIndexPath).row

            btnPlus.addTarget(self, action: #selector(btnPlusTouched(_:)), for: UIControlEvents.touchUpInside)
            btnMinus.addTarget(self, action: #selector(btnMinusTouched(_:)), for: UIControlEvents.touchUpInside)
//            btnDelete.addTarget(self, action: #selector(btnDeleteTouched(_:)), for: UIControlEvents.touchUpInside)
            
            }
            }
                
            else{
                
                coverWrapper.isHidden = false
                imageView.isHidden = true
                
        if self.cartDataSource.count > indexPath.row {
        if let cart = self.cartDataSource.object(at: (indexPath as NSIndexPath).row) as? Cart {
            if let project = ProjectHelper.getProject(cart.projectId){
                let projectDetails = ProjectHelper.getAllProjectDetails(cart.projectId)
                let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData) as! NSDictionary

                let cover = ProjectHelper.getCoverImage(cart.projectId, true)
                let coverWidth: CGFloat = CGFloat((productData.object(forKey: "cover_width") as! NSString).floatValue)
                let coverHeight: CGFloat = CGFloat((productData.object(forKey: "cover_height") as! NSString).floatValue)

                var wrapperWidth = coverWrapper.frame.size.width
                if let widthConstraint = Globals.getConstraint("width", view: coverWrapper) {
                    wrapperWidth = widthConstraint.constant
                }
                let wrapperHeight = CGFloat(coverHeight / coverWidth) * wrapperWidth

                if let heightConstraint = Globals.getConstraint("height", view: coverWrapper) {
                    heightConstraint.constant = wrapperHeight
                }

                MemomeHelper.setCoverView(coverWrapper, project: project, cover: cover, productData: productData, size: CGSize(width: wrapperWidth, height: wrapperHeight), extraTop: 0)

                if let productTitle = productData.object(forKey: "title") as? String {
                    lblPhotoType.text = productTitle
                }

                if project.title == ""{
                    if let productTitle = productData.object(forKey: "title") as? String {
                        lblTitle.text = productTitle
                    }
                }
                else{
                    lblTitle.text = project.title
                }

                lblPhotoCount.text = String(format: "%i Photos", arguments: [projectDetails!.count])

                if project.corporateVoucherId > 0 {
                    lblPhotoCount.text = lblPhotoCount.text! + "\nFree \(project.corporateVoucherQty)x"
                }

                lblPrice.text = "IDR " + Globals.numberFormat(NSNumber(value: cart.price as Double))
                lblQty.text = Globals.numberFormat(NSNumber(value: cart.qty as Int32))

                btnPlus.layer.cornerRadius = 5
                btnMinus.layer.cornerRadius = 5

                btnPlus.index = (indexPath as NSIndexPath).row
                btnMinus.index = (indexPath as NSIndexPath).row
                btnDelete.index = (indexPath as NSIndexPath).row

                btnPlus.addTarget(self, action: #selector(btnPlusTouched(_:)), for: UIControlEvents.touchUpInside)
                btnMinus.addTarget(self, action: #selector(btnMinusTouched(_:)), for: UIControlEvents.touchUpInside)
                btnDelete.addTarget(self, action: #selector(btnDeleteTouched(_:)), for: UIControlEvents.touchUpInside)
            }
        }

        } //cartDataSource
            } //
        return cell
            
        } //checkoutTableView
        
        if tableView == shippingTableView{
            let cell = self.shippingTableView.dequeueReusableCell(withIdentifier: "shippingMethodTableViewCell", for: indexPath)
            
            let imgCheck: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lblTitle: UILabel = cell.viewWithTag(2) as! UILabel
            let lblPrice: UILabel = cell.viewWithTag(3) as! UILabel
            let lblDelivery: UILabel = cell.viewWithTag(4) as! UILabel
            
            if self.shippingMethodDataSource.count > indexPath.row {
            if let cellInfoArray = self.shippingMethodDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
                var cost = ""
                var priceShipping = ""
                var delivery = ""
                
                if let service = cellInfoArray.object(forKey: "service") as? String {
                    cost = service
                    if cost == "REG" || cost == "CTC"{
                        cost = "REGULAR"
                    }else if cost == "YES" || cost == "CTCYES"{
                        cost = "EXPRESS"
                    }
                }
                
                if cost != Constant.shippingFree.capitalized {
                    cost += " "
                    
//                    if let desc = cellInfoArray.object(forKey: "description") as? String {
//                        cost += "(" + desc + ")"
//                    }
                    
                    if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                        if prices.count > 0 {
                            if let price = prices.object(at: 0) as? NSDictionary {
                                if let value = price.object(forKey: "value") as? NSNumber {
                                    priceShipping = "IDR " + Globals.numberFormat(value)
                                }
                                if let etd = price.object(forKey: "etd") as? String {
                                    if etd == "1-1"{
                                        delivery = "1-2 days"
                                    }else{
                                        delivery = "\(etd) days"
                                    }
                                }
                                
                            }
//                            if let price = prices.object(at: 0) as? NSDictionary {
//                                if let etd = price.object(forKey: "etd") as? NSNumber {
//                                    delivery = "\(etd)"
//                                }
//                            }
                        }
                    }
                    
                }
                
                lblTitle.text = cost
                lblPrice.text = priceShipping
                lblDelivery.text = delivery
            }
            
            if (indexPath as NSIndexPath).row == self.selectedShippingMethod {
                imgCheck.image = UIImage(named: "icon-radio-checked.png")
            }
            else{
                imgCheck.image = UIImage(named: "icon-radio-unchecked.png")
            }
                
            }
            
            return cell
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if tableView == shippingTableView {
            self.shippingCost = 0
            self.shippingEtd = "1-3"
            if (indexPath as NSIndexPath).row < self.shippingMethodDataSource.count {
                self.selectedShippingMethod = (indexPath as NSIndexPath).row
                self.reloadShippingMethodTableView()
                
                if let cellInfoArray = self.shippingMethodDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
                    
                    if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                        if prices.count > 0 {
                            
                            if let price = prices.object(at: 0) as? NSDictionary {
                                
                                let attributes: NSMutableDictionary = NSMutableDictionary()
                                
                                if let services = cellInfoArray.object(forKey: "service") as? String{
                                    attributes.setValue(services, forKey: "service")
                                }
                                
                                if let value = price.object(forKey: "value") as? NSNumber {
                                    
                                    if let service = price.object(forKey: "etd") as? String {
                                        var etd = service
                                        if etd == "1-1"{
                                            etd = "1-2"
                                            self.shippingEtd = etd
                                        }else{
                                        self.shippingEtd = etd
                                        }
                                    }
                                    else {
                                        self.shippingEtd = "1-3"
                                    }
                                    attributes.setValue(self.shippingEtd, forKey: "etd")
                                    
                                    self.shippingCost = value.doubleValue
                                    
                                    lblShipping.text = "IDR " + Globals.numberFormat(NSNumber(value: shippingCost as Double))
                                    attributes.setValue(value, forKey: "value")
                                    self.loadDataCart()
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func reloadShippingMethodTableView(){
        self.shippingTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.shippingTableView) {
            height.constant = self.shippingTableView.contentSize.height
        }
    }
    
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.dismissSelf()
    }
    
    
    @IBAction func btnAddressInputTouched(_ sender: AnyObject) {
        let attributes: NSMutableDictionary = NSMutableDictionary()
        if let address = self.selectedAddress {
            attributes.setValue(address, forKey: "address")
        }
        self.showViewController("AddressViewController", attributes: attributes)
    }
    
    @IBAction func btnNextTouched(_ sender: AnyObject) {
        if ControllerHelper.getHomeViewController()!.isUploading {
            Globals.showAlertWithTitle("Memome is uploading your images", message: "Sorry, we could not continue to check this order out. Currently memome is busy uploading your previous order. Please wait until your images uploaded, then you can check this order out.", viewController: self)
        }
        else {
            var isGood = true
            
            if isGood && self.selectedShippingMethod == -1 {
                Globals.showAlertWithTitle("Checkout Error", message: "Please select a shipping method.", viewController: self)
                isGood = false
            }
            
            if isGood && self.lblShipping.text == "IDR 0" {
                Globals.showAlertWithTitle("Checkout Error", message: "Please select a shipping method.", viewController: self)
                isGood = false
            }
            
            if isGood && self.grandTotal < self.minTransaction && !self.isJustCorporate() {
                Globals.showAlertWithTitle("Checkout Error", message: String(format: "Min transaction is IDR %@", arguments: [Globals.numberFormat(NSNumber(value: self.minTransaction as Double))]), viewController: self)
                isGood = false
            }
            
            if isGood {
                Globals.showConfirmAlertWithTitle("Checkout", message: "Are you sure you have entered your data correctly?", viewController: self, completion: { (action) -> Void in
                    self.payment(String(Date().timeIntervalSince1970))
                })
            }
        }

    }
    
    
    // general methods
    func loadDataAddress() {
        if !self.isLoading() {
            self.showLoading()
            Globals.getDataFromUrl(Globals.getApiUrl("users/view"),
                noCache: true,
                requestType: Globals.HTTPRequestType.http_GET,
                params: [
                    "clientId" : Globals.getProperty("clientId"),
                    "accessToken" : Globals.getDataSetting("accessToken")!
                ],
                completion: { (result) -> Void in
                    self.hideLoading()
                    
                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                        
                        self.addressDataSource.removeAllObjects()
                        if let addresses = result.object(forKey: "userAddresses") as? NSArray {
                            self.addressDataSource.addObjects(from: addresses as [AnyObject])
                            if addresses.count > 0 {
//                                if self.selectedAddress == nil {
                                    self.selectedAddress = addresses.object(at: 0) as? NSDictionary
//                                }
                                
                                self.showUseAddress()
                                
                                self.lblShipping.text = "IDR " + Globals.numberFormat(NSNumber(value: self.shippingCost as Double))
                                
                                self.loadAddress(self.selectedAddress!)
                            }
//                            self.btnAddressChoose.setDataSource(self.addressDataSource)
                            self.btnAddressEdit.setDataSource(self.addressDataSource)
                        }
                    }
                    else{
                        Globals.showAlertWithTitle("Profile Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                    }
            })
        }
    }
    
    func reloadPhotoTableView() {
        self.checkoutTableView.reloadData()
        
        if let height = Globals.getConstraint("height", view: self.checkoutTableView) {
            height.constant = self.checkoutTableView.contentSize.height
        }
    }
    
    func btnPlusTouched(_ sender: AnyObject){
        if let btn = sender as? EditButton {
            if isReOrder{
                if let cart = self.productReorderDataSource.object(at: btn.index) as? NSMutableDictionary{
                    
                    if let quantity = cart.object(forKey: "qty") as? String{
                        var qty = Int(quantity)! + 1
                        cart.setValue(String(qty), forKey: "qty")
                    }
                        self.loadDataCart()
                }
            }else{
                if let cart = self.cartDataSource.object(at: btn.index) as? Cart {
                    _ = CartHelper.updateCart(cart.id, qty: cart.qty+1)
                    self.loadDataCart()
                }
            }
        }
    }
    
    func btnMinusTouched(_ sender: AnyObject){
        if let btn = sender as? EditButton {
            if isReOrder{
                if let cart = self.productReorderDataSource.object(at: btn.index) as? NSMutableDictionary{
//                    orderQuantity -= 1
                    if let quantity = cart.object(forKey: "qty") as? String{
                        var qty = Int(quantity)! - 1
                        cart.setValue(String(qty), forKey: "qty")
                    }
                    self.loadDataCart()
                }
            }else{
            if let cart = self.cartDataSource.object(at: btn.index) as? Cart {
                if cart.qty > 1 {
                    _ = CartHelper.updateCart(cart.id, qty: cart.qty-1)
                    self.loadDataCart()
                }
            }
        }
    }
    }
    
    func btnDeleteTouched(_ sender: AnyObject){
        if let btn = sender as? EditButton {
            if let cart = self.cartDataSource.object(at: btn.index) as? Cart {
                Globals.showConfirmAlertWithTitle("Delete this item", message: "Are you sure want to delete this item?", viewController: self, completion: { (action) in
                    CartHelper.deleteFromCart(cart.id)
                    self.loadDataCart()
                })
            }
        }
    }
    
    
    @IBAction func shippingMethodTouchUpInside(_ sender: Any) {
    
        if self.namePickedAddress.text?.trim() != ""{
            self.shippingChecker(selectedAddress)
        }
        else{
            Globals.showAlertWithTitle("Checkout Error", message: "Please add an address", viewController: self)
        }
        
    }
    
    
    func shippingChecker(_ selectedAddress: NSDictionary?){
       self.selectedAddress = selectedAddress
        
        if let address = self.selectedAddress{
            let attributes: NSMutableDictionary = NSMutableDictionary()
            attributes.setValue(address, forKey: "address")
            
            self.showViewController("ShippingViewController", attributes: attributes)
        }
    }
    
    
    func txtAddressWrapperTouched(_ sender:UITapGestureRecognizer){
        self.txtAddress.becomeFirstResponder()
    }
    
    func loadDataCart(){
        
        if isReOrder{

            if let products = order_attribute.object(forKey: "order_product") as? NSArray{
                
                total = 0
                grandTotal = 0
                
                for product in products{
                    
                    var  subtotal: Double = 0
                    if let price = (product as! NSDictionary).object(forKey: "price") as? String{
                        subtotal = Double(price)!
                    }
                    if let qty = (product as! NSDictionary).object(forKey: "qty") as? String{
                        subtotal = subtotal * Double(qty)!
                        
                        total = total + subtotal
                        grandTotal = grandTotal + subtotal
                    }
                   
                }
            }
             self.lblSubTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: total as Double))
            
            lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc as Double))
            self.lblShipping.text = "IDR " + Globals.numberFormat(NSNumber(value: shippingCost as Double))
            
            if lblShipping.text != "IDR 0" {
                grandTotal += shippingCost
            }
            
            if lblDiscount.text != "IDR 0"{
                grandTotal -= disc
            }
            
            if self.lblDiscount.text == "IDR 0"{
                discountView.isHidden = true
                if let height = Globals.getConstraint("height", view: discountView){
                    height.constant = -8
                }
            }else{
                discountView.isHidden = false
                if let height = Globals.getConstraint("height", view: discountView) {
                    height.constant = 15
                }
            }
            
            self.lblTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: grandTotal as Double))
            print("\(grandTotal)")
            var etd: [NSString] = self.shippingEtd.split(separator: "-") as [NSString]
            if etd.count > 1 {
                btnArrivalEstimate.setTitle(GeneralHelper.calculateShipping(Date(), from: 5+Int(etd[0].intValue), to: 4+(2*Int(etd[1].intValue))), for: .normal)
            }
            else {
                btnArrivalEstimate.setTitle(GeneralHelper.calculateShipping(Date(), from: 4, to: 7), for: .normal)
            }
            self.reloadPhotoTableView()
        }
        else{
        self.cartDataSource.removeAllObjects()

        self.cartDataSource.addObjects(from: CartHelper.getCart() as [AnyObject])
        grandTotal = 0.0
        total = 0.0
        for item in self.cartDataSource {
            if let cart = item as? Cart {
                grandTotal += cart.total
                total += cart.total

                if let project = ProjectHelper.getProject(cart.projectId) {
                    if project.corporateVoucherId > 0 {
                        grandTotal -= Double(project.corporateVoucherQty) * cart.price
                    }
                }

            }
        }

        lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc as Double))
            
        self.lblShipping.text = "IDR " + Globals.numberFormat(NSNumber(value: shippingCost as Double))

        if lblShipping.text != "IDR 0" {
            grandTotal += shippingCost
        }

        if lblDiscount.text != "IDR 0"{
            grandTotal -= disc
        }
            
        if self.lblDiscount.text == "IDR 0"{
            discountView.isHidden = true
               if let height = Globals.getConstraint("height", view: discountView){
                height.constant = -8
            }
        }else{
            discountView.isHidden = false
                if let height = Globals.getConstraint("height", view: discountView) {
                height.constant = 15
                }
        }

        self.lblTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: grandTotal as Double))
            
        finalSum = grandTotal

        self.lblSubTotal.text = "IDR " + Globals.numberFormat(NSNumber(value: total as Double))

        var etd: [NSString] = self.shippingEtd.split(separator: "-") as [NSString]
        if etd.count > 1 {
            btnArrivalEstimate.setTitle(GeneralHelper.calculateShipping(Date(), from: 5+Int(etd[0].intValue), to: 4+(2*Int(etd[1].intValue))), for: .normal)
        }
        else {
            btnArrivalEstimate.setTitle(GeneralHelper.calculateShipping(Date(), from: 4, to: 7), for: .normal)
        }

        self.reloadPhotoTableView()
        }
    }
    
    func renderOrderData(_ order: NSDictionary){
        
        if let number = order.object(forKey: "id") as? String {
            self.orderId = number
        }
        
//        if let subtotal = order.object(forKey: "subtotal") as? String {
//            total = Double(subtotal)!
//            grandTotal = total
//        }

        var shipping = ""
        var shippingAddress = ""
        if let item = order.object(forKey: "shipping_first_name") as? String {
            shipping = shipping + item + " "
        }
        
        if let item = order.object(forKey: "shipping_last_name") as? String {
            shipping = shipping + item
        }
        
        namePickedAddress.text = shipping
        
        if let item = order.object(forKey: "shipping_address") as? String {
            addressPickedAddress.text = item
        }

        if let item = order.object(forKey: "shipping_city") as? String {
            shippingAddress = shippingAddress + item + ", "
        }
        
        if let item = order.object(forKey: "shipping_province") as? String {
            shippingAddress = shippingAddress + item + " - "
        }

        if let item = order.object(forKey: "shipping_postal_code") as? String {
            shippingAddress = shippingAddress + item
        }
        
        cityPickedAddress.text = shippingAddress

        if let item = order.object(forKey: "shipping_phone") as? String {
            phonePickedAddress.text = item
        }
        
    }
    
    
    func reloadReorderProductTableView(){
        
        self.checkoutTableView.reloadData()
        if let height = Globals.getConstraint("height", view: self.checkoutTableView){
            height.constant = self.checkoutTableView.contentSize.height
        }
    }
    
    func loadDataPayment() {
//        let payments = [
//            ["title" : Constant.paymentTransferName, "value" : Constant.paymentTransfer],
//            ["title" : Constant.paymentCreditCardName, "value" : Constant.paymentCreditCard]
//        ]
//        self.bsdPaymentMethod.viewController = self
//        self.bsdPaymentMethod.delegate = self
//        self.bsdPaymentMethod.defaultTitle = "Payment Method"
//        self.bsdPaymentMethod.title = "Payment Method"
//        self.bsdPaymentMethod.headerBackgroundColor = mainColor
//        self.bsdPaymentMethod.itemTintColor = mainColor
//        self.bsdPaymentMethod.titleFont = titleFont
//        self.bsdPaymentMethod.buttonFont = buttonFont
//        self.bsdPaymentMethod.hideDoneButton = true
//        self.bsdPaymentMethod.setup()
        
//        self.bsdPaymentMethod.setDataSource(NSMutableArray(array: payments))
//        self.bsdPaymentMethod.setSelectedIndex(0)
    }
    
    
    func setupSelectedAddress(_ selectedAddress: NSDictionary?){
        self.selectedAddress = selectedAddress
        
        if let address = self.selectedAddress {
            
            let attributes: NSMutableDictionary = NSMutableDictionary()
            attributes.setValue(address, forKey: "address")
            
            if let coupon = self.selectedCoupon {
                attributes.setValue(coupon, forKey: "coupon")
            }
            
            attributes.setValue(self.selectedPaymentType, forKey: "paymentType")
            attributes.setValue(self.selectedPaymentName, forKey: "paymentName")
            
            self.showViewController("CheckoutReviewViewController", attributes: attributes)
            
            
        }
        if let address = self.selectedAddress {
            
//            self.selectedAddressWrapper.isHidden = false
            
            if let firstName = address.object(forKey: "first_name") as? String {
//                self.lblSelectedAddressName.text = firstName
            }
            else{
//                self.lblSelectedAddressName.text = ""
            }
            
            if let lastName = address.object(forKey: "last_name") as? String {
//                self.lblSelectedAddressName.text = self.lblSelectedAddressName.text?.appendingFormat(" %@", lastName)
            }
            
            var stringAddress = ""
            if let a = address.object(forKey: "address") as? String {
                stringAddress = a + "\n"
            }
            
            var city = ""
            var province = ""
            var zipCode = ""
            if let item = address.object(forKey: "cityName") as? String {
                city = item
            }
            if let item = address.object(forKey: "provinceName") as? String {
                province = item
            }
            
            if let item = address.object(forKey: "postal_code") as? String {
                zipCode = item
            }
            
            var stringCity = ""
            if city != "" {
                stringCity += city
            }
            if province != "" {
                if stringCity != "" {
                    stringCity += ", "
                }
                stringCity += province
            }
            if zipCode != "" {
                if stringCity != "" {
                    stringCity += " - "
                }
                stringCity += zipCode
            }
            
            stringAddress += stringCity + "\n"
            
            if let phone = address.object(forKey: "phone") as? String {
                stringAddress += phone
            }
            
//            self.lblSelectedAddressDescription.text = stringAddress
        }
        else{
//            self.selectedAddressWrapper.isHidden = true
        }
    }
    
    func setCoupon(_ coupon: NSDictionary?){
        self.selectedCoupon = coupon
        
        if let selCoupon = self.selectedCoupon {
            
            discountDesc.text = "Discount Promo Code"
            
            if let code = selCoupon.object(forKey: "code") as? String {
                couponString = code
            }
            
            if let price = selCoupon.object(forKey: "price") as? NSString {
                disc = price.doubleValue
                if price.doubleValue > 0{
                    couponString += " (IDR " + Globals.numberFormat(NSNumber(value: disc)) + ")"
                    self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                    self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                }
            }
            else if let price = selCoupon.object(forKey: "price") as? NSNumber {
                disc = price.doubleValue
                if price.doubleValue > 0{
                    couponString += " (IDR " + Globals.numberFormat(NSNumber(value: disc)) + ")"
                    self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                    self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                }
            }
            if let percent = selCoupon.object(forKey: "percent") as? NSString {
                if percent.doubleValue > 0{
                    
                    disc = percent.doubleValue * (self.total) / 100
                    
                    couponString += " (" + Globals.numberFormat(NSNumber(value: percent.doubleValue)) + "%)"
                    
                    self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                    self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                }
                
            }
            else if let percent = selCoupon.object(forKey: "percent") as? NSNumber {
                if percent.doubleValue > 0{
                    couponString += " (" + Globals.numberFormat(NSNumber(value: percent.doubleValue)) + "%)"
                }
            }
            
            self.lbldiscountDesc.text = couponString
            
            if let price = selCoupon.object(forKey: "price") as? NSNumber {
                disc = price.doubleValue
                if price.doubleValue > 0{
                    self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: price.doubleValue))
                    self.lblDiscount.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                    self.lbldiscountValue.text = "IDR " + Globals.numberFormat(NSNumber(value: disc))
                }
            }
            
            self.couponWrapperView.isHidden = false
            if let height = Globals.getConstraint("height", view: self.couponWrapperView) {
                height.constant = 35
            }
            if let height = Globals.getConstraint("discountBottom", view: self.couponWrapperView) {
                height.constant = 8
            }
//            self.btnUseCoupon.isHidden = true
        }
        else{
            self.couponWrapperView.isHidden = true
            if let height = Globals.getConstraint("height", view: self.couponWrapperView) {
                height.constant = 0
            }
            if let height = Globals.getConstraint("discountBottom", view: self.couponWrapperView) {
                height.constant = 0
            }
//            self.btnUseCoupon.isHidden = false
        }
        
        loadDataCart()
        
    }
    
    func isJustCorporate() -> Bool {
        let carts = CartHelper.getCart()
        for item in carts {
            if let cart = item as? Cart {
                if let project = ProjectHelper.getProject(cart.projectId) {
                    if project.corporateVoucherId <= 0 || (project.corporateVoucherId > 0 && cart.qty > project.corporateVoucherQty) {
                        return false
                    }
                }
            }
        }
        return true
    }
    
    func loadAddress(_ address: NSDictionary) {

        let firstName = self.selectedAddress?.object(forKey: "first_name") as! String
        let lastName = self.selectedAddress?.object(forKey: "last_name") as! String
        let address = self.selectedAddress?.object(forKey: "address") as! String
        let phone = self.selectedAddress?.object(forKey: "phone") as! String
        let city = self.selectedAddress?.object(forKey: "cityName") as! String
        let province = self.selectedAddress?.object(forKey: "provinceName") as! String
        let postalCode = self.selectedAddress?.object(forKey: "postal_code") as! String
        
        namePickedAddress.textColor = UIColor.black
        namePickedAddress.font = UIFont.init(name: "MyriadPro-Bold", size: 15)
        addressPickedAddress.textColor = UIColor.black
        addressPickedAddress.font = UIFont.init(name: "MyriadPro-Regular", size: 15)
        cityPickedAddress.textColor = UIColor.black
        cityPickedAddress.font = UIFont.init(name: "MyriadPro-Regular", size: 15)
        phonePickedAddress.textColor = UIColor.black
        phonePickedAddress.font = UIFont.init(name: "MyriadPro-Regular", size: 15)
        self.namePickedAddress.text = "\(firstName) \(lastName)"
        self.addressPickedAddress.text = address
        self.cityPickedAddress.text = "\(city), \(province) - \(postalCode)"
        self.phonePickedAddress.text = phone
        
        self.loadDataShippingMethod()
    }
    
    
    func showUseAddress() {
        if addressDataSource.count > 0 {
            if let height = Globals.getConstraint("height", view: self.useAddressWrapper) {
                height.constant = 0
            }
            if let top = Globals.getConstraint("useAddressTop", view: self.useAddressWrapper) {
                top.constant = 0
            }
            self.pickedAddressView.isHidden = false
            self.shippingMethod.isHidden = false
            reloadShippingMethodTableView()
            if let height = Globals.getConstraint("height", view: self.lblShippingMethod) {
                height.constant = 18
            }
            if let height = Globals.getConstraint("shippingMethodTop", view: self.lblShippingMethod) {
                height.constant = 8
            }
            if let height = Globals.getConstraint("shippingMethodBottom", view: self.lblShippingMethod) {
                height.constant = 8
            }
        }
        else {
            if let height = Globals.getConstraint("height", view: self.useAddressWrapper) {
                height.constant = 8
            }
            if let top = Globals.getConstraint("useAddressTop", view: self.useAddressWrapper) {
                top.constant = 8
            }
            self.pickedAddressView.isHidden = true
            self.shippingMethod.isHidden = true
            reloadShippingMethodTableView()
            if let height = Globals.getConstraint("height", view: self.lblShippingMethod) {
                height.constant = 0
            }
            if let height = Globals.getConstraint("shippingMethodTop", view: self.lblShippingMethod) {
                height.constant = 0
            }
            if let height = Globals.getConstraint("shippingMethodBottom", view: self.lblShippingMethod) {
                height.constant = 0
            }
        }
    }
    
    func loadDataProvince() {
        if !self.isLoading() {
            self.showLoading()
            
            self.provinceDataSource.removeAllObjects()
            
            Globals.getDataFromUrl(Globals.getApiUrl("regions/getProvinces"),
               requestType: Globals.HTTPRequestType.http_GET,
               params: [
                "clientId" : Globals.getProperty("clientId")
                ],
               completion: { (result) -> Void in
                self.hideLoading()
                
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    
                    if let provinces = result.object(forKey: "provinces") as? NSArray {
                        self.provinceDataSource.addObjects(from: provinces as [AnyObject])
                    }
                    
//                    self.btnProvince.setDataSource(self.provinceDataSource)
                    
                    if self.selectedProvinceId != 0 {
                        var i = 0
                        for item in self.provinceDataSource {
                            if let province = item as? NSDictionary {
                                if let provinceId = province.object(forKey: "id") as? NSString {
                                    if provinceId.integerValue == self.selectedProvinceId {
                                        self.btnProvince.setSelectedIndex(i)
                                        self.btnProvince.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
                                    }
                                }
                            }
                            i += 1
                        }
                        
                        self.loadDataCity()
                    }
                    
                }
                else{
                    Globals.showAlertWithTitle("Province Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
            })
        }
    }
    
    func loadDataCity() {
        if !self.isLoading() {
            self.showLoading()
            
            self.cityDataSource.removeAllObjects()
            
            Globals.getDataFromUrl(Globals.getApiUrl("regions/getCities"),
               requestType: Globals.HTTPRequestType.http_GET,
               params: [
                "clientId" : Globals.getProperty("clientId"),
                "provinceId" : String(self.selectedProvinceId)
                ],
               completion: { (result) -> Void in
                self.hideLoading()
                
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    
                    if let cities = result.object(forKey: "cities") as? NSArray {
                        self.cityDataSource.addObjects(from: cities as [AnyObject])
                    }
                    
                    self.btnCity.setDataSource(self.cityDataSource)
                    
                    if self.selectedCityId != 0 {
                        var i = 0
                        for item in self.cityDataSource {
                            if let city = item as? NSDictionary {
                                if let cityId = city.object(forKey: "id") as? NSString {
                                    if cityId.integerValue == self.selectedCityId {
                                        self.btnCity.setSelectedIndex(i)
                                        self.btnCity.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
                                    }
                                }
                            }
                            i += 1
                        }
                    }
                }
                else{
                    Globals.showAlertWithTitle("City Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
            })
        }
        
    }
    

    
    @IBAction func btnScanTouchUpInside(_ sender: Any) {
        if txtCoupun.text != ""{
          
                 self.showViewController("RedeemVoucherViewController", attributes: ["coupon" : self.selectedCoupon, "isVoucher" : true])
        }
        else{
//             if isVoucher == false{
            self.showViewController("RedeemVoucherViewController", attributes: ["coupon" : self.selectedCoupon, "isVoucher" : false, "lblVoucher" : (Int(self.disc) != 0 ? String(Int(self.disc)) : "")])
//            }
        }
    }
    
    func didCaptureQRCodeWithContent(_ content: String) -> Bool {
        //        if content.substring(to: content.index(content.startIndex, offsetBy: 6)) == "MEMOME" {
        let code = content //content.substring(from: content.index(content.startIndex, offsetBy: 7))
        if !self.isLoading() {
            self.showLoading()
            
            Globals.getDataFromUrl(Globals.getApiUrl("vouchers/search"),
                                   requestType: Globals.HTTPRequestType.http_POST,
                                   params: [
                                    "clientId" : Globals.getProperty("clientId"),
                                    "accessToken" : Globals.getDataSetting("accessToken") as! String,
                                    "voucherCode" : code
                ],
                                   completion: { (result) -> Void in
                                    self.hideLoading()
                                    
                                    if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                                        
                                        if let voucher = result.object(forKey: "voucher") as? NSDictionary {
                                            
                                            self.setCoupon(voucher)
                                            
                                        }
                                        else{
                                            NSLog("error : %@", result)
                                            Globals.showAlertWithTitle("Coupon Error", message: "Unknown Error Occurred", viewController: self, completion:  nil)
                                        }
                                        
                                    }
                                    else{
                                        Globals.showAlertWithTitle("Coupon Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self, completion:  nil)
                                    }
            })
            return true
        }
        else {
            return false
        }
    }
    
    @IBAction func btnApplyTouchUpInside(_ sender: Any) {
        
//        if self.couponWrapperView.isHidden == true {
        if isVoucher == false{
            Globals.showAlertWithTitle("Sorry...", message: "You can not apply for both PROMO CODE and GIFT CARD", viewController: self)
        }else{
        if let _code = self.txtCoupun.text {
            var code = _code
            if code != "" {
//                if code.substring(to: code.index(code.startIndex, offsetBy: 6)) == "MEMOME" {
//                    code = String(format: "MEMOME-%@", code)
//                }
//                else{
                    let _ = self.didCaptureQRCodeWithContent(code)

//                }
            }
            else{
                couponWrapperView.isHidden = true
                if let height = Globals.getConstraint("height", view: self.couponWrapperView) {
                    height.constant = 0
                }
                if let height = Globals.getConstraint("discountBottom", view: self.couponWrapperView) {
                    height.constant = 0
                }
                grandTotal -= disc
                disc = 0
                Globals.showAlertWithTitle("Redeem Voucher Error", message: "Please enter a valid promo code", viewController: self)
                loadDataCart()
            }
        }
        else{
            couponWrapperView.isHidden = true
            if let height = Globals.getConstraint("height", view: self.couponWrapperView) {
                height.constant = 0
            }
            if let height = Globals.getConstraint("discountBottom", view: self.couponWrapperView) {
                height.constant = 0
            }
            grandTotal += disc
            disc = 0
            Globals.showAlertWithTitle("Redeem Voucher Error", message: "Please enter a valid promo code", viewController: self)
            loadDataCart()
        }

        }
    }

    func addButtonTouchUpInside(sender: Any){
        
        print("BUTTON TAPPED")
        
        self.showViewController("AddressViewController", attributes: NSDictionary())
    }
    
    @IBAction func buttonAddTouch(_ sender: Any) {
        self.showViewController("AddressViewController", attributes: NSDictionary())
    }
    
    func payment(_ orderID: String) {
        if !self.isLoading() {
            self.showLoading()
            
            var orders:[MidtransItemDetail] = []
            
            if let products = order_attribute.object(forKey: "order_product") as? NSArray{
                for product in products{
                    var title = ""
                    if let productTitle = (product as! NSDictionary).object(forKey: "product_title") as? String{
                        title = productTitle
                    }
                    
                    var productId = ""
                    if let product_id = (product as! NSDictionary).object(forKey: "product_id") as? String{
                        productId = product_id
                    }
                    
                    var price: NSNumber = NSNumber()
                    if let price_product = (product as! NSDictionary).object(forKey: "price") as? NSString{
                        let price1 = price_product.doubleValue
                        if price_product.doubleValue == 0 {

                        }
                        else{
                            price = NSNumber(value: price.doubleValue as Double)
                        }
                        let myInt = Int(price1)
                        price = NSNumber(value: myInt)
                    
                        
                    }
                    
                    var qty: NSNumber = NSNumber()
                    if let quantity = (product as! NSDictionary).object(forKey: "qty") as? String{
                        if let myInt = Int(quantity){
                        qty = NSNumber(value: myInt)
                        }
                    }
                    
                    let order = MidtransItemDetail.init(itemID: productId, name: title, price: price, quantity: qty)
                    
                    print(productId + title + "\(price)" + "\(qty)")
                    orders.append(order!)
                    print("order added")
                }
            }
            
            if !isReOrder{
                
            for item in CartHelper.getCart() {
                if let cart = item as? Cart {
                    if let project = ProjectHelper.getProject(cart.projectId) {
                        let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData) as! NSDictionary
                        var title = "";
                        if project.title == ""{
                            if let productTitle = productData.object(forKey: "title") as? String {
                                title = productTitle
                            }
                        }
                        else{
                            title = project.title
                        }
                        let order = MidtransItemDetail.init(itemID: String(project.productId), name: title, price: cart.price as NSNumber, quantity: cart.qty as NSNumber)
                        print(String(project.productId) + title + "\(cart.price as NSNumber)" + "\(cart.qty as NSNumber)")
                        orders.append(order!)
                        print("order added")
                    }
                }
            }
                
        }
            if let cellInfoArray = self.shippingMethodDataSource.object(at: self.selectedShippingMethod) as? NSDictionary {
                if let prices = cellInfoArray.object(forKey: "cost") as? NSArray {
                    if prices.count > 0 {
                        
                        if let price = prices.object(at: 0) as? NSDictionary {
                            
                            let value = price.object(forKey: "value") as? NSNumber ?? 0
                            let description = cellInfoArray.object(forKey: "description") as? String ?? ""
                            let service = cellInfoArray.object(forKey: "service") as? String ?? ""
                            
                            let code = cellInfoArray.object(forKey: "code") as? String ?? ""
                            
                            let order = MidtransItemDetail.init(itemID: service, name: description, price: value, quantity: 1)
                            
                            orders.append(order!)
                            print("shipping added")
                        }
                    }
                }
            }
            
            if let voucher = self.selectedCoupon {
                
                var disc: Double = 0
                var discInt: Int = 0
                var discount: NSNumber = NSNumber()
                if let price = voucher.object(forKey: "price") as? NSString {
                    if price.doubleValue > 0{
                        disc = -price.doubleValue
                    }
                }
                if let percent = voucher.object(forKey: "percent") as? NSString {
                    if percent.doubleValue > 0{
                        disc = -percent.doubleValue * (self.total) / 100
                    }
                }
                
                let id = voucher.object(forKey: "id") as? String ?? ""
                
                let code = voucher.object(forKey: "code") as? String ?? ""
                
                discInt = Int(disc)
                discount = NSNumber(value: discInt)
                
                let order = MidtransItemDetail.init(itemID: id, name: code, price: discount, quantity: 1)
                print(id + code + "\(discount)")
                orders.append(order!)
                print("voucher added")
                
            }
            
            if codes != ""{
                
                var discInt: Int = 0
                var discount: NSNumber = NSNumber()
                discInt = Int(-disc)
                discount = NSNumber(value: discInt)
                
//                let order = MidtransItemDetail.init(itemID: "1", name: codes, price: discount, quantity: 1)
                
                let order = MidtransItemDetail.init(itemID: self.codes_id, name: self.codes_name, price: discount, quantity: 1)
                print(self.codes_name + "\(discount)")
                orders.append(order!)
                print("voucher added")
                
            }
            
            let customerAddress = MidtransAddress.init(firstName: self.selectedAddress!.object(forKey: "first_name") as! String, lastName: self.selectedAddress!.object(forKey: "last_name") as! String, phone: self.selectedAddress!.object(forKey: "phone") as! String, address: self.selectedAddress!.object(forKey: "address") as! String, city: self.selectedAddress!.object(forKey: "cityName") as! String, postalCode: self.selectedAddress!.object(forKey: "postal_code") as! String, countryCode: "IDN")
            
            let customerDetail = MidtransCustomerDetails.init(firstName: self.selectedAddress!.object(forKey: "first_name") as! String, lastName: self.selectedAddress!.object(forKey: "last_name") as! String, email: self.selectedAddress!.object(forKey: "email") as! String, phone: self.selectedAddress!.object(forKey: "phone") as! String, shippingAddress: customerAddress, billingAddress: customerAddress)
            
            let transactionDetail = MidtransTransactionDetails.init(orderID: orderID, andGrossAmount: self.finalSum as NSNumber)
            print(orderID + "\(self.finalSum as NSNumber)")
            //            Globals.showWebViewWithUrl(Constant.getPaymentUrl(orderID, grandTotal: String(self.total)), parentView: self, title: "PAYMENT")
            
            print("\(transactionDetail) /n \(orders)")
            
            MidtransMerchantClient.shared().requestTransactionToken(with: transactionDetail!, itemDetails: orders, customerDetails: customerDetail) { (response, error) in
                if (response != nil) {
                    self.hideLoading()
                    
                    MidtransCreditCardConfig.shared().paymentType = .oneclick
                    MidtransCreditCardConfig.shared().saveCardEnabled = true
                    
                    //1-click need token storage enabled
                    MidtransCreditCardConfig.shared().tokenStorageEnabled = true
                    
                    //1-click need 3ds enabled
                    MidtransCreditCardConfig.shared().secure3DEnabled = true
                    
                    //handle response
                    let vc = MidtransUIPaymentViewController.init(token: response)
                    vc?.paymentDelegate = self
                    self.present(vc!, animated: true, completion: nil)
                }
                else {
                    self.hideLoading()
                    print("error request transaction token \(error.debugDescription)")
                    //handle error
                }
            }
        }
    }
    
    func submit(_ status: String) {
        if !self.isLoading() {
            self.showLoading()
            
            if isReOrder{
                submitReorder()
            } else{
            
            let params = NSMutableDictionary()
            
            let shipping = self.shippingMethodDataSource.object(at: self.selectedShippingMethod) as! NSDictionary
            
            params.setValue(Globals.getProperty("clientId"), forKey: "clientId")
            params.setValue(Globals.getDataSetting("accessToken"), forKey: "accessToken")
            params.setValue(self.selectedAddress!.object(forKey: "province_id") as! String, forKey: "provinceId")
            params.setValue(self.selectedAddress!.object(forKey: "city_id") as! String, forKey: "cityId")
            params.setValue(self.selectedAddress!.object(forKey: "first_name") as! String, forKey: "firstName")
            params.setValue(self.selectedAddress!.object(forKey: "last_name") as! String, forKey: "lastName")
            params.setValue(self.selectedAddress!.object(forKey: "address") as! String, forKey: "address")
            params.setValue(self.selectedAddress!.object(forKey: "provinceName") as! String, forKey: "province")
            params.setValue(self.selectedAddress!.object(forKey: "cityName") as! String, forKey: "city")
            params.setValue(self.selectedAddress!.object(forKey: "postal_code") as! String, forKey: "postalCode")
            params.setValue(self.selectedAddress!.object(forKey: "phone") as! String, forKey: "phone")
            params.setValue(self.selectedAddress!.object(forKey: "email") as! String, forKey: "email")
            params.setValue(self.paymentType, forKey: "paymentType")
            params.setValue(transactionId, forKey: "transactionId")
            
            let code = shipping.object(forKey: "code") as? String
            if let service = shipping.object(forKey: "service") as? String{
                params.setValue(code! + " - " + service, forKey: "shippingType")
            }
            
            params.setValue(String(0), forKey: "shippingPrice")
            if shipping.object(forKey: "code") as! String != Constant.shippingFree {
                if let prices = shipping.object(forKey: "cost") as? NSArray {
                    if prices.count > 0 {
                        if let price = prices.object(at: 0) as? NSDictionary {
                            if let value = price.object(forKey: "value") as? NSNumber {
                                params.setValue(String(value.doubleValue), forKey: "shippingPrice")
                            }
                        }
                    }
                }
            }
            
            params.setValue(String(0), forKey: "shippingEtd")
            if shipping.object(forKey: "code") as! String != Constant.shippingFree {
                if let prices = shipping.object(forKey: "cost") as? NSArray {
                    if prices.count > 0 {
                        if let price = prices.object(at: 0) as? NSDictionary {
                            if let value = price.object(forKey: "etd") as? String {
                                params.setValue(value, forKey: "shippingEtd")
                            }
                        }
                    }
                }
            }
            
            if let voucher = self.selectedCoupon {
                params.setValue(voucher.object(forKey: "code") as! String, forKey: "voucherCode")
                params.setValue(voucher.object(forKey: "id") as! String, forKey: "voucherId")
                var disc: Double = 0
                if let price = voucher.object(forKey: "price") as? NSString {
                    if price.doubleValue > 0{
                        disc = price.doubleValue
                    }
                }
                if let percent = voucher.object(forKey: "percent") as? NSString {
                    if percent.doubleValue > 0{
                        disc = percent.doubleValue * (self.total) / 100
                    }
                }
                params.setValue(String(disc), forKey: "voucherTotal")
            }
                
//            if codes != ""{
//
//                params.setValue(self.codes_name, forKey: "voucherCode")
//                params.setValue(self.codes_id, forKey: "voucherId")
//                params.setValue(String(disc), forKey: "voucherTotal")
//
//            }
            
            let orders = NSMutableArray()
            let orderDetails = NSMutableArray()
            
            var sumReorder = 0
                
            for item in CartHelper.getCart() {
                if let cart = item as? Cart {
                    if let project = ProjectHelper.getProject(cart.projectId) {
                        let order = NSMutableDictionary()
                        order.setValue(String(project.id), forKey: "project_id")
                        order.setValue(String(project.productId), forKey: "product_id")
                        
                        if cart.reorderId != nil && cart.reorderId != 0 {
                            order.setValue(String(cart.reorderId), forKey: "reorder_id")
                        }else{
                            order.setValue(String(0), forKey: "reorder_id")
                            sumReorder += 1
                        }
                    
                        order.setValue(project.title, forKey: "title")
                        order.setValue(project.subtitle, forKey: "subtitle")
                        order.setValue(String(cart.qty), forKey: "qty")
                        order.setValue(String(project.corporateVoucherId), forKey: "corporate_voucher_id")
                        order.setValue(String(project.corporateVoucherCode), forKey: "corporate_voucher_code")
                        
                        var hasCover = false
                        if let _ = ProjectHelper.getCoverImage(project.id) {
                            hasCover = true
                        }
                        
                        
                        if let details = ProjectHelper.getAllProjectDetails(project.id) {
                            var i=0
                            let productDetails = NSKeyedUnarchiver.unarchiveObject(with: project.productDetailData) as! NSArray
                            
                            if hasCover {
                                i += 1
                            }
                            
                            var x = 0
                            while i < details.count {
                                if let projectDetail = details.object(at: i) as? ProjectDetail {
                                    let productDetail = productDetails.object(at: x) as! NSDictionary
                                    let orderDetail = NSMutableDictionary()
                                    orderDetail.setValue(String(project.id), forKey: "project_id")
                                    orderDetail.setValue(String(projectDetail.id), forKey: "project_detail_id")
                                    orderDetail.setValue(productDetail.object(forKey: "id") as! String, forKey: "product_detail_id")
                                    
                                    orderDetails.add(orderDetail)
                                    
                                }
                                i += 1
                                x += 1
                            }
                            
                        }
                        
                        orders.add(order)
                    }
                }
            }
                
                
            params.setValue(String(data: try! JSONSerialization.data(withJSONObject: orders, options: JSONSerialization.WritingOptions(rawValue: 0)), encoding: String.Encoding.utf8), forKey: "orderProduct")
            params.setValue(String(data: try! JSONSerialization.data(withJSONObject: orderDetails, options: JSONSerialization.WritingOptions(rawValue: 0)), encoding: String.Encoding.utf8), forKey: "orderProductDetail")
                
//            if isReOrder{
//                params.setValue(self.orderId, forKey: "orderid_reorder")
//            } else{
                params.setValue("0", forKey: "orderid_reorder")
//            }
            
            Globals.getDataFromUrl(Globals.getApiUrl("orders/add"),
               requestType: Globals.HTTPRequestType.http_POST,
               params: params,
               completion: { (result) -> Void in
                self.hideLoading()
                
                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                    
                    if let order = result.object(forKey: "order") as? NSDictionary {
                        if let id = order.object(forKey: "id") as? String {
                            if id == "0" {
                                Globals.showAlertWithTitle("Checkout Error", message: "Checkout failed. Please try again.", viewController: self)
                            }
                            else{
                                
                                ControllerHelper.getHomeViewController()?.uploadingCart.removeAllObjects()
                                
                                var i=0;
                                for item in CartHelper.getCart() {
                                    
                                    if let cart = item as? Cart {
                                        
                                        if let project = ProjectHelper.getProject(cart.projectId) {
                                            ControllerHelper.getHomeViewController()?.uploadingCart.add(project)
                                        }
                                        
                                    }
                                    
                                    i += 1
                                }
                                
                                ControllerHelper.getHomeViewController()?.uploadingGrandTotal = self.grandTotal
                                ControllerHelper.getHomeViewController()?.uploadingPaymentType = self.paymentType
                                
                                for item in self.cartDataSource {
                                    if let cart = item as? Cart {
                                        ProjectHelper.deleteCorporateVoucher(cart.projectId)
                                        
                                    }
                                }
                                
                                CartHelper.clearCart()
                                
                                var orderNumber = id
                                if let number = order.object(forKey: "number") as? String {
                                    orderNumber = number
                                }
                                
                                if sumReorder == 0{
                                    self.showViewController("MyPurchasesViewController", attributes: nil)

                                    }
                                    else{
                                    self.showHomeViewController()
                                    if let homeVC = ControllerHelper.getHomeViewController() {
                                        homeVC.showViewController("ProgressViewController", attributes: [
                                            "orderId" : id,
                                            "orderNumber" : orderNumber,
                                            "grandTotal" : String(self.grandTotal),
                                            "paymentType" : self.paymentType
                                            //                                        "isReorder" : self.isReOrder
                                            ])
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
                else{
                    Globals.showAlertWithTitle("Checkout Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                }
            })
                
            } //else
        }
    }
    
    func submitReorder(){
        let params = NSMutableDictionary()
        
        let shipping = self.shippingMethodDataSource.object(at: self.selectedShippingMethod) as! NSDictionary
        
        params.setValue(Globals.getProperty("clientId"), forKey: "clientId")
        params.setValue(Globals.getDataSetting("accessToken"), forKey: "accessToken")
        params.setValue(self.selectedAddress!.object(forKey: "province_id") as! String, forKey: "provinceId")
        params.setValue(self.selectedAddress!.object(forKey: "city_id") as! String, forKey: "cityId")
        params.setValue(self.selectedAddress!.object(forKey: "first_name") as! String, forKey: "firstName")
        params.setValue(self.selectedAddress!.object(forKey: "last_name") as! String, forKey: "lastName")
        params.setValue(self.selectedAddress!.object(forKey: "address") as! String, forKey: "address")
        params.setValue(self.selectedAddress!.object(forKey: "provinceName") as! String, forKey: "province")
        params.setValue(self.selectedAddress!.object(forKey: "cityName") as! String, forKey: "city")
        params.setValue(self.selectedAddress!.object(forKey: "postal_code") as! String, forKey: "postalCode")
        params.setValue(self.selectedAddress!.object(forKey: "phone") as! String, forKey: "phone")
        params.setValue(self.selectedAddress!.object(forKey: "email") as! String, forKey: "email")
        params.setValue(self.paymentType, forKey: "paymentType")
        params.setValue(transactionId, forKey: "transactionId")
        
        let code = shipping.object(forKey: "code") as? String
        if let service = shipping.object(forKey: "service") as? String{
            params.setValue(code! + " - " + service, forKey: "shippingType")
        }
        
        params.setValue(String(0), forKey: "shippingPrice")
        if shipping.object(forKey: "code") as! String != Constant.shippingFree {
            if let prices = shipping.object(forKey: "cost") as? NSArray {
                if prices.count > 0 {
                    if let price = prices.object(at: 0) as? NSDictionary {
                        if let value = price.object(forKey: "value") as? NSNumber {
                            params.setValue(String(value.doubleValue), forKey: "shippingPrice")
                        }
                    }
                }
            }
        }
        
        params.setValue(String(0), forKey: "shippingEtd")
        if shipping.object(forKey: "code") as! String != Constant.shippingFree {
            if let prices = shipping.object(forKey: "cost") as? NSArray {
                if prices.count > 0 {
                    if let price = prices.object(at: 0) as? NSDictionary {
                        if let value = price.object(forKey: "etd") as? String {
                            params.setValue(value, forKey: "shippingEtd")
                        }
                    }
                }
            }
        }
        
        if let voucher = self.selectedCoupon {
            params.setValue(voucher.object(forKey: "code") as! String, forKey: "voucherCode")
            params.setValue(voucher.object(forKey: "id") as! String, forKey: "voucherId")
            var disc: Double = 0
            if let price = voucher.object(forKey: "price") as? NSString {
                if price.doubleValue > 0{
                    disc = price.doubleValue
                }
            }
            if let percent = voucher.object(forKey: "percent") as? NSString {
                if percent.doubleValue > 0{
                    disc = percent.doubleValue * (self.total) / 100
                }
            }
            params.setValue(String(disc), forKey: "voucherTotal")
        }
        
        
        let orders = NSMutableArray()
        let orderDetails = NSMutableArray()
        

        if let products = order_attribute.object(forKey: "order_product") as? NSArray{
            for product in products{
                
            let order = NSMutableDictionary()
            
                if let project_id = (product as! NSDictionary).object(forKey: "id") as? String{
                order.setValue(project_id, forKey: "project_id")
            }
            if let product_id = (product as! NSDictionary).object(forKey: "product_id") as? String{
                order.setValue(product_id, forKey: "product_id")
            }
               
                order.setValue(String(0), forKey: "reorder_id")
                
            if let title = (product as! NSDictionary).object(forKey: "title") as? String{
                order.setValue(title, forKey: "title")
            }
            if let subtitle = (product as! NSDictionary).object(forKey: "subtitle") as? String{
                order.setValue(subtitle, forKey: "subtitle")
            }
            
            if let qty = (product as! NSDictionary).object(forKey: "qty") as? String{
                order.setValue(qty, forKey: "qty")
            }
            if let corporate_voucher_id = (product as! NSDictionary).object(forKey: "corporate_voucher_id") as? String{
                order.setValue(corporate_voucher_id, forKey: "corporate_voucher_id")
            }
            if let corporate_voucher_code = (product as! NSDictionary).object(forKey: "corporate_voucher_code") as? String{
                order.setValue(corporate_voucher_code, forKey: "corporate_voucher_code")
            }
            
            orders.add(order)
                
            }
        }
        
       
            let orderDetail = NSMutableDictionary()
            orderDetail.setValue("30", forKey: "project_id")
            orderDetail.setValue("176", forKey: "project_detail_id")
            orderDetail.setValue("51", forKey: "product_detail_id")
            
            orderDetails.add(orderDetail)
        if order_attribute.object(forKey: "orderid_reorder") as? String != "0"{
            let orderid_reorder = order_attribute.object(forKey: "orderid_reorder") as? String
            params.setValue(orderid_reorder, forKey: "orderid_reorder")
        }else{
        params.setValue(self.orderId, forKey: "orderid_reorder")
        }
        
        params.setValue(String(data: try! JSONSerialization.data(withJSONObject: orders, options: JSONSerialization.WritingOptions(rawValue: 0)), encoding: String.Encoding.utf8), forKey: "orderProduct")
        params.setValue(String(data: try! JSONSerialization.data(withJSONObject: orderDetails, options: JSONSerialization.WritingOptions(rawValue: 0)), encoding: String.Encoding.utf8), forKey: "orderProductDetail")
        
        
        Globals.getDataFromUrl(Globals.getApiUrl("orders/add"),
                               requestType: Globals.HTTPRequestType.http_POST,
                               params: params,
                               completion: { (result) -> Void in
                                self.hideLoading()
                                
                                if(Int((result.object(forKey: "message") as! NSDictionary).object(forKey: "code") as! NSNumber) == 200){
                                    
                                    if let order = result.object(forKey: "order") as? NSDictionary {
                                        if let id = order.object(forKey: "id") as? String {
                                            if id == "0" {
                                                Globals.showAlertWithTitle("Checkout Error", message: "Checkout failed. Please try again.", viewController: self)
                                            }
                                            else{
                                                
                                                ControllerHelper.getHomeViewController()?.uploadingCart.removeAllObjects()
                                                
                                                if let products = self.order_attribute.object(forKey: "order_product") as? NSArray{
                                                    for product in products{
                                                        if let project_id = (product as! NSDictionary).object(forKey: "id") as? String{ ControllerHelper.getHomeViewController()?.uploadingCart.add(project_id)
                                                        }
                                                    }
                                                }
                                                
                                                ControllerHelper.getHomeViewController()?.uploadingGrandTotal = self.grandTotal
                                                ControllerHelper.getHomeViewController()?.uploadingPaymentType = self.paymentType
                                                
                                                for item in self.cartDataSource {
                                                    if let cart = item as? Cart {
                                                        ProjectHelper.deleteCorporateVoucher(cart.projectId)
                                                        
                                                    }
                                                }
                                                
                                                CartHelper.clearCart()
                                                
                                                var orderNumber = id
                                                if let number = order.object(forKey: "number") as? String {
                                                    orderNumber = number
                                                }
                                                
                                                self.showViewController("MyPurchasesViewController", attributes: nil)
                                                self.dismissSelf()
                                            }
                                        }
                                    }
                                    
                                    
                                }
                                else{
                                    Globals.showAlertWithTitle("Checkout Error", message: (result.object(forKey: "message") as! NSDictionary).object(forKey: "content") as! String, viewController: self)
                                }
        })
        
    
    }
    
    // MARK: - MidtransUIPaymentViewControllerDelegate
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, save result: MidtransMaskedCreditCard!) {
        print("save masked credit card")
    }
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, saveCardFailed error: Error!) {
        print("save credit card")
    }
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentPending result: MidtransTransactionResult!) {
        print("payment pending")
        self.paymentType = result.paymentType
        self.transactionId = result.transactionId
        self.submit("pending")
    }
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentSuccess result: MidtransTransactionResult!) {
        print("payment success")
        self.paymentType = result.paymentType
        self.transactionId = result.transactionId
        self.submit("success")
    }
    
    func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentFailed error: Error!) {
        print("payment failed")
        Globals.showAlertWithTitle("Payment Failed", message: "Please use another payment method", viewController: self)
    }
    
    func paymentViewController_paymentCanceled(_ viewController: MidtransUIPaymentViewController!) {
        print("payment cancelled")
        Globals.showAlertWithTitle("Payment Canceled", message: "Please make a payment if you want to proceed", viewController: self)
    }
}
