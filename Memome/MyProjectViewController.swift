//
//  MyProjectViewController.swift
//  Memome
//
//  Created by Bobby Stenly Irawan on 1/25/16.
//  Copyright © 2016 Trio Digital Agency. All rights reserved.
//

import UIKit

class MyProjectViewController: BaseViewController, BSSliderDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var projectScrollView: UIScrollView!
    @IBOutlet weak var projectContentScrollView: UIView!
    @IBOutlet weak var projectSlider: BSSlider!
    
    @IBOutlet weak var imgBgProject: UIImageView!
    
    @IBOutlet weak var lblCreatedAt: UILabel!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var projectDetailTableView: UITableView!
    @IBOutlet weak var btnAddToCart: UIButton!
    
    var projectDataSource: NSMutableArray! = NSMutableArray()
    var projectDetailDataSource: NSMutableArray! = NSMutableArray()
    var selectedProject: Project?
    
    var highlightColor: UIColor = UIColor.darkGray//(red: 80.0/255.0, green: 210.0/255.0, blue: 194.0/255.0, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.btnShoppingCart = self.btnCart
        self.initShoppingCartButton()
        
        self.btnView.layer.borderColor = self.highlightColor.cgColor
        self.btnView.layer.borderWidth = 1.0
        self.btnView.layer.cornerRadius = self.btnView.frame.size.height / 2.0
        
        self.btnDelete.layer.borderColor = self.highlightColor.cgColor
        self.btnDelete.layer.borderWidth = 1.0
        self.btnDelete.layer.cornerRadius = self.btnView.frame.size.height / 2.0
        
        self.projectSlider.autoplay = false
        self.projectSlider.setDelegate(self)
        
        self.projectDetailTableView.register(UINib(nibName: "ProductDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "productDetailTableViewCell")
        
        self.setEmpty()
        
        self.loadDataProject()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.projectSlider.reloadSlides()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // BSSliderDataSource
    func setupSlides(_ slider: BSSlider) {
        if let slidesContainer = slider.getSlidesContainer() {
            let width: CGFloat = slider.bounds.size.width
            let height: CGFloat = slider.bounds.size.height
            var i: CGFloat = 0
            for item in self.projectDataSource {
                if let project = item as? Project {
                    let contents = Bundle.main.loadNibNamed("ProjectSlide", owner: self, options: nil)
                    let slide: UIView = contents!.last as! UIView
                    slide.frame = CGRect(x: i * width, y: 0, width: width, height: height)
                    slide.clipsToBounds = true
                    
                    let slideContent: UIView = slide.viewWithTag(1)!
                    let coverWrapper: UIView = slide.viewWithTag(3)!
                    
                    let cover = ProjectHelper.getCoverImage(project.id)
                    
                    let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData as Data) as! NSDictionary
                    let coverWidth: CGFloat = CGFloat((productData.object(forKey: "cover_width") as! NSString).floatValue)
                    let coverHeight: CGFloat = CGFloat((productData.object(forKey: "cover_height") as! NSString).floatValue)
                    
                    let wrapperWidth = width * 0.53
                    let wrapperHeight = CGFloat(coverHeight / coverWidth) * wrapperWidth
                    
                    if let widthConstraint = Globals.getConstraint("width", view: coverWrapper) {
                        widthConstraint.constant = wrapperWidth
                    }
                    
                    if let heightConstraint = Globals.getConstraint("height", view: coverWrapper) {
                        heightConstraint.constant = wrapperHeight
                    }
                    
                    if let bottomConstraint = Globals.getConstraint("coverViewWrapperBottom", view: slideContent) {
                        bottomConstraint.constant = width * 0.115
                    }
                    
                    MemomeHelper.setCoverView(coverWrapper, project: project, cover: cover, productData: productData, size: CGSize(width: wrapperWidth, height: wrapperHeight))
                    
                    
                    slidesContainer.addSubview(slide)
                }
                
                
                i += 1
            }
        }
    }
    
    func onSlideChanged(_ slider: BSSlider, index: Int) {
        self.projectDetailDataSource.removeAllObjects()
        
//        self.setEmpty()
        
        if let project = self.projectDataSource.object(at: index) as? Project {
            let productData = NSKeyedUnarchiver.unarchiveObject(with: project.productData as Data) as! NSDictionary
            
            if project.corporateVoucherId > 0 {
                self.projectDetailDataSource.add([
                    "icon" : "icon-about-us.png",
                    "text" : String(format: "Free \(project.corporateVoucherQty) print - \(project.corporateVoucherName)")
                    ])
            }
            
            var title: String! = ""
            if project.title != "" {
                title = String(format: "\"%@\" ", arguments: [project.title])
            }
            
            if let productName = productData.object(forKey: "title") as? String {
                title = title + productName.capitalized
            }
            
            self.projectDetailDataSource.add([
                "icon" : "icon-my-project.png",
                "text" : title
                ])
            
            var count = 0
            if let details = ProjectHelper.getAllProjectDetails(project.id) {
                count = details.count
            }
            self.projectDetailDataSource.add([
                "icon" : "icon-images.png",
                "text" : String(format: "%i photos", arguments: [count])
                ])
            
            if let size = productData.object(forKey: "size") as? String {
                self.projectDetailDataSource.add([
                    "icon" : "icon-size.png",
                    "text" : size
                    ])
            }
            
            var hasDiscount: Bool = false
            if let priceDiscount = productData.object(forKey: "special_price") as? NSString {
                if priceDiscount.doubleValue > Double(0) {
                    
                    hasDiscount = true
                    let discountPrice = Globals.currencyFormat(NSNumber(value: priceDiscount.doubleValue as Double))
                    var normalPrice = ""
                    if let price = productData.object(forKey: "price") as? NSString {
                        normalPrice = Globals.currencyFormat(NSNumber(value: price.doubleValue as Double))
                    }
                    
                    let price = NSMutableAttributedString(string: String(format: "%@     %@", arguments: [normalPrice, discountPrice]))
                    
                    let normalPriceLength = NSString(string: normalPrice).length
                    price.addAttribute(NSStrikethroughStyleAttributeName,
                        value: NSUnderlineStyle.styleSingle.rawValue,
                        range: NSRange(location: 0, length: normalPriceLength))
                    price.addAttribute(NSForegroundColorAttributeName,
                        value: UIColor(white: 0.3, alpha: 1),
                        range: NSRange(location: 0, length: normalPriceLength))
                    
                    self.projectDetailDataSource.add([
                        "icon" : "icon-payment.png",
                        "attributedText" : price
                        ])
                }
            }
            
            if !hasDiscount {
                if let price = productData.object(forKey: "price") as? NSString {
                    self.projectDetailDataSource.add([
                        "icon" : "icon-payment.png",
                        "text" : Globals.currencyFormat(NSNumber(value: price.doubleValue as Double))
                        ])
                }
            }
            
            if let info = productData.object(forKey: "info") as? String {
                self.projectDetailDataSource.add([
                    "icon" : "icon-about-us.png",
                    "text" : info
                    ])
            }
            
            let dateComponent = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day,NSCalendar.Unit.month,NSCalendar.Unit.year], from: project.createdAt as Date)
            self.lblCreatedAt.text = String(format: "Created at %i %@ %i", arguments: [
                dateComponent.day!,
                Globals.getMonthName(dateComponent.month!),
                dateComponent.year!
                ])
            self.projectSlider.isHidden = false
            self.imgBgProject.isHidden = true
            
            self.selectedProject  = project
        }
        
        self.reloadProjectDetailTableView()
    }
    
    
    //Table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.projectDetailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let maxFloatHeight : CGFloat = 9999
        let height : CGFloat = 32
        let labelWidth : CGFloat = tableView.frame.size.width - 92
        let cellInfoArray : NSDictionary = self.projectDetailDataSource.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        var title : NSString! = ""
        if let temp = cellInfoArray.object(forKey: "text") as? NSString {
            title = temp
        }
        else if let temp = cellInfoArray.object(forKey: "attributedText") as? NSAttributedString {
            title = temp.string as NSString
        }
        
        let titleHeight : CGFloat = title.boundingRect(
            with: CGSize(width: labelWidth, height: maxFloatHeight),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 14)!],
            context: nil
            ).size.height
        
        return max(height + titleHeight, 52)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productDetailTableViewCell", for: indexPath)
        
        let imgIcon = cell.viewWithTag(1) as! UIImageView
        let lblText = cell.viewWithTag(2) as! UILabel
        
        if let cellInfoArray = self.projectDetailDataSource.object(at: (indexPath as NSIndexPath).row) as? NSDictionary {
            if let icon = cellInfoArray.object(forKey: "icon") as? String {
                imgIcon.image = UIImage(named: icon)
            }
            else{
                imgIcon.image = nil
            }
            
            if let text = cellInfoArray.object(forKey: "text") as? String {
                lblText.text = text
            }
            else{
                lblText.text = ""
            }
            
            if let text = cellInfoArray.object(forKey: "attributedText") as? NSAttributedString {
                lblText.attributedText = text
            }
        }
        
        
        
        return cell
    }

    
    //Actions
    @IBAction func btnBackTouched(_ sender: AnyObject) {
        self.projectSlider.removeFromSuperview()
        self.dismissSelf()
    }
    
    @IBAction func btnViewTouched(_ sender: AnyObject) {
        if let project = self.selectedProject {
            self.showViewController("ProjectPreviewViewController", attributes: [
                "project" : project
                ])
        }
    }
    
    @IBAction func btnDeleteTouched(_ sender: AnyObject) {
        if let project = self.selectedProject {
            Globals.showConfirmAlertWithTitle("Delete Project", message: "Are you sure want to delete this project?\nYou can't undo this action.", viewController: self, completion: { (action) -> Void in
                ProjectHelper.deleteProjectFromCart(project.id)
                ProjectHelper.deleteProject(project.id)
                self.setEmpty()
                self.loadDataProject()
                self.updateShoppingCartButton()
            })
        }
    }
    
    @IBAction func btnAddToCartTouched(_ sender: AnyObject) {
        if let project = self.selectedProject {
            if CartHelper.isProductExists(project) {
                BSNotification.show("This project is already in your cart.", view: self)
            }
            else{
                CartHelper.addToCart(project, qty: 1)
                BSNotification.show("This project has been added to your cart.", view: self)
                self.updateShoppingCartButton()
            }
        }
    }
    
    
    //general methods 
    func loadDataProject() {
        self.projectDataSource.removeAllObjects()
        
        if let projects = ProjectHelper.getAllProjects() as? [AnyObject] {
            self.projectDataSource.addObjects(from: projects)
        }
        
        self.projectSlider.reloadSlides()
    }
    
    func reloadProjectDetailTableView() {
        self.projectDetailTableView.reloadData()
        
        if let height = Globals.getConstraint("height", view: self.projectDetailTableView) {
            height.constant = self.projectDetailTableView.contentSize.height
        }
    }
    
    func setEmpty(){
        self.selectedProject = nil
        self.lblCreatedAt.text = ""
        self.imgBgProject.isHidden = false
        self.projectSlider.isHidden = true
        
        self.projectDetailDataSource.removeAllObjects()
        
        self.projectDetailDataSource.add([
            "icon" : "icon-my-project.png",
            "text" : ""
            ])
        self.projectDetailDataSource.add([
            "icon" : "icon-images.png",
            "text" : ""
            ])
        self.projectDetailDataSource.add([
            "icon" : "icon-size.png",
            "text" : ""
            ])
        self.projectDetailDataSource.add([
            "icon" : "icon-payment.png",
            "text" : ""
            ])
        self.projectDetailDataSource.add([
            "icon" : "icon-about-us.png",
            "text" : ""
            ])
        
        self.reloadProjectDetailTableView()
    }
}
